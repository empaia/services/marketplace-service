import logging

from marketplace_service.client_token import ClientToken
from marketplace_service.py_ead_validation.py_ead_validation.ead_validator import create_validator
from marketplace_service.utils import generate_tags_model, read_tag_csv_to_dict

from .settings import Settings

settings = Settings()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

app_tags_dict = read_tag_csv_to_dict()
app_tags_model = generate_tags_model(app_tags_dict)

vault_client_token = ClientToken(None)

ead_validator = create_validator(enable_legacy_support=False)
ead_validator_legacy = create_validator(enable_legacy_support=True)
