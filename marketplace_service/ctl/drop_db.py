from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db
from .migrate_db import TABLE_NAME_PREFIX

TABLES_MISC = [
    f"{TABLE_NAME_PREFIX}_migration_steps",
]

TABLES_V0 = [
    "v0_apps_descriptions",
    "v0_apps_media",
    "v0_apps_tags",
    "v0_apps_metadata",
    "v0_apps_mutable_data",
    "v0_apps_images",
    "v0_apps",
]

TABLES_V1 = [
    "v1_portal_apps",
    "v1_portal_apps_view",
    "v1_portal_apps_details",
    "v1_portal_apps_media_ref",
    "v1_portal_apps_media",
    "v1_apps",
    "v1_app_ui_bundles",
    "v1_container_images",
    "v1_portal_app_licenses",
]

TABLES_V1_CLEARANCES = [
    "v1_clearances",
    "v1_clearance_items",
]

TABLES = [*TABLES_MISC, *TABLES_V0, *TABLES_V1, *TABLES_V1_CLEARANCES]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table} CASCADE;")
        except UndefinedTableError as e:
            print(e)
