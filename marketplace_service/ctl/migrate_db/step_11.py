#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_11(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        CREATE TABLE v1_portal_app_licenses (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            organization_id text NOT NULL,
            portal_app_id uuid NOT NULL,
            active boolean NOT NULL DEFAULT true,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL,
            UNIQUE (organization_id, portal_app_id)
        );
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
