#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_02(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    # rename v0 tables
    await conn.execute("ALTER TABLE s_apps_descriptions RENAME TO v0_apps_descriptions")
    await conn.execute("ALTER TABLE s_apps_metadata RENAME TO v0_apps_metadata")
    await conn.execute("ALTER TABLE s_apps_mutable_data RENAME TO v0_apps_mutable_data")
    await conn.execute("ALTER TABLE s_apps_media RENAME TO v0_apps_media")
    await conn.execute("ALTER TABLE s_apps_media_ref RENAME TO v0_apps_media_ref")
    await conn.execute("ALTER TABLE s_apps_images RENAME TO v0_apps_images")
    await conn.execute("ALTER TABLE s_apps RENAME TO v0_apps")

    # portal app details
    await conn.execute(
        """
        CREATE TABLE v1_portal_apps_details (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            name text,
            description JSON NOT NULL,
            marketplace_url text
        );
        """
    )

    # portal app media object
    await conn.execute(
        """
        CREATE TABLE v1_portal_apps_media (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            media_purpose text NOT NULL,
            index integer NOT NULL,
            minio_media_id text NOT NULL,
            internal_path text,
            content_type text,
            caption text,
            alt_text text,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL
        );
        """
    )

    # reference portal app media
    await conn.execute(
        """
        CREATE TABLE v1_portal_apps_media_ref (
            portal_app_preview_id uuid,
            media_id uuid,
            PRIMARY KEY(portal_app_preview_id, media_id)
        );
        """
    )

    # technical app data
    await conn.execute(
        """
        CREATE TABLE v1_apps (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            portal_app_id uuid NOT NULL,
            status text NOT NULL,
            version text,
            app_ui_url text,
            registry_image_url text,
            ead JSON,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL
        );
        """
    )

    # portal app
    await conn.execute(
        """
        CREATE TABLE v1_portal_apps (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            organization_id text NOT NULL,
            status text NOT NULL,
            active_preview_id uuid,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL
        );
        """
    )

    # portal app preview
    await conn.execute(
        """
        CREATE TABLE v1_portal_apps_preview (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            portal_app_id uuid NOT NULL,
            details_id uuid REFERENCES v1_portal_apps_details(id) ON UPDATE CASCADE ON DELETE CASCADE,
            app_id uuid REFERENCES v1_apps(id) ON UPDATE CASCADE ON DELETE CASCADE,
            tags JSONB,
            status text NOT NULL,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL,
            review_comment text,
            reviewer_id text,
            reviewed_at timestamp
        );
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps ADD FOREIGN KEY (active_preview_id) REFERENCES v1_portal_apps_preview(id);
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps_preview ADD FOREIGN KEY (portal_app_id) REFERENCES v1_portal_apps(id);
        """
    )

    # create indexer
    await conn.execute(
        """
        CREATE INDEX v1_preview_portal_app_id ON v1_portal_apps_preview USING btree (portal_app_id);
        CREATE INDEX v1_portal_app_preview_tags ON v1_portal_apps_preview USING btree (tags);
        CREATE INDEX v1_portal_app_preview_status ON v1_portal_apps_preview USING btree (status);

        CREATE INDEX v1_portal_app_organization ON v1_portal_apps USING btree (organization_id);
        CREATE INDEX v1_portal_app_status ON v1_portal_apps USING btree (status);
        CREATE INDEX v1_portal_app_active_preview ON v1_portal_apps USING btree (active_preview_id);

        CREATE INDEX v1_apps_portal_app ON v1_apps USING btree (portal_app_id)
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
