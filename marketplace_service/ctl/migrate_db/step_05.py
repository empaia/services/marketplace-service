#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_05(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        CREATE TABLE v1_app_ui_bundles (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            name text,
            temp_directory text,
            status text NOT NULL,
            app_ui_url text,
            app_ui_config jsonb,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL,
            app_id uuid NOT NULL,
            organization_id text
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE v1_container_images (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            name text,
            temp_directory text,
            status text NOT NULL,
            registry_image_url text,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL,
            app_id uuid NOT NULL,
            organization_id text
        );
        """
    )

    await conn.execute(
        """
        INSERT INTO v1_app_ui_bundles (
            id, name, status, app_ui_url, creator_id, created_at, updated_at, app_id
        )
        SELECT
            uuid_generate_v4(), a.app_ui_url, 'UNZIP_FINISHED', a.app_ui_url,
            a.creator_id, a.created_at, a.updated_at, a.id
        FROM v1_apps as a;

        INSERT INTO v1_container_images (
            id, name, status, registry_image_url, creator_id, created_at, updated_at, app_id
        )
        SELECT
            uuid_generate_v4(), a.registry_image_url, 'PUSH_FINISHED',
            a.registry_image_url, a.creator_id, a.created_at, a.updated_at, a.id
        FROM v1_apps as a;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_apps
        ADD COLUMN app_ui_id uuid REFERENCES v1_app_ui_bundles(id) ON DELETE CASCADE;

        ALTER TABLE v1_apps
        ADD COLUMN container_image_id uuid REFERENCES v1_container_images(id) ON DELETE CASCADE;
        """
    )

    await conn.execute(
        """
        UPDATE v1_apps
        SET app_ui_id = ui.id
        FROM v1_app_ui_bundles AS ui
        WHERE ui.app_id = v1_apps.id;

        UPDATE v1_apps
        SET container_image_id = ci.id
        FROM v1_container_images as ci
        WHERE ci.app_id = v1_apps.id;
        """
    )

    await conn.execute(
        """
        UPDATE v1_container_images
        SET organization_id = pa.organization_id
        FROM v1_apps AS a
        INNER JOIN v1_portal_apps as pa ON a.portal_app_id = pa.id
        WHERE a.id = v1_container_images.app_id;

        UPDATE v1_app_ui_bundles
        SET organization_id = pa.organization_id
        FROM v1_apps AS a
        INNER JOIN v1_portal_apps as pa ON a.portal_app_id = pa.id
        WHERE a.id = v1_app_ui_bundles.app_id;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_apps DROP COLUMN app_ui_url;
        ALTER TABLE v1_apps DROP COLUMN registry_image_url;
        ALTER TABLE v1_app_ui_bundles DROP COLUMN app_id;
        ALTER TABLE v1_container_images DROP COLUMN app_id;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
