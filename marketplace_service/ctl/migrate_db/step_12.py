#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_12(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        CREATE TABLE v1_clearances (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            organization_id text NOT NULL,
            is_hidden boolean NOT NULL,
            active_item_id uuid,
            updater_type text NOT NULL,
            updater_id text NOT NULL,
            updated_at timestamp NOT NULL,
            creator_type text NOT NULL,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE v1_clearance_items (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            clearance_id uuid NOT NULL REFERENCES v1_clearances ( id ),
            product_name text NOT NULL,
            description JSON,
            tags JSONB,
            tag_remark text,
            product_release_at timestamp NOT NULL,
            product_release_at_precision text NOT NULL,
            info_updated_at timestamp NOT NULL,
            info_provider text NOT NULL,
            info_source_type text NOT NULL,
            info_source_content text,
            creator_type text NOT NULL,
            creator_id text NOT NULL,
            created_at timestamp NOT NULL
        );
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_clearances ADD FOREIGN KEY (active_item_id) REFERENCES v1_clearance_items(id);
        """
    )

    await conn.execute(
        """
        CREATE INDEX v1_clearance_id ON v1_clearances USING btree (id);
        CREATE INDEX v1_clearance_items_id ON v1_clearance_items USING btree (id);
        CREATE INDEX v1_clearance_items_tags ON v1_clearance_items USING btree (tags);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
