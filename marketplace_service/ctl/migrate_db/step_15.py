#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_15(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    # replace tag 'HE' with tag 'H_AND_E'
    await conn.execute(
        """
        UPDATE v1_portal_apps_view
        SET tags = jsonb_set(
            tags,
            '{STAIN}',
            (
                SELECT jsonb_agg(
                    CASE
                        WHEN value = 'HE' THEN 'H_AND_E'
                        ELSE value
                    END
                )
                FROM jsonb_array_elements_text(tags->'STAIN')
            )
        )
        WHERE tags ? 'STAIN' AND tags->'STAIN' @> '["HE"]';
        """
    )

    # remove possible dublicates (H_AND_E)
    await conn.execute(
        """
        UPDATE v1_portal_apps_view
        SET tags = jsonb_set(
            tags,
            '{STAIN}',
            (
                SELECT jsonb_agg(DISTINCT value)
                FROM jsonb_array_elements_text(tags->'STAIN'))
            )
        WHERE tags ? 'STAIN';
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
