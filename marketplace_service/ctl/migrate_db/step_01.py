#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    # apps description
    await conn.execute(
        """
        CREATE TABLE s_apps_descriptions (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            namespace_hash text,
            content JSON NOT NULL
        );
        """
    )

    # apps metadeata
    await conn.execute(
        """
        CREATE TABLE s_apps_metadata (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            vendor_name text,
            vendor_uuid text,
            vendor_id integer,
            name text NOT NULL,
            description JSON NOT NULL,
            created_at timestamp,
            updated_at timestamp,
            tags JSONB
        );
        """
    )

    # apps mutable data
    await conn.execute(
        """
        CREATE TABLE s_apps_mutable_data (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            description text NOT NULL,
            store_docs_url text,
            store_url text,
            change_log text NOT NULL
        );
        """
    )

    # apps media
    await conn.execute(
        """
        CREATE TABLE s_apps_media (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            media_type text,
            step_number integer,
            image text,
            image_url text,
            alternative_image text,
            alternative_image_url text,
            video text,
            video_url text,
            description text,
            medium text,
            filename text,
            locale text,
            resized_images JSON,
            resized_alternative_images JSON
        );
        """
    )

    # reference apps mutable / media
    await conn.execute(
        """
        CREATE TABLE s_apps_media_ref (
            amd_id uuid,
            media_id uuid,
            PRIMARY KEY(amd_id, media_id)
        );
        """
    )

    # apps images
    await conn.execute(
        """
        CREATE TABLE s_apps_images (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            name text NOT NULL,
            namespace text NOT NULL,
            tag text NOT NULL,
            hash text,
            size int,
            download_count int,
            uploaded_at timestamp
        );
        """
    )

    # apps
    await conn.execute(
        """
        CREATE TABLE s_apps (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            app_ui_url text,
            mapp_id uuid REFERENCES s_apps_metadata(id) ON DELETE CASCADE,
            aim_id uuid REFERENCES s_apps_images(id) ON DELETE CASCADE,
            amd_id uuid REFERENCES s_apps_mutable_data(id) ON DELETE CASCADE,
            ead_id uuid REFERENCES s_apps_descriptions(id) ON DELETE CASCADE,
            version text NOT NULL,
            created_at timestamp NOT NULL,
            status text,
            submitter_keycloak_id text
        );
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
