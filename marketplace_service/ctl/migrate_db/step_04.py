#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_04(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps_media
            ALTER COLUMN caption TYPE jsonb
            USING to_jsonb(string_to_array(('EN;' || caption), ';'));
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps_media
            ALTER COLUMN alt_text TYPE jsonb
            USING to_jsonb(string_to_array(('EN;' || alt_text), ';'));
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps_details ALTER COLUMN description TYPE json USING description::jsonb;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
