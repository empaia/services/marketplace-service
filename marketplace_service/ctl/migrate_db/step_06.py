#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_06(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        ALTER TABLE v1_container_images ADD COLUMN error_message text;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps ADD COLUMN active_preview_legacy_id uuid;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps ADD FOREIGN KEY (active_preview_legacy_id) REFERENCES v1_portal_apps_preview(id);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
