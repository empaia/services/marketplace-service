#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_07(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps RENAME COLUMN active_preview_legacy_id TO v1_active_preview_id;
        ALTER TABLE v1_portal_apps RENAME COLUMN active_preview_id TO v2_active_preview_id;
        ALTER TABLE v1_portal_apps ADD COLUMN v3_active_preview_id uuid;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps ADD FOREIGN KEY (v3_active_preview_id) REFERENCES v1_portal_apps_preview(id);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
