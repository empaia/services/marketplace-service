#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_08(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        ALTER INDEX v1_preview_portal_app_id RENAME TO v1_view_portal_app_id;
        ALTER INDEX v1_portal_apps_preview_pkey RENAME TO v1_portal_apps_view_pkey;
        ALTER INDEX v1_portal_app_preview_tags RENAME TO v1_portal_app_view_tags;
        ALTER INDEX v1_portal_app_preview_status RENAME TO v1_portal_app_view_status;
        ALTER INDEX v1_portal_app_active_preview RENAME TO v1_portal_app_active_view;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v1_portal_apps_preview RENAME TO v1_portal_apps_view;
        ALTER TABLE v1_portal_apps_media_ref RENAME COLUMN portal_app_preview_id TO portal_app_view_id;
        ALTER TABLE v1_portal_apps RENAME COLUMN v1_active_preview_id TO v1_active_view_id;
        ALTER TABLE v1_portal_apps RENAME COLUMN v2_active_preview_id TO v2_active_view_id;
        ALTER TABLE v1_portal_apps RENAME COLUMN v3_active_preview_id TO v3_active_view_id;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
