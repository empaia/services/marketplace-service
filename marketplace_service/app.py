import os
import re
from contextlib import asynccontextmanager
from pathlib import Path

import asyncpg
import certifi
import urllib3
from aiohttp import ClientSession, ClientTimeout, TCPConnector
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from minio import Minio

from . import __version__ as version
from .api.v1 import (
    add_routes_v1_admin,
    add_routes_v1_compute,
    add_routes_v1_customer,
    add_routes_v1_moderator,
    add_routes_v1_product_provider,
    add_routes_v1_public,
    add_routes_v1_reviewer,
    add_routes_v1_vendor,
)
from .late_init import LateInit
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""


late_init = LateInit()

app_v1_public = FastAPI(openapi_url=openapi_url)
app_v1_customer = FastAPI(openapi_url=openapi_url)
app_v1_compute = FastAPI(openapi_url=openapi_url)
app_v1_vendor = FastAPI(openapi_url=openapi_url)
app_v1_reviewer = FastAPI(openapi_url=openapi_url)
app_v1_admin = FastAPI(openapi_url=openapi_url)
app_v1_product_provider = FastAPI(openapi_url=openapi_url)
app_v1_moderator = FastAPI(openapi_url=openapi_url)


async def initialize_on_startup():
    # initialize asyncpg
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )

    # initialize aio session for vault access
    timeout = ClientTimeout(total=settings.connection_timeout)
    connector = TCPConnector(limit_per_host=100, force_close=True)
    aio_client = ClientSession(timeout=timeout, connector=connector)

    setattr(app_v1_public.state, "client_session", aio_client)
    setattr(app_v1_customer.state, "client_session", aio_client)
    setattr(app_v1_vendor.state, "client_session", aio_client)
    setattr(app_v1_admin.state, "client_session", aio_client)

    # initialize minio
    endpoint = re.sub(r"https?://", "", settings.minio_url)
    minio_client = Minio(
        endpoint,
        access_key=settings.minio_access_key,
        secret_key=settings.minio_secret_key,
        secure=settings.minio_secure,
        http_client=urllib3.PoolManager(
            timeout=urllib3.util.Timeout(connect=settings.connection_timeout, read=1),
            maxsize=10,
            cert_reqs="CERT_REQUIRED",
            ca_certs=os.environ.get("SSL_CERT_FILE") or certifi.where(),
            retries=urllib3.Retry(connect=5, status_forcelist=[500, 502, 503, 504]),
        ),
    )
    setattr(app_v1_public.state, "minio_session", minio_client)
    setattr(app_v1_customer.state, "minio_session", minio_client)
    setattr(app_v1_vendor.state, "minio_session", minio_client)
    setattr(app_v1_reviewer.state, "minio_session", minio_client)
    setattr(app_v1_admin.state, "minio_session", minio_client)


async def cleanup_on_teardown():
    # close aio session
    await app_v1_public.state.client_session.close()


Path(f"{settings.data_dir}/app_ui_bundles").expanduser().mkdir(parents=True, exist_ok=True)
Path(f"{settings.data_dir}/container_images").expanduser().mkdir(parents=True, exist_ok=True)


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    await initialize_on_startup()

    yield

    await cleanup_on_teardown()


app = FastAPI(
    debug=settings.debug,
    title="Marketplace Service",
    version=version,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
    lifespan=lifespan,
)

if settings.cors_allow_origins:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.get("/alive", tags=["Private"])
async def _():
    return {"status": "ok", "version": version}


add_routes_v1_public(app_v1_public, late_init)
add_routes_v1_customer(app_v1_customer, late_init)
add_routes_v1_compute(app_v1_compute, late_init)
add_routes_v1_vendor(app_v1_vendor, late_init)
add_routes_v1_reviewer(app_v1_reviewer, late_init)
add_routes_v1_admin(app_v1_admin, late_init)
add_routes_v1_product_provider(app_v1_product_provider, late_init)
add_routes_v1_moderator(app_v1_moderator, late_init)

app.mount("/v1/public", app_v1_public)
app.mount("/v1/customer", app_v1_customer)
app.mount("/v1/compute", app_v1_compute)
app.mount("/v1/vendor", app_v1_vendor)
app.mount("/v1/reviewer", app_v1_reviewer)
app.mount("/v1/admin", app_v1_admin)
app.mount("/v1/product-provider", app_v1_product_provider)
app.mount("/v1/moderator", app_v1_moderator)
