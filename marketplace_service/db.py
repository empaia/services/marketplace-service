import json

from asyncpg import Connection

from marketplace_service.clients.app_client import AppClient
from marketplace_service.clients.clearance_client import ClearanceClient
from marketplace_service.clients.license_client import LicenseClient
from marketplace_service.clients.statistics_client import StatisticsClient


def _jsonb_dumps(value):
    return b"\x01" + json.dumps(value).encode("utf-8")


def _jsonb_loads(value: bytes):
    return json.loads(value[1:].decode("utf-8"))


async def set_type_codecs(conn: Connection):
    await conn.set_type_codec("json", encoder=json.dumps, decoder=json.loads, schema="pg_catalog", format="text")
    await conn.set_type_codec("jsonb", encoder=_jsonb_dumps, decoder=_jsonb_loads, schema="pg_catalog", format="binary")


class Clients:
    def __init__(self, conn):
        self.app_client = AppClient(conn=conn)
        self.license_client = LicenseClient(conn=conn)
        self.clearance_client = ClearanceClient(conn=conn)
        self.statistics_client = StatisticsClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
