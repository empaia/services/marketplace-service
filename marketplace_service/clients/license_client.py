from typing import List

from asyncpg import PostgresError, UniqueViolationError
from fastapi import HTTPException
from pydantic import UUID4

from marketplace_service.clients.db_commons import portal_app_exists
from marketplace_service.clients.helpers.general_sql_helper import get_basic_update_statement
from marketplace_service.clients.helpers.license_sql_helper import (
    get_insert_multiple_licenses_statement,
    get_query_licenses_statement,
    get_select_licenses_statement,
)
from marketplace_service.clients.helpers.utils import log_and_raise_postgres_error
from marketplace_service.custom_models.v1.licenses import (
    LicenseQuery,
    OrganizationLicense,
    OrganizationLicenseList,
    PostOrganizationLicenseList,
)


class LicenseClient:
    def __init__(self, conn):
        self.conn = conn

    async def post_licenses(self, license_list: PostOrganizationLicenseList, user_id: str):
        insert_values = []
        for license_item in license_list.items:
            _ = await portal_app_exists(self.conn, portal_app_id=license_item.portal_app_id)
            insert_values.append((license_item.organization_id, license_item.portal_app_id, user_id))

        sql_insert_licenses = await get_insert_multiple_licenses_statement()

        try:
            license_result = []
            for insert_value in insert_values:
                license_record = await self.conn.fetchrow(sql_insert_licenses, *insert_value)
                license_result.append(OrganizationLicense(**license_record))
            return OrganizationLicenseList(items=license_result, item_count=len(license_result))
        except UniqueViolationError as e1:
            raise HTTPException(
                status_code=400,
                detail="At least one combination of organiztion_id and portal_app_id is already present in the DB",
            ) from e1
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Licenses could not be created")

    async def query_licenses(self, query: LicenseQuery, skip: int = None, limit: int = None) -> OrganizationLicenseList:
        sql_select_statement = await get_query_licenses_statement(
            filter_by_orga=(query and query.organizations),
            filter_by_portal_app=(query and query.portal_apps),
            skip_entries=(skip is not None),
            limit_entries=(limit is not None),
        )

        args = []
        if query:
            if query.organizations:
                args.append(query.organizations)
            if query.portal_apps:
                args.append(query.portal_apps)
        if limit is not None:
            args.append(limit)
        if skip is not None:
            args.append(skip)

        try:
            licenses_records = await self.conn.fetch(sql_select_statement, *args)
            licenses_result = []
            for record in licenses_records:
                licenses_result.append(OrganizationLicense(**record["license_obj"]))
            if len(licenses_result) == 0:
                return OrganizationLicenseList(items=[], item_count=0)
            return OrganizationLicenseList(items=licenses_result, item_count=licenses_records[0]["full_count"])
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Licenses could not be queried")

    async def get_license(self, license_id: UUID4) -> OrganizationLicense:
        sql_select_license = await get_select_licenses_statement(where_condition="WHERE id = $1")

        try:
            record = await self.conn.fetchrow(sql_select_license, license_id)
            if record is None or len(record) == 0:
                raise HTTPException(status_code=404, detail=f"License with ID {license_id} does not exist")
            return OrganizationLicense(**record["license_obj"])
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Failed to retrieve license")

    async def organization_may_access_portal_app(self, organization_id: str, portal_app_id: str):
        _ = await portal_app_exists(self.conn, portal_app_id=portal_app_id)
        query = LicenseQuery(organizations=[organization_id], portal_apps=[str(portal_app_id)])
        organization_license = await self.query_licenses(query)
        if not organization_license or len(organization_license.items) == 0 or not organization_license.items[0].active:
            raise HTTPException(status_code=403, detail="Organization is not allowed to access portal app data")

    async def get_licensed_portal_apps_for_orga_and_app(
        self, organization_id: str, portal_app_ids: List[str]
    ) -> List[str]:
        portal_app_ids = None if len(portal_app_ids) == 0 else portal_app_ids
        query = LicenseQuery(organizations=[organization_id], portal_apps=portal_app_ids)
        organization_licenses: OrganizationLicenseList = await self.query_licenses(query)
        return [str(li.portal_app_id) for li in organization_licenses.items if li.active]

    async def get_licensed_portal_app_for_orga(self, organization_id: str) -> List[str]:
        query = LicenseQuery(organizations=[organization_id], portal_apps=None)
        organization_licenses: OrganizationLicenseList = await self.query_licenses(query)
        return [str(li.portal_app_id) for li in organization_licenses.items if li.active]

    async def revoke_license(self, license_id: UUID4) -> OrganizationLicense:
        return await self._set_license_active_field(license_id=license_id, active=False)

    async def activate_license(self, license_id: UUID4) -> OrganizationLicense:
        return await self._set_license_active_field(license_id=license_id, active=True)

    async def _set_license_active_field(self, license_id: UUID4, active: bool) -> OrganizationLicense:
        _ = await self.get_license(license_id=license_id)
        sql_update_statement = await get_basic_update_statement("v1_portal_app_licenses", "active", "id")

        try:
            _ = await self.conn.fetch(sql_update_statement, active, license_id)
            return await self.get_license(license_id=license_id)
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Failed to update license")
