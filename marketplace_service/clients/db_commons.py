from asyncpg import PostgresError
from fastapi import HTTPException
from pydantic import UUID4

from marketplace_service.clients.helpers.general_sql_helper import get_basic_select_where_statement
from marketplace_service.clients.helpers.utils import log_and_raise_postgres_error


async def portal_app_exists(conn, portal_app_id: UUID4):
    sql = await get_basic_select_where_statement("v1_portal_apps", "WHERE id = $1")

    try:
        row = await conn.fetchrow(sql, portal_app_id)
    except PostgresError as e:
        log_and_raise_postgres_error(e, f"Failed querying portal app for ID {portal_app_id}")

    if row is None:
        raise HTTPException(status_code=404, detail=f"No valid portal app for ID {portal_app_id}.")

    return row
