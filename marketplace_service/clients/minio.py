import uuid
from io import BytesIO
from typing import List

from fastapi import HTTPException, UploadFile
from minio import Minio
from minio.deleteobjects import DeleteObject
from minio.error import S3Error
from PIL import Image, UnidentifiedImageError
from urllib3.exceptions import MaxRetryError

from marketplace_service.custom_models.commons import (
    SUPPORTED_IMAGE_CONTENT_TYPES,
    SUPPORTED_MANUAL_CONTENT_TYPES,
    MinioClientWrapper,
)
from marketplace_service.models.marketplace.app import ResizedMediaUrlsObject

from ..singletons import logger, settings

RESIZED_IMAGE_WIDTH = [60, 400, 800, 1200]


def is_minio_reachable(minio_client: Minio) -> bool:
    try:
        if not minio_client.bucket_exists(bucket_name=settings.minio_bucket):
            logger.debug("Invalid bucket name")
        return True
    except MaxRetryError as err:
        logger.error(f"MinIO object storage can not be reached. {[err]}")
        return False


async def put_media_object(minio_client_wrapper: MinioClientWrapper, file: UploadFile) -> str:
    parent_key = str(uuid.uuid4())
    if file.content_type in SUPPORTED_IMAGE_CONTENT_TYPES:
        await put_multires_images(minio_client_wrapper=minio_client_wrapper, parent_key=parent_key, file=file)
        return parent_key
    elif file.content_type in SUPPORTED_MANUAL_CONTENT_TYPES:
        data = BytesIO(await file.read())
        put_minio_media(minio_client_wrapper=minio_client_wrapper, object_name=parent_key, data=data)
        return parent_key
    else:
        raise HTTPException(status_code=415, detail=f"Unsupported media type: {file.content_type}")


async def put_multires_images(minio_client_wrapper: MinioClientWrapper, parent_key: str, file: UploadFile) -> str:
    content_type = file.content_type.split("/")[1]

    data = BytesIO(await file.read())
    try:
        original_image = Image.open(data)
        minio_images = {}

        org_img_bytes = BytesIO()
        with original_image.copy() as org:
            org.save(org_img_bytes, format=content_type)
            org_img_bytes.seek(0)

        minio_images.update({parent_key: org_img_bytes})  # push original image

        for resize_width in RESIZED_IMAGE_WIDTH:
            with original_image.copy() as img_copy:
                wpercent = resize_width / float(img_copy.size[0])
                resize_height = int((float(img_copy.size[1]) * float(wpercent)))
                resized_img = img_copy.resize((resize_width, resize_height), Image.Resampling.LANCZOS)

                img_byte_arr = BytesIO()
                resized_img.save(img_byte_arr, format=content_type)
                img_byte_arr.seek(0)
                minio_images.update({f"{parent_key}_w{resize_width}": img_byte_arr})  # push downsampled image

        for key in minio_images:
            put_minio_media(minio_client_wrapper=minio_client_wrapper, data=minio_images[key], object_name=key)

    except UnidentifiedImageError as err1:
        logger.error(f"Failed to process image file [{err1}]")
        raise HTTPException(status_code=404, detail="Failed to process image file") from err1
    except Exception as err2:
        logger.error(f"Error occured while uploading image file [{err2}]")
        raise HTTPException(status_code=404, detail="Failed to process image file") from err2
    finally:
        data.close()


def put_minio_media(minio_client_wrapper: MinioClientWrapper, object_name: str, data: BytesIO):
    try:
        minio_client_wrapper.minio_client.put_object(
            bucket_name=settings.minio_bucket, object_name=object_name, data=data, length=data.getbuffer().nbytes
        )
    except MaxRetryError as err:
        logger.error(f"MinIO object storage can not be reached. {[err]}")
        raise HTTPException(status_code=404, detail="MinIO object storage can not be reached") from err


def delete_minio_media(minio_client_wrapper: MinioClientWrapper, object_name: str):
    try:
        minio_client_wrapper.minio_client.remove_object(bucket_name=settings.minio_bucket, object_name=object_name)
    except MaxRetryError as err:
        logger.error(f"MinIO object storage can not be reached. {[err]}")
        raise HTTPException(status_code=404, detail="MinIO object storage can not be reached") from err


def delete_minio_media_objects(minio_client_wrapper: MinioClientWrapper, objects_to_delete: List[str]):
    try:
        errors = minio_client_wrapper.minio_client.remove_objects(
            bucket_name=settings.minio_bucket, delete_object_list=objects_to_delete
        )
        log_and_raise_deletion_errors(errors)
    except MaxRetryError as err:
        logger.error(f"MinIO object storage can not be reached. {[err]}")
        raise HTTPException(status_code=404, detail="MinIO object storage can not be reached") from err


def delete_all_minio_media_objects(minio_client_wrapper: MinioClientWrapper):
    try:
        delete_object_list = map(
            lambda x: DeleteObject(x.object_name),
            minio_client_wrapper.minio_client.list_objects(bucket_name=settings.minio_bucket, recursive=True),
        )
        errors = minio_client_wrapper.minio_client.remove_objects(
            bucket_name=settings.minio_bucket, delete_object_list=delete_object_list
        )
        log_and_raise_deletion_errors(errors)
    except MaxRetryError as e1:
        logger.error(f"MinIO object storage can not be reached. {[e1]}")
        raise HTTPException(status_code=400, detail="MinIO object storage can not be reached") from e1
    except Exception as e2:
        logger.error(f"Error while truncating MinIO storage for bucket '{settings.minio_bucket}'. {[e2]}")
        raise HTTPException(
            status_code=400, detail=f"MinIO object storage for bucket '{settings.minio_bucket}' could not be truncated"
        ) from e2


def log_and_raise_deletion_errors(errors):
    has_error = False
    for error in errors:
        logger.error(f"Error occured while deleting objects from MinIO: {error}")
        has_error = True
    if has_error:
        raise HTTPException(status_code=400, detail="Errors occured while truncating MinIO storage")


def get_resized_images(minio_client_wrapper: MinioClientWrapper, parent_key: str) -> ResizedMediaUrlsObject:
    resized_urls = ResizedMediaUrlsObject()
    for width in RESIZED_IMAGE_WIDTH:
        key = f"{parent_key}_w{width}"
        url = get_presigned_url(minio_client_wrapper=minio_client_wrapper, key=key)
        if width == 60:
            resized_urls.w60 = url
        elif width == 400:
            resized_urls.w400 = url
        elif width == 800:
            resized_urls.w800 = url
        elif width == 1200:
            resized_urls.w1200 = url
    return resized_urls


def key_exists(minio_client_wrapper: MinioClientWrapper, key: str) -> bool:
    try:
        minio_client_wrapper.minio_client.stat_object(bucket_name=settings.minio_bucket, object_name=key)
        return True
    except S3Error:
        return False


def get_presigned_url(minio_client_wrapper: MinioClientWrapper, key: str) -> str:
    try:
        presigned_url = minio_client_wrapper.minio_client.get_presigned_url(
            "GET", bucket_name=settings.minio_bucket, object_name=key
        )
        return rewrite_presigned_url(presigned_url, settings.minio_url, settings.minio_external_url)
    except MaxRetryError as err:
        logger.error(f"MinIO object storage can not be reached [{err}]")
        return None


def get_image_data(minio_client_wrapper: MinioClientWrapper, key: str) -> BytesIO:
    try:
        response = minio_client_wrapper.minio_client.get_object(bucket_name=settings.minio_bucket, object_name=key)
        image_data = BytesIO(response.read())
        image_data.seek(0)
        return image_data
    except MaxRetryError as err:
        raise HTTPException(status_code=404, detail="MinIO object storage can not be reached") from err


def rewrite_presigned_url(presigned_url, internal_url_base, external_url_base):
    internal_url_base = internal_url_base.rstrip("/")
    external_url_base = external_url_base.rstrip("/")
    assert presigned_url.startswith(internal_url_base)
    internal_url_len = len(internal_url_base)
    presigned_url_path = presigned_url[internal_url_len:].lstrip("/")
    return f"{external_url_base}/{presigned_url_path}"
