from marketplace_service.clients.helpers.utils import log_statement
from marketplace_service.models.marketplace.app import ApiVersion, BaseQuery, ListingStatus


@log_statement
async def get_insert_portal_app_statement(external_ids: bool = False):
    values = "uuid_generate_v4(), $1, $2, $3, $4"
    if external_ids:
        values = "$1, $2, $3, $4, $5"
    sql = """
        INSERT INTO public.v1_portal_apps (
            id, organization_id, organization_name, status, creator_id, created_at, updated_at
        )
        VALUES(
            {values}, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
        )
        RETURNING {returning};
    """
    return sql.format(values=values, returning=_portal_app_column_names())


@log_statement
async def get_insert_app_view_statement():
    sql = """
        INSERT INTO public.v1_portal_apps_view (
            id, portal_app_id, details_id, app_id, tags, status, creator_id, research_only, api_version, created_at
        )
        VALUES(
            uuid_generate_v4(), $1, $2, $3, $4, $5, $6, $7, $8, CURRENT_TIMESTAMP
        )
        RETURNING {returning};
    """
    return sql.format(returning=_app_view_column_names())


@log_statement
async def get_insert_portal_app_details_statement():
    sql = """
        INSERT INTO public.v1_portal_apps_details (
            id, name, description, marketplace_url
        )
        VALUES(
            uuid_generate_v4(), $1, $2, $3
        )
        RETURNING * ;
    """
    return sql


@log_statement
async def get_insert_portal_app_media_statement():
    sql = """
        INSERT INTO public.v1_portal_apps_media (
            id,
            media_purpose,
            index,
            minio_media_id,
            internal_path,
            content_type,
            caption,
            alt_text,
            created_at,
            updated_at
        )
        VALUES(
            uuid_generate_v4(), $1, $2, $3, $4, $5, $6, $7, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
        )
        RETURNING {returning};
    """
    return sql.format(returning=_portal_apps_media_column_names())


@log_statement
async def get_insert_portal_app_media_reference_statement():
    sql = """
        INSERT INTO public.v1_portal_apps_media_ref (portal_app_view_id, media_id)
        VALUES ($1, $2)
        RETURNING * ;
    """
    return sql


@log_statement
async def get_delete_portal_app_and_app_view_statement():
    sql = """
        WITH pa AS (
            DELETE FROM public.v1_portal_apps
            WHERE id = $1
            RETURNING id
        )
        DELETE FROM public.v1_portal_apps_view
        WHERE id = ANY($2);
    """
    return sql


@log_statement
async def get_where_clause_for_portal_app_query(
    organization_id: str, query: BaseQuery, status: ListingStatus = None, start_index: int = 1
):
    query_dict = query.model_dump()
    transformed_query = ""
    query_terms = []
    query_data = []
    index = start_index

    if status is not None:
        query_terms.append(f"pa.status = ${index}")
        query_data.append(status)
        index += 1

    if organization_id is not None:
        query_terms.append(f"pa.organization_id = ${index}")
        query_data.append(organization_id)
        index += 1

    for key, val in query_dict.items():
        if val is None:
            continue

        if key == "analysis":
            query_terms.append(f"(pav.tags->'ANALYSIS')::jsonb ?| ${index}")
        elif key == "indications":
            query_terms.append(f"(pav.tags->'INDICATION')::jsonb ?| ${index}")
        elif key == "stains":
            query_terms.append(f"(pav.tags->'STAIN')::jsonb ?| ${index}")
        elif key == "tissues":
            query_terms.append(f"(pav.tags->'TISSUE')::jsonb ?| ${index}")
        else:
            if isinstance(val, list):
                query_terms.append(f"{key}=ANY(${index})")
            else:
                query_terms.append(f"{key}=${index}")

        query_data.append(val)

        index += 1

    if index > start_index:
        transformed_query = "WHERE " + " AND ".join(query_terms)
    return transformed_query, query_data, index


@log_statement
async def get_query_plain_portal_app_statement(
    filter_by_orga: bool = False, filter_by_status: bool = False, active_app_version: ApiVersion = None
):
    index = 1
    conditions = []
    if active_app_version:
        conditions.append(f"pa.{active_app_version.value}_active_view_id IS NOT NULL")
    if filter_by_orga:
        conditions.append(f"pa.organization_id = ${index}")
        index += 1
    if filter_by_status:
        conditions.append(f"pa.status = ANY(${index})")

    where_clause = ""
    if len(conditions) > 0:
        for c in conditions:
            if where_clause == "":
                where_clause = c
            else:
                where_clause = f"{where_clause} AND {c}"
        where_clause = f"WHERE {where_clause}"

    base_sql = """
        SELECT
        {portal_app} as portal_app,
        {v1_active_view_subquery},
        {v2_active_view_subquery},
        {v3_active_view_subquery},
        count(*) OVER() AS full_count
        FROM public.v1_portal_apps AS pa
        {where_clause}
        ORDER BY pa.created_at ;
    """

    return base_sql.format(
        portal_app=_build_jsonb_portal_app(),
        v1_active_view_subquery=get_app_view_subquery_statement(ApiVersion.V1),
        v2_active_view_subquery=get_app_view_subquery_statement(ApiVersion.V2),
        v3_active_view_subquery=get_app_view_subquery_statement(ApiVersion.V3),
        where_clause=where_clause,
    )


def get_app_view_subquery_statement(active_app_version: ApiVersion):
    base_sql = """
        COALESCE((
            SELECT jsonb_build_object(
                'portal_app_view', {portal_app_view},
                'portal_app_view_details', {portal_app_view_details},
                'portal_app_view_app', {portal_app_view_app},
                'app_ui', {app_ui},
                'container_image', {container_image}
            )
            FROM public.v1_portal_apps_view AS pav
            LEFT OUTER JOIN public.v1_portal_apps_details AS pad ON pav.details_id = pad.id
            LEFT OUTER JOIN public.v1_apps AS a ON pav.app_id = a.id
            LEFT OUTER JOIN public.v1_app_ui_bundles AS ui ON a.app_ui_id = ui.id
            LEFT OUTER JOIN public.v1_container_images AS ci ON a.container_image_id = ci.id
            WHERE pav.id = pa.{active_app_version}_active_view_id
        ), '[]'::jsonb) {active_app_version}_active_view
    """

    return base_sql.format(
        portal_app_view=_build_jsonb_portal_app_view(),
        portal_app_view_details=_build_jsonb_portal_app_view_details(),
        portal_app_view_app=_build_jsonb_portal_app_view_app(),
        app_ui=_build_jsonb_app_ui_obj(),
        container_image=_build_jsonb_container_image_obj(),
        active_app_version=active_app_version.value,
    )


@log_statement
async def get_select_plain_portal_app_statement():
    base_sql = """
        SELECT
        {portal_app} as portal_app,
        {v1_active_view_subquery},
        {v2_active_view_subquery},
        {v3_active_view_subquery}
        FROM public.v1_portal_apps AS pa
        WHERE pa.id = $1 ;
    """

    return base_sql.format(
        portal_app=_build_jsonb_portal_app(),
        v1_active_view_subquery=get_app_view_subquery_statement(ApiVersion.V1),
        v2_active_view_subquery=get_app_view_subquery_statement(ApiVersion.V2),
        v3_active_view_subquery=get_app_view_subquery_statement(ApiVersion.V3),
    )


@log_statement
async def get_select_app_views_statement(where_condition: str):
    sql = """
        SELECT
        {portal_app} as portal_app,
        {portal_app_view} as portal_app_view,
        {portal_app_view_details} as portal_app_view_details,
        {portal_app_view_app} as portal_app_view_app,
        {app_ui} as app_ui,
        {container_image} as container_image
        FROM public.v1_portal_apps_view AS pav
        LEFT OUTER JOIN public.v1_portal_apps AS pa ON pav.portal_app_id = pa.id
        LEFT OUTER JOIN public.v1_portal_apps_details AS pad ON pav.details_id = pad.id
        LEFT OUTER JOIN public.v1_apps AS a ON pav.app_id = a.id
        LEFT OUTER JOIN public.v1_app_ui_bundles AS ui ON a.app_ui_id = ui.id
        LEFT OUTER JOIN public.v1_container_images AS ci ON a.container_image_id = ci.id
        {where_condition}
        ORDER BY pav.created_at DESC ;
    """
    return sql.format(
        portal_app=_build_jsonb_portal_app(),
        portal_app_view=_build_jsonb_portal_app_view(),
        portal_app_view_details=_build_jsonb_portal_app_view_details(),
        portal_app_view_app=_build_jsonb_portal_app_view_app(),
        app_ui=_build_jsonb_app_ui_obj(),
        container_image=_build_jsonb_container_image_obj(),
        where_condition=where_condition,
    )


@log_statement
async def get_select_app_view_media_statement():
    sql = """
        SELECT {returning_pav}, {returning_pam}, pamr.portal_app_view_id, pamr.media_id
        FROM public.v1_portal_apps_view AS pav
        LEFT OUTER JOIN public.v1_portal_apps_media_ref AS pamr
        ON pamr.portal_app_view_id = pav.id
        LEFT OUTER JOIN public.v1_portal_apps_media AS pam
        ON pam.id = pamr.media_id
        WHERE pav.id = $1 ;
    """
    return sql.format(
        returning_pav=_app_view_column_names(prefix="pav."),
        returning_pam=_portal_apps_media_column_names(prefix="pam."),
    )


@log_statement
async def get_delete_and_insert_media_ref_entry_statement():
    sql = """
        WITH delete_ref as (
            DELETE FROM public.v1_portal_apps_media_ref
            WHERE portal_app_view_id = $1 AND media_id = $2
            RETURNING portal_app_view_id as id
        )
        INSERT INTO public.v1_portal_apps_media_ref (portal_app_view_id, media_id)
        VALUES ((select id from delete_ref), $3)
        RETURNING * ;
    """
    return sql


@log_statement
async def get_select_app_by_app_view_id_statement():
    sql = """
        SELECT {returning_app_views},
            {returning_apps},
            ui.app_ui_url,
            ui.app_ui_config,
            ci.registry_image_url
        FROM public.v1_portal_apps_view AS pav
        INNER JOIN public.v1_apps AS a ON pav.app_id = a.id
        LEFT OUTER JOIN public.v1_app_ui_bundles AS ui ON a.app_ui_id = ui.id
        LEFT OUTER JOIN public.v1_container_images AS ci ON a.container_image_id = ci.id
        WHERE pav.id = $1 ;
    """
    return sql.format(returning_app_views=_app_view_column_names(prefix="pav."), returning_apps=_app_column_names("a."))


@log_statement
async def get_select_app_ids_for_portal_apps_statement():
    sql = """
        SELECT portal_app_id, id
        FROM public.v1_apps
        WHERE portal_app_id = ANY($1) ;
    """
    return sql


@log_statement
async def get_update_portal_app_review_statement():
    sql = """
        UPDATE public.v1_portal_apps_view
        SET review_comment = $1, reviewer_id = $2, reviewed_at = CURRENT_TIMESTAMP
        WHERE id = $3 ;
    """
    return sql


@log_statement
async def get_update_portal_app_status_statement():
    sql = """
        UPDATE public.v1_portal_apps
        SET status = $1, v1_active_view_id = $2, v2_active_view_id = $3, v3_active_view_id = $4
        WHERE id = $5 ;
    """
    return sql


@log_statement
async def get_update_portal_app_view_non_functional_flag_statement():
    sql = """
        UPDATE public.v1_portal_apps_view
        SET non_functional = $1
        WHERE id = $2 ;
    """
    return sql


# private


def _build_jsonb_portal_app():
    sql = """
        jsonb_build_object(
            'id', pa.id,
            'organization_id', pa.organization_id,
            'organization_name', pa.organization_name,
            'status', pa.status,
            'v1_active_view_id', pa.v1_active_view_id,
            'v2_active_view_id', pa.v2_active_view_id,
            'v3_active_view_id', pa.v3_active_view_id,
            'creator_id', pa.creator_id,
            'created_at', EXTRACT(epoch FROM pa.created_at)::int,
            'updated_at', EXTRACT(epoch FROM pa.updated_at)::int
        )
    """
    return sql


def _build_jsonb_portal_app_view():
    sql = """
        jsonb_build_object(
            'id', pav.id,
            'portal_app_id', pav.portal_app_id,
            'tags', pav.tags,
            'status', pav.status,
            'creator_id', pav.creator_id,
            'created_at', EXTRACT(epoch FROM pav.created_at)::int,
            'review_comment', pav.review_comment,
            'reviewer_id', pav.reviewer_id,
            'reviewed_at', EXTRACT(epoch FROM pav.reviewed_at)::int,
            'details_id', pav.details_id,
            'app_id', pav.app_id,
            'non_functional', pav.non_functional,
            'research_only', pav.research_only,
            'api_version', pav.api_version
        )
    """
    return sql


def _build_jsonb_portal_app_view_details():
    sql = """
        jsonb_build_object(
            'id', pad.id,
            'name', pad.name,
            'description', pad.description,
            'marketplace_url', pad.marketplace_url
        )
    """
    return sql


def _build_jsonb_portal_app_view_app():
    sql = """
        jsonb_build_object(
            'id', a.id,
            'portal_app_id', a.portal_app_id,
            'status', a.status,
            'version', a.version,
            'app_ui_id', a.app_ui_id,
            'container_image_id', a.container_image_id,
            'app_documentation_url', a.app_documentation_url,
            'ead', a.ead,
            'creator_id', a.creator_id,
            'created_at', EXTRACT(epoch FROM a.created_at)::int,
            'updated_at', EXTRACT(epoch FROM a.updated_at)::int
        )
    """
    return sql


def _build_jsonb_app_ui_obj():
    sql = """
        jsonb_build_object(
            'id', ui.id,
            'name', ui.name,
            'temp_directory', ui.temp_directory,
            'status', ui.status,
            'app_ui_url', ui.app_ui_url,
            'app_ui_config', ui.app_ui_config,
            'creator_id', ui.creator_id,
            'created_at', EXTRACT(epoch FROM ui.created_at)::int,
            'updated_at', EXTRACT(epoch FROM ui.updated_at)::int
        )
    """
    return sql


def _build_jsonb_container_image_obj():
    sql = """
        jsonb_build_object(
            'id', ci.id,
            'name', ci.name,
            'temp_directory', ci.temp_directory,
            'status', ci.status,
            'registry_image_url', ci.registry_image_url,
            'creator_id', ci.creator_id,
            'created_at', EXTRACT(epoch FROM ci.created_at)::int,
            'updated_at', EXTRACT(epoch FROM ci.updated_at)::int
        )
    """
    return sql


def _portal_app_column_names(prefix: str = ""):
    sql = """
        {prefix}id,
        {prefix}organization_id,
        {prefix}organization_name,
        {prefix}status,
        {prefix}creator_id,
        EXTRACT(epoch FROM {prefix}created_at)::int as created_at,
        EXTRACT(epoch FROM {prefix}updated_at)::int as updated_at
    """
    return sql.format(prefix=prefix)


def _app_view_column_names(prefix: str = ""):
    sql = """
        {prefix}id,
        {prefix}portal_app_id,
        {prefix}details_id,
        {prefix}app_id,
        {prefix}tags,
        {prefix}status,
        {prefix}creator_id,
        {prefix}research_only,
        {prefix}api_version,
        EXTRACT(epoch FROM {prefix}created_at)::int as created_at,
        EXTRACT(epoch FROM {prefix}reviewed_at)::int as reviewed_at
    """
    return sql.format(prefix=prefix)


def _portal_apps_media_column_names(prefix: str = ""):
    sql = """
        {prefix}id,
        {prefix}media_purpose,
        {prefix}index,
        {prefix}minio_media_id,
        {prefix}internal_path,
        {prefix}content_type,
        {prefix}caption,
        {prefix}alt_text,
        EXTRACT(epoch FROM {prefix}created_at)::int as created_at,
        EXTRACT(epoch FROM {prefix}updated_at)::int as updated_at
    """
    return sql.format(prefix=prefix)


def _app_column_names(prefix: str = ""):
    sql = """
        {prefix}id,
        {prefix}portal_app_id,
        {prefix}status,
        {prefix}version,
        {prefix}ead,
        {prefix}creator_id,
        EXTRACT(epoch FROM {prefix}created_at)::int as created_at,
        EXTRACT(epoch FROM {prefix}updated_at)::int as updated_at,
        {prefix}app_ui_id,
        {prefix}container_image_id,
        {prefix}app_documentation_url
    """
    return sql.format(prefix=prefix)
