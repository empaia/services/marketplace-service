import functools
from typing import List

from asyncpg import PostgresError
from fastapi import HTTPException

from marketplace_service.custom_models.commons import TagGroup
from marketplace_service.custom_models.v1.clearances import ExtendedTagList
from marketplace_service.models.marketplace.app import AppDetails, AppTag, PostAppTag, TagList
from marketplace_service.singletons import app_tags_dict, logger
from marketplace_service.utils import translate_dict


def translate_tags(tags: dict) -> TagList:
    tag_list = translate_extended_tags(tags)
    if tag_list is not None:
        tag_list = TagList(
            stains=tag_list.stains if tag_list.stains else [],
            tissues=tag_list.tissues if tag_list.tissues else [],
            indications=tag_list.indications if tag_list.indications else [],
            analysis=tag_list.analysis if tag_list.analysis else [],
            clearances=tag_list.clearances if tag_list.clearances else [],
        )
    return tag_list


def translate_extended_tags(tags: dict) -> ExtendedTagList:
    if not tags:
        return None

    tag_list = ExtendedTagList()
    for key in tags.keys():
        if key not in app_tags_dict:
            raise HTTPException(status_code=422, detail=f"Persisted tag group not in tag definitions [{key}]")

        group_dict = app_tags_dict[key]
        if isinstance(tags[key], list):
            for tag_name in tags[key]:
                if tag_name not in group_dict:
                    raise HTTPException(
                        status_code=422, detail=f"Persisted tag name not in tag definitions [{tag_name}]"
                    )
                tag_translations = translate_dict(group_dict[tag_name])
                getattr(tag_list, TagGroup.value_of(key)).append(
                    AppTag(name=tag_name, tag_translations=tag_translations)
                )
        else:
            if tags[key] not in group_dict:
                raise HTTPException(status_code=422, detail=f"Persisted tag name not in tag definitions [{tags[key]}]")

            tag_translations = translate_dict(group_dict[tags[key]])
            app_tag = AppTag(name=tag_name, tag_translations=tag_translations)
            getattr(tag_list, TagGroup.value_of(key)).append(app_tag)
    return tag_list


def validate_tags(tags: List[PostAppTag], invalid_tag_groups: List[str] = None) -> dict:
    valid_tags = {}

    if tags:
        for tag in tags:
            if invalid_tag_groups and tag.tag_group in invalid_tag_groups:
                raise HTTPException(status_code=422, detail=f"Tag group '{tag.tag_group}' not permitted for operation")
            if tag.tag_group not in app_tags_dict:
                raise HTTPException(status_code=422, detail=f"Invalid tag group [{tag.tag_group}]")
            if tag.tag_name not in app_tags_dict[tag.tag_group]:
                raise HTTPException(status_code=422, detail=f"Invalid tag name [{tag.tag_name}]")

            if tag.tag_group in valid_tags:
                valid_tags[tag.tag_group].append(tag.tag_name)
            else:
                valid_tags[tag.tag_group] = [tag.tag_name]

    return valid_tags


def validate_details(details: AppDetails):
    # description in english constraint
    description = details.description.copy()
    validate_multi_language_dict("App description", description)


def validate_multi_language_dict(data_field_name: str, multi_lang_dict):
    if "EN" not in multi_lang_dict and "en" not in multi_lang_dict:
        raise HTTPException(status_code=422, detail=f"{data_field_name} must contain an entry in english (EN)")


def log_statement(func):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            result = await func(*args, **kwargs)
            logger.debug(f"{func.__name__}: {result}")
            return result
        except Exception as e:
            logger.error(f"Exception raised in {func.__name__}. exception: {str(e)}")
            raise e

    return wrapper


def log_and_raise_postgres_error(e: PostgresError, detail_message: str):
    logger.error(e)
    raise HTTPException(status_code=500, detail=f"{detail_message}. See logs for more details.")


def log_and_raise_error(e: Exception, status_code: int, detail_message: str):
    logger.error(e)
    raise HTTPException(status_code=status_code, detail=f"{detail_message}. See logs for more details.")


def log_postgres_error(e: PostgresError):
    logger.error(e)
