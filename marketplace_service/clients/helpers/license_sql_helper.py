from marketplace_service.clients.helpers.utils import log_statement


@log_statement
async def get_insert_multiple_licenses_statement():
    sql = """
        INSERT INTO public.v1_portal_app_licenses (
            id,
            organization_id,
            portal_app_id,
            active,
            creator_id,
            created_at,
            updated_at
        )
        VALUES(
            uuid_generate_v4(), $1, $2, true, $3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
        )
        RETURNING {returning};
    """
    return sql.format(returning=_licenses_column_names())


@log_statement
async def get_query_licenses_statement(
    filter_by_orga: bool, filter_by_portal_app: bool, skip_entries: bool, limit_entries: bool
):
    index = 0
    conditions = []
    if filter_by_orga:
        index += 1
        conditions.append(f"organization_id = ANY(${index})")
    if filter_by_portal_app:
        index += 1
        conditions.append(f"portal_app_id = ANY(${index})")

    where_clause = ""
    if len(conditions) == 1:
        where_clause = f"WHERE {conditions[0]}"
    if len(conditions) == 2:
        where_clause = f"WHERE {conditions[0]} AND {conditions[1]}"

    limitation = ""
    if limit_entries:
        index += 1
        limitation = f"LIMIT ${index} "
    if skip_entries:
        index += 1
        limitation = f"{limitation}OFFSET ${index}"

    base_sql = """
        SELECT
        {license} as license_obj,
        count(*) OVER() AS full_count
        FROM public.v1_portal_app_licenses
        {where_clause}
        ORDER BY created_at
        {limitation} ;
    """

    return base_sql.format(license=_build_jsonb_license_obj(), where_clause=where_clause, limitation=limitation)


@log_statement
async def get_select_licenses_statement(where_condition: str):
    sql = """
        SELECT
        {license} as license_obj
        FROM public.v1_portal_app_licenses
        {where_condition}
        ORDER BY created_at DESC ;
    """
    return sql.format(
        license=_build_jsonb_license_obj(),
        where_condition=where_condition,
    )


def _build_jsonb_license_obj():
    sql = """
        jsonb_build_object(
            'id', id,
            'organization_id', organization_id,
            'portal_app_id', portal_app_id,
            'active', active,
            'creator_id', creator_id,
            'created_at', EXTRACT(epoch FROM created_at)::int,
            'updated_at', EXTRACT(epoch FROM updated_at)::int
        )
    """
    return sql


def _licenses_column_names(prefix: str = ""):
    sql = """
        id,
        organization_id,
        portal_app_id,
        active,
        creator_id,
        EXTRACT(epoch FROM created_at)::int as created_at,
        EXTRACT(epoch FROM updated_at)::int as updated_at
    """
    return sql.format(prefix=prefix)
