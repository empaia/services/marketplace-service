from marketplace_service.clients.helpers.utils import log_statement


@log_statement
async def get_basic_select_where_statement(table_name: str, where_condition: str):
    sql = """
        SELECT *
        FROM public.{table_name}
        {where_condition} ;
    """
    return sql.format(table_name=table_name, where_condition=where_condition)


@log_statement
async def get_basic_delete_where_statement(table_name: str, where_condition: str):
    sql = """
        DELETE FROM public.{table_name}
        {where_condition}
        RETURNING * ;
    """
    return sql.format(table_name=table_name, where_condition=where_condition)


@log_statement
async def get_basic_update_statement(table_name: str, column_name_set: str, column_name_eq: str):
    return await get_basic_update_where_statement(table_name, column_name_set, f"WHERE {column_name_eq} = $2")


@log_statement
async def get_basic_update_where_statement(table_name: str, column_name_set: str, where_condition: str):
    sql = """
        WITH t as (
            SELECT {column_name_set} FROM public.{table_name} {where_condition}
        )
        UPDATE public.{table_name}
        SET {column_name_set} = $1
        {where_condition}
        RETURNING (SELECT * FROM t);
    """
    return sql.format(table_name=table_name, column_name_set=column_name_set, where_condition=where_condition)
