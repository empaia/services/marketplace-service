from marketplace_service.clients.helpers.utils import log_statement

STATISTICS_TO_JSON = """json_strip_nulls(json_build_object(
    'statistics_id', statistics_id,
    'page', page,
    'accessed_at', EXTRACT(EPOCH FROM accessed_at)::int
))"""


@log_statement
async def get_insert_statistics():
    sql = """
        INSERT INTO public.v1_statistics (
            statistics_id, page, accessed_at
        )
        VALUES(
            $1, $2, current_date
        )
        RETURNING * ;
    """
    return sql


@log_statement
async def get_select_statistics():
    sql = f"""
        SELECT json_agg(
            {STATISTICS_TO_JSON}
            ORDER BY accessed_at DESC
        ) agg_statistics
        FROM public.v1_statistics
    """
    return sql
