from marketplace_service.clients.helpers.utils import log_statement


@log_statement
async def get_insert_app_statement(external_ids: bool):
    values = "uuid_generate_v4(), $1, $2, $3, $4, $5, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $6, $7, $8"
    if external_ids is True:
        values = "$1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $7, $8, $9"

    sql = """
        INSERT INTO public.v1_apps (
            id,
            portal_app_id,
            status,
            version,
            ead,
            creator_id,
            created_at,
            updated_at,
            app_ui_id,
            container_image_id,
            app_documentation_url
        )
        VALUES(
            {values}
        )
        RETURNING id,
            portal_app_id,
            status,
            version,
            ead,
            creator_id,
            EXTRACT(epoch FROM created_at)::int as created_at,
            EXTRACT(epoch FROM updated_at)::int as updated_at,
            app_ui_id,
            container_image_id,
            app_documentation_url;
    """
    return sql.format(values=values)


@log_statement
async def get_insert_app_ui_statement():
    values = "uuid_generate_v4(), $1, $2, $3, $4, $5, $6, $7, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP"

    sql = """
        INSERT INTO public.v1_app_ui_bundles (
            id,
            name,
            temp_directory,
            status,
            app_ui_url,
            app_ui_config,
            creator_id,
            organization_id,
            created_at,
            updated_at
        )
        VALUES(
            {values}
        )
        {returning};
    """
    return sql.format(values=values, returning=app_ui_bundle_column_names())


@log_statement
async def get_insert_container_image_statement():
    values = "uuid_generate_v4(), $1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP"

    sql = """
        INSERT INTO public.v1_container_images (
            id,
            name,
            temp_directory,
            status,
            registry_image_url,
            creator_id,
            organization_id,
            created_at,
            updated_at
        )
        VALUES(
            {values}
        )
        RETURNING id,
            name,
            temp_directory,
            status,
            registry_image_url,
            creator_id,
            organization_id,
            EXTRACT(epoch FROM created_at)::int as created_at,
            EXTRACT(epoch FROM updated_at)::int as updated_at;
    """
    return sql.format(values=values)


@log_statement
async def get_update_app_ui_statement():
    sql = """
        UPDATE public.v1_app_ui_bundles
        SET temp_directory = $1, status = $2, updated_at = CURRENT_TIMESTAMP
        WHERE id = $3
        {returning};
    """
    return sql.format(returning=app_ui_bundle_column_names())


@log_statement
async def get_update_container_image_status_statement(
    update_directory: bool, update_registry_url: bool, update_error_message: bool
):
    index = 2
    directory = ""
    if update_directory:
        directory = f" temp_directory = ${index},"
        index += 1

    registry_image_url = ""
    if update_registry_url:
        registry_image_url = f" registry_image_url = ${index},"
        index += 1

    error_message = ""
    if update_error_message:
        error_message = f" error_message = ${index},"
        index += 1

    id_clause = f"id = ${index}"

    sql = """
        UPDATE public.v1_container_images
        SET status = $1,{temp_directory}{registry_image_url}{error_message} updated_at = CURRENT_TIMESTAMP
        WHERE {id_clause}
        RETURNING id,
            name,
            temp_directory,
            status,
            registry_image_url,
            creator_id,
            organization_id,
            EXTRACT(epoch FROM created_at)::int as created_at,
            EXTRACT(epoch FROM updated_at)::int as updated_at;
    """
    return sql.format(
        temp_directory=directory,
        registry_image_url=registry_image_url,
        error_message=error_message,
        id_clause=id_clause,
    )


@log_statement
async def get_select_active_app_statement():
    sql = """
        SELECT a.id,
            a.portal_app_id,
            a.status,
            a.version,
            a.ead,
            a.creator_id,
            EXTRACT(epoch FROM a.created_at)::int as created_at,
            EXTRACT(epoch FROM a.updated_at)::int as updated_at,
            a.container_image_id,
            ui.id AS app_ui_id,
            ui.app_ui_url,
            ui.app_ui_config,
            ci.registry_image_url
        FROM public.v1_apps AS a
        INNER JOIN public.v1_portal_apps_view AS pav ON a.id = pav.app_id
        INNER JOIN public.v1_portal_apps AS pa ON pa.id = pav.portal_app_id
        LEFT JOIN public.v1_app_ui_bundles AS ui ON a.app_ui_id = ui.id
        LEFT JOIN public.v1_container_images AS ci ON a.container_image_id = ci.id
        WHERE pa.id = $1 ;
    """
    return sql


@log_statement
async def get_select_app_with_app_ui_and_container_image_statement(where_clause):
    sql = """
        SELECT a.id,
            a.portal_app_id,
            a.status,
            a.version,
            a.ead,
            a.creator_id,
            a.app_documentation_url,
            EXTRACT(epoch FROM a.created_at)::int as created_at,
            EXTRACT(epoch FROM a.updated_at)::int as updated_at,
            a.container_image_id,
            ui.id AS app_ui_id,
            ui.app_ui_url,
            ui.app_ui_config,
            ci.registry_image_url
        FROM public.v1_apps as a
        LEFT JOIN public.v1_app_ui_bundles AS ui ON a.app_ui_id = ui.id
        LEFT JOIN public.v1_container_images AS ci ON a.container_image_id = ci.id
        {where_clause} ;
    """
    return sql.format(where_clause=where_clause)


@log_statement
async def get_select_app_ui_bundle_for_app_statement():
    sql = """
        SELECT ui.*
        FROM public.v1_app_ui_bundles as ui
        INNER JOIN public.v1_apps as a ON ui.id = a.app_ui_id
        WHERE a.id = $1 ;
    """
    return sql


def app_ui_bundle_column_names():
    return """
        RETURNING id,
            name,
            temp_directory,
            status,
            app_ui_url,
            app_ui_config,
            creator_id,
            organization_id,
            EXTRACT(epoch FROM created_at)::int as created_at,
            EXTRACT(epoch FROM updated_at)::int as updated_at
    """
