from marketplace_service.clients.helpers.utils import log_statement
from marketplace_service.custom_models.commons import TagGroup
from marketplace_service.custom_models.v1.clearances import ClearanceQuery


@log_statement
async def get_insert_multiple_clearances_statement():
    sql = """
        INSERT INTO public.v1_clearances (
            id,
            organization_id,
            is_hidden,
            active_item_id,
            updater_type,
            updater_id,
            updated_at,
            creator_type,
            creator_id,
            created_at
        )
        VALUES(
            uuid_generate_v4(), $1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $4, $5, CURRENT_TIMESTAMP
        )
        RETURNING {returning};
    """
    return sql.format(returning=_clearance_column_names())


@log_statement
async def get_insert_clearance_item_statement():
    sql = """
        INSERT INTO public.v1_clearance_items (
            id,
            clearance_id,
            product_name,
            description,
            tags,
            tag_remark,
            product_release_at,
            product_release_at_precision,
            info_updated_at,
            info_provider,
            info_source_type,
            info_source_content,
            creator_type,
            creator_id,
            created_at
        )
        VALUES(
            uuid_generate_v4(),
            $1, $2, $3, $4, $5,
            to_timestamp($6), $7,
            to_timestamp($8),
            $9, $10, $11, $12, $13,
            CURRENT_TIMESTAMP
        )
        RETURNING {returning};
    """
    return sql.format(returning=_clearance_item_column_names())


@log_statement
async def get_update_clearance_where_statement(column_name_set: str, where_condition: str):
    sql = """
        UPDATE public.v1_clearances
        SET
            {column_name_set} = $1,
            updater_type = $2,
            updater_id = $3,
            updated_at = CURRENT_TIMESTAMP
        {where_condition} ;
    """
    return sql.format(column_name_set=column_name_set, where_condition=where_condition)


async def get_query_aggregated_clearance_statement(extended_where_clause: str = "", limitations: str = ""):
    if extended_where_clause and extended_where_clause != "":
        extended_where_clause = f"AND {extended_where_clause}"
    sql = """
        SELECT
            {agg_clearance} as pub_agg_clearance,
            count(*) OVER() AS full_count
        FROM public.v1_clearances as c
        LEFT JOIN public.v1_clearance_items as ci ON c.active_item_id = ci.id
        WHERE
            c.active_item_id IS NOT NULL
            AND c.is_hidden IS NOT TRUE
            {extended_where_clause}
        ORDER BY c.updated_at DESC
        {limitations} ;
    """
    return sql.format(
        agg_clearance=_build_jsonb_aggergated_clearance(),
        extended_where_clause=extended_where_clause,
        limitations=limitations,
    )


async def get_select_clearances_statement(where_clause: str = ""):
    sql = """
        SELECT
            {clearance} as clearance_obj,
            'related_item_history', json_agg(
                {history} ORDER BY ci.created_at DESC
            ) AS history_obj
        FROM public.v1_clearances as c
        LEFT JOIN public.v1_clearance_items as ci ON c.id = ci.clearance_id
        {where_clause}
        GROUP BY c.id ;
    """
    return sql.format(
        clearance=_build_jsonb_clearance(), history=_build_jsonb_clearance_item(), where_clause=where_clause
    )


@log_statement
async def get_select_clearance_items_statement(where_condition: str):
    sql = """
        SELECT
        {clearance} as clearance_item_obj
        FROM public.v1_clearance_items as ci
        {where_condition}
        ORDER BY created_at DESC ;
    """
    return sql.format(
        clearance=_build_jsonb_clearance_item(),
        where_condition=where_condition,
    )


@log_statement
async def build_query_where_clause(query: ClearanceQuery, start_index: int = 1):
    query_dict = query.model_dump()
    transformed_query = ""
    query_terms = []
    query_data = []
    index = start_index

    for key, val in query_dict.items():
        if val is None:
            continue

        if key == "organizations":
            query_terms.append(f"c.organization_id = ANY(${index})")
        else:
            try:
                tag_group = TagGroup.enum_of(key)
                query_terms.append(f"(ci.tags->'{tag_group}')::jsonb ?| ${index}")
            except ValueError:
                # not a valid tag gropup
                continue

        query_data.append(val)
        index += 1

    if index > start_index:
        transformed_query = " AND ".join(query_terms)
    return transformed_query, query_data, index


def _build_jsonb_aggergated_clearance():
    sql = """
        jsonb_build_object(
            'id', c.id,
            'organization_id', c.organization_id,
            'updated_at', EXTRACT(epoch FROM c.updated_at)::int,
            'created_at', EXTRACT(epoch FROM c.created_at)::int,
            'product_name', ci.product_name,
            'description', ci.description,
            'tags', ci.tags,
            'tag_remark', ci.tag_remark,
            'product_release_at', EXTRACT(epoch FROM ci.product_release_at)::int,
            'product_release_at_precision', ci.product_release_at_precision,
            'info_updated_at', EXTRACT(epoch FROM ci.info_updated_at)::int,
            'info_provider', ci.info_provider,
            'info_source_type', ci.info_source_type,
            'info_source_content', ci.info_source_content
        )
    """
    return sql


def _build_jsonb_clearance():
    sql = """
        jsonb_build_object(
            'id', c.id,
            'organization_id', c.organization_id,
            'is_hidden', c.is_hidden,
            'active_item_id', c.active_item_id,
            'updater_type', c.updater_type,
            'updater_id', c.updater_id,
            'updated_at', EXTRACT(epoch FROM c.updated_at)::int,
            'creator_type', c.creator_type,
            'creator_id', c.creator_id,
            'created_at', EXTRACT(epoch FROM c.created_at)::int
        )
    """
    return sql


def _build_jsonb_clearance_item():
    sql = """
        jsonb_build_object(
            'id', ci.id,
            'clearance_id', ci.clearance_id,
            'product_name', ci.product_name,
            'description', ci.description,
            'tags', ci.tags,
            'tag_remark', ci.tag_remark,
            'product_release_at', EXTRACT(epoch FROM ci.product_release_at)::int,
            'product_release_at_precision', ci.product_release_at_precision,
            'info_updated_at', EXTRACT(epoch FROM ci.info_updated_at)::int,
            'info_provider', ci.info_provider,
            'info_source_type', ci.info_source_type,
            'info_source_content', ci.info_source_content,
            'creator_type', ci.creator_type,
            'creator_id', ci.creator_id,
            'created_at', EXTRACT(epoch FROM ci.created_at)::int
        )
    """
    return sql


def _clearance_column_names():
    sql = """
        id,
        organization_id,
        is_hidden,
        active_item_id,
        creator_type,
        creator_id,
        EXTRACT(epoch FROM created_at)::int as created_at,
        updater_type,
        updater_id,
        EXTRACT(epoch FROM updated_at)::int as updated_at
    """
    return sql.format()


def _clearance_item_column_names():
    sql = """
        id,
        clearance_id,
        product_name,
        description,
        tags,
        tag_remark,
        EXTRACT(epoch FROM product_release_at)::int as product_release_at,
        product_release_at_precision,
        EXTRACT(epoch FROM info_updated_at)::int as info_updated_at,
        info_provider,
        info_source_type,
        info_source_content,
        creator_type,
        creator_id,
        EXTRACT(epoch FROM created_at)::int as created_at
    """
    return sql.format()
