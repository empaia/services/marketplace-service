from typing import Dict, List

from asyncpg import PostgresError
from fastapi import HTTPException, UploadFile
from pydantic import UUID4

from marketplace_service.clients.db_commons import portal_app_exists
from marketplace_service.clients.helpers.apps_sql_helper_v1 import (
    get_insert_app_statement,
    get_insert_app_ui_statement,
    get_insert_container_image_statement,
    get_select_active_app_statement,
    get_select_app_ui_bundle_for_app_statement,
    get_select_app_with_app_ui_and_container_image_statement,
    get_update_app_ui_statement,
    get_update_container_image_status_statement,
)
from marketplace_service.clients.helpers.general_sql_helper import (
    get_basic_delete_where_statement,
    get_basic_select_where_statement,
    get_basic_update_statement,
    get_basic_update_where_statement,
)
from marketplace_service.clients.helpers.portal_app_sql_helper_v1 import (
    get_delete_and_insert_media_ref_entry_statement,
    get_delete_portal_app_and_app_view_statement,
    get_insert_app_view_statement,
    get_insert_portal_app_details_statement,
    get_insert_portal_app_media_reference_statement,
    get_insert_portal_app_media_statement,
    get_insert_portal_app_statement,
    get_query_plain_portal_app_statement,
    get_select_app_by_app_view_id_statement,
    get_select_app_ids_for_portal_apps_statement,
    get_select_app_view_media_statement,
    get_select_app_views_statement,
    get_select_plain_portal_app_statement,
    get_update_portal_app_review_statement,
    get_update_portal_app_status_statement,
    get_update_portal_app_view_non_functional_flag_statement,
)
from marketplace_service.clients.helpers.utils import (
    log_and_raise_postgres_error,
    translate_tags,
    validate_details,
    validate_multi_language_dict,
    validate_tags,
)
from marketplace_service.clients.minio import get_presigned_url, get_resized_images, put_media_object
from marketplace_service.custom_models.commons import (
    EAD_SCHEMA_DRAFT3,
    EAD_SCHEMA_DRAFT3_ALT,
    EAD_SCHEMA_V3,
    SUPPORTED_IMAGE_CONTENT_TYPES,
    SUPPORTED_MANUAL_CONTENT_TYPES,
    MinioClientWrapper,
)
from marketplace_service.custom_models.v1.resources import (
    AppUI,
    AppUIStatus,
    ContainerImage,
    ContainerImageStatus,
    InternalAppUI,
    InternalContainerImage,
)
from marketplace_service.models.marketplace.app import (
    ApiVersion,
    AppDetails,
    AppMediaPurpose,
    AppStatus,
    AppUiConfiguration,
    BaseQuery,
    ClosedActiveAppViews,
    ClosedApp,
    ClosedAppView,
    ClosedPortalApp,
    ClosedPortalAppList,
    CustomerAppViewQuery,
    CustomerPortalAppQuery,
    ListingStatus,
    MediaList,
    MediaObject,
    PortalAppQuery,
    PostAdminApp,
    PostAdminAppView,
    PostAdminPortalApp,
    PostAppDetails,
    PostAppTag,
    PublicActiveAppViews,
    PublicAppView,
    PublicPortalApp,
    PublicPortalAppList,
)
from marketplace_service.py_ead_validation.py_ead_validation.exceptions import (
    EadSchemaValidationError,
    EadValidationError,
)
from marketplace_service.utils import translate_dict

from ..singletons import ead_validator, ead_validator_legacy, logger, settings


class AppClient:
    def __init__(self, conn):
        self.conn = conn

    async def post_portal_app(
        self,
        organization_id: str,
        organization_name: str,
        user_id: str,
        status: ListingStatus = ListingStatus.DRAFT,
        portal_app_id: str = None,
    ) -> ClosedPortalApp:
        external_id = False
        values = [organization_id, organization_name, status, user_id]
        if portal_app_id is not None:
            external_id = True
            values = [portal_app_id, organization_id, organization_name, status, user_id]

        sql_insert_portal_app = await get_insert_portal_app_statement(external_ids=external_id)

        try:
            app_record = await self.conn.fetchrow(sql_insert_portal_app, *values)
            return ClosedPortalApp(
                id=app_record["id"],
                organization_id=app_record["organization_id"],
                organization_name=app_record["organization_name"],
                status=app_record["status"],
                creator_id=app_record["creator_id"],
                created_at=app_record["created_at"],
                updated_at=app_record["updated_at"],
                active_app_views=ClosedActiveAppViews(),
            )
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Portal app could not be created")

    async def post_admin_portal_app(
        self,
        minio_client: MinioClientWrapper,
        post_portal_app: PostAdminPortalApp,
        user_id: str,
        external_ids: bool = False,
        existing_portal_app: bool = False,
    ) -> ClosedPortalApp:
        portal_app_id = await self.retrieve_portal_app_id(post_portal_app, existing_portal_app, external_ids, user_id)

        for version in ApiVersion:
            post_app = getattr(post_portal_app.active_apps, version.value)
            if post_app:
                if "namespace" not in post_app.ead:
                    raise HTTPException(status_code=422, detail="Invalid EAD: Missing field 'namespace'.")
                await self._ead_namespace_exists(post_app.ead["namespace"])
                _ = await self._validate_active_post_app(post_app, version)
                _ = await self.post_admin_portal_app_view(
                    minio_client=minio_client,
                    portal_app_id=portal_app_id,
                    user_id=user_id,
                    post_portal_app=post_portal_app,
                    post_app=post_app,
                    active_api_version=version,
                    external_ids=external_ids,
                )

        return await self.get_closed_portal_app(minio_client=minio_client, portal_app_id=portal_app_id)

    async def post_admin_portal_app_view(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: str,
        user_id: str,
        post_portal_app: PostAdminPortalApp,
        post_app: PostAdminApp,
        active_api_version: ApiVersion,
        external_ids: bool = False,
    ):
        app_view = await self.post_portal_app_view(
            minio_client, portal_app_id, user_id, False, active_api_version, post_portal_app.research_only
        )
        _ = await self.put_portal_app_view_details(
            minio_client, portal_app_id, app_view.id, post_portal_app.details, active_api_version
        )
        _ = await self.put_app_view_tags(minio_client, portal_app_id, app_view.id, post_portal_app.tags)

        app = await self.post_app(
            portal_app_id=portal_app_id,
            post_app=post_app,
            organization_id=post_portal_app.organization_id,
            user_id=user_id,
            status=AppStatus.APPROVED,
            external_ids=external_ids,
            enable_ead_legacy_support=True,
        )

        _ = await self.link_app_to_app_view(minio_client, portal_app_id, app_view.id, app.id)
        _ = await self._update_status_app_view(
            minio_client, portal_app_id, app_view.id, active_api_version, None, AppStatus.APPROVED
        )

        return await self.get_closed_app_view(
            minio_client=minio_client, portal_app_id=portal_app_id, app_view_id=app_view.id
        )

    async def post_admin_app_view(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: str,
        user_id: str,
        post_app_view: PostAdminAppView,
        active_api_version: ApiVersion,
        existing_app: bool = False,
    ):
        raw_portal_app = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        app_view = await self.post_portal_app_view(
            minio_client, portal_app_id, user_id, False, active_api_version, post_app_view.research_only
        )
        _ = await self.put_portal_app_view_details(
            minio_client, portal_app_id, app_view.id, post_app_view.details, active_api_version
        )
        _ = await self.put_app_view_tags(minio_client, portal_app_id, app_view.id, post_app_view.tags)

        app_id = None
        if existing_app:
            if post_app_view.app and post_app_view.app.id is None:
                raise HTTPException(status_code=422, detail="Id field needs to be set if existing_app is True")
            if not await self._app_exists(post_app_view.app.id):
                raise HTTPException(status_code=400, detail=f"App with ID {post_app_view.app.id} does not exist")
            app_id = post_app_view.app.id
        else:
            app = await self.post_app(
                portal_app_id=portal_app_id,
                post_app=post_app_view.app,
                organization_id=raw_portal_app["organization_id"],
                user_id=user_id,
                status=AppStatus.APPROVED,
                external_ids=False,
                enable_ead_legacy_support=True,
            )
            app_id = app.id

        _ = await self.link_app_to_app_view(minio_client, portal_app_id, app_view.id, app_id)
        _ = await self._update_status_app_view(
            minio_client, portal_app_id, app_view.id, active_api_version, None, AppStatus.APPROVED
        )

        return await self.get_closed_app_view(
            minio_client=minio_client, portal_app_id=portal_app_id, app_view_id=app_view.id
        )

    async def retrieve_portal_app_id(
        self, post_portal_app: PostAdminPortalApp, existing_portal_app: bool, external_ids: bool, user_id: str
    ) -> str:
        if existing_portal_app and post_portal_app.id is not None:
            # New app view for existing portal app and a (hopefully valid) portal app ID is given
            _ = await portal_app_exists(conn=self.conn, portal_app_id=post_portal_app.id)
            return post_portal_app.id
        elif existing_portal_app and post_portal_app.id is None:
            # New app view for existing portal app but no ID given
            raise HTTPException(
                status_code=404, detail="Valid portal app ID is required, when 'existing_portal_app' is True."
            )
        elif external_ids and post_portal_app.id is not None:
            # New portal app with given portal app ID
            portal_app = await self.post_portal_app(
                organization_id=post_portal_app.organization_id,
                organization_name=post_portal_app.organization_name,
                user_id=user_id,
                status=ListingStatus.DRAFT,
                portal_app_id=post_portal_app.id,
            )
            return portal_app.id
        else:
            # New portal app and app view with fresh IDs
            portal_app = await self.post_portal_app(
                organization_id=post_portal_app.organization_id,
                organization_name=post_portal_app.organization_name,
                user_id=user_id,
                status=ListingStatus.DRAFT,
            )
            return portal_app.id

    async def delete_admin_portal_app(self, portal_app_id: UUID4) -> bool:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)

        sql_get_app_views = await get_basic_select_where_statement("v1_portal_apps_view", "WHERE portal_app_id = $1")
        sql_get_media = await get_basic_select_where_statement(
            "v1_portal_apps_media_ref", "WHERE portal_app_view_id = ANY($1)"
        )
        sql_delete_details = await get_basic_delete_where_statement("v1_portal_apps_details", "WHERE id = ANY($1)")
        sql_delete_apps = await get_basic_delete_where_statement("v1_apps", "WHERE id = ANY($1)")
        sql_delete_media_ref_statement = await get_basic_delete_where_statement(
            "v1_portal_apps_media_ref", "WHERE portal_app_view_id = ANY($1)"
        )
        sql_delete_media = await get_basic_delete_where_statement("v1_portal_apps_media", "WHERE id = ANY($1)")
        sql_delete_portal_app_and_app_view = await get_delete_portal_app_and_app_view_statement()

        try:
            app_view_rows = await self.conn.fetch(sql_get_app_views, portal_app_id)
            app_view_ids = [view["id"] for view in app_view_rows]
            details_ids = [view["details_id"] for view in app_view_rows]
            app_ids = [view["app_id"] for view in app_view_rows]

            media_rows = await self.conn.fetch(sql_get_media, app_view_ids)
            media_ids = [media["media_id"] for media in media_rows]

            await self.conn.execute(sql_delete_portal_app_and_app_view, portal_app_id, app_view_ids)
            await self.conn.execute(sql_delete_media, media_ids)
            await self.conn.execute(sql_delete_media_ref_statement, app_view_ids)
            await self.conn.execute(sql_delete_details, details_ids)
            await self.conn.execute(sql_delete_apps, app_ids)
            return True
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to delete portal app with corresponding data")

    async def get_public_portal_app(self, minio_client: MinioClientWrapper, portal_app_id: UUID4) -> PublicPortalApp:
        closed_portal_app = await self.get_closed_portal_app(minio_client, portal_app_id)

        public_portal_app = PublicPortalApp(
            id=portal_app_id,
            organization_id=closed_portal_app.organization_id,
            status=closed_portal_app.status,
            creator_id=closed_portal_app.creator_id,
            created_at=closed_portal_app.created_at,
            updated_at=closed_portal_app.updated_at,
        )

        v1_app = await self.public_to_closed_app_view(closed_portal_app.active_app_views.v1)
        v2_app = await self.public_to_closed_app_view(closed_portal_app.active_app_views.v2)
        v3_app = await self.public_to_closed_app_view(closed_portal_app.active_app_views.v3)
        public_portal_app.active_app_views = PublicActiveAppViews(v1=v1_app, v2=v2_app, v3=v3_app)

        return public_portal_app

    async def public_to_closed_app_view(self, closed_app_view: ClosedAppView) -> PublicAppView:
        if closed_app_view is None:
            return None

        return PublicAppView(
            version=closed_app_view.version,
            details=closed_app_view.details,
            media=closed_app_view.media,
            tags=closed_app_view.tags,
            non_functional=closed_app_view.non_functional,
            created_at=closed_app_view.created_at,
            reviewed_at=closed_app_view.reviewed_at,
        )

    async def get_related_public_portal_apps(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, skip: int, limit: int
    ) -> PublicPortalAppList:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        portal_app = await self.get_closed_portal_app(minio_client, portal_app_id)

        app_query = await self._build_related_portal_apps_query(portal_app.active_app_views)
        raw_apps = await self._query_raw_portal_apps(
            organization_id=None, query=app_query, statuses=[ListingStatus.LISTED]
        )

        # delete entry with portal app id
        for i, raw_app in enumerate(raw_apps["items"]):
            if str(raw_app["portal_app"]["id"]) == str(portal_app_id):
                del raw_apps["items"][i]
                raw_apps["item_count"] = raw_apps["item_count"] - 1
                break

        return await self._build_public_portal_app_list(minio_client, raw_apps, app_query, skip, limit)

    async def set_app_view_non_functional_state(
        self, minio_client: MinioClientWrapper, portal_app_id: str, app_view_id: str, is_non_functional: bool
    ) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        sql_update_non_functional = await get_update_portal_app_view_non_functional_flag_statement()

        try:
            _ = await self.conn.fetchrow(sql_update_non_functional, is_non_functional, app_view_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Could not updated portal app non_functional state")

    async def put_query_closed_portal_app(
        self,
        minio_client: MinioClientWrapper,
        query: BaseQuery,
        licensed_portal_app_ids: List[str],
        skip: int,
        limit: int,
    ) -> ClosedPortalAppList:
        # we only want to query apps here that are listed or have been listed (no draft ones)
        app_records = await self._query_raw_portal_apps(
            organization_id=None,
            query=query,
            statuses=[ListingStatus.LISTED, ListingStatus.DELISTED, ListingStatus.ADMIN_DELISTED],
        )
        return await self._build_closed_portal_app_list(
            minio_client, app_records, query, licensed_portal_app_ids, skip, limit
        )

    async def put_query_public_portal_apps(
        self, minio_client: MinioClientWrapper, query: BaseQuery, skip: int, limit: int
    ) -> PublicPortalAppList:
        app_records = await self._query_raw_portal_apps(
            organization_id=None, query=query, statuses=[ListingStatus.LISTED]
        )
        return await self._build_public_portal_app_list(minio_client, app_records, query, skip, limit)

    async def put_portal_app_view_details(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        details: PostAppDetails,
        api_version: ApiVersion,
    ) -> ClosedAppView:
        validate_details(details)
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        raw_app_view = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        if api_version is None:
            api_version = ApiVersion(raw_app_view["api_version"])

        marketplace_url = f"{settings.portal_url.strip('/')}/marketplace/app/{portal_app_id}/{api_version.value}"
        sql_insert_portal_app_details = await get_insert_portal_app_details_statement()
        sql_update_details = await get_basic_update_statement("v1_portal_apps_view", "details_id", "id")

        try:
            details_record = await self.conn.fetchrow(
                sql_insert_portal_app_details, details.name, details.description, marketplace_url
            )
            await self.conn.fetchrow(sql_update_details, details_record["id"], app_view_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Could not persist portal app view details")

    async def delete_portal_app_view_details(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        sql_remove_details_ref = await get_basic_update_where_statement(
            "v1_portal_apps_view", "details_id", "WHERE id = $2"
        )
        sql_delete_details = await get_basic_delete_where_statement("v1_portal_apps_details", "WHERE id = $1")

        try:
            details_record = await self.conn.fetchrow(sql_remove_details_ref, None, app_view_id)
            await self.conn.fetchrow(sql_delete_details, details_record["details_id"])
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Could not remove portal app view details")

    async def put_app_view_tags(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4, tags: List[PostAppTag]
    ) -> ClosedAppView:
        tags_json = validate_tags(tags=tags, invalid_tag_groups=["IMAGE_TYPE"])
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        sql_update_app_view_tags = await get_basic_update_statement("v1_portal_apps_view", "tags", "id")

        try:
            await self.conn.fetchrow(sql_update_app_view_tags, tags_json, app_view_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Could not persist app tags")

    async def delete_app_view_tags(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        sql_delete_tags = await get_basic_update_where_statement("v1_portal_apps_view", "tags", "WHERE id = $2")

        try:
            await self.conn.fetchrow(sql_delete_tags, None, app_view_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to remove portal app view tags")

    async def link_app_to_app_view(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4, app_id: UUID4
    ) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)
        _ = await self._app_exists(app_id)

        sql_update_app_id = await get_basic_update_statement("v1_portal_apps_view", "app_id", "id")

        try:
            await self.conn.fetchrow(sql_update_app_id, app_id, app_view_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(
                e, f"Failed to put app ID {app_id} for portal app ID {portal_app_id} and view ID {app_view_id}"
            )

    async def unlink_active_app_from_app_view(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        sql_remove_details_ref = await get_basic_update_where_statement(
            "v1_portal_apps_view", "app_id", "WHERE id = $2"
        )

        try:
            _ = await self.conn.fetchrow(sql_remove_details_ref, None, app_view_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to delete app ID from portal app view with ID {app_view_id}")

    async def put_portal_app_view_media(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        media_purpose: AppMediaPurpose,
        index: int,
        caption: Dict[str, str],
        alt_text: Dict[str, str],
        media_file: UploadFile,
        is_admin: bool = False,
    ) -> ClosedPortalApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        app_view = await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        media_to_update = await self._validate_media(app_view, media_purpose, index, media_file)
        if caption:
            validate_multi_language_dict("Caption", caption)
        if alt_text:
            validate_multi_language_dict("Alternative text", alt_text)
        if not is_admin:
            await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        minio_media_id = await put_media_object(minio_client, media_file)

        sql_insert_portal_app_media = await get_insert_portal_app_media_statement()

        try:
            media_record = await self.conn.fetchrow(
                sql_insert_portal_app_media,
                media_purpose,
                index,
                minio_media_id,
                media_file.filename,
                media_file.content_type,
                caption,
                alt_text,
            )
            if media_to_update:
                sql_update_media_ref = await get_delete_and_insert_media_ref_entry_statement()
                await self.conn.fetchrow(sql_update_media_ref, app_view_id, media_to_update.id, media_record["id"])
            else:
                sql_insert_media_ref = await get_insert_portal_app_media_reference_statement()
                await self.conn.fetchrow(sql_insert_media_ref, app_view_id, media_record["id"])
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(
                e, f"Failed to put media for portal app ID {portal_app_id} and app view ID {app_view_id}"
            )

    async def remove_media_from_portal_app_view(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        purpose: AppMediaPurpose,
        index: int,
    ) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.DRAFT)

        media = await self.get_portal_app_view_media(app_view_id)

        if media is None:
            raise HTTPException(status_code=404, detail="No matching media for given app view ID.")

        entry_to_delte = None
        for media_entry in media:
            if media_entry["media_purpose"] == purpose and media_entry["index"] == index:
                entry_to_delte = media_entry
                break

        if entry_to_delte is None:
            raise HTTPException(status_code=404, detail="No matching media reference for given purpose and index.")

        remove_media_statement = await get_basic_delete_where_statement(
            "v1_portal_apps_media_ref", "WHERE portal_app_view_id = $1 AND media_id = $2"
        )
        try:
            _ = await self.conn.fetchrow(
                remove_media_statement, entry_to_delte["portal_app_view_id"], entry_to_delte["media_id"]
            )
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to remove media for portal app ID {portal_app_id}")

    async def get_portal_app_view_media(self, app_view_id: UUID4):
        sql_select_view_medias = await get_select_app_view_media_statement()

        try:
            app_view_medias = await self.conn.fetch(sql_select_view_medias, app_view_id)
            return app_view_medias
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to get media for app view ID {app_view_id}")

    async def request_app_view_approval(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> ClosedApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)

        app_view = await self._validate_app_view(minio_client, portal_app_id, app_view_id, AppStatus.DRAFT)
        active_app_version = await self._get_active_app_version(app_view)

        app_id = status = None
        if app_view.app is not None:
            app_id = app_view.app.id
        else:
            status = AppStatus.PENDING

        return await self._update_status_app_view(
            minio_client, portal_app_id, app_view.id, active_app_version, app_id, status
        )

    async def revoke_app_view_approval(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> ClosedApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)

        app_view = await self._validate_app_view(minio_client, portal_app_id, app_view_id, AppStatus.PENDING)
        active_app_version = await self._get_active_app_version(app_view)

        return await self._update_status_app_view(
            minio_client,
            portal_app_id,
            app_view.id,
            active_app_version,
            app_view.app.id,
            status=AppStatus.DRAFT,
        )

    async def put_admin_set_status_portal_app(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        status: ListingStatus,
        review_comment: str,
        user_id: str,
    ):
        raw_portal_app = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        if review_comment is None:
            review_comment = "Updated and reviewed by admin."
        ids = []
        for version in ApiVersion:
            column_name = f"{version.value}_active_view_id"
            if raw_portal_app and column_name in raw_portal_app and raw_portal_app[column_name] is not None:
                active_app_view_id = raw_portal_app[column_name]
                await self._update_app_view_review(active_app_view_id, review_comment, user_id)
                ids.append(active_app_view_id)
            else:
                ids.append(None)
        await self._update_status_portal_app(portal_app_id, ids[0], ids[1], ids[2], status)
        return await self.get_closed_portal_app(minio_client=minio_client, portal_app_id=portal_app_id)

    async def put_approve_portal_app_view_review(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        review_comment: str,
        user_id: str,
    ) -> ClosedPortalApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.PENDING)

        app_view = await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        if app_view.non_functional is not True:
            await self._app_in_state(portal_app_id, app_view_id, AppStatus.APPROVED)

        active_app_version = await self._get_active_app_version(app_view)

        await self._update_status_app_view(
            minio_client, portal_app_id, app_view_id, active_app_version, status=AppStatus.APPROVED
        )
        # TODO: update correct active app view
        await self._update_app_view_review(app_view_id, review_comment, user_id)
        await self._update_status_portal_app(portal_app_id, app_view_id, None, None, ListingStatus.LISTED)
        return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)

    async def put_reject_portal_app_view_review(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        review_comment: str,
        user_id: str,
    ) -> ClosedPortalApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_view_in_state(portal_app_id, app_view_id, AppStatus.PENDING)

        app_view = await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        active_app_version = await self._get_active_app_version(app_view)

        return await self._update_status_app_view(
            minio_client, portal_app_id, app_view_id, active_app_version, status=AppStatus.REJECTED
        )

    async def put_app_status(self, portal_app_id: UUID4, app_id: UUID4, status: AppStatus) -> ClosedApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        _ = await self._app_exists(app_id)

        return await self._update_status_app(app_id, status)

    async def post_app(
        self,
        portal_app_id: UUID4,
        post_app: PostAdminApp,
        organization_id: str,
        user_id: str,
        status: AppStatus = AppStatus.DRAFT,
        external_ids: bool = False,
        enable_ead_legacy_support: bool = False,
    ) -> ClosedApp:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        version = await self._validate_ead(post_app.ead, enable_ead_legacy_support)

        app_ui_id = None
        if post_app.app_ui_url is not None or post_app.app_ui_configuration is not None:
            internal_app_ui = InternalAppUI(
                name=post_app.ead["name"],
                status=AppUIStatus.UNZIP_FINISHED,
                app_ui_configuration=post_app.app_ui_configuration,
                app_ui_url=post_app.app_ui_url,
                creator_id=user_id,
                temp_directory=None,
                organization_id=organization_id,
            )
            app_ui = await self.post_app_ui(internal_app_ui)
            app_ui_id = app_ui.id

        internal_container_image = InternalContainerImage(
            name=post_app.ead["name"],
            status=ContainerImageStatus.PUSH_FINISHED,
            registry_image_url=post_app.registry_image_url,
            creator_id=user_id,
            id=None,
            temp_directory=None,
            error_message=None,
            organization_id=organization_id,
        )
        container_image = await self.post_container_image(internal_container_image)

        if external_ids:
            if post_app.id is None:
                raise HTTPException(status_code=404, detail="Missing external ID for app.")
            value_list = [
                post_app.id,
                portal_app_id,
                status,
                version,
                post_app.ead,
                user_id,
                app_ui_id,
                container_image.id,
                post_app.app_documentation_url,
            ]
        else:
            value_list = [
                portal_app_id,
                status,
                version,
                post_app.ead,
                user_id,
                app_ui_id,
                container_image.id,
                post_app.app_documentation_url,
            ]

        sql_insert_app = await get_insert_app_statement(external_ids)
        try:
            app_record = await self.conn.fetchrow(
                sql_insert_app,
                *value_list,
            )
            return await self.get_app_by_id(app_record["id"])
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to post app for portal app ID {portal_app_id}")

    async def post_app_ui(self, app_ui: InternalAppUI):
        sql_post_app_ui = await get_insert_app_ui_statement()

        app_ui_config = None
        if app_ui.app_ui_configuration:
            app_ui_config = app_ui.app_ui_configuration.model_dump()

        try:
            app_ui_record = await self.conn.fetchrow(
                sql_post_app_ui,
                app_ui.name,
                app_ui.temp_directory,
                app_ui.status,
                app_ui.app_ui_url,
                app_ui_config,
                app_ui.creator_id,
                app_ui.organization_id,
            )
            return await self._build_app_ui_bundle_model(app_ui_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to post app ui")

    async def update_app_ui_state(self, app_ui_id: UUID4, directory: str, status: AppUIStatus) -> AppUI:
        sql_update_app_ui = await get_update_app_ui_statement()

        try:
            app_ui_record = await self.conn.fetchrow(sql_update_app_ui, directory, status, app_ui_id)
            return await self._build_app_ui_bundle_model(app_ui_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to update app UI bundle.")

        return None

    async def post_app_ui_id(self, app_id: UUID4, app_ui_id: UUID4) -> ClosedApp:
        sql_post_container_image_to_app = await get_basic_update_where_statement(
            "v1_apps", "app_ui_id", "WHERE id = $2"
        )

        try:
            _ = await self.conn.fetchrow(sql_post_container_image_to_app, str(app_ui_id), str(app_id))
            return self.get_app_by_id(app_id=app_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update app ui for app {app_id}")

    async def post_container_image(self, container_image: InternalContainerImage):
        sql_post_container_image = await get_insert_container_image_statement()

        try:
            container_image_record = await self.conn.fetchrow(
                sql_post_container_image,
                container_image.name,
                container_image.temp_directory,
                container_image.status,
                container_image.registry_image_url,
                container_image.creator_id,
                container_image.organization_id,
            )
            return await self._build_container_image_model(container_image_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to post container image")

    async def post_container_image_id(self, app_id: UUID4, container_image_id: UUID4) -> ClosedApp:
        sql_post_container_image_to_app = await get_basic_update_where_statement(
            "v1_apps", "container_image_id", "WHERE id = $2"
        )

        try:
            _ = await self.conn.fetchrow(sql_post_container_image_to_app, str(container_image_id), str(app_id))
            return self.get_app_by_id(app_id=app_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update container image for app {app_id}")

    async def get_apps(self, portal_app_id: UUID4) -> List[ClosedApp]:
        sql_get_apps = await get_select_app_with_app_ui_and_container_image_statement("WHERE a.portal_app_id = $1")

        try:
            app_records = await self.conn.fetch(sql_get_apps, str(portal_app_id))
            apps_result = []
            if app_records is None:
                return apps_result
            for app_record in app_records:
                closed_app = await self._build_closed_app(app_record)
                apps_result.append(closed_app)
            return apps_result
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to get apps for portal app ID {portal_app_id}")

    async def get_app(self, portal_app_id: UUID4, app_view_id: UUID4) -> ClosedApp:
        sql_get_app = await get_select_app_by_app_view_id_statement()

        try:
            app_record = await self.conn.fetchrow(sql_get_app, app_view_id)
            if app_record is None:
                raise HTTPException(status_code=404, detail="No valid app for app view.")
            if app_record["portal_app_id"] != portal_app_id:
                raise HTTPException(status_code=400, detail="Invalid portal app ID for app.")
            return await self._build_closed_app(app_record)
        except PostgresError as e:
            log_and_raise_postgres_error(
                e, f"Failed to get app for portal app ID {portal_app_id} and app view ID {app_view_id}"
            )

    async def get_app_by_id(self, app_id: UUID4) -> ClosedApp:
        sql_get_app = await get_select_app_with_app_ui_and_container_image_statement("WHERE a.id = $1")

        try:
            app_record = await self.conn.fetchrow(sql_get_app, app_id)
            if app_record is None:
                raise HTTPException(status_code=404, detail=f"No valid app for ID {app_id}.")
            return await self._build_closed_app(app_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to get app by ID {app_id}")

    async def delete_app(self, portal_app_id: UUID4, app_id: UUID4) -> ClosedApp:
        sql_get_app = await get_select_app_with_app_ui_and_container_image_statement("WHERE a.id = $1")
        sql_delte_app = await get_basic_delete_where_statement("v1_apps", "WHERE id = $1")

        try:
            app_record = await self.conn.fetchrow(sql_get_app, app_id)
            if app_record is None:
                raise HTTPException(status_code=404, detail=f"No valid app for ID {app_id}.")
            if app_record["portal_app_id"] != portal_app_id:
                raise HTTPException(status_code=400, detail="Invalid portal app ID for app.")
            if app_record["status"] != AppStatus.DRAFT:
                raise HTTPException(status_code=400, detail="Can not delete app in 'DRAFT' state.")
            _ = await self.conn.fetchrow(sql_delte_app, app_id)
            return await self._build_closed_app(app_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to delete app ID {app_id} for portal app ID {portal_app_id}")

    async def get_closed_portal_app(self, minio_client: MinioClientWrapper, portal_app_id: UUID4) -> ClosedPortalApp:
        app_record = await self.get_raw_portal_app(portal_app_id)
        if app_record is None:
            raise HTTPException(status_code=404, detail=f"No valid portal app for ID {portal_app_id}.")

        return await self._build_closed_portal_app(minio_client, app_record)

    async def get_raw_portal_app(self, portal_app_id: str) -> dict:
        sql = await get_select_plain_portal_app_statement()

        try:
            app_record = await self.conn.fetchrow(sql, portal_app_id)
            return app_record
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to get portal app for ID {portal_app_id}")

    async def get_app_ids_for_portal_app_ids(self, portal_app_ids: List[str]) -> Dict[str, List[str]]:
        sql = await get_select_app_ids_for_portal_apps_statement()

        try:
            app_ids = await self.conn.fetchrow(sql, portal_app_ids)
            app_id_dict = {}
            if app_ids is not None:
                for app in app_ids:
                    if app["portal_app_id"] in app_id_dict:
                        app["portal_app_id"].append(app["id"])
                    else:
                        app["portal_app_id"] = [app["id"]]
            return app_id_dict
        except PostgresError as e:
            log_and_raise_postgres_error(
                e, f"Failed to retrieve app ids for portal app IDs [{', '.join(portal_app_ids)}]"
            )

    async def get_active_app(self, portal_app_id: str) -> ClosedApp:
        sql = await get_select_active_app_statement()

        try:
            app_record = await self.conn.fetchrow(sql, portal_app_id)
            if app_record is None:
                return None
            return await self._build_closed_app(app_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to get active app for portal app ID {portal_app_id}")

    async def post_portal_app_view(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        user_id: str,
        edit: bool,
        active_api_version: ApiVersion,
        research_only: bool,
    ) -> ClosedAppView:
        raw_portal_app = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        await self._draft_app_view_exists(portal_app_id)

        sql = await get_insert_app_view_statement()

        try:
            app_view = None
            active_app_id_field = f"{active_api_version.value}_active_view_id"
            if edit and active_app_id_field in raw_portal_app and raw_portal_app[active_app_id_field] is not None:
                app_view_record = await self.get_raw_portal_app_view(portal_app_id, raw_portal_app[active_app_id_field])
                app_view = await self.conn.fetchrow(
                    sql,
                    portal_app_id,
                    app_view_record["portal_app_view_details"]["id"],
                    app_view_record["portal_app_view_app"]["id"],
                    app_view_record["portal_app_view"]["tags"],
                    AppStatus.DRAFT,
                    user_id,
                    research_only,
                    active_api_version.value,
                )
            else:
                app_view = await self.conn.fetchrow(
                    sql,
                    portal_app_id,
                    None,
                    None,
                    None,
                    AppStatus.DRAFT,
                    user_id,
                    research_only,
                    active_api_version.value,
                )
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view["id"])
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to create new app view")

    async def get_portal_app_views(self, minio_client: MinioClientWrapper, portal_app_id: UUID4) -> List[ClosedAppView]:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)

        sql = await get_select_app_views_statement("WHERE pav.portal_app_id = $1")

        try:
            app_view_records = await self.conn.fetch(sql, portal_app_id)
            app_view_results = []
            for app_view_record in app_view_records:
                app_view = await self._build_closed_app_view(
                    minio_client, app_view_record, app_view_record["portal_app"]["organization_id"]
                )
                app_view_results.append(app_view)
            return app_view_results
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query app views")

    async def get_pending_reviews(self, minio_client: MinioClientWrapper) -> List[ClosedAppView]:
        sql = await get_select_app_views_statement("WHERE pav.status = 'PENDING'")

        try:
            app_view_records = await self.conn.fetch(sql)
            app_view_results = []
            for app_view_record in app_view_records:
                app_view = await self._build_closed_app_view(
                    minio_client, app_view_record, app_view_record["portal_app"]["organization_id"]
                )
                app_view_results.append(app_view)
            return app_view_results
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query pending portal app views")

    async def get_closed_app_view(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> ClosedAppView:
        raw_portal_app = await self.get_raw_portal_app_view(portal_app_id, app_view_id)

        if raw_portal_app:
            return await self._build_closed_app_view(
                minio_client, raw_portal_app, raw_portal_app["portal_app"]["organization_id"]
            )

        raise HTTPException(status_code=404, detail=f"App view with ID {app_view_id} does not exist.")

    async def get_public_app_view(
        self, minio_client: MinioClientWrapper, portal_app_id: UUID4, app_view_id: UUID4
    ) -> PublicAppView:
        raw_portal_app = await self.get_raw_portal_app_view(portal_app_id, app_view_id)

        if raw_portal_app:
            return await self._build_public_app_view(minio_client, raw_portal_app)

        raise HTTPException(status_code=404, detail=f"App view with ID {app_view_id} does not exist.")

    async def get_raw_portal_app_view(self, portal_app_id: UUID4, app_view_id: UUID4) -> ClosedAppView:
        _ = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)

        sql = await get_select_app_views_statement("WHERE pav.id = $1")

        try:
            app_view_record = await self.conn.fetchrow(sql, app_view_id)
            return app_view_record
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query portal app views")

    async def query_app_views(
        self, minio_client: MinioClientWrapper, query: CustomerAppViewQuery
    ) -> List[ClosedAppView]:
        where_condition = "WHERE a.id = ANY($1)"
        values = [query.apps]
        if query.api_versions:
            where_condition = f"{where_condition} AND pav.api_version = ANY($2)"
            values.append([av.value for av in query.api_versions])

        sql = await get_select_app_views_statement(where_condition=where_condition)

        try:
            app_view_records = await self.conn.fetch(sql, *values)
            app_view_results = []
            for app_view_record in app_view_records:
                app_view = await self._build_closed_app_view(
                    minio_client, app_view_record, app_view_record["portal_app"]["organization_id"]
                )
                app_view_results.append(app_view)
            return app_view_results
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query app views")

    async def get_portal_apps_for_orga(
        self, minio_client: MinioClientWrapper, organization_id: str
    ) -> List[ClosedPortalApp]:
        sql = await get_basic_select_where_statement("v1_portal_apps", "WHERE organization_id = $1")

        try:
            portal_app_records = await self.conn.fetch(sql, organization_id)
            if portal_app_records is None:
                return []

            ids = [str(portal_app["id"]) for portal_app in portal_app_records]
            portal_apps = []
            for portal_app_id in ids:
                pa = await self.get_closed_portal_app(minio_client, portal_app_id)
                portal_apps.append(pa)

            return portal_apps
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query portal apps.")

    async def get_app_ui_bundle(self, app_ui_bundle_id: UUID4) -> AppUI:
        sql = await get_basic_select_where_statement("v1_app_ui_bundles", "WHERE id = $1")

        try:
            app_ui_record = await self.conn.fetchrow(sql, app_ui_bundle_id)
            return await self._build_app_ui_bundle_model(app_ui_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to load app UI.")

    async def get_app_ui_bundles(self, organization_id: str = None, status: AppUIStatus = None) -> List[AppUI]:
        where_condition = ""
        values = []
        if organization_id is not None:
            where_condition = "WHERE organization_id = $1"
            values.append(organization_id)
        if status is not None:
            where_condition = f"{where_condition} AND status = $2"
            values.append(status)

        sql = await get_basic_select_where_statement("v1_app_ui_bundles", where_condition)

        try:
            ui_records = await self.conn.fetch(sql, *values)
            app_uis = []
            for ui in ui_records:
                app_uis.append(await self._build_app_ui_bundle_model(ui))
            return app_uis
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to load app UIs.")

    async def get_or_create_app_ui_bundle(self, minio_client: MinioClientWrapper, app_id: UUID4, user_id: str) -> AppUI:
        app_ui_bundle = await self.get_app_ui_bundle_for_app(app_id=app_id)

        if not app_ui_bundle:
            app = await self.get_app_by_id(app_id)
            portal_app = await self.get_closed_portal_app(minio_client, app.portal_app_id)

            internal_app_ui = InternalAppUI(
                name=app.ead["name"],
                status=AppUIStatus.INITIALIZED,
                app_ui_configuration=None,
                app_ui_url=None,
                creator_id=user_id,
                temp_directory=None,
                organization_id=portal_app.organization_id,
            )

            app_ui_bundle = await self.post_app_ui(internal_app_ui)
            _ = await self._update_app_with_ui_bundle(app_id, app_ui_bundle.id)

        return app_ui_bundle

    async def _update_app_with_ui_bundle(self, app_id: UUID4, app_ui_bundle_id: UUID4):
        sql = await get_basic_update_where_statement("v1_apps", "app_ui_id", "WHERE id = $2")

        try:
            ui_record = await self.conn.fetchrow(sql, app_ui_bundle_id, app_id)
            return ui_record
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update app_ui_bundle_id for app ID {app_id}")

    async def get_app_ui_bundle_for_app(self, app_id: UUID4) -> AppUI:
        sql = await get_select_app_ui_bundle_for_app_statement()

        try:
            ui_record = await self.conn.fetchrow(sql, app_id)
            return await self._build_app_ui_bundle_model(ui_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to load app UI bundle for app ID")

    async def delete_app_ui_bundle(self, app_ui_bundle_id: UUID4, creator_id: str) -> AppUI:
        # TODO: Remove container image
        await self._app_ui_is_referenced(app_ui_id=app_ui_bundle_id)
        raw_app_ui_bundle = await self._delete_app_ui(app_ui_id=app_ui_bundle_id)
        return await self._build_app_ui_bundle_model(raw_app_ui_bundle=raw_app_ui_bundle)

    async def get_container_image(self, container_image_id: str) -> ContainerImage:
        raw_container_image = await self.get_raw_container_image(container_image_id=container_image_id)
        return await self._build_container_image_model(raw_container_image=raw_container_image)

    async def get_container_images(
        self, organization_id: str = None, status: ContainerImageStatus = None
    ) -> List[ContainerImage]:
        raw_container_images = await self.get_raw_container_images(organization_id=organization_id, status=status)
        container_images = []
        for ci in raw_container_images:
            container_images.append(await self._build_container_image_model(ci))
        return container_images

    async def get_internal_container_images(
        self, organization_id: str = None, status: ContainerImageStatus = None
    ) -> List[InternalContainerImage]:
        raw_container_images = await self.get_raw_container_images(organization_id=organization_id, status=status)
        container_images = []
        for ci in raw_container_images:
            container_images.append(await self._build_internal_container_image_model(ci))
        return container_images

    async def get_raw_container_image(self, container_image_id: str):
        sql = await get_basic_select_where_statement("v1_container_images", "WHERE id = $1")

        try:
            return await self.conn.fetchrow(sql, container_image_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to load container images.")

    async def get_raw_container_images(self, organization_id: str = None, status: ContainerImageStatus = None):
        where_condition = ""
        values = []

        if organization_id is not None:
            where_condition = "organization_id = $1"
            values.append(organization_id)
        if status is not None:
            if where_condition != "":
                where_condition = f"{where_condition} AND status = $2"
            else:
                where_condition = "status = $1"
            values.append(status.value)

        if where_condition != "":
            where_condition = f"WHERE {where_condition}"

        sql = await get_basic_select_where_statement("v1_container_images", where_condition)

        try:
            return await self.conn.fetch(sql, *values)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to load container images.")

    async def delete_container_image(self, container_image_id: UUID4, creator_id: str = None) -> ContainerImage:
        # TODO: Check for creator_id ?
        await self._container_image_is_referenced(container_image_id=container_image_id)
        raw_container_image = await self._delete_container_image(container_image_id=container_image_id)
        return await self._build_container_image_model(raw_container_image=raw_container_image)

    # private

    async def _build_closed_app(
        self, app_record, registry_image_url=None, app_ui_id=None, app_ui_url=None, app_ui_config=None
    ) -> ClosedApp:
        if registry_image_url is None and "registry_image_url" in app_record:
            registry_image_url = app_record["registry_image_url"]

        has_frontend = False
        if app_ui_id or app_record["app_ui_id"]:
            has_frontend = True

            if app_ui_url is None and "app_ui_url" in app_record:
                app_ui_url = app_record["app_ui_url"]

            if app_ui_config is None and "app_ui_config" in app_record:
                app_ui_config = app_record["app_ui_config"]

        if not app_ui_config:
            app_ui_config = AppUiConfiguration()

        return ClosedApp(
            id=app_record["id"],
            portal_app_id=app_record["portal_app_id"],
            version=app_record["version"],
            ead=app_record["ead"],
            registry_image_url=registry_image_url,
            app_ui_url=app_ui_url,
            app_ui_configuration=app_ui_config,
            app_documentation_url=app_record["app_documentation_url"],
            has_frontend=has_frontend,
            status=AppStatus(app_record["status"]),
            creator_id=app_record["creator_id"],
            created_at=app_record["created_at"],
            updated_at=app_record["updated_at"],
        )

    async def _build_closed_portal_app(self, minio_client: MinioClientWrapper, portal_app_record) -> ClosedPortalApp:
        raw_portal_app = dict(portal_app_record["portal_app"]).copy()
        del raw_portal_app["v1_active_view_id"]
        del raw_portal_app["v2_active_view_id"]
        del raw_portal_app["v3_active_view_id"]

        closed_portal_app = ClosedPortalApp.model_validate(raw_portal_app)
        organization_id = closed_portal_app.organization_id

        v1_app = (
            await self._build_closed_app_view(minio_client, portal_app_record["v1_active_view"], organization_id)
            if portal_app_record["v1_active_view"] != []
            else None
        )
        v2_app = (
            await self._build_closed_app_view(minio_client, portal_app_record["v2_active_view"], organization_id)
            if portal_app_record["v2_active_view"] != []
            else None
        )
        v3_app = (
            await self._build_closed_app_view(minio_client, portal_app_record["v3_active_view"], organization_id)
            if portal_app_record["v3_active_view"] != []
            else None
        )
        closed_portal_app.active_app_views = ClosedActiveAppViews(v1=v1_app, v2=v2_app, v3=v3_app)

        return closed_portal_app

    async def _build_public_app_view(self, minio_client: MinioClientWrapper, app_view_record) -> PublicAppView:
        details = await self._build_app_view_details(app_view_record)
        media = await self._get_portal_app_media(minio_client, app_view_record["portal_app_view"]["id"])

        tags = translate_tags(app_view_record["portal_app_view"]["tags"])

        return PublicAppView(
            version=app_view_record["portal_app_view_app"]["version"],
            details=details,
            media=media,
            tags=tags,
            non_functional=app_view_record["portal_app_view"]["non_functional"],
            research_only=app_view_record["portal_app_view"]["research_only"],
            created_at=app_view_record["portal_app_view"]["created_at"],
            reviewed_at=app_view_record["portal_app_view"]["reviewed_at"],
        )

    async def _build_closed_app_view(
        self, minio_client: MinioClientWrapper, app_view_record: dict, organization_id: str
    ) -> ClosedAppView:
        if app_view_record["portal_app_view"]["id"] is None:
            return None

        closed_app = None
        if "portal_app_view_app" in app_view_record:
            if app_view_record["portal_app_view_app"]["id"] is not None:
                closed_app = await self._build_closed_app(
                    app_view_record["portal_app_view_app"],
                    app_view_record["container_image"]["registry_image_url"],
                    app_view_record["app_ui"]["id"],
                    app_view_record["app_ui"]["app_ui_url"],
                    app_view_record["app_ui"]["app_ui_config"],
                )
        details = await self._build_app_view_details(app_view_record)
        media = await self._get_portal_app_media(minio_client, app_view_record["portal_app_view"]["id"])

        tags = translate_tags(app_view_record["portal_app_view"]["tags"])

        portal_app_id = app_view_record["portal_app_view"]["portal_app_id"]
        api_version = app_view_record["portal_app_view"]["api_version"]

        return ClosedAppView(
            id=app_view_record["portal_app_view"]["id"],
            portal_app_id=portal_app_id,
            organization_id=organization_id,
            version=app_view_record["portal_app_view_app"]["version"],
            details=details,
            media=media,
            app=closed_app,
            tags=tags,
            status=AppStatus(app_view_record["portal_app_view"]["status"]),
            creator_id=app_view_record["portal_app_view"]["creator_id"],
            created_at=app_view_record["portal_app_view"]["created_at"],
            review_comment=app_view_record["portal_app_view"]["review_comment"],
            reviewer_id=app_view_record["portal_app_view"]["reviewer_id"],
            reviewed_at=app_view_record["portal_app_view"]["reviewed_at"],
            non_functional=app_view_record["portal_app_view"]["non_functional"],
            research_only=app_view_record["portal_app_view"]["research_only"],
            api_version=ApiVersion(api_version),
        )

    async def _build_app_view_details(self, app_view_record) -> AppDetails:
        details = None
        if "portal_app_view_details" in app_view_record:
            detail_record = app_view_record["portal_app_view_details"]
            if detail_record["id"] is not None:
                details = AppDetails(
                    name=detail_record["name"],
                    description=translate_dict(detail_record["description"]),
                    marketplace_url=detail_record["marketplace_url"],
                )
        return details

    async def _build_closed_portal_app_list(
        self,
        minio_client: MinioClientWrapper,
        app_records,
        query: PortalAppQuery,
        licensed_portal_app_ids: List[str],
        skip: int = None,
        limit: int = None,
    ) -> ClosedPortalAppList:
        if app_records["item_count"] == 0:
            return app_records

        portal_apps = []
        for app_record in app_records["items"]:
            is_relevant = await self._check_portal_app_filter_criteria(app_record, query)
            if is_relevant and app_record["portal_app"]["id"] in licensed_portal_app_ids:
                closed_portal_app = await self._build_closed_portal_app(minio_client, app_record)
                portal_apps.append(closed_portal_app)

        item_count, relevant_portal_apps = self._slice_portal_app_list(portal_apps, skip, limit)

        return ClosedPortalAppList(item_count=item_count, items=relevant_portal_apps)

    async def _build_public_portal_app_list(
        self, minio_client: MinioClientWrapper, app_records, query: PortalAppQuery, skip: int = None, limit: int = None
    ) -> PublicPortalAppList:
        if app_records["item_count"] == 0:
            return app_records

        portal_apps = []
        for app_record in app_records["items"]:
            is_relevant = await self._check_portal_app_filter_criteria(app_record, query)
            if is_relevant:
                public_app = await self._build_public_portal_app(minio_client, app_record)
                portal_apps.append(public_app)

        item_count, relevant_portal_apps = self._slice_portal_app_list(portal_apps, skip, limit)

        return PublicPortalAppList(item_count=item_count, items=relevant_portal_apps)

    async def _check_portal_app_filter_criteria(self, app_record, query: BaseQuery) -> bool:
        if query is None:
            return True

        if isinstance(query, PortalAppQuery):
            active_app_view = await self._get_active_app_view(app_record, query.active_app_version)
            if active_app_view is None:
                return False

            tags = active_app_view["portal_app_view"]["tags"]

            if query.tissues:
                if "TISSUE" not in tags or not bool(set(query.tissues).intersection(tags["TISSUE"])):
                    return False
            if query.stains:
                if "STAIN" not in tags or not bool(set(query.stains).intersection(tags["STAIN"])):
                    return False
            if query.analysis:
                if "ANALYSIS" not in tags or not bool(set(query.analysis).intersection(tags["ANALYSIS"])):
                    return False
            if query.indications:
                if "INDICATION" not in tags or not bool(set(query.indications).intersection(tags["INDICATION"])):
                    return False
            if query.clearances:
                if "CLEARANCE" not in tags or not bool(set(query.clearances).intersection(tags["CLEARANCE"])):
                    return False

        if isinstance(query, CustomerPortalAppQuery):
            if query.apps:
                app_list = await self._get_active_app_ids_from_portal_app(app_record)
                if not bool(set(query.apps).intersection(app_list)):
                    return False

            active_app_view = await self._get_active_app_view(app_record, None)
            if active_app_view is None:
                return False

            tags = active_app_view["portal_app_view"]["tags"]

            if query.tissues:
                if "TISSUE" not in tags or not bool(set(query.tissues).intersection(tags["TISSUE"])):
                    return False
            if query.stains:
                if "STAIN" not in tags or not bool(set(query.stains).intersection(tags["STAIN"])):
                    return False

        return True

    async def _get_active_app_ids_from_portal_app(self, portal_app) -> list:
        id_list = []
        for version in ApiVersion:
            active_app_field = f"{version.value}_active_view"
            if portal_app[active_app_field] != [] and "portal_app_view_app" in portal_app[active_app_field]:
                id_list.append(portal_app[active_app_field]["portal_app_view_app"]["id"])
        return id_list

    async def _get_active_app_view(self, portal_app, api_version: ApiVersion) -> dict:
        if api_version:
            version = f"{api_version.value}_active_view"
            if version in portal_app:
                return None if portal_app[version] == [] else portal_app[version]
            return None
        if portal_app["v3_active_view"] != []:
            return portal_app["v3_active_view"]
        if portal_app["v2_active_view"] != []:
            return portal_app["v2_active_view"]
        if portal_app["v1_active_view"] != []:
            return portal_app["v1_active_view"]
        return None

    def _slice_portal_app_list(self, generic_list: list, skip: int = None, limit: int = None):
        if skip is None and limit is None:
            return len(generic_list), generic_list
        if skip is not None:
            if skip < 0:
                raise HTTPException(status_code=422, detail="Skip may not be negative")
            if skip > len(generic_list):
                return len(generic_list), []
        else:
            skip = 0
        if limit is not None:
            if limit < 0:
                raise HTTPException(status_code=422, detail="Limit may not be negative")
            if limit > len(generic_list):
                limit = len(generic_list)
        else:
            limit = len(generic_list)
        return len(generic_list), generic_list[skip:limit]

    async def _build_public_portal_app(self, minio_client: MinioClientWrapper, app_record) -> PublicPortalApp:
        public_app = PublicPortalApp(
            id=app_record["portal_app"]["id"],
            organization_id=app_record["portal_app"]["organization_id"],
            status=app_record["portal_app"]["status"],
            creator_id=app_record["portal_app"]["creator_id"],
            created_at=app_record["portal_app"]["created_at"],
            updated_at=app_record["portal_app"]["updated_at"],
            active_app_views=PublicActiveAppViews(),
        )

        if app_record["v1_active_view"] != []:
            public_app.active_app_views.v1 = await self._build_public_app_view(
                minio_client, app_record["v1_active_view"]
            )
        if app_record["v2_active_view"] != []:
            public_app.active_app_views.v2 = await self._build_public_app_view(
                minio_client, app_record["v2_active_view"]
            )
        if app_record["v3_active_view"] != []:
            public_app.active_app_views.v3 = await self._build_public_app_view(
                minio_client, app_record["v3_active_view"]
            )

        return public_app

    async def _build_container_image_model(self, raw_container_image: dict) -> ContainerImage:
        error_message = None
        if "error_message" in raw_container_image:
            error_message = raw_container_image["error_message"]

        return ContainerImage(
            id=str(raw_container_image["id"]),
            name=raw_container_image["name"],
            status=ContainerImageStatus(raw_container_image["status"]),
            registry_image_url=raw_container_image["registry_image_url"],
            creator_id=raw_container_image["creator_id"],
            created_at=raw_container_image["created_at"],
            updated_at=raw_container_image["updated_at"],
            error_message=error_message,
            connected_apps=[],  # TODO
        )

    async def _build_internal_container_image_model(self, raw_container_image: dict) -> InternalContainerImage:
        error_message = None
        if "error_message" in raw_container_image:
            error_message = raw_container_image["error_message"]

        return InternalContainerImage(
            id=str(raw_container_image["id"]),
            name=raw_container_image["name"],
            status=ContainerImageStatus(raw_container_image["status"]),
            registry_image_url=raw_container_image["registry_image_url"],
            temp_directory=raw_container_image["temp_directory"],
            organization_id=raw_container_image["organization_id"],
            creator_id=raw_container_image["creator_id"],
            error_message=error_message,
        )

    async def _build_app_ui_bundle_model(self, raw_app_ui_bundle: dict) -> AppUI:
        app_ui_conf = None
        if raw_app_ui_bundle is None:
            return app_ui_conf
        if raw_app_ui_bundle["app_ui_config"]:
            app_ui_conf = AppUiConfiguration(**raw_app_ui_bundle["app_ui_config"])

        return AppUI(
            id=str(raw_app_ui_bundle["id"]),
            name=raw_app_ui_bundle["name"],
            status=AppUIStatus(raw_app_ui_bundle["status"]),
            app_ui_url=raw_app_ui_bundle["app_ui_url"],
            app_ui_configuration=app_ui_conf,
            creator_id=raw_app_ui_bundle["creator_id"],
            created_at=raw_app_ui_bundle["created_at"],
            updated_at=raw_app_ui_bundle["updated_at"],
            connected_apps=[],  # TODO: Get connected apps
        )

    async def _build_related_portal_apps_query(self, active_app_views: ClosedActiveAppViews) -> PortalAppQuery:
        tag_dict = {}
        for version in ApiVersion:
            app_view = getattr(active_app_views, version.value)
            if app_view and app_view.tags:
                for tag in ["tissues", "stains", "indications", "analysis"]:
                    tag_group = getattr(app_view.tags, tag)
                    if tag_group:
                        if tag in tag_dict:
                            tag_dict[tag].extend(tag_group)
                        else:
                            tag_dict[tag] = tag_group

        return PortalAppQuery(
            tissues=[t.name for t in tag_dict["tissues"]] if "tissues" in tag_dict else None,
            stains=[t.name for t in tag_dict["stains"]] if "stains" in tag_dict else None,
            indications=[t.name for t in tag_dict["indications"]] if "indications" in tag_dict else None,
            analysis=[t.name for t in tag_dict["analysis"]] if "analysis" in tag_dict else None,
        )

    async def _update_status_app_view(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        active_app_version: ApiVersion,
        app_id: UUID4 = None,
        status: AppStatus = None,
    ) -> ClosedAppView:
        sql_update_app_status = await get_basic_update_statement("v1_apps", "status", "id")
        sql_update_app_view_status = await get_basic_update_statement("v1_portal_apps_view", "status", "id")

        active_app_field = f"{active_app_version.value}_active_view_id"
        sql_update_portal_app_status = await get_basic_update_statement("v1_portal_apps", active_app_field, "id")
        try:
            if app_id:
                _ = await self.conn.fetchrow(sql_update_app_status, AppStatus.PENDING, app_id)
                _ = await self.conn.fetchrow(sql_update_app_view_status, AppStatus.PENDING, app_view_id)
            if status:
                _ = await self.conn.fetchrow(sql_update_app_view_status, status, app_view_id)
                if status == AppStatus.APPROVED:
                    _ = await self.conn.fetchrow(sql_update_portal_app_status, app_view_id, portal_app_id)
            return await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update app view status for ID {app_view_id}")

    async def _update_app_view_review(self, app_view_id: UUID4, review_comment: str, user_id: str):
        sql_update_portal_app_status_review = await get_update_portal_app_review_statement()
        try:
            _ = await self.conn.fetchrow(sql_update_portal_app_status_review, review_comment, user_id, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update review for app view with ID {app_view_id}")

    async def _update_status_portal_app(
        self,
        portal_app_id: UUID4,
        v1_app_view_id: UUID4,
        v2_app_view_id: UUID4,
        v3_app_view_id: UUID4,
        status: ListingStatus,
    ) -> ClosedPortalApp:
        sql_update_portal_app_active_app_view = await get_update_portal_app_status_statement()
        try:
            _ = await self.conn.fetchrow(
                sql_update_portal_app_active_app_view,
                status,
                v1_app_view_id,
                v2_app_view_id,
                v3_app_view_id,
                portal_app_id,
            )
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update portal app status for ID {portal_app_id}")

    async def _update_status_app(self, app_id: UUID4, status: AppStatus) -> ClosedApp:
        sql_update_app_status = await get_basic_update_statement("v1_apps", "status", "id")

        try:
            _ = await self.conn.fetchrow(sql_update_app_status, status, app_id)
            return await self.get_app_by_id(app_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update app status for ID {app_id}")

    async def _query_raw_portal_apps(
        self, organization_id: str, query: BaseQuery, statuses: List[ListingStatus] = None
    ) -> dict:
        args = []
        if organization_id is not None:
            args.append(organization_id)
        if statuses is not None:
            str_states = [s.value for s in statuses]
            args.append(str_states)

        active_app_version = None
        query_dict = query.model_dump()
        if "active_app_version" in query_dict:
            active_app_version = query_dict["active_app_version"]

        sql_query_portal_app = await get_query_plain_portal_app_statement(
            filter_by_orga=(organization_id is not None),
            filter_by_status=(statuses is not None),
            active_app_version=active_app_version,
        )

        try:
            portal_app_records = await self.conn.fetch(sql_query_portal_app, *args)
            if len(portal_app_records) < 1:
                return {"item_count": 0, "items": []}

            return {"item_count": portal_app_records[0]["full_count"], "items": portal_app_records}
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query portal apps")

    async def _get_portal_app_media(self, minio_client: MinioClientWrapper, app_view_id: str) -> dict:
        sql_select_media = await get_basic_select_where_statement(
            "v1_portal_apps_media",
            "WHERE id IN (SELECT media_id FROM public.v1_portal_apps_media_ref WHERE portal_app_view_id = $1)",
        )

        try:
            media_data = await self.conn.fetch(sql_select_media, app_view_id)
            if media_data:
                return await self._parse_media_data(minio_client, media_data)
            else:
                return None
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to retrieve portal app media")

    async def _parse_media_data(self, minio_client: MinioClientWrapper, media_data) -> MediaList:
        result = MediaList()
        for media in media_data:
            media_purpose = media["media_purpose"]
            presigned_media_url = (
                get_presigned_url(minio_client, media["minio_media_id"]) if minio_client.is_reachable else None
            )
            resized_presigned_image_urls = None
            if media_purpose in ["peek", "banner", "workflow"]:
                resized_presigned_image_urls = (
                    get_resized_images(minio_client, media["minio_media_id"]) if minio_client.is_reachable else None
                )
            media_obj = MediaObject(
                id=media["id"],
                index=media["index"],
                caption=translate_dict(media["caption"]),
                internal_path=media["internal_path"],
                alt_text=translate_dict(media["alt_text"]),
                content_type=media["content_type"],
                presigned_media_url=presigned_media_url,
                resized_presigned_media_urls=resized_presigned_image_urls,
            )
            getattr(result, media_purpose).append(media_obj)
        # make sure lists are sorted ascending
        result.peek.sort(key=lambda x: x.index, reverse=False)
        result.banner.sort(key=lambda x: x.index, reverse=False)
        result.workflow.sort(key=lambda x: x.index, reverse=False)
        return result

    async def _validate_app_view(
        self,
        minio_client: MinioClientWrapper,
        portal_app_id: UUID4,
        app_view_id: UUID4,
        expected_status: AppStatus = None,
    ) -> ClosedAppView:
        app_view = await self.get_closed_app_view(minio_client, portal_app_id, app_view_id)
        if app_view is None:
            raise HTTPException(status_code=404, detail="App view does not exist.")

        missing_data = []
        if app_view.details is None:
            missing_data.append("details")
        if app_view.app is None:
            missing_data.append("app data")
        if app_view.media is None:
            # TODO: check if minimal media is available
            missing_data.append("media")

        if app_view.non_functional:
            missing_data.remove("app data")
        if len(missing_data) > 0:
            raise HTTPException(
                status_code=400, detail=f"Incomplete portal app data (missing {', '.join(missing_data)})."
            )
        if expected_status is not None:
            if expected_status != app_view.status:
                status = f"(expected status: {expected_status}, actual status: {app_view.status})."
                raise HTTPException(status_code=400, detail=f"Can not change portal app status {status}.")
        return app_view

    async def _get_active_app_version(self, app_view: ClosedAppView) -> ApiVersion:
        if app_view.non_functional:
            return ApiVersion.V3  # Assume non functional app will be avaliable for V3 API
        schema = app_view.app.ead["$schema"]
        if schema.endswith(EAD_SCHEMA_V3):
            return ApiVersion.V3
        if schema.endswith(EAD_SCHEMA_DRAFT3) or schema.endswith(EAD_SCHEMA_DRAFT3_ALT):
            if app_view.app.app_ui_url is not None:
                return ApiVersion.V2
            else:
                return ApiVersion.V1
        raise HTTPException(status_code=400, detail="Could not determine active app version for EAD schema")

    async def _validate_active_post_app(self, post_app: PostAdminApp, version: ApiVersion):
        if version == ApiVersion.V3:
            _ = await self._validate_ead(post_app.ead, enable_legacy_support=False)
        if version == ApiVersion.V2:
            # V2 can be added by admin route without providing an app ui url as an ui bundle can be uploaded manually
            _ = await self._validate_ead(post_app.ead, enable_legacy_support=True)
        if version == ApiVersion.V1:
            _ = await self._validate_ead(post_app.ead, enable_legacy_support=True)
            if post_app.app_ui_url is not None:
                raise HTTPException(status_code=400, detail="Active app V1 must provide an App UI")

    async def _validate_ead(self, ead: dict, enable_legacy_support: bool = False) -> str:
        try:
            if enable_legacy_support:
                ead_validator_legacy.validate(ead)
                version = ead_validator_legacy.get_namespace_version(ead)
            else:
                ead_validator.validate(ead)
                version = ead_validator.get_namespace_version(ead)
            return version
        except EadSchemaValidationError as e:
            error = "Validation of EAD failed: EAD does not match schema."
            logger.debug(f"EadSchemaValidationError: {e}")
            raise HTTPException(status_code=422, detail=error) from e
        except EadValidationError as e:
            error = f"Validation of EAD failed: {e}"
            logger.debug(f"EadValidationError: {e}")
            raise HTTPException(status_code=422, detail=error) from e

    async def _get_ead_namespace_version(self, ead_namespace: str):
        partial_namespace = ead_namespace.split(".")

        if len(partial_namespace) == 5:
            return partial_namespace[-1]
        if len(partial_namespace) == 6:
            return f"{partial_namespace[-2]}.{partial_namespace[-1]}"

        # In case EAD validation failed somehow
        raise HTTPException(
            status_code=422,
            detail="Malformed EAD namespace (expected format: 'org.namespace.vendor_name.vendor_app.version')",
        )

    async def _ead_namespace_exists(self, ead_namespace: str):
        sql = await get_basic_select_where_statement("v1_apps", "WHERE ead->>'namespace' = $1")

        try:
            app_record = await self.conn.fetchrow(sql, ead_namespace)
            if app_record is not None and len(app_record) > 0:
                raise HTTPException(status_code=400, detail=f"EAD with namespace '{ead_namespace}' already exists.")
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query apps")

    async def _validate_media(
        self,
        app_views: ClosedAppView,
        media_purpose: AppMediaPurpose,
        index: int,
        media_file: UploadFile,
    ):
        media_to_update = None
        # TODO: meaningful validation
        if media_purpose == AppMediaPurpose.MANUAL:
            if media_file.content_type not in SUPPORTED_MANUAL_CONTENT_TYPES:
                contents = "; ".join(SUPPORTED_MANUAL_CONTENT_TYPES)
                raise HTTPException(
                    status_code=415, detail=f"Media of type 'MANUAL' must be of content-type: {contents}"
                )
        else:
            if media_file.content_type not in SUPPORTED_IMAGE_CONTENT_TYPES:
                contents = "; ".join(SUPPORTED_IMAGE_CONTENT_TYPES)
                raise HTTPException(
                    status_code=415,
                    detail=f"Media of type 'PEEK', 'BANNER' and 'WORKFLOW' must be of content-type: {contents}",
                )
            if media_purpose == AppMediaPurpose.BANNER or media_purpose == AppMediaPurpose.PEEK:
                if index not in range(0, 2):
                    raise HTTPException(status_code=400, detail="Invalid index for media banner.")

        # check if we need to update media
        if app_views.media and media_purpose in app_views.media:
            for media_obj in app_views.media[media_purpose]:
                if media_obj.index == index:
                    media_to_update = media_obj

        return media_to_update

    async def _app_exists(self, app_id: str):
        sql = await get_basic_select_where_statement("v1_apps", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, app_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed querying app for ID {app_id}")

        if row is None:
            raise HTTPException(status_code=404, detail=f"No valid app for ID {app_id}.")

        return row

    async def container_image_exists(self, container_image_id: str):
        sql = await get_basic_select_where_statement("v1_container_images", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, container_image_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed querying container image for ID {container_image_id}")

        if row is None:
            raise HTTPException(status_code=404, detail=f"No valid container image for ID {container_image_id}.")

        if row["status"] in [
            ContainerImageStatus.UPLOAD_FINISHED,
            ContainerImageStatus.PUSH_INITIATED,
            ContainerImageStatus.PUSH_FINISHED,
        ]:
            raise HTTPException(status_code=404, detail=f"Container image already uploaded for ID {container_image_id}")

        return row

    async def set_container_image_status(
        self,
        container_image_id: str,
        status: ContainerImageStatus,
        temp_directory: str = None,
        registry_image_url: str = None,
        error_message: str = None,
    ):
        sql = await get_update_container_image_status_statement(
            update_directory=(temp_directory is not None),
            update_registry_url=(registry_image_url is not None),
            update_error_message=(error_message is not None),
        )

        values = [status]
        if temp_directory is not None:
            values.append(temp_directory)
        if registry_image_url is not None:
            values.append(registry_image_url)
        if error_message is not None:
            values.append(error_message)
        values.append(container_image_id)

        # TODO: check status update constraints?

        try:
            row = await self.conn.fetchrow(sql, *values)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update container image status for ID {container_image_id}")

        return row

    async def set_container_image_registry_image_url(self, container_image_id: str, registry_image_url: str):
        sql = await get_basic_update_where_statement("v1_container_images", "registry_image_url", "WHERE id = $2")

        try:
            row = await self.conn.fetchrow(sql, registry_image_url, container_image_id)
        except PostgresError as e:
            log_and_raise_postgres_error(
                e, f"Failed to update registry url for container image ID {container_image_id}"
            )

        return row

    async def app_ui_bundle_exists(self, app_ui_bundle_id: str):
        sql = await get_basic_select_where_statement("v1_app_ui_bundles", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, app_ui_bundle_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed querying container image for ID {app_ui_bundle_id}")

        if row is None:
            raise HTTPException(status_code=404, detail=f"No valid app UI bundle for ID {app_ui_bundle_id}.")

        if row["status"] in [AppUIStatus.UPLOAD_FINISHED, AppUIStatus.UNZIP_INITIATED, AppUIStatus.UNZIP_FINISHED]:
            raise HTTPException(status_code=404, detail=f"App UI bundle already uploaded for ID {app_ui_bundle_id}")

        return row

    async def set_app_ui_bundle_status(self, app_ui_bundle_id: str, status: AppUIStatus):
        sql = await get_basic_update_where_statement("v1_container_images", "status", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, status, app_ui_bundle_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to update app UI bundle status for ID {app_ui_bundle_id}")

        return row

    async def validate_app_in_portal_app(self, portal_app_id: UUID4, app_id: UUID4):
        portal_app_raw = await portal_app_exists(conn=self.conn, portal_app_id=portal_app_id)
        app_raw = await self._app_exists(app_id=app_id)

        if app_raw["portal_app_id"] is None or app_raw["portal_app_id"] != portal_app_raw["id"]:
            raise HTTPException(
                status_code=404, detail=f"App ID {app_id} not related to portal app ID {portal_app_id}."
            )

    async def _draft_app_view_exists(self, portal_app_id: UUID4, app_view_id: str = None):
        sql = await get_basic_select_where_statement(
            "v1_portal_apps_view", "WHERE portal_app_id = $1 AND status = 'DRAFT'"
        )

        try:
            row = await self.conn.fetchrow(sql, portal_app_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to retrieve app view in DRAFT state")

        if row is not None:
            if app_view_id is None:
                raise HTTPException(
                    status_code=400, detail=f"App view in 'DRAFT' state already exists. [ID: {row['id']}]."
                )
            elif app_view_id != row["id"]:
                raise HTTPException(status_code=400, detail="app_view_id is not valid for given portal_app_id.")

        return row

    async def _app_in_state(self, portal_app_id: UUID4, app_view_id: str, expected_status: AppStatus):
        closed_app = await self.get_app(portal_app_id, app_view_id)

        if closed_app.status != expected_status:
            raise HTTPException(
                status_code=400,
                detail=f"App status needs to be '{expected_status.value}' (Actual: '{closed_app.status.value}').",
            )

    async def _app_view_in_state(self, portal_app_id: UUID4, app_view_id: str, status: AppStatus):
        sql = await get_basic_select_where_statement("v1_portal_apps_view", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, app_view_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to select app view")

        if row is None:
            raise HTTPException(status_code=404, detail=f"No valid app view for ID {app_view_id}")
        else:
            if row["portal_app_id"] != portal_app_id:
                raise HTTPException(status_code=400, detail="Invalid portal app ID for app view.")
            if row["status"] != status:
                raise HTTPException(
                    status_code=400, detail=f"App view not in '{status}' state and can not be modified."
                )

        return row

    async def _container_image_is_referenced(self, container_image_id: UUID4):
        sql = await get_basic_select_where_statement("v1_apps", "WHERE container_image_id = $1")

        try:
            app_rows = await self.conn.fetch(sql, str(container_image_id))
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to select app by container image ID.")

        if app_rows is not None and len(app_rows) > 0:
            raise HTTPException(status_code=400, detail="Container image is referenced by app and cannot be deleted.")

    async def _app_ui_is_referenced(self, app_ui_id: UUID4):
        sql = await get_basic_select_where_statement("v1_apps", "WHERE app_ui_id = $1")

        try:
            app_rows = await self.conn.fetch(sql, str(app_ui_id))
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to select app by app ui ID..")

        if app_rows is not None and len(app_rows) > 0:
            raise HTTPException(status_code=400, detail="App UI is referenced by app and cannot be deleted.")

    async def _delete_container_image(self, container_image_id: UUID4):
        sql = await get_basic_delete_where_statement("v1_container_images", "WHERE id = $1")

        try:
            container_image = await self.conn.fetchrow(sql, str(container_image_id))
            return container_image
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to delete container image.")

    async def _delete_app_ui(self, app_ui_id: UUID4):
        sql = await get_basic_delete_where_statement("v1_app_ui_bundles", "WHERE id = $1")

        try:
            app_ui_bundles = await self.conn.fetchrow(sql, str(app_ui_id))
            return app_ui_bundles
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to delete app UI.")
