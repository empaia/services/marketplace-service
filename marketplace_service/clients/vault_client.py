from http import HTTPStatus

from aiohttp import ClientSession
from fastapi import HTTPException
from pydantic import ValidationError

from marketplace_service.models.marketplace.app import (
    AppConfiguration,
    AppConfigurationType,
    ClosedApp,
    PostAppConfiguration,
)

from ..singletons import logger, settings, vault_client_token


async def post_app_configuration(
    app: ClosedApp,
    customer_id: str,
    config_type: AppConfigurationType,
    configuration: PostAppConfiguration,
    http_client: ClientSession,
) -> AppConfiguration:
    app_id = str(app.id)
    app_namespace = app.ead["namespace"]
    # TODO: Validation requires full (global and customer) config at the moment
    # ead_validator.validate_with_config(app.ead, configuration.content)
    base_vault_url = f"{settings.vault_url}/v1/{settings.vault_secret_engine}/data/{app_namespace}"
    payload = {"data": configuration.content}

    if config_type == AppConfigurationType.GLOBAL:
        _ = await wrapped_post_request(http_client, f"{base_vault_url}/{config_type.value}", payload)

    if config_type == AppConfigurationType.CUSTOMER:
        if customer_id:
            _ = await wrapped_post_request(http_client, f"{base_vault_url}/{config_type.value}/{customer_id}", payload)
        else:
            raise HTTPException(
                status_code=400, detail="Attempt to post customer configuration but customer_id is missing"
            )

    return await get_app_config(
        app_id=app_id, app_namespace=app_namespace, http_client=http_client, customer_id=customer_id
    )


async def get_app_config(
    app_id: str, app_namespace: str, http_client: ClientSession, customer_id: str
) -> AppConfiguration:
    base_vault_url = f"{settings.vault_url}/v1/{settings.vault_secret_engine}/data/{app_namespace}"
    # get global app configuration from vault
    r_global = await wrapped_get_request(
        http_client,
        f"{base_vault_url}/{AppConfigurationType.GLOBAL.value}",
    )
    global_config = {}
    if r_global.status == HTTPStatus.OK:
        global_config = await validate_conf(r_global)

    # get customer app configuration from vault
    customer_config = {}
    if customer_id:
        r_customer = await wrapped_get_request(
            http_client,
            f"{base_vault_url}/{AppConfigurationType.CUSTOMER.value}/{customer_id}",
        )
        if r_customer.status == HTTPStatus.OK:
            customer_config = await validate_conf(r_customer)

    return await parse_response(app_id, global_config, customer_config)


async def validate_conf(resp):
    response = await resp.json()
    config = {}
    if "data" in response and "data" in response["data"]:
        config = dict(response["data"]["data"])

    return config


async def parse_response(app_id: str, global_config: dict, customer_config: dict):
    try:
        raw_config = {"app_id": app_id, "global": global_config, "customer": customer_config}
        return AppConfiguration(**raw_config)
    except ValidationError as err:
        logger.error(f"App configuration validation failed [{err}]")
        raise HTTPException(status_code=500, detail="Failed to validate configuration of vault data.") from err


async def wrapped_get_request(http_client: ClientSession, url: str):
    if vault_client_token.get_token() is None:
        await login_client(http_client)

    r = await http_client.get(url, headers={"X-Vault-Token": vault_client_token.get_token(), "X-Vault-Request": "true"})

    if r.status == HTTPStatus.FORBIDDEN:
        await login_client(http_client)
        r = await http_client.get(
            url, headers={"X-Vault-Token": vault_client_token.get_token(), "X-Vault-Request": "true"}
        )

    if r.status in [HTTPStatus.OK, HTTPStatus.NOT_FOUND]:
        return r

    resp = await r.json()
    logger.error(f"Failed to read from vault: {resp}")
    raise HTTPException(status_code=500, detail="Failed to read from vault.")


async def wrapped_post_request(http_client: ClientSession, url: str, payload):
    if vault_client_token.get_token() is None:
        await login_client(http_client)

    r = await http_client.post(
        url, json=payload, headers={"X-Vault-Token": vault_client_token.get_token(), "X-Vault-Request": "true"}
    )

    if r.status == HTTPStatus.FORBIDDEN:
        await login_client(http_client)
        r = await http_client.post(
            url, json=payload, headers={"X-Vault-Token": vault_client_token.get_token(), "X-Vault-Request": "true"}
        )

    if r.status == HTTPStatus.OK:
        resp = await r.json()
        if "errors" in resp:
            raise HTTPException(status_code=400, detail=resp["errors"])
        return r
    else:
        resp = await r.json()
        logger.debug(f"Failed to write to vault: {resp}")
        raise HTTPException(status_code=400, detail="Failed to write to vault.")


async def login_client(http_client: ClientSession):
    data = {"role_id": settings.vault_role_id, "secret_id": settings.vault_secret_id}
    r = await http_client.post(f"{settings.vault_url}/v1/auth/approle/login", json=data)
    resp = await r.json()

    if r.status != HTTPStatus.OK:
        logger.debug(f"Login to vault failed: {resp}")
        raise HTTPException(status_code=400, detail="Login to vault failed.")

    vault_client_token.set_token(resp["auth"]["client_token"])
