from asyncpg import PostgresError, UniqueViolationError
from pydantic import UUID4

from marketplace_service.clients.helpers.statistics_sql_helper import get_insert_statistics, get_select_statistics
from marketplace_service.clients.helpers.utils import log_and_raise_postgres_error, log_postgres_error
from marketplace_service.models.marketplace.app import StatisticsList


class StatisticsClient:
    def __init__(self, conn):
        self.conn = conn

    async def register(self, statistics_id: UUID4, page: str):
        sql_insert_statistics = await get_insert_statistics()
        values = [statistics_id, page]
        try:
            await self.conn.fetchrow(sql_insert_statistics, *values)
        except UniqueViolationError:
            pass
        except PostgresError as e:
            log_postgres_error(e)

    async def get_statistics(self):
        sql_select_statistics = await get_select_statistics()
        try:
            rows = await self.conn.fetchrow(sql_select_statistics)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Statistics could not be retrieved")

        return StatisticsList(item_count=len(rows["agg_statistics"]), items=rows["agg_statistics"])
