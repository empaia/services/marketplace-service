from typing import List

from asyncpg import ForeignKeyViolationError, PostgresError
from fastapi import HTTPException
from pydantic import UUID4

from marketplace_service.clients.helpers.clearance_sql_helper import (
    build_query_where_clause,
    get_insert_clearance_item_statement,
    get_insert_multiple_clearances_statement,
    get_query_aggregated_clearance_statement,
    get_select_clearance_items_statement,
    get_select_clearances_statement,
    get_update_clearance_where_statement,
)
from marketplace_service.clients.helpers.general_sql_helper import (
    get_basic_select_where_statement,
    get_basic_update_statement,
)
from marketplace_service.clients.helpers.utils import (
    log_and_raise_postgres_error,
    translate_extended_tags,
    validate_multi_language_dict,
    validate_tags,
)
from marketplace_service.custom_models.v1.clearances import (
    Clearance,
    ClearanceItem,
    ClearanceQuery,
    DatePrecision,
    EditorType,
    ExtendedPostClearance,
    ExtendedPostClearanceItem,
    InfoProvider,
    InfoSourceContent,
    InfoSourceType,
    PostInfoSourceContent,
    PublicAggregatedClearance,
    PublicAggregatedClearanceList,
)
from marketplace_service.utils import translate_dict


class ClearanceClient:
    def __init__(self, conn):
        self.conn = conn

    async def post_clearance(
        self, post_clearance: ExtendedPostClearance, user_id: str, creator_type: EditorType
    ) -> Clearance:
        clearance: Clearance = await self.post_empty_clearance(
            post_clearance=post_clearance, user_id=user_id, creator_type=creator_type
        )
        if post_clearance.clearance_item is not None:
            clearance_item = await self.post_clearance_item(
                clearance_id=clearance.id,
                post_clearance_item=post_clearance.clearance_item,
                user_id=user_id,
                creator_type=creator_type,
            )
            clearance.active_clearance_item = clearance_item
            clearance.history = [clearance_item]

        return clearance

    async def post_empty_clearance(
        self, post_clearance: ExtendedPostClearance, user_id: str, creator_type: EditorType
    ) -> Clearance:
        sql_insert_clearance = await get_insert_multiple_clearances_statement()

        values = [post_clearance.organization_id, post_clearance.is_hidden, None, creator_type, user_id]

        try:
            clearance_record = await self.conn.fetchrow(sql_insert_clearance, *values)
            return Clearance(
                id=clearance_record["id"],
                organization_id=clearance_record["organization_id"],
                active_clearance_item=None,
                is_hidden=clearance_record["is_hidden"],
                updater_type=clearance_record["updater_type"],
                updater_id=clearance_record["updater_id"],
                updated_at=clearance_record["updated_at"],
                creator_type=clearance_record["creator_type"],
                creator_id=clearance_record["creator_id"],
                created_at=clearance_record["created_at"],
            )
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Clearance could not be created")

    async def query_clearances(
        self, clearance_query: ClearanceQuery, skip: int = None, limit: int = None
    ) -> PublicAggregatedClearanceList:
        where_stmt, data, index = await build_query_where_clause(clearance_query)

        limitations = ""
        if limit:
            limitations = f"LIMIT ${index} "
            data.append(limit)
            index += 1
        if skip:
            limitations = f"{limitations}OFFSET ${index}"
            data.append(skip)

        sql_query_clearances = await get_query_aggregated_clearance_statement(where_stmt, limitations=limitations)

        try:
            agg_clearance_records = await self.conn.fetch(sql_query_clearances, *data)

            if agg_clearance_records is None or len(agg_clearance_records) == 0:
                return PublicAggregatedClearanceList(items=[], item_count=0)

            clearances = []
            for record in agg_clearance_records:
                clearances.append(await self._build_aggregated_clearance(record["pub_agg_clearance"]))

            return PublicAggregatedClearanceList(items=clearances, item_count=agg_clearance_records[0]["full_count"])
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query all clearances")

    async def get_clearances(self, organization_id: str = None) -> List[Clearance]:
        values = []
        where_clause = ""
        if organization_id:
            where_clause = "WHERE c.organization_id = $1"
            values.append(organization_id)

        sql_select_clearance_statement = await get_select_clearances_statement(where_clause)

        try:
            clearance_records = await self.conn.fetch(sql_select_clearance_statement, *values)
            if clearance_records is None:
                raise HTTPException(status_code=404, detail="No clearance entries in DB.")
            clearances = []
            for record in clearance_records:
                clearances.append(await self._build_clearance(record))
            return clearances
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to query all clearances")

    async def get_clearance(self, clearance_id: UUID4, organization_id: str = None) -> Clearance:
        values = [clearance_id]
        where_clause = "WHERE c.id = $1"
        if organization_id:
            where_clause = f"{where_clause} AND c.organization_id = $2"
            values.append(organization_id)

        sql_select_clearance_statement = await get_select_clearances_statement(where_clause)

        try:
            clearance_record = await self.conn.fetchrow(sql_select_clearance_statement, *values)
            if clearance_record is None or len(clearance_record) == 0:
                raise HTTPException(
                    status_code=404,
                    detail=f"Clearance with ID {clearance_id} does not exist or \
                        is not accessible for {organization_id}.",
                )
            return await self._build_clearance(clearance_record)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed to query clearance for ID {clearance_id}")

    async def update_clearance_hidden_status(
        self, clearance_id: UUID4, hide: bool, user_type: EditorType, user_id: str
    ) -> Clearance:
        return await self.update_clearance(
            clearance_id=clearance_id, column_name="is_hidden", value=hide, user_type=user_type, user_id=user_id
        )

    async def update_clearance_active_item(
        self, clearance_id: UUID4, clearance_item_id: UUID4, user_type: EditorType, user_id: str
    ):
        await self._clearance_item_exists(clearance_item_id=clearance_item_id)
        return await self.update_clearance(
            clearance_id=clearance_id,
            column_name="active_item_id",
            value=clearance_item_id,
            user_type=user_type,
            user_id=user_id,
        )

    async def update_clearance(
        self, clearance_id: UUID4, column_name: str, value, user_type: EditorType, user_id: str
    ) -> Clearance:
        await self._clearance_exists(clearance_id=clearance_id)
        sql_update_statement = await get_update_clearance_where_statement(
            column_name_set=column_name, where_condition="WHERE id = $4"
        )

        try:
            _ = await self.conn.fetch(sql_update_statement, value, user_type, user_id, clearance_id)
            return await self.get_clearance(clearance_id=clearance_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to update clearance")

    async def post_clearance_item(
        self,
        clearance_id: UUID4,
        post_clearance_item: ExtendedPostClearanceItem,
        user_id: str,
        creator_type: EditorType,
    ) -> ClearanceItem:
        tags_json = validate_tags(post_clearance_item.tags)
        validate_multi_language_dict("Clearance description", post_clearance_item.description)

        sql_insert_clearance_item_statement = await get_insert_clearance_item_statement()
        sql_update_clearance_active_item_statement = await get_basic_update_statement(
            "v1_clearances", "active_item_id", "id"
        )

        info_source_content = {"contents": None}
        if post_clearance_item.info_source_content:
            info_source_content["contents"] = []
            for content in post_clearance_item.info_source_content:
                info_source_content["contents"].append(content.model_dump_json())

        try:
            clearance_item_record = await self.conn.fetchrow(
                sql_insert_clearance_item_statement,
                clearance_id,
                post_clearance_item.product_name,
                post_clearance_item.description,
                tags_json,
                post_clearance_item.tag_remark,
                post_clearance_item.product_release_at,
                post_clearance_item.product_release_at_precision,
                post_clearance_item.info_updated_at,
                post_clearance_item.info_provider,
                post_clearance_item.info_source_type,
                info_source_content,
                creator_type,
                user_id,
            )
            # set newly created clearance item as active
            _ = await self.conn.fetchrow(
                sql_update_clearance_active_item_statement, clearance_item_record["id"], clearance_id
            )
            return await self._build_clearance_item(clearance_item_record)
        except ForeignKeyViolationError as e1:
            raise HTTPException(
                status_code=404, detail=f"Clearance for ID {clearance_id} does not exist in database."
            ) from e1
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Clearance item could not be created")

    async def get_clearance_item(self, clearance_id: UUID4, clearance_item_id: UUID4) -> ClearanceItem:
        await self._clearance_exists(clearance_id=clearance_id)

        sql_select_clearance_item_statement = await get_select_clearance_items_statement("WHERE ci.id = $1")

        try:
            record = await self.conn.fetchrow(sql_select_clearance_item_statement, clearance_item_id)
            if record is None or len(record) == 0:
                raise HTTPException(status_code=404, detail=f"Clearance with ID {clearance_id} does not exist.")
            return await self._build_clearance_item(record["clearance_item_obj"])
        except PostgresError as e:
            log_and_raise_postgres_error(e, "Failed to retrieve clearance item")

    # private members

    async def _clearance_exists(self, clearance_id: UUID4):
        sql = await get_basic_select_where_statement("v1_clearances", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, clearance_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed querying clearance for ID {clearance_id}")

        if row is None:
            raise HTTPException(status_code=404, detail=f"No valid clearance for ID {clearance_id}.")

        return row

    async def _clearance_item_exists(self, clearance_item_id: UUID4):
        sql = await get_basic_select_where_statement("v1_clearance_items", "WHERE id = $1")

        try:
            row = await self.conn.fetchrow(sql, clearance_item_id)
        except PostgresError as e:
            log_and_raise_postgres_error(e, f"Failed querying clearance item for ID {clearance_item_id}")

        if row is None:
            raise HTTPException(status_code=404, detail=f"No valid clearance item for ID {clearance_item_id}.")

        return row

    async def _build_info_source_content(self, info_source_content_dict) -> InfoSourceContent:
        if info_source_content_dict is None:
            return None
        raw_info_source_content = info_source_content_dict.get("contents")
        info_source_content = None
        if raw_info_source_content:
            info_source_content = []
            for content in raw_info_source_content:
                cont = PostInfoSourceContent.model_validate_json(content)
                info_source_content_label = None if cont.label is None else translate_dict(cont.label)
                info_source_content.append(
                    InfoSourceContent(reference_url=cont.reference_url, label=info_source_content_label)
                )
        return info_source_content

    async def _build_aggregated_clearance(self, agg_clearance_record) -> PublicAggregatedClearance:
        desc = None
        if agg_clearance_record["description"] is not None:
            desc = translate_dict(agg_clearance_record["description"])

        tags = translate_extended_tags(agg_clearance_record["tags"])
        info_source_content = await self._build_info_source_content(agg_clearance_record.get("info_source_content"))

        return PublicAggregatedClearance(
            id=agg_clearance_record["id"],
            organization_id=agg_clearance_record["organization_id"],
            product_name=agg_clearance_record["product_name"],
            description=desc,
            tags=tags,
            tag_remark=agg_clearance_record["tag_remark"],
            product_release_at=agg_clearance_record["product_release_at"],
            product_release_at_precision=agg_clearance_record["product_release_at_precision"],
            info_updated_at=agg_clearance_record["info_updated_at"],
            info_provider=agg_clearance_record["info_provider"],
            info_source_type=agg_clearance_record["info_source_type"],
            info_source_content=info_source_content,
            updated_at=agg_clearance_record["updated_at"],
            created_at=agg_clearance_record["created_at"],
        )

    async def _build_clearance(self, clearance_record) -> Clearance:
        if "clearance_obj" not in clearance_record:
            raise HTTPException(status_code=404, detail="No valid clearance item")

        active_clearance_item_id = clearance_record["clearance_obj"]["active_item_id"]

        history = []
        active_clearance_item = None
        if "history_obj" in clearance_record and isinstance(clearance_record["history_obj"], list):
            for item in clearance_record["history_obj"]:
                if item["id"] is not None:
                    clearance_item = await self._build_clearance_item(item)
                    history.append(clearance_item)
                    if active_clearance_item_id and str(clearance_item.id) == str(active_clearance_item_id):
                        active_clearance_item = clearance_item

        return Clearance(
            id=clearance_record["clearance_obj"]["id"],
            organization_id=clearance_record["clearance_obj"]["organization_id"],
            is_hidden=clearance_record["clearance_obj"]["is_hidden"],
            active_clearance_item=active_clearance_item,
            history=history if len(history) > 0 else None,
            creator_type=clearance_record["clearance_obj"]["creator_type"],
            creator_id=clearance_record["clearance_obj"]["creator_id"],
            created_at=clearance_record["clearance_obj"]["created_at"],
            updater_type=clearance_record["clearance_obj"]["updater_type"],
            updater_id=clearance_record["clearance_obj"]["updater_id"],
            updated_at=clearance_record["clearance_obj"]["updated_at"],
        )

    async def _build_clearance_item(self, clearance_item_record) -> ClearanceItem:
        desc = None
        if clearance_item_record["description"] is not None:
            desc = translate_dict(clearance_item_record["description"])

        tags = translate_extended_tags(clearance_item_record["tags"])
        info_source_content = await self._build_info_source_content(clearance_item_record.get("info_source_content"))

        return ClearanceItem(
            id=clearance_item_record["id"],
            clearance_id=clearance_item_record["clearance_id"],
            product_name=clearance_item_record["product_name"],
            description=desc,
            tags=tags,
            tag_remark=clearance_item_record["tag_remark"],
            product_release_at=clearance_item_record["product_release_at"],
            product_release_at_precision=DatePrecision(clearance_item_record["product_release_at_precision"]),
            info_updated_at=clearance_item_record["info_updated_at"],
            info_provider=InfoProvider(clearance_item_record["info_provider"]),
            info_source_type=InfoSourceType(clearance_item_record["info_source_type"]),
            info_source_content=info_source_content,
            creator_type=EditorType(clearance_item_record["creator_type"]),
            creator_id=clearance_item_record["creator_id"],
            created_at=clearance_item_record["created_at"],
        )
