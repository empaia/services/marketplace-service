from fastapi import Body, Path, Query

from marketplace_service.models.marketplace.app import ApiVersion, AppMediaPurpose, AppStatus

organization_id = Path(description="Organization ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
app_id = Path(description="App ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
portal_app_id = Path(description="Portal App ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
media_id = Path(description="Media ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
app_view_id = Path(description="App view ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
container_image_id = Path(description="Container image ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
app_ui_bundle_id = Path(description="App UI resources ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
customer_id = Path(description="Organization ID of the customer")
license_id = Path(description="License ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
clearance_id = Path(description="Clearance ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
clearance_item_id = Path(description="Clearance item ID", examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"])
purpose = Path(description="Media purpose type", examples=[AppMediaPurpose.PEEK])
index = Path(
    description="Index of the media. This parameter is used to determine the order in which media data is displayed.",
)
resource_filename = Path(description="Filename of the frontend resource")

app_config = Body(None, description="App configuration", examples=[{"key1": "value", "key2": 100}])
app_status = Body(None, description="Portal app status", examples=[AppStatus.PENDING])

customer_id_query = Query(None, description="Organization ID of the customer")
edit = Query(
    default=False,
    examples=[False],
    description="When edit is set to true, last active app data will be set as template.",
)
api_version = Query(
    default=ApiVersion.V3.value,
    examples=[ApiVersion.V3.value],
    description="Supported API version of an app view.",
)
research_only = Query(
    default=False,
    examples=[False],
    description="When research_only is set to true, app usage is intended for research only.",
)
external_ids = Query(
    default=False,
    description="If set to true, external IDs for portal app and app can be defined in the post model.",
)
existing_portal_app = Query(
    default=False,
    description="If set to true, new portal app view and technical app will be posted to existing portal app \
        specified in the request body.",
)
existing_app = Query(
    default=False,
    description="If set to true, new existing app will be added to app view.",
)
