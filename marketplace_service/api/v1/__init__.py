from marketplace_service.api.v1.routes_admin import add_routes_admin
from marketplace_service.api.v1.routes_compute import add_routes_compute
from marketplace_service.api.v1.routes_customer import add_routes_customer
from marketplace_service.api.v1.routes_general import add_routes_general
from marketplace_service.api.v1.routes_moderator import add_routes_moderator
from marketplace_service.api.v1.routes_product_provider import add_routes_product_provider
from marketplace_service.api.v1.routes_public import add_routes_public
from marketplace_service.api.v1.routes_reviewer import add_routes_reviewer
from marketplace_service.api.v1.routes_tus import add_routes_tus
from marketplace_service.api.v1.routes_vendor import add_routes_vendor

from .singletons import settings


def add_routes_v1_public(app, late_init):
    add_routes_general(app)
    add_routes_public(app, late_init)


def add_routes_v1_customer(app, late_init):
    add_routes_customer(app, late_init)


def add_routes_v1_compute(app, late_init):
    add_routes_compute(app, late_init)


def add_routes_v1_vendor(app, late_init):
    if settings.enable_vendor_routes:
        add_routes_vendor(app, late_init)
        add_routes_tus(app, late_init)


def add_routes_v1_reviewer(app, late_init):
    if settings.enable_reviewer_routes:
        add_routes_reviewer(app, late_init)


def add_routes_v1_admin(app, late_init):
    if settings.enable_admin_routes:
        add_routes_admin(app, late_init)


def add_routes_v1_product_provider(app, late_init):
    if settings.enable_product_provider_routes:
        add_routes_product_provider(app, late_init)


def add_routes_v1_moderator(app, late_init):
    add_routes_moderator(app, late_init)
