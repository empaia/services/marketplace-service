from typing import List

from fastapi import Depends, Header
from pydantic import UUID4
from starlette.requests import Request

from marketplace_service.clients.minio import is_minio_reachable
from marketplace_service.custom_models.commons import MinioClientWrapper
from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import AppStatus, ClosedApp, ClosedAppView

from .. import params
from .singletons import api_v1_integration


def minio_session_dep(request: Request) -> MinioClientWrapper:
    minio_client_wrapper = MinioClientWrapper()
    minio_client_wrapper.minio_client = request.app.state.minio_session
    minio_client_wrapper.is_reachable = is_minio_reachable(request.app.state.minio_session)
    return minio_client_wrapper


def add_routes_reviewer(app, late_init):
    @app.get(
        "/pending-reviews",
        summary="Get all pending portal apps for given organization",
        response_model=List[ClosedAppView],
        tags=["Portal App"],
    )
    async def _(
        user_id: str = Header(...),
        payload=api_v1_integration.reviewer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_pending_reviews(minio_client)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/approve",
        summary="Approve a given app view for a portal app",
        response_model=ClosedAppView,
        tags=["Portal App"],
    )
    async def _(
        review_comment: str = None,
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        payload=api_v1_integration.reviewer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_approve_portal_app_view_review(
                    minio_client, portal_app_id, app_view_id, review_comment, user_id
                )

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/reject",
        summary="Reject a given view of a portal app",
        response_model=ClosedAppView,
        tags=["Portal App"],
    )
    async def _(
        review_comment: str = None,
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        payload=api_v1_integration.reviewer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_reject_portal_app_view_review(
                    minio_client, portal_app_id, app_view_id, review_comment, user_id
                )

    @app.put(
        "/portal-apps/{portal_app_id}/apps/{app_id}/approve",
        summary="Approve a given app of a portal app",
        response_model=ClosedApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.reviewer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_app_status(portal_app_id, app_id, AppStatus.APPROVED)

    @app.put(
        "/portal-apps/{portal_app_id}/apps/{app_id}/reject",
        summary="Reject a given app of a portal app",
        response_model=ClosedApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.reviewer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_app_status(portal_app_id, app_id, AppStatus.REJECTED)
