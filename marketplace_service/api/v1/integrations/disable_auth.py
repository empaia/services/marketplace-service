from fastapi import Depends


def _dummy():
    pass


class DisableAuth:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

    def public_depends(self):
        return Depends(_dummy)

    def customer_depends(self):
        return Depends(_dummy)

    def customer_vault_depends(self):
        return Depends(_dummy)

    def compute_depends(self):
        return Depends(_dummy)

    def vendor_depends(self):
        return Depends(_dummy)

    def reviewer_depends(self):
        return Depends(_dummy)

    def admin_depends(self):
        return Depends(_dummy)

    def product_provider_depends(self):
        return Depends(_dummy)

    def moderator_depends(self):
        return Depends(_dummy)
