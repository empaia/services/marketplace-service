from typing import Any

from fastapi import Depends, HTTPException, Request
from fastapi.openapi.models import OAuthFlowClientCredentials, OAuthFlows
from fastapi.security import OAuth2
from pydantic import AnyHttpUrl, BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict

from ....emapia_receiver_auth import Auth


def _dummy():
    pass


class AuthSettings(BaseSettings):
    idp_url: AnyHttpUrl
    audience: str
    refresh_interval: int = 300  # seconds
    openapi_token_url: str = ""
    openapi_auth_url: str = ""

    model_config = SettingsConfigDict(env_prefix="mps_", env_file=".env", extra="ignore")


class TokenPayload(BaseModel):
    user_id: str


class Payload(BaseModel):
    token: TokenPayload
    request: Any = None


def find_key_or_fail(_dict: dict, recursive_key_names: list[str]):
    if recursive_key_names is None or len(recursive_key_names) == 0:
        return _dict

    value = _dict
    for key in recursive_key_names:
        value = value.get(key)
        if value is None:
            key_str = ", ".join(fn for fn in recursive_key_names if fn)
            raise HTTPException(
                status_code=403,
                detail=f"Invalid access token. Expected field(s) [{key_str}] does not exist.",
            )
    return value


def validate_token_user_id(decoded_token: dict, request: Request):
    sub = find_key_or_fail(_dict=decoded_token, recursive_key_names=["sub"])
    user_id = request.headers.get("user-id")
    if not sub or sub != user_id:
        raise HTTPException(status_code=412, detail="Access Token sub field does not match the user-id Header.")


def validate_token_organization_id(decoded_token: dict, request: Request):
    organization_id = request.headers.get("organization-id")
    user_organizations = find_key_or_fail(_dict=decoded_token, recursive_key_names=["organizations"])
    if organization_id and organization_id not in user_organizations.keys():
        raise HTTPException(
            status_code=403,
            detail="Given organization-id does not match organizations in access token. API access denied.",
        )


def validate_token_is_compute_provider(decoded_token: dict, request: Request):
    organizations = decoded_token.get("organizations")
    if organizations is None:
        raise HTTPException(status_code=403, detail="Invalid access token. Key 'organizations' does not exist.")

    for key in organizations:
        orga = organizations.get(key)
        if orga is None:
            continue
        roles = orga.get("roles")
        if roles and "service-compute-provider" in roles:
            return

    raise HTTPException(status_code=403, detail="Requesting client is not a service compute provider.")


def validate_token_organization_user_role(decoded_token: dict, request: Request, expected_organization_role: str):
    organization_id = request.headers.get("organization-id")
    orga_roles = find_key_or_fail(_dict=decoded_token, recursive_key_names=["organizations", organization_id, "roles"])
    orga_user_roles = [str(role).lower() for role in orga_roles]
    if not orga_user_roles or str(expected_organization_role).lower() not in orga_user_roles:
        raise HTTPException(status_code=403, detail="Unauthorized organization user roles. API access denied.")


def validate_token_global_user_role(decoded_token: dict, request: Request, expected_global_role: str):
    roles = find_key_or_fail(
        _dict=decoded_token, recursive_key_names=["resource_access", "auth_service_client", "roles"]
    )
    if expected_global_role not in roles:
        raise HTTPException(status_code=403, detail="Unauthorized global user roles. API access denied.")


def make_oauth2_wrapper(
    auth: Auth,
    auth_settings: AuthSettings,
    check_user_header: bool = False,
    check_organization_header: bool = False,
    expected_organization_role: str = None,
    expected_global_role: str = None,
):
    oauth2_scheme = OAuth2(
        flows=OAuthFlows(clientCredentials=OAuthFlowClientCredentials(tokenUrl=auth_settings.openapi_token_url))
    )

    def oauth2_wrapper(request: Request, token=Depends(oauth2_scheme)):
        decoded_token = auth.decode_token(token)
        if check_user_header:
            validate_token_user_id(decoded_token=decoded_token, request=request)
        if check_organization_header:
            validate_token_organization_id(decoded_token=decoded_token, request=request)
        if expected_organization_role is not None:
            if expected_organization_role == "service-compute-provider":
                validate_token_is_compute_provider(decoded_token=decoded_token, request=request)
            else:
                validate_token_organization_user_role(
                    decoded_token=decoded_token, request=request, expected_organization_role=expected_organization_role
                )
        if expected_global_role is not None:
            validate_token_global_user_role(
                decoded_token=decoded_token, request=request, expected_global_role=expected_global_role
            )
        return decoded_token

    return oauth2_wrapper


class EmpaiaApiIntegration:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

        self.auth_settings = AuthSettings()

        self.auth_mps = Auth(
            idp_url=str(self.auth_settings.idp_url).rstrip("/"),
            refresh_interval=self.auth_settings.refresh_interval,
            audience=self.auth_settings.audience,
            logger=self.logger,
        )

        self.oauth2_wrapper_mps_customer = make_oauth2_wrapper(
            auth=self.auth_mps,
            auth_settings=self.auth_settings,
            check_organization_header=True,
            expected_organization_role="service-app-reader",
        )
        self.oauth2_wrapper_mps_customer_vault = make_oauth2_wrapper(
            auth=self.auth_mps,
            auth_settings=self.auth_settings,
            check_organization_header=True,
            expected_organization_role="service-app-config-reader",
        )
        self.oauth2_wrapper_mps_compute = make_oauth2_wrapper(
            auth=self.auth_mps, auth_settings=self.auth_settings, expected_organization_role="service-compute-provider"
        )
        self.oauth2_wrapper_mps_vendor = make_oauth2_wrapper(
            auth=self.auth_mps,
            auth_settings=self.auth_settings,
            check_user_header=True,
            check_organization_header=True,
            expected_organization_role="app-maintainer",
        )
        self.oauth2_wrapper_mps_reviewer = make_oauth2_wrapper(
            auth=self.auth_mps,
            auth_settings=self.auth_settings,
            check_user_header=True,
            expected_global_role="moderator",
        )
        self.oauth2_wrapper_mps_admin = make_oauth2_wrapper(
            auth=self.auth_mps, auth_settings=self.auth_settings, check_user_header=True, expected_global_role="admin"
        )
        self.oauth2_wrapper_mps_product_provider = make_oauth2_wrapper(
            auth=self.auth_mps,
            auth_settings=self.auth_settings,
            check_user_header=True,
            check_organization_header=True,
            expected_organization_role="clearance-maintainer",
        )
        self.oauth2_wrapper_mps_moderator = make_oauth2_wrapper(
            auth=self.auth_mps,
            auth_settings=self.auth_settings,
            check_user_header=True,
            expected_global_role="moderator",
        )

        self.logger.info("Loaded EmpaiaApiIntegration")

    def public_depends(self):
        return Depends(_dummy)

    def customer_depends(self):
        return Depends(self.oauth2_wrapper_mps_customer)

    def customer_vault_depends(self):
        return Depends(self.oauth2_wrapper_mps_customer_vault)

    def compute_depends(self):
        return Depends(self.oauth2_wrapper_mps_compute)

    def vendor_depends(self):
        return Depends(self.oauth2_wrapper_mps_vendor)

    def reviewer_depends(self):
        return Depends(self.oauth2_wrapper_mps_reviewer)

    def admin_depends(self):
        return Depends(self.oauth2_wrapper_mps_admin)

    def product_provider_depends(self):
        return Depends(self.oauth2_wrapper_mps_product_provider)

    def moderator_depends(self):
        return Depends(self.oauth2_wrapper_mps_moderator)
