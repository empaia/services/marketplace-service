from fastapi import Depends, HTTPException, status


def _unauthorized():
    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Integration not configured")


class Default:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

    def public_depends(self):
        return Depends(_unauthorized)

    def customer_depends(self):
        return Depends(_unauthorized)

    def customer_vault_depends(self):
        return Depends(_unauthorized)

    def compute_depends(self):
        return Depends(_unauthorized)

    def vendor_depends(self):
        return Depends(_unauthorized)

    def reviewer_depends(self):
        return Depends(_unauthorized)

    def admin_depends(self):
        return Depends(_unauthorized)

    def product_provider_depends(self):
        return Depends(_unauthorized)

    def moderator_depends(self):
        return Depends(_unauthorized)
