import io
import zipfile
from pathlib import Path
from typing import List, Optional

from aiohttp import ClientSession
from fastapi import Depends, File, Form, Header, HTTPException, UploadFile
from pydantic import UUID4
from starlette.requests import Request

from marketplace_service.clients.minio import is_minio_reachable
from marketplace_service.clients.vault_client import get_app_config, post_app_configuration
from marketplace_service.custom_models.commons import SUPPORTED_ARCHIVE_CONTENT_TYPES, MinioClientWrapper
from marketplace_service.custom_models.v1.resources import (
    AppUI,
    AppUIStatus,
    ContainerImage,
    ContainerImageStatus,
    InternalAppUI,
    InternalContainerImage,
)
from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import (
    ApiVersion,
    AppConfiguration,
    AppConfigurationType,
    AppMediaPurpose,
    AppUiConfiguration,
    ClosedApp,
    ClosedAppView,
    ClosedPortalApp,
    ConfigurationModel,
    MediaMetadata,
    PostApp,
    PostAppConfiguration,
    PostAppDetails,
    PostAppTag,
)

from ...clients.helpers.utils import log_and_raise_error
from .. import params
from .singletons import api_v1_integration, settings


def minio_session_dep(request: Request) -> MinioClientWrapper:
    minio_client_wrapper = MinioClientWrapper()
    minio_client_wrapper.minio_client = request.app.state.minio_session
    minio_client_wrapper.is_reachable = is_minio_reachable(request.app.state.minio_session)
    return minio_client_wrapper


def client_session_dep(request: Request) -> ClientSession:
    return request.app.state.client_session


def add_routes_vendor(app, late_init):
    @app.post(
        "/portal-apps",
        summary="Create a new portal app",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.post_portal_app(
                    organization_id=organization_id, organization_name=None, user_id=user_id
                )

    @app.get(
        "/portal-apps",
        summary="Get all portal apps for the given organization",
        response_model=List[ClosedPortalApp],
        tags=["Portal App"],
    )
    async def _(
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_portal_apps_for_orga(minio_client, organization_id)

    @app.get(
        "/portal-apps/{portal_app_id}",
        summary="Get a portal app by ID",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_closed_portal_app(minio_client, portal_app_id)

    @app.post(
        "/portal-apps/{portal_app_id}/app-views",
        summary="Add a new app view version for a portal app",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        edit: bool = params.edit,
        api_version: ApiVersion = params.api_version,
        research_only: bool = params.research_only,
        portal_app_id: UUID4 = params.portal_app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                # TODO: advanced validation of edit, api_version and research_only
                return await client.app_client.post_portal_app_view(
                    minio_client=minio_client,
                    portal_app_id=portal_app_id,
                    user_id=user_id,
                    edit=edit,
                    active_api_version=api_version,
                    research_only=research_only,
                )

    @app.get(
        "/portal-apps/{portal_app_id}/app-views",
        summary="Get all app views for a portal app",
        response_model=List[ClosedAppView],
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_portal_app_views(minio_client, portal_app_id)

    @app.get(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}",
        summary="Get an app view by ID",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_closed_app_view(minio_client, portal_app_id, app_view_id)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        summary="Add details to app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        details: PostAppDetails,
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_portal_app_view_details(
                    minio_client=minio_client,
                    portal_app_id=portal_app_id,
                    app_view_id=app_view_id,
                    details=details,
                    api_version=None,
                )

    @app.delete(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        summary="Remove details from app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.delete_portal_app_view_details(minio_client, portal_app_id, app_view_id)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        summary="Add media to app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        media_metadata: MediaMetadata = Form(...),
        media_file: UploadFile = File(...),
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        purpose: AppMediaPurpose = params.purpose,
        index: int = params.index,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_portal_app_view_media(
                    minio_client,
                    portal_app_id,
                    app_view_id,
                    purpose,
                    index,
                    media_metadata.caption,
                    media_metadata.alternative_text,
                    media_file,
                )

    @app.delete(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        summary="Remove media from app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        purpose: AppMediaPurpose = params.purpose,
        index: int = params.index,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.remove_media_from_portal_app_view(
                    minio_client, portal_app_id, app_view_id, purpose, index
                )

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags",
        summary="Add tags to app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        tags: List[PostAppTag],
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_app_view_tags(minio_client, portal_app_id, app_view_id, tags)

    @app.delete(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags",
        summary="Remove tags from app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.delete_app_view_tags(minio_client, portal_app_id, app_view_id)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/apps/{app_id}",
        summary="Add active app (ID) to app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        app_id: str = params.app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.link_app_to_app_view(minio_client, portal_app_id, app_view_id, app_id)

    @app.delete(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/apps",
        summary="Remove app from app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.unlink_active_app_from_app_view(minio_client, portal_app_id, app_view_id)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/functional",
        summary="Set portal app view to functional (default)",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.set_app_view_non_functional_state(
                    minio_client, portal_app_id, app_view_id, False
                )

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/non-functional",
        summary="Set app view to non-functional",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.set_app_view_non_functional_state(
                    minio_client, portal_app_id, app_view_id, True
                )

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/request-approval",
        summary="Request review and approval for app view by reviewer/admin",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.request_app_view_approval(minio_client, portal_app_id, app_view_id)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/revoke-approval",
        summary="Revoke approval for app view",
        response_model=ClosedAppView,
        tags=["App views"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.revoke_app_view_approval(minio_client, portal_app_id, app_view_id)

    @app.get(
        "/portal-apps/{portal_app_id}/apps",
        summary="Get all apps for portal app ID",
        response_model=List[ClosedApp],
        tags=["App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_apps(portal_app_id)

    @app.post(
        "/portal-apps/{portal_app_id}/apps",
        summary="Create a new app for portal app",
        response_model=ClosedApp,
        tags=["App"],
    )
    async def _(
        app: PostApp,
        portal_app_id: UUID4 = params.portal_app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.post_app(portal_app_id, app, organization_id, user_id)

    @app.get(
        "/portal-apps/{portal_app_id}/apps/{app_id}",
        summary="Get an app by ID",
        response_model=ClosedApp,
        tags=["App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            await client.app_client.validate_app_in_portal_app(portal_app_id=portal_app_id, app_id=app_id)
            return await client.app_client.get_app_by_id(app_id)

    @app.delete(
        "/portal-apps/{portal_app_id}/apps/{app_id}",
        summary="Delete an existing app in draft state",
        response_model=ClosedApp,
        tags=["App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            await client.app_client.validate_app_in_portal_app(portal_app_id=portal_app_id, app_id=app_id)
            return await client.app_client.delete_app(portal_app_id, app_id)

    @app.get(
        "/apps/{app_id}/config",
        summary="Get the global configuration only of an existing app",
        response_model=AppConfiguration,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        http_client: ClientSession = Depends(client_session_dep),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            config = await get_app_config(
                app_id=str(app_id), app_namespace=app.ead["namespace"], http_client=http_client, customer_id=None
            )
            return config

    @app.get(
        "/apps/{app_id}/config/{customer_id}",
        summary="Get the configuration of an existing app for a customer",
        response_model=AppConfiguration,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        customer_id: Optional[str] = params.customer_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        http_client: ClientSession = Depends(client_session_dep),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            config = await get_app_config(
                app_id=str(app_id), app_namespace=app.ead["namespace"], http_client=http_client, customer_id=customer_id
            )
            return config

    @app.post(
        "/apps/{app_id}/config/global",
        summary="Post a global configuration for an existing app",
        response_model=ConfigurationModel,
        tags=["App"],
    )
    async def _(
        configuration: PostAppConfiguration,
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        http_client: ClientSession = Depends(client_session_dep),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            config = await post_app_configuration(
                app=app,
                config_type=AppConfigurationType.GLOBAL,
                configuration=configuration,
                http_client=http_client,
                customer_id=None,
            )
            return config.global_

    @app.post(
        "/apps/{app_id}/config/customer/{customer_id}",
        summary="Post a configuration for an existing app",
        response_model=ConfigurationModel,
        tags=["App"],
    )
    async def _(
        configuration: PostAppConfiguration,
        app_id: UUID4 = params.app_id,
        customer_id: str = params.customer_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        http_client: ClientSession = Depends(client_session_dep),
        payload=api_v1_integration.vendor_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            config = await post_app_configuration(
                app=app,
                config_type=AppConfigurationType.CUSTOMER,
                configuration=configuration,
                http_client=http_client,
                customer_id=customer_id,
            )
            return config.customer

    # !!! Endpoints are not not entirely implemented and tested !!!
    if False:

        @app.put(
            "/portal-apps/{portal_app_id}/apps/{app_id}/container-images/{container_image_id}",
            summary="Add container image to existing app",
            response_model=ClosedApp,
            tags=["App"],
        )
        async def _(
            portal_app_id: UUID4 = params.portal_app_id,
            app_id: UUID4 = params.app_id,
            container_image_id: UUID4 = params.container_image_id,
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                await client.app_client_v1.validate_app_in_portal_app(portal_app_id=portal_app_id, app_id=app_id)
                app = await client.app_client_v1.post_container_image_id(app_id, container_image_id)
                return app

        @app.put(
            "/portal-apps/{portal_app_id}/apps/{app_id}/app-ui-bundles/{app_ui_bundle_id}",
            summary="Add app UI archive to existing app",
            response_model=dict,
            tags=["App"],
        )
        async def _(
            portal_app_id: UUID4 = params.portal_app_id,
            app_id: UUID4 = params.app_id,
            app_ui_bundle_id=params.app_ui_bundle_id,
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                await client.app_client_v1.validate_app_in_portal_app(portal_app_id=portal_app_id, app_id=app_id)
                app = await client.app_client_v1.post_app_ui_id(app_id=app_id, app_ui_id=app_ui_bundle_id)
                return app

        @app.post(
            "/container-images",
            summary="Post container image",
            response_model=ContainerImage,
            tags=["Resources"],
        )
        async def _(
            container_image_name: str = Form(...),
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                internal_container_image = InternalContainerImage(
                    name=container_image_name,
                    status=ContainerImageStatus.INITIALIZED,
                    creator_id=user_id,
                    organization_id=organization_id,
                )
                container_image = await client.app_client_v1.post_container_image(internal_container_image)
                return container_image

        @app.delete(
            "/container-images/{container_image_id}",
            summary="Delete container image if not used",
            response_model=ContainerImage,
            tags=["Resources"],
        )
        async def _(
            container_image_id: str = params.container_image_id,
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                # TODO: check creator id?
                await client.app_client_v1._container_image_is_referenced(container_image_id=container_image_id)
                await client.app_client_v1.set_container_image_status(
                    container_image_id=container_image_id, status=ContainerImageStatus.MARKED_FOR_DELETION
                )
                return await client.app_client_v1.get_container_image(container_image_id=container_image_id)

        @app.get(
            "/container-images",
            summary="Get container images",
            response_model=List[ContainerImage],
            tags=["Resources"],
        )
        async def _(
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                container_images = await client.app_client_v1.get_container_images(organization_id=organization_id)
                return container_images

        @app.post(
            "/app-ui-bundles",
            summary="Post app ui",
            response_model=AppUI,
            tags=["Resources"],
        )
        async def _(
            app_ui_name: str = Form(...),
            app_ui_config: Optional[AppUiConfiguration] = Form(...),
            app_ui_file: UploadFile = File(...),
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            if app_ui_file.content_type not in SUPPORTED_ARCHIVE_CONTENT_TYPES:
                raise HTTPException(status_code=400, detail=f"Content type {app_ui_file.content_type} not supported.")

            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                internal_app_ui = InternalAppUI(
                    name=app_ui_name,
                    status=AppUIStatus.INITIALIZED,
                    app_ui_configuration=app_ui_config,
                    creator_id=organization_id,
                    organization_id=organization_id,
                )
                app_ui = await client.app_client_v1.post_app_ui(internal_app_ui)

                app_ui_files_path = f"{settings.data_dir}/app_ui_bundles/{str(app_ui.id)}"
                Path(app_ui_files_path).expanduser().mkdir(parents=True, exist_ok=True)

                try:
                    file_as_bytes = io.BytesIO(app_ui_file.file.read())
                    with zipfile.ZipFile(file_as_bytes, "r") as zip_ref:
                        zip_ref.extractall(app_ui_files_path)
                except zipfile.BadZipFile as e:
                    log_and_raise_error(e, status_code=400, detail_message="Failed to unzip archive")

                app_ui = await client.app_client_v1.update_app_ui_state(
                    app_ui.id, app_ui_files_path, AppUIStatus.UNZIP_FINISHED
                )
                return app_ui

        @app.delete(
            "/app-ui-bundles/{app_ui_bundle_id}",
            summary="Delete app ui if not used",
            response_model=AppUI,
            tags=["Resources"],
        )
        async def _(
            app_ui_bundle_id: str = params.app_ui_bundle_id,
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            # TODO: delete app ui resources
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                app_ui = await client.app_client_v1.delete_app_ui_bundle(
                    app_ui_bundle_id=app_ui_bundle_id, creator_id=user_id
                )
                return app_ui

        @app.get(
            "/app-ui-bundles",
            summary="Get app uis",
            response_model=List[AppUI],
            tags=["Resources"],
        )
        async def _(
            user_id: str = Header(...),
            organization_id: str = Header(...),
            http_client: ClientSession = Depends(client_session_dep),
            payload=api_v1_integration.vendor_depends(),
        ):
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                # TODO: flag for state ?
                app_uis = await client.app_client_v1.get_app_ui_bundles(organization_id=organization_id)
                return app_uis
