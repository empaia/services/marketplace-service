from pydantic import UUID4

from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import ClosedApp

from .. import params
from .singletons import api_v1_integration


def add_routes_compute(app, late_init):
    @app.get(
        "/organizations/{organization_id}/apps/{app_id}",
        summary="Get app by ID",
        response_model=ClosedApp,
        tags=["App"],
    )
    async def _(
        organization_id: str = params.organization_id,
        app_id: UUID4 = params.app_id,
        payload=api_v1_integration.compute_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            await client.license_client.organization_may_access_portal_app(organization_id, app.portal_app_id)
            return app
