import time
from typing import List

from fastapi import Header, HTTPException
from pydantic import UUID4

from marketplace_service.custom_models.v1.clearances import (
    Clearance,
    ClearanceItem,
    EditorType,
    ExtendedPostClearance,
    ExtendedPostClearanceItem,
    InfoProvider,
    PostClearance,
    PostClearanceItem,
    PostClearanceItemActivation,
)
from marketplace_service.db import db_clients

from .. import params
from .singletons import api_v1_integration


def add_routes_product_provider(app, late_init):
    @app.post(
        "/clearances",
        summary="Post a new empty clearance entry",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        post_clearance: PostClearance,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                post_admin_clearance = ExtendedPostClearance(
                    is_hidden=post_clearance.is_hidden, organization_id=organization_id
                )
                if post_clearance.clearance_item is not None:
                    try:
                        temp_item = post_clearance.clearance_item.model_dump()
                        temp_item["info_provider"] = InfoProvider.ORGANIZATION
                        temp_item["info_updated_at"] = int(time.time())
                        post_admin_clearance.clearance_item = ExtendedPostClearanceItem(**temp_item)
                    except ValueError as e:
                        raise HTTPException(status_code=422, detail=str(e))
                return await client.clearance_client.post_clearance(
                    post_clearance=post_admin_clearance, user_id=user_id, creator_type=EditorType.USER
                )

    @app.get(
        "/clearances",
        summary="Get all clearance entries",
        response_model=List[Clearance],
        tags=["Clearances"],
    )
    async def _(
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.get_clearances(organization_id=organization_id)

    @app.get(
        "/clearances/{clearance_id}",
        summary="Get clearance entry by ID",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.get_clearance(clearance_id=clearance_id)

    @app.put(
        "/clearances/{clearance_id}/hide",
        summary="Hide clearance entry",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.update_clearance_hidden_status(
                    clearance_id=clearance_id, hide=True, user_type=EditorType.USER, user_id=user_id
                )

    @app.put(
        "/clearances/{clearance_id}/unhide",
        summary="Unhide clearance entry",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.update_clearance_hidden_status(
                    clearance_id=clearance_id, hide=False, user_type=EditorType.USER, user_id=user_id
                )

    @app.post(
        "/clearances/{clearance_id}/item",
        summary="Post clearance item by ID",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        post_clearance_item: PostClearanceItem,
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                extended_post_clearance_item = None
                try:
                    temp_item = post_clearance_item.model_dump()
                    temp_item["info_provider"] = InfoProvider.ORGANIZATION
                    temp_item["info_updated_at"] = int(time.time())
                    extended_post_clearance_item = ExtendedPostClearanceItem(**temp_item)
                except ValueError as e:
                    raise HTTPException(status_code=422, detail=str(e))

                clearance_item = await client.clearance_client.post_clearance_item(
                    clearance_id=clearance_id,
                    post_clearance_item=extended_post_clearance_item,
                    user_id=user_id,
                    creator_type=EditorType.USER,
                )
                return await client.clearance_client.get_clearance(
                    clearance_id=clearance_item.clearance_id, organization_id=organization_id
                )

    @app.get(
        "/clearances/{clearance_id}/item/{item_id}",
        summary="Get clearance item by ID",
        response_model=ClearanceItem,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        item_id: UUID4 = params.clearance_item_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.get_clearance_item(
                    clearance_id=clearance_id, clearance_item_id=item_id
                )

    @app.put(
        "/clearances/{clearance_id}/activate",
        summary="Post clearance item by ID",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        post_item_activation: PostClearanceItemActivation,
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        organization_id: str = Header(...),
        payload=api_v1_integration.product_provider_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.update_clearance_active_item(
                    clearance_id=clearance_id,
                    clearance_item_id=post_item_activation.active_item_id,
                    user_type=EditorType.USER,
                    user_id=user_id,
                )
