from ...singletons import logger, settings
from .integrations import get_api_v1_integration

api_v1_integration = get_api_v1_integration(settings=settings, logger=logger)
