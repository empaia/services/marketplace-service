from marketplace_service.custom_models.v1.clearances import ExtendedTagList

from ...singletons import app_tags_model
from .singletons import api_v1_integration


def add_routes_general(app):
    @app.get(
        "/tags",
        summary="Get all available tags",
        response_model=ExtendedTagList,
        tags=["General"],
    )
    async def _(
        payload=api_v1_integration.public_depends(),
    ):
        return app_tags_model
