import io
import zipfile
from pathlib import Path
from typing import List

from aiohttp import ClientSession
from fastapi import Depends, File, Form, Header, HTTPException, UploadFile
from pydantic import UUID4
from starlette.requests import Request

from marketplace_service.clients.helpers.utils import log_and_raise_error
from marketplace_service.clients.minio import delete_all_minio_media_objects, is_minio_reachable, put_minio_media
from marketplace_service.ctl.drop_db import TABLES_V1, TABLES_V1_CLEARANCES
from marketplace_service.custom_models.commons import (
    SUPPORTED_ARCHIVE_CONTENT_TYPES,
    SUPPORTED_IMAGE_CONTENT_TYPES,
    MinioClientWrapper,
)
from marketplace_service.custom_models.v1.clearances import (
    AdminPostClearance,
    Clearance,
    ClearanceItem,
    EditorType,
    ExtendedPostClearanceItem,
    PostClearanceItemActivation,
)
from marketplace_service.custom_models.v1.licenses import (
    LicenseQuery,
    OrganizationLicense,
    OrganizationLicenseList,
    PostOrganizationLicenseList,
)
from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import (
    ApiVersion,
    AppMediaPurpose,
    AppStatus,
    ClosedApp,
    ClosedAppView,
    ClosedPortalApp,
    ListingStatus,
    MediaMetadata,
    PostAdminAppView,
    PostAdminPortalApp,
)

from ...custom_models.v1.resources import AppUI, AppUIStatus
from .. import params
from .singletons import api_v1_integration, settings


def minio_session_dep(request: Request) -> MinioClientWrapper:
    minio_client_wrapper = MinioClientWrapper()
    minio_client_wrapper.minio_client = request.app.state.minio_session
    minio_client_wrapper.is_reachable = is_minio_reachable(request.app.state.minio_session)
    return minio_client_wrapper


def client_session_dep(request: Request) -> ClientSession:
    return request.app.state.client_session


def add_routes_admin(app, late_init):
    ##########################################################################################
    #                                                                                        #
    #                                    Portal Apps                                         #
    #                                                                                        #
    ##########################################################################################

    @app.post(
        "/portal-apps",
        summary="Create a new portal app with all dependancies except media data",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app: PostAdminPortalApp,
        external_ids: bool = params.external_ids,
        existing_portal_app: bool = params.existing_portal_app,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.post_admin_portal_app(
                    minio_client=minio_client,
                    post_portal_app=portal_app,
                    user_id=user_id,
                    external_ids=external_ids,
                    existing_portal_app=existing_portal_app,
                )

    @app.get(
        "/portal-apps/{portal_app_id}",
        summary="Get portal app by ID",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: str = params.portal_app_id,
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_closed_portal_app(minio_client, portal_app_id)

    @app.post(
        "/portal-apps/{portal_app_id}/app-views",
        summary="Create a new app view with all dependancies except media data",
        response_model=ClosedAppView,
        tags=["Portal App"],
    )
    async def _(
        app_view: PostAdminAppView,
        portal_app_id: UUID4 = params.portal_app_id,
        api_version: ApiVersion = params.api_version,
        existing_app: bool = params.existing_app,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.post_admin_app_view(
                    minio_client=minio_client,
                    portal_app_id=portal_app_id,
                    user_id=user_id,
                    post_app_view=app_view,
                    active_api_version=api_version,
                    existing_app=existing_app,
                )

    @app.delete(
        "/portal-apps/{portal_app_id}",
        summary="Delete a portal app with all related metadata",
        response_model=bool,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.delete_admin_portal_app(portal_app_id)

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        summary="Add media to portal app view",
        response_model=ClosedAppView,
        tags=["Portal App"],
    )
    async def _(
        media_metadata: MediaMetadata = Form(...),
        media_file: UploadFile = File(...),
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        purpose: AppMediaPurpose = params.purpose,
        index: int = params.index,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_portal_app_view_media(
                    minio_client,
                    portal_app_id,
                    app_view_id,
                    purpose,
                    index,
                    media_metadata.caption,
                    media_metadata.alternative_text,
                    media_file,
                    is_admin=True,
                )

    @app.delete(
        "/portal-apps/{portal_app_id}/media/{media_id}",
        summary="Delete media by ID",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        media_id: UUID4 = params.media_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        raise HTTPException(status_code=501, detail="Not implemented")

    @app.put(
        "/portal-apps/{portal_app_id}/status",
        summary="Set status of portal app",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        status: ListingStatus,
        comment: str = None,
        portal_app_id: UUID4 = params.portal_app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client.put_admin_set_status_portal_app(
                    minio_client, portal_app_id, status, comment, user_id
                )

    @app.put(
        "/portal-apps/{portal_app_id}/app-views/{app_view_id}/status",
        summary="Set status for a portal app view",
        response_model=ClosedAppView,
        tags=["Portal App"],
    )
    async def _(
        status: AppStatus,
        portal_app_id: UUID4 = params.portal_app_id,
        app_view_id: UUID4 = params.app_view_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                return await client.app_client._update_status_app_view(
                    minio_client, portal_app_id, app_view_id, status=status
                )

    @app.put(
        "/portal-apps/{portal_app_id}/organization-logo",
        summary="Set or update a logo for a given organization id",
        tags=["Portal App"],
    )
    async def _(
        media_file: UploadFile = File(...),
        portal_app_id: str = params.portal_app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        if media_file.content_type not in SUPPORTED_IMAGE_CONTENT_TYPES:
            raise HTTPException(status_code=415, detail=f"Unsupported media type: {media_file.content_type}")

        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            portal_app = await client.app_client.get_raw_portal_app(portal_app_id=portal_app_id)
            if portal_app is None:
                raise HTTPException(status_code=404, detail=f"No valid portal app for ID {portal_app_id}.")

            org_logo_key = f"{portal_app_id}_vendor_logo"
            data = io.BytesIO(await media_file.read())
            put_minio_media(minio_client_wrapper=minio_client, object_name=org_logo_key, data=data)

    ##########################################################################################
    #                                                                                        #
    #                                           Apps                                         #
    #                                                                                        #
    ##########################################################################################

    @app.get(
        "/apps/{app_id}",
        summary="Get an existing app by ID",
        response_model=str,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        raise HTTPException(status_code=501, detail="Not implemented")

    @app.delete(
        "/apps/{app_id}",
        summary="Delete an existing app",
        response_model=str,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        raise HTTPException(status_code=501, detail="Not implemented")

    @app.put(
        "/apps/{app_id}/app-ui-bundles",
        summary="Update app ui with zipped ap ui bundle",
        response_model=AppUI,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        app_ui_file: UploadFile = File(...),
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        if app_ui_file.content_type not in SUPPORTED_ARCHIVE_CONTENT_TYPES:
            raise HTTPException(status_code=400, detail=f"Content type {app_ui_file.content_type} not supported.")

        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.app_client.conn.transaction():
                app_ui_bundle = await client.app_client.get_or_create_app_ui_bundle(minio_client, app_id, user_id)

                app_ui_files_path = f"{settings.data_dir}/app_ui_bundles/{str(app_ui_bundle.id)}"
                Path(app_ui_files_path).expanduser().mkdir(parents=True, exist_ok=True)

                try:
                    file_as_bytes = io.BytesIO(app_ui_file.file.read())
                    with zipfile.ZipFile(file_as_bytes, "r") as zip_ref:
                        zip_ref.extractall(app_ui_files_path)
                except zipfile.BadZipFile as e:
                    log_and_raise_error(e, status_code=400, detail_message="Failed to unzip archive")

                app_ui_bundle = await client.app_client.update_app_ui_state(
                    app_ui_bundle.id, app_ui_files_path, AppUIStatus.UNZIP_FINISHED
                )
                return app_ui_bundle

    @app.put(
        "/apps/{app_id}/status",
        summary="Change status of an existing app",
        response_model=ClosedApp,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        raise HTTPException(status_code=501, detail="Not implemented")

    ##########################################################################################
    #                                                                                        #
    #                                    Licenses                                            #
    #                                                                                        #
    ##########################################################################################

    @app.post(
        "/licenses",
        summary="Grant licenses",
        response_model=OrganizationLicenseList,
        tags=["Licenses"],
    )
    async def _(
        licenses: PostOrganizationLicenseList,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.license_client.conn.transaction():
                return await client.license_client.post_licenses(licenses, user_id)

    @app.put(
        "/licenses/query",
        summary="Query licenses",
        response_model=OrganizationLicenseList,
        tags=["Licenses"],
    )
    async def _(
        query: LicenseQuery,
        skip: int = None,
        limit: int = None,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.license_client.conn.transaction():
                return await client.license_client.query_licenses(query=query, skip=skip, limit=limit)

    @app.get(
        "/licenses/{license_id}",
        summary="Add license for organization",
        response_model=OrganizationLicense,
        tags=["Licenses"],
    )
    async def _(
        license_id: UUID4 = params.license_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.license_client.conn.transaction():
                return await client.license_client.get_license(license_id=license_id)

    @app.put(
        "/licenses/{license_id}/revoke",
        summary="Revoke license for organization",
        response_model=OrganizationLicense,
        tags=["Licenses"],
    )
    async def _(
        license_id: UUID4 = params.license_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.license_client.conn.transaction():
                return await client.license_client.revoke_license(license_id=license_id)

    @app.put(
        "/licenses/{license_id}/activate",
        summary="Activate a revoked license for organization",
        response_model=OrganizationLicense,
        tags=["Licenses"],
    )
    async def _(
        license_id: UUID4 = params.license_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.license_client.conn.transaction():
                return await client.license_client.activate_license(license_id=license_id)

    ##########################################################################################
    #                                                                                        #
    #                                 Clearances                                             #
    #                                                                                        #
    ##########################################################################################

    @app.post(
        "/clearances",
        summary="Post a new exmpty clearance entry",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        post_clearance: AdminPostClearance,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.post_clearance(
                    post_clearance=post_clearance, user_id=user_id, creator_type=EditorType.ADMIN
                )

    # TODO: Make this a query route with possibility to query for organization_id?
    @app.get(
        "/clearances",
        summary="Get all clearance entries",
        response_model=List[Clearance],
        tags=["Clearances"],
    )
    async def _(
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.get_clearances()

    @app.get(
        "/clearances/{clearance_id}",
        summary="Get clearance entry by ID",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.get_clearance(clearance_id=clearance_id)

    @app.put(
        "/clearances/{clearance_id}/hide",
        summary="Hide clearance entry",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.update_clearance_hidden_status(
                    clearance_id=clearance_id, hide=True, user_type=EditorType.ADMIN, user_id=user_id
                )

    @app.put(
        "/clearances/{clearance_id}/unhide",
        summary="Unhide clearance entry",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.update_clearance_hidden_status(
                    clearance_id=clearance_id, hide=False, user_type=EditorType.ADMIN, user_id=user_id
                )

    @app.post(
        "/clearances/{clearance_id}/item",
        summary="Post clearance item by ID",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        post_clearance_item: ExtendedPostClearanceItem,
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                clearance_item = await client.clearance_client.post_clearance_item(
                    clearance_id=clearance_id,
                    post_clearance_item=post_clearance_item,
                    user_id=user_id,
                    creator_type=EditorType.ADMIN,
                )
                return await client.clearance_client.get_clearance(clearance_id=clearance_item.clearance_id)

    @app.get(
        "/clearances/{clearance_id}/item/{item_id}",
        summary="Get clearance item by ID",
        response_model=ClearanceItem,
        tags=["Clearances"],
    )
    async def _(
        clearance_id: UUID4 = params.clearance_id,
        item_id: UUID4 = params.clearance_item_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.get_clearance_item(
                    clearance_id=clearance_id, clearance_item_id=item_id
                )

    @app.put(
        "/clearances/{clearance_id}/activate",
        summary="Post clearance item by ID",
        response_model=Clearance,
        tags=["Clearances"],
    )
    async def _(
        post_item_activation: PostClearanceItemActivation,
        clearance_id: UUID4 = params.clearance_id,
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.clearance_client.conn.transaction():
                return await client.clearance_client.update_clearance_active_item(
                    clearance_id=clearance_id,
                    clearance_item_id=post_item_activation.active_item_id,
                    user_type=EditorType.ADMIN,
                    user_id=user_id,
                )

    ##########################################################################################
    #                                                                                        #
    #                                    Misc                                                #
    #                                                                                        #
    ##########################################################################################

    @app.put(
        "/truncate-db",
        summary="[CAUTION] Truncates all v1 portal apps db tables",
        response_model=bool,
        tags=["Misc"],
    )
    async def _(
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            async with conn.transaction():
                for table in TABLES_V1:
                    try:
                        await conn.execute(f"TRUNCATE TABLE {table} CASCADE;")
                    except Exception as e:
                        log_and_raise_error(e, 500, "Failed to truncate v1 db tables")

        delete_all_minio_media_objects(minio_client)
        return True

    @app.put(
        "/truncate-db-clearances",
        summary="[CAUTION] Truncates all clearance db tables",
        response_model=bool,
        tags=["Misc"],
    )
    async def _(
        user_id: str = Header(...),
        payload=api_v1_integration.admin_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            async with conn.transaction():
                for table in TABLES_V1_CLEARANCES:
                    try:
                        await conn.execute(f"TRUNCATE TABLE {table} CASCADE;")
                    except Exception as e:
                        log_and_raise_error(e, 500, "Failed to truncate v1 clearance db tables")

        return True
