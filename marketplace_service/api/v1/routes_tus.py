from ast import literal_eval
from pathlib import Path

from aiofile import AIOFile
from aiofiles.os import remove as async_remove
from fastapi import Header, HTTPException
from fastapi.responses import Response
from starlette.requests import Request

from marketplace_service.api.v1 import tus_utils
from marketplace_service.custom_models.commons import SUPPORTED_CI_FILE_EXTENSIONS, SUPPORTED_UI_FILE_EXTENSIONS
from marketplace_service.custom_models.v1.resources import AppUIStatus, ContainerImageStatus, UploadType
from marketplace_service.db import db_clients

from . import string_constants as sc
from .singletons import api_v1_integration, logger, settings


def add_routes_tus(app, late_init):
    @app.get(
        "/alive",
        summary="Alive",
        response_model=dict,
        tags=["Upload"],
    )
    async def _(
        payload=api_v1_integration.vendor_depends(),
    ):
        container_image_dir_exists = Path(settings.container_image_dir).exists()
        app_bundles_dir_exists = Path(settings.app_bundles_dir).exists()
        info_dir_exists = Path(settings.info_dir).exists()
        if not container_image_dir_exists or not app_bundles_dir_exists:
            return {
                "status": f"""
                container image dir exists: {container_image_dir_exists}.
                app bundles dir exists: {app_bundles_dir_exists}.
                info dir exists: {info_dir_exists}.
                """
            }
        else:
            return {"status": "ok"}

    @app.options(
        "/files/",
        summary="Options",
        tags=["Upload"],
    )
    async def _(
        payload=api_v1_integration.vendor_depends(),
    ):
        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = tus_utils.get_basic_header(allow_cors)

        headers["Tus-Version"] = sc.TUS_VERSION()
        headers["Tus-Max-Size"] = str(settings.tus_max_size)
        headers["Tus-Extension"] = sc.TUS_EXTENSIONS()
        return Response(headers=headers, status_code=204)

    @app.post(
        "/files",
        summary="Post",
        tags=["Upload"],
    )
    async def _(
        # only if server supports "Creation With Upload" extension
        content_length: int = Header(None),
        # total file size
        upload_length: int = Header(None),
        upload_metadata: str = Header(None),
        tus_resumable: str = Header(...),
        # non-tus headers below.
        metadata_encoding: str = Header(None),
        payload=api_v1_integration.vendor_depends(),
    ):
        logger.debug(
            f"header IN - POST: "
            f" --- {content_length}"
            f" --- {upload_length}"
            f" --- {upload_metadata}"
            f" --- {tus_resumable}"
            f" --- {metadata_encoding}"
        )

        info_dir = settings.info_dir
        data_dir = settings.data_dir

        meta_data = tus_utils.parse_metadata(upload_metadata, metadata_encoding)

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = tus_utils.get_basic_header(allow_cors)

        if upload_length > int(settings.tus_max_size):
            raise HTTPException(
                status_code=400,
                detail=f"File size exceeds maximum (File size: {upload_length} | Max: {settings.tus_max_size}).",
            )

        if "type" not in meta_data:
            raise HTTPException(
                status_code=400,
                detail="Key 'type' in 'Upload-Metadata' Headeris required",
            )

        upload_types = [member.value for member in UploadType]
        if meta_data["type"] not in upload_types:
            raise HTTPException(
                status_code=400,
                detail=f"Value for key 'type' in 'Upload-Metadata' Headeris must be in [{upload_types}]",
            )

        if "file_name" not in meta_data:
            raise HTTPException(
                status_code=400,
                detail="Key 'file_name' in 'Upload-Metadata' Headeris required",
            )

        if "id" not in meta_data:
            raise HTTPException(
                status_code=400,
                detail="Key 'id' in 'Upload-Metadata' Header required",
            )

        try:
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)

                if meta_data["type"] == UploadType.CONTAINER_IMAGE:
                    valid_file_extension = (
                        len([ext for ext in SUPPORTED_CI_FILE_EXTENSIONS if meta_data["file_name"].endswith(ext)]) > 0
                    )
                    if not valid_file_extension:
                        raise HTTPException(status_code=400, detail="Invalid file extension for container image.")

                    await client.app_client.container_image_exists(meta_data["id"])
                    await client.app_client.set_container_image_status(
                        container_image_id=meta_data["id"], status=ContainerImageStatus.UPLOAD_PENDING
                    )

                if meta_data["type"] == UploadType.APP_UI_BUNDLE:
                    valid_file_extension = (
                        len([ext for ext in SUPPORTED_UI_FILE_EXTENSIONS if meta_data["file_name"].endswith(ext)]) > 0
                    )
                    if not valid_file_extension:
                        raise HTTPException(status_code=400, detail="Invalid file extension for app UI bundle.")

                    await client.app_client.app_ui_bundle_exists(meta_data["id"])
                    await client.app_client.set_app_ui_bundle_status(meta_data["id"], AppUIStatus.UPLOAD_PENDING)

            upload_dir, filename = tus_utils.get_upload_dir_and_filename(data_dir, meta_data)

            file_id = await tus_utils.register_new_upload(
                info_dir,
                data_dir,
                upload_dir,
                filename,
                upload_length,
                meta_data,
            )
        except tus_utils.FileAlreadyExistsAtGivenPath as e:
            raise HTTPException(
                status_code=409, detail="File mentioned in header [Upload-Metadata].file_name already exists."
            ) from e
        except tus_utils.IdAlreadyExistsAtGivenPath as e:
            raise HTTPException(status_code=409, detail="Provided Id already exists.") from e
        except tus_utils.ForbiddenCharSequenceInGivenPath as e:
            raise HTTPException(
                status_code=422, detail="Forbidden char sequence in upload [Upload-Metadata].file_name detected."
            ) from e

        host = settings.host
        headers["Location"] = f"{host}/v1/vendor/files/{file_id}"

        return Response(headers=headers, status_code=201, content=None)

    @app.head(
        "/files/{file_id}",
        summary="Head",
        tags=["Upload"],
    )
    async def _(file_id: str, tus_resumable: str = Header(...), payload=api_v1_integration.vendor_depends()):
        logger.debug(f"header IN - HEAD: --- {tus_resumable}")

        info_dir = settings.info_dir
        data_dir = settings.data_dir
        info_file = Path(f"{info_dir}/{file_id}.info")

        if not info_file.is_file():
            raise HTTPException(status_code=404, detail=f"Ressource {file_id} does not exist.")

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = tus_utils.get_basic_header(allow_cors)

        async with AIOFile(info_file, "r") as f:
            info_file_content = await f.read()

        info_file_content = literal_eval(info_file_content)
        meta_data = info_file_content["MetaData"]
        _sub_dir, _filename = tus_utils.get_upload_dir_and_filename(
            data_dir,
            meta_data,
        )

        offset_serverside = info_file_content["Offset"]
        upload_length = info_file_content["Size"]
        headers["Upload-Offset"] = str(offset_serverside)
        headers["Upload-Length"] = str(upload_length)
        return Response(headers=headers, status_code=200, content=None)

    @app.patch(
        "/files/{file_id}",
        summary="Patch",
        tags=["Upload"],
    )
    async def _(
        request: Request,
        file_id: str,
        content_type: str = Header(None),
        content_length: int = Header(None),
        upload_offset: int = Header(None),
        tus_resumable: str = Header(...),
        payload=api_v1_integration.vendor_depends(),
    ):
        logger.debug(
            f"header IN - PATCH: "
            f" --- {content_type}"
            f" --- {content_length}"
            f" --- {upload_offset}"
            f" --- {tus_resumable}"
        )

        info_dir = settings.info_dir
        data_dir = settings.data_dir
        info_file = Path(f"{info_dir}/{file_id}.info")

        if not info_file.is_file():
            raise HTTPException(status_code=404, detail=f"Ressource {file_id} does not exist.")

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = tus_utils.get_basic_header(allow_cors)

        if content_type != "application/offset+octet-stream":
            raise HTTPException(status_code=415, detail="Content type [application/offset+octet-stream] required.")

        async with AIOFile(info_file, "r") as f:
            info_file_content = await f.read()

        info_file_content = literal_eval(info_file_content)
        meta_data = info_file_content["MetaData"]
        sub_dir, filename = tus_utils.get_upload_dir_and_filename(data_dir, meta_data)

        offset_serverside = info_file_content["Offset"]
        if offset_serverside != upload_offset:
            raise HTTPException(status_code=409, detail="Invalid Offset.")

        body = b""
        async for chunk in request.stream():
            body += chunk

        new_offset = await tus_utils.update_upload(
            info_dir,
            data_dir,
            sub_dir,
            filename,
            file_id,
            info_file_content,
            body,
        )

        upload_length = info_file_content["Size"]

        if new_offset == upload_length:
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)

                if meta_data["type"] == UploadType.CONTAINER_IMAGE:
                    await client.app_client.set_container_image_status(
                        container_image_id=meta_data["id"],
                        status=ContainerImageStatus.UPLOAD_FINISHED,
                        temp_directory=f"{data_dir}/{sub_dir}/{filename}",
                    )

                if meta_data["type"] == UploadType.APP_UI_BUNDLE:
                    await client.app_client.set_app_ui_bundle_status(meta_data["id"], AppUIStatus.UPLOAD_FINISHED)

        headers["Upload-Offset"] = str(new_offset)
        return Response(headers=headers, status_code=204, content=None)

    @app.delete(
        "/files/{file_id}",
        summary="Options",
        tags=["Upload"],
    )
    async def _(
        file_id: str,
        payload=api_v1_integration.vendor_depends(),
    ):
        info_dir = settings.info_dir
        data_dir = settings.data_dir
        info_file = Path(f"{info_dir}/{file_id}.info")

        if not info_file.is_file():
            raise HTTPException(status_code=404, detail=f"Ressource {file_id} does not exist.")

        async with AIOFile(info_file, "r") as f:
            info_file_content = await f.read()

        info_file_content = literal_eval(info_file_content)
        meta_data = info_file_content["MetaData"]
        sub_dir, filename = tus_utils.get_upload_dir_and_filename(data_dir, meta_data)

        if filename:
            await async_remove(Path(data_dir).joinpath(f"{sub_dir}{filename}"))
        else:
            await async_remove(Path(data_dir).joinpath(file_id))
        await async_remove(info_file)

        return Response(status_code=200)
