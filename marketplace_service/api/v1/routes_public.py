from typing import Annotated

from fastapi import Depends, Header
from pydantic import UUID4
from starlette.requests import Request

from marketplace_service.clients.minio import is_minio_reachable
from marketplace_service.custom_models.commons import MinioClientWrapper
from marketplace_service.custom_models.v1.clearances import ClearanceQuery, PublicAggregatedClearanceList
from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import (
    PortalAppQuery,
    PublicPortalApp,
    PublicPortalAppList,
    StatisticsPage,
)

from .. import params
from .singletons import api_v1_integration


def minio_session_dep(request: Request) -> MinioClientWrapper:
    minio_client_wrapper = MinioClientWrapper()
    minio_client_wrapper.minio_client = request.app.state.minio_session
    minio_client_wrapper.is_reachable = is_minio_reachable(request.app.state.minio_session)
    return minio_client_wrapper


def add_routes_public(app, late_init):
    @app.put(
        "/portal-apps/query",
        summary="Query all accepted portal apps",
        response_model=PublicPortalAppList,
        tags=["Portal App"],
    )
    async def _(
        query: PortalAppQuery,
        skip: int = None,
        limit: int = None,
        statistics_id: Annotated[UUID4 | None, Header()] = None,
        payload=api_v1_integration.public_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)

            if statistics_id is not None:
                await client.statistics_client.register(
                    statistics_id=statistics_id, page=StatisticsPage.PUBLIC_MARKETPLACE
                )

            return await client.app_client.put_query_public_portal_apps(minio_client, query, skip, limit)

    @app.get(
        "/portal-apps/{portal_app_id}",
        summary="Get accepted portal app by ID",
        response_model=PublicPortalApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        payload=api_v1_integration.public_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_public_portal_app(minio_client, portal_app_id)

    @app.put(
        "/portal-apps/{portal_app_id}/related-portal-apps",
        summary="Get related portal apps",
        response_model=PublicPortalAppList,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        skip: int = 0,
        limit: int = 10,
        payload=api_v1_integration.public_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.app_client.get_related_public_portal_apps(minio_client, portal_app_id, skip, limit)

    @app.put(
        "/clearances/query",
        summary="Query all active clearances",
        response_model=PublicAggregatedClearanceList,
        tags=["Clearances"],
    )
    async def _(
        query: ClearanceQuery,
        skip: int = None,
        limit: int = None,
        statistics_id: Annotated[UUID4 | None, Header()] = None,
        payload=api_v1_integration.public_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)

            if statistics_id is not None:
                await client.statistics_client.register(
                    statistics_id=statistics_id, page=StatisticsPage.PUBLIC_AI_REGISTER
                )

            return await client.clearance_client.query_clearances(clearance_query=query, skip=skip, limit=limit)
