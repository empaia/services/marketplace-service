from pathlib import Path
from typing import List

from aiohttp import ClientSession
from fastapi import Depends, Header, HTTPException
from fastapi.responses import FileResponse, StreamingResponse
from PIL import Image
from pydantic import UUID4
from starlette.requests import Request

from marketplace_service.clients.minio import get_image_data, is_minio_reachable, key_exists
from marketplace_service.clients.vault_client import get_app_config
from marketplace_service.custom_models.commons import MinioClientWrapper
from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import (
    AppConfiguration,
    AppUiConfiguration,
    ClosedApp,
    ClosedAppView,
    ClosedPortalApp,
    ClosedPortalAppList,
    CustomerAppViewQuery,
    CustomerPortalAppQuery,
)

from .. import params
from .singletons import api_v1_integration, logger, settings


def minio_session_dep(request: Request) -> MinioClientWrapper:
    minio_client_wrapper = MinioClientWrapper()
    minio_client_wrapper.minio_client = request.app.state.minio_session
    minio_client_wrapper.is_reachable = is_minio_reachable(request.app.state.minio_session)
    return minio_client_wrapper


def client_session_dep(request: Request) -> ClientSession:
    return request.app.state.client_session


def add_routes_customer(app, late_init):
    @app.put(
        "/portal-apps/query",
        summary="Query portal app for the customers organization",
        response_model=ClosedPortalAppList,
        tags=["Portal App"],
    )
    async def _(
        query: CustomerPortalAppQuery,
        skip: int = None,
        limit: int = None,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            licensed_portal_app_ids = await client.license_client.get_licensed_portal_app_for_orga(organization_id)
            return await client.app_client.put_query_closed_portal_app(
                minio_client, query, licensed_portal_app_ids, skip, limit
            )

    @app.get(
        "/portal-apps/{portal_app_id}",
        summary="Get portal app for the customers organization",
        response_model=ClosedPortalApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: str = params.portal_app_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=portal_app_id
            )
            return await client.app_client.get_closed_portal_app(minio_client, portal_app_id)

    @app.get(
        "/portal-apps/{portal_app_id}/active-app",
        summary="Get an app by portal app ID including all technical data",
        response_model=ClosedApp,
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=portal_app_id
            )
            return await client.app_client.get_active_app(portal_app_id)

    @app.get(
        "/portal-apps/{portal_app_id}/apps",
        summary="Get all apps connected to a portal app",
        response_model=List[ClosedApp],
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: UUID4 = params.portal_app_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=portal_app_id
            )
            return await client.app_client.get_apps(portal_app_id=portal_app_id)

    @app.get(
        "/portal-apps/{portal_app_id}/organization-logo",
        summary="Set or update a logo for a given organization id",
        tags=["Portal App"],
    )
    async def _(
        portal_app_id: str = params.portal_app_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=portal_app_id
            )

            portal_app = await client.app_client.get_raw_portal_app(portal_app_id=portal_app_id)
            if portal_app is None:
                raise HTTPException(status_code=404, detail=f"No valid portal app for ID {portal_app_id}.")

            org_logo_key = f"{portal_app_id}_vendor_logo"
            if not key_exists(minio_client_wrapper=minio_client, key=org_logo_key):
                raise HTTPException(status_code=404, detail=f"Organization logo does not exist.")

            image_data = get_image_data(minio_client_wrapper=minio_client, key=org_logo_key)
            # determine the media type with pillow
            image = Image.open(image_data)
            image_format = image.format
            image_data.seek(0)

            return StreamingResponse(image_data, media_type=f"image/{image_format.lower()}")

    @app.put(
        "/app-views/query",
        summary="Query app views by app IDs and API version",
        response_model=List[ClosedAppView],
        tags=["App views"],
    )
    async def _(
        query: CustomerAppViewQuery,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
        minio_client: MinioClientWrapper = Depends(minio_session_dep),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app_views = await client.app_client.query_app_views(minio_client=minio_client, query=query)
            licensed_portal_apps = await client.license_client.get_licensed_portal_app_for_orga(organization_id)
            result_app_views = [v for v in app_views if str(v.portal_app_id) in licensed_portal_apps]
            return result_app_views

    @app.get(
        "/apps/{app_id}",
        summary="Get app by ID",
        response_model=ClosedApp,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=app.portal_app_id
            )
            return app

    @app.get(
        "/apps/{app_id}/config",
        summary="Get the configuration of an existing app",
        response_model=AppConfiguration,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        organization_id: str = Header(...),
        http_client: ClientSession = Depends(client_session_dep),
        payload=api_v1_integration.customer_vault_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=app.portal_app_id
            )
            config = await get_app_config(
                app_id=str(app_id),
                app_namespace=app.ead["namespace"],
                http_client=http_client,
                customer_id=organization_id,
            )
            return config

    @app.get(
        "/apps/{app_id}/app-ui-config",
        summary="Get the configuration of an app ui",
        response_model=AppUiConfiguration,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            app = await client.app_client.get_app_by_id(app_id)
            await client.license_client.organization_may_access_portal_app(
                organization_id=organization_id, portal_app_id=app.portal_app_id
            )
            return app.app_ui_configuration

    @app.get(
        "/apps/{app_id}/app-ui-files/{resource_filename:path}",
        summary="Obtain app UI files",
        response_class=FileResponse,
        tags=["App"],
    )
    async def _(
        app_id: UUID4 = params.app_id,
        resource_filename: str = params.resource_filename,
        organization_id: str = Header(...),
        payload=api_v1_integration.customer_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            if "index.htm" in resource_filename:
                # only check first file (index.htm/html) for app license
                app = await client.app_client.get_app_by_id(app_id)
                await client.license_client.organization_may_access_portal_app(
                    organization_id=organization_id, portal_app_id=app.portal_app_id
                )

            app_ui_bundle = await client.app_client.get_app_ui_bundle_for_app(app_id)

            if app_ui_bundle is None:
                raise HTTPException(status_code=404, detail=f"No valid app UI bundle for app ID {app_id}")

            app_file_path = f"{settings.data_dir}/app_ui_bundles/{str(app_ui_bundle.id)}"
            full_path = await get_valid_file_path(
                app_file_path=app_file_path, filename=resource_filename, app_id=app_id
            )
            response = FileResponse(full_path)
            return response

    async def get_valid_file_path(app_file_path: str, filename: str, app_id: UUID4):
        base_app_file_path = Path(app_file_path).resolve()
        if not base_app_file_path.exists():
            raise HTTPException(status_code=404, detail=f"No app UI resources for app ID {app_id}.")

        if filename is None or filename == "":
            if base_app_file_path.joinpath("index.html").exists():
                filename = "index.html"
            elif base_app_file_path.joinpath("index.htm").exists():
                filename = "index.htm"
            else:
                raise HTTPException(status_code=404, detail="No index file found for app UI ressource.")

        full_path = base_app_file_path.joinpath(filename).resolve()
        if not str(full_path).startswith(app_file_path):
            logger.debug(f"Tried to access a resource outside of app UI bundle responsibility at {full_path}")
            raise HTTPException(status_code=404, detail="Access to requested resource path forbidden")

        if not full_path.exists():
            raise HTTPException(status_code=404, detail=f"Resource {filename} does not exist for app ID {app_id}.")

        return full_path
