from fastapi import Header

from marketplace_service.db import db_clients
from marketplace_service.models.marketplace.app import StatisticsList

from .singletons import api_v1_integration


def add_routes_moderator(app, late_init):
    @app.get(
        "/statistics",
        summary="Get all pending portal apps for given organization",
        response_model=StatisticsList,
        tags=["Statistics"],
    )
    async def _(
        user_id: str = Header(...),
        payload=api_v1_integration.moderator_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.statistics_client.get_statistics()
