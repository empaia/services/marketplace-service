from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    disable_openapi: bool = False
    root_path: str = None
    cors_allow_origins: Set[str] = None
    debug: bool = False
    db_host: str = "localhost"
    db_port: str = 5432
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "mps"
    api_v1_integration: str = None
    enable_vendor_routes: bool = False
    enable_reviewer_routes: bool = False
    enable_admin_routes: bool = False
    enable_product_provider_routes: bool = False
    connection_timeout: int = 300
    vault_url: str = None
    vault_secret_engine: str = None
    vault_role_id: str = None
    vault_secret_id: str = None
    minio_url: str = None
    minio_external_url: str = None
    minio_access_key: str = None
    minio_bucket: str = None
    minio_secret_key: str = None
    minio_secure: bool = False
    portal_url: str = None
    tus_max_size: int = 10240000000
    info_dir: str = "/mps/info"
    data_dir: str = "/mps/data"
    host: str = "http://localhost:8000"

    # daemon
    daemon_poll_interval: int = 15
    docker_registry: str = ""
    docker_username: str = ""
    docker_password: str = ""

    model_config = SettingsConfigDict(env_file=".env", env_prefix="MPS_", extra="ignore")
