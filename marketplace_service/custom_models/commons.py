from enum import Enum

from minio import Minio
from pydantic import conint

ItemCount = conint(ge=0)

SUPPORTED_MANUAL_CONTENT_TYPES = [
    "application/pdf",  # PDF .pdf
    # "application/rtf",  # Rich text format .rtf
    # # MS Word .doc/.docx
    # "application/msword",
    # "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    # # MS PowerPoint .ppt/.pptx
    # "application/vnd.ms-powerpoint",
    # "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    # # OpenDocument .odt/.odp
    # "application/vnd.oasis.opendocument.text",
    # "application/vnd.oasis.opendocument.presentation",
]

SUPPORTED_IMAGE_CONTENT_TYPES = ["image/bmp", "image/jpeg", "image/png"]

SUPPORTED_ARCHIVE_CONTENT_TYPES = [
    "application/zip",
    "application/x-zip-compressed",
    # "application/x-7z-compressed",
    # "application/x-rar-compressed",
    # "application/octet-stream",
    "multipart/x-zip",
]

SUPPORTED_UI_FILE_EXTENSIONS = [".zip"]

SUPPORTED_CI_FILE_EXTENSIONS = [".tar", ".tar.gz"]

EAD_SCHEMA_V3 = "ead-schema.v3.json"
EAD_SCHEMA_DRAFT3 = "ead-schema.v1-draft3.json"
EAD_SCHEMA_DRAFT3_ALT = "ead-app-schema-draft-3.json"


class MinioClientWrapper:
    is_reachable: bool
    minio_client: Minio


class TagGroup(str, Enum):
    TISSUE = "tissues"
    STAIN = "stains"
    ANALYSIS = "analysis"
    INDICATION = "indications"
    CLEARANCE = "clearances"
    IMAGE_TYPE = "image_types"
    PROCEDURE = "procedures"
    SCANNER = "scanners"

    @classmethod
    def value_of(cls, group_name) -> str:
        for key, val in cls.__members__.items():
            if key == group_name:
                return val
        else:
            raise ValueError(f"Invalid tag group name: {group_name}")

    @classmethod
    def enum_of(cls, value):
        for key, val in cls.__members__.items():
            if val == value:
                return key
        else:
            raise ValueError(f"Invalid enum value: {value}")
