from sqlite3 import Timestamp
from typing import List, Optional

from pydantic import UUID4, Field, conlist

from marketplace_service.custom_models.commons import ItemCount
from marketplace_service.models.commons import RestrictedBaseModel
from marketplace_service.models.v3.commons import Id


class PostOrganizationLicense(RestrictedBaseModel):
    organization_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Organization ID")
    portal_app_id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Portal App ID")


class PostOrganizationLicenseList(RestrictedBaseModel):
    items: List[PostOrganizationLicense]


class OrganizationLicense(PostOrganizationLicense):
    id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="ID of the license")
    active: bool = Field(examples=[True], description="Flag that indicates if license ia active")
    creator_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Creator ID")
    created_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")


class OrganizationLicenseList(RestrictedBaseModel):
    items: List[OrganizationLicense]
    item_count: ItemCount = Field(
        examples=[1],
        description="Total number of licenses matching the query",
    )


class LicenseQuery(RestrictedBaseModel):
    organizations: Optional[conlist(Id, min_length=1)] = Field(
        default=None,
        examples=[["b10648a7-340d-43fc-a2d9-4d91cc86f33f"]],
        description="List of organization IDs",
    )
    portal_apps: Optional[conlist(Id, min_length=1)] = Field(
        default=None,
        examples=[["b10648a7-340d-43fc-a2d9-4d91cc86f33f"]],
        description="List of portal app IDs",
    )
