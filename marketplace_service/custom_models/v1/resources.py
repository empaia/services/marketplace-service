from enum import Enum
from sqlite3 import Timestamp
from typing import List, Optional

from pydantic import UUID4, Field

from marketplace_service.models.commons import RestrictedBaseModel
from marketplace_service.models.marketplace.app import AppUiConfiguration


class ContainerImageStatus(str, Enum):
    INITIALIZED = "INITIALIZED"
    UPLOAD_PENDING = "UPLOAD_PENDING"
    UPLOAD_FINISHED = "UPLOAD_FINISHED"
    UPLOAD_ERROR = "UPLOAD_ERROR"  # TODO: is this needed?
    PUSH_INITIATED = "PUSH_INITIATED"
    PUSH_FINISHED = "PUSH_FINISHED"
    PUSH_ERROR = "PUSH_ERROR"
    MARKED_FOR_DELETION = "MARKED_FOR_DELETION"


class AppUIStatus(str, Enum):
    INITIALIZED = "INITIALIZED"
    UPLOAD_PENDING = "UPLOAD_PENDING"
    UPLOAD_FINISHED = "UPLOAD_FINISHED"
    UNZIP_INITIATED = "UNZIP_INITIATED"
    UNZIP_FINISHED = "UNZIP_FINISHED"


class UploadType(str, Enum):
    CONTAINER_IMAGE = "CONTAINER_IMAGE"
    APP_UI_BUNDLE = "APP_UI_BUNDLE"


class ContainerImageCore(RestrictedBaseModel):
    name: str = Field(examples=["PD-L1_v1"], description="Qualified name of the container image")
    status: ContainerImageStatus = Field(
        examples=[ContainerImageStatus.UPLOAD_PENDING],
        description="Upload/push status of the container image",
    )
    error_message: Optional[str] = Field(
        examples=["Failed to push to registry"], description="Error message that is written out by the MPS daemon"
    )
    registry_image_url: Optional[str] = Field(
        examples=["https://registry.gitlab.com/empaia/integration/ap_xyz"],
        description="Url to the container image in the registry",
    )
    creator_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Creator ID")


class InternalContainerImage(ContainerImageCore):
    id: Optional[UUID4] = Field(
        examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="ID of the container image"
    )
    temp_directory: Optional[str] = Field(
        examples=["path/to/container-image"], description="Internal path to the container image"
    )
    organization_id: Optional[str] = Field(
        examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Organization ID of the creator"
    )


class ContainerImage(ContainerImageCore):
    id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="ID of the container image")
    connected_apps: Optional[List[str]] = Field(
        examples=[["b10648a7-340d-43fc-a2d9-4d91cc86f33f"]],
        description="List of app ids using the given container image",
    )
    created_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")


class AppUICore(RestrictedBaseModel):
    name: str = Field(examples=["PD-L1_UserInferface_v1"], description="Qualified name of the app UI")
    status: AppUIStatus = Field(
        examples=[AppUIStatus.UPLOAD_PENDING],
        description="Upload/unzip status of the app UI",
    )
    app_ui_configuration: Optional[AppUiConfiguration] = Field(examples=[{}], description="App UI configuration")
    app_ui_url: Optional[str] = Field(
        examples=["http://app1.emapaia.org"], description="Url where the app UI is located"
    )
    creator_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Creator ID")


class InternalAppUI(AppUICore):
    temp_directory: Optional[str] = Field(
        examples=["path/to/container-image"], description="Internal path to the container image"
    )
    organization_id: Optional[str] = Field(
        examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Organization ID of the creator"
    )


class AppUI(AppUICore):
    id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="ID of the app UI")
    connected_apps: Optional[List[str]] = Field(
        examples=[["b10648a7-340d-43fc-a2d9-4d91cc86f33f"]], description="List of app ids using the given app ui"
    )
    created_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")


class AppUIMetadata(RestrictedBaseModel):
    name: str = Field(examples=["PD-L1_UserInferface_v1"], description="Qualified name of the app UI")
    app_ui_configuration: AppUiConfiguration
