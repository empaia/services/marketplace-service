from enum import Enum
from typing import Dict, List, Optional

from pydantic import UUID4, Field, HttpUrl, ValidationInfo, field_validator

from marketplace_service.custom_models.commons import ItemCount
from marketplace_service.models.commons import RestrictedBaseModel
from marketplace_service.models.marketplace.app import AppTag, PostAppTag, TagList, TextTranslation
from marketplace_service.models.v3.commons import Timestamp


class EditorType(str, Enum):
    USER = "USER"
    ADMIN = "ADMIN"


class InfoProvider(str, Enum):
    EMPAIA = "EMPAIA"
    ORGANIZATION = "ORGANIZATION"


class DatePrecision(str, Enum):
    DATE = "DATE"
    MONTH_YEAR = "MONTH_YEAR"
    QUARTER = "QUARTER"
    YEAR = "YEAR"
    TBA = "TBA"
    UNKNOWN = "UNKNOWN"


class InfoSourceType(str, Enum):
    EMAIL_CONTACT = "EMAIL_CONTACT"
    PUBLIC_WEBSITE = "PUBLIC_WEBSITE"
    UNKNOWN = "UNKNOWN"


class ExtendedTagList(TagList):
    image_types: List[AppTag] = Field(default=[], description="List of image types used for analysis")
    procedures: List[AppTag] = Field(default=[], description="List of histopathological preparation procedures")
    scanners: List[AppTag] = Field(default=[], description="List of supported WSI scanners")


class InfoSourceContentCore(RestrictedBaseModel):
    reference_url: HttpUrl = Field(examples=["https://info.example.org"], description="Url of the info source content")


class PostInfoSourceContent(InfoSourceContentCore):
    label: Optional[Dict[str, str]] = Field(
        default=None,
        examples=[{"EN": "Label in english", "DE": "Label auf Deutsch"}],
        description="Info source content label",
    )


class InfoSourceContent(InfoSourceContentCore):
    label: Optional[List[TextTranslation]] = Field(default=None, description="Info source content label")


class ClearanceItemCore(RestrictedBaseModel):
    product_name: str = Field(examples=["Product name"], description="Brand and product name")
    tag_remark: Optional[str] = Field(
        default=None,
        examples=["Some text concerning tags"],
        description="Additional information on product tags that can not be expressed through existing tags",
    )
    product_release_at: Timestamp = Field(
        examples=[1598611645], description="UNIX timestamp in seconds - set by server"
    )
    product_release_at_precision: DatePrecision = Field(
        examples=[DatePrecision.QUARTER], description="Date precision type"
    )
    info_source_type: InfoSourceType = Field(
        examples=[InfoSourceType.EMAIL_CONTACT], description="Source type where the product info was obtained"
    )


class PostClearanceItem(ClearanceItemCore):
    description: Dict[str, str] = Field(
        examples=[{"EN": "Description in english", "DE": "Beschreibung auf Deutsch"}], description="Description"
    )
    tags: Optional[List[PostAppTag]] = None
    info_source_content: Optional[List[PostInfoSourceContent]] = Field(
        default=None,
        description="List of info source contents",
    )

    @field_validator("info_source_content")
    def validate_info_source_content(cls, info_source_content, info: ValidationInfo):
        source_type = info.data.get("info_source_type")
        if source_type == InfoSourceType.PUBLIC_WEBSITE and info_source_content is None:
            raise ValueError("Info source content filed must be provided for info source type 'PUBLIC_WEBSITE'")
        if source_type in [InfoSourceType.EMAIL_CONTACT, InfoSourceType.UNKNOWN] and info_source_content is not None:
            raise ValueError("Info source content not allowed for info source type 'EMAIL_CONTACT' or 'UNKNOWN'")
        return info_source_content


class ExtendedPostClearanceItem(PostClearanceItem):
    info_provider: InfoProvider = Field(
        examples=[InfoProvider.ORGANIZATION], description="Provider of the clearance/product info"
    )
    info_updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")

    @field_validator("info_provider")
    def validate_info_source_type(cls, info_provider, info: ValidationInfo):
        info_source_type = info.data.get("info_source_type")
        if info_provider == InfoProvider.EMPAIA and info_source_type is InfoSourceType.UNKNOWN:
            raise ValueError(
                "Info source type for provider 'EMPAIA' must be either 'PUBLIC_WEBSITE' or 'EMAIL_CONTACT'"
            )
        if info_provider == InfoProvider.ORGANIZATION and info_source_type is InfoSourceType.EMAIL_CONTACT:
            raise ValueError(
                "Info source type for provider 'ORGANIZATION' must be either 'UNKNOWN' or 'PUBLIC_WEBSITE'"
            )
        return info_provider


class ClearanceItem(ClearanceItemCore):
    id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Clearance item ID")
    clearance_id: UUID4 = Field(
        examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Corrsponding clearance ID"
    )
    description: Optional[List[TextTranslation]] = Field(default=None, description="Description")
    tags: Optional[ExtendedTagList] = None
    info_updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    info_provider: InfoProvider = Field(
        examples=[InfoProvider.ORGANIZATION], description="Provider of the clearance/product info"
    )
    info_source_content: Optional[List[InfoSourceContent]] = Field(
        default=None,
        description="List of info source contents",
    )
    creator_type: EditorType = Field(examples=[EditorType.USER], description="Creator type")
    creator_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Creator ID")
    created_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")


class PostClearanceCore(RestrictedBaseModel):
    is_hidden: bool = Field(examples=[True], description="Flag that indicates if clearance is hidden")


class PostClearance(PostClearanceCore):
    clearance_item: Optional[PostClearanceItem] = Field(
        default=None, description="When given, an initial active clearance item is posted"
    )


class ExtendedPostClearance(PostClearance):
    organization_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Organization ID")
    clearance_item: Optional[ExtendedPostClearanceItem] = Field(
        default=None, description="When given, an initial active clearance item is posted"
    )


class AdminPostClearance(ExtendedPostClearance):
    pass


class PostClearanceItemActivation(RestrictedBaseModel):
    active_item_id: UUID4 = Field(
        examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Clearance item ID to activate"
    )


class Clearance(PostClearanceCore):
    id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Clearance ID")
    organization_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Organization ID")
    active_clearance_item: Optional[ClearanceItem] = Field(default=None, description="Active clearance item ID")
    history: Optional[List[ClearanceItem]] = None
    updater_type: EditorType = Field(examples=[EditorType.USER], description="Updater type")
    updater_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Updater ID")
    updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    creator_type: EditorType = Field(examples=[EditorType.USER], description="Creator type")
    creator_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Creator ID")
    created_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")


class PublicAggregatedClearance(ClearanceItemCore):
    id: UUID4 = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Clearance ID")
    organization_id: str = Field(examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="Organization ID")
    description: Optional[List[TextTranslation]] = Field(default=None, description="Description")
    tags: Optional[ExtendedTagList] = None
    info_updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    info_provider: InfoProvider = Field(
        examples=[InfoProvider.ORGANIZATION], description="Provider of the clearance/product info"
    )
    info_source_content: Optional[List[InfoSourceContent]] = Field(
        default=None,
        description="List of info source contents",
    )
    # modified dates
    updated_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")
    created_at: Timestamp = Field(examples=[1598611645], description="UNIX timestamp in seconds - set by server")


class PublicAggregatedClearanceList(RestrictedBaseModel):
    items: List[PublicAggregatedClearance]
    item_count: ItemCount = Field(
        examples=[1],
        description="Total number of clearances matching the query",
    )


class ClearanceQuery(RestrictedBaseModel):
    organizations: Optional[List[str]] = Field(
        default=None, examples=[["b10648a7-340d-43fc-a2d9-4d91cc86f33f"]], description="Filter option for organizations"
    )
    tissues: Optional[List[str]] = Field(
        default=None, examples=[["SKIN", "BREAST"]], description="Filter option for tissue types"
    )
    stains: Optional[List[str]] = Field(
        default=None, examples=[["H_AND_E", "PHH3"]], description="Filter option for stain types"
    )
    indications: Optional[List[str]] = Field(
        default=None, examples=[["MELANOMA", "PROSTATE_CANCER"]], description="Filter option for indication types"
    )
    analysis: Optional[List[str]] = Field(
        default=None, examples=[["GRADING", "QUANTIFICATION"]], description="Filter option for analysis types"
    )
    clearances: Optional[List[str]] = Field(
        default=None, examples=[["CE_IVD", "CE_IVDR"]], description="Filter option for clearance/certification types"
    )
    image_types: Optional[List[str]] = Field(
        default=None, examples=[["WSI", "ROI"]], description="Filter option for image types used for analysis"
    )
    procedures: Optional[List[str]] = Field(
        default=None,
        examples=[["BIOPSY", "RESECTION"]],
        description="Filter option for histopathological preparation procedures",
    )
    scanners: Optional[List[str]] = Field(
        default=None,
        examples=[["LEICA_APERIO_AT2", "3DHISTECH_PANNORAMIC_480_DX"]],
        description="Filter option for WSI scanner",
    )
