import asyncio
from datetime import datetime, timedelta
from pathlib import Path

import aiodocker
import asyncpg

from marketplace_service.custom_models.v1.resources import ContainerImageStatus

from ..db import db_clients
from ..singletons import settings
from .singletons import logger

loop = asyncio.get_event_loop()


async def get_image_name_or_id(response):
    if len(response) >= 1 and "stream" in response[0]:
        stream = str(response[0]["stream"])
        if "Loaded image ID:" in stream:
            return stream.replace("Loaded image ID: ", "").replace("\n", "")
        if "Loaded image:" in stream:
            return stream.replace("Loaded image: ", "").replace("\n", "")
    return None


async def get_auth_config():
    if settings.docker_username and settings.docker_password:
        auth_config = {"username": settings.docker_username, "password": settings.docker_password}
        return auth_config
    return {}


async def docker_push(
    pool: asyncpg.Pool,
    docker_client: aiodocker.Docker,
    container_image_id: str,
    organization_id: str,
    image_directory: str,
):
    logger.debug("Executing coroutine: Pushing image to registry")
    async with pool.acquire() as conn:
        clients = await db_clients(conn=conn)

        if not Path(image_directory).exists():
            logger.error("Error while pushing to registry: Image file does not exist")
            await clients.app_client.set_container_image_status(
                container_image_id=container_image_id,
                status=ContainerImageStatus.PUSH_ERROR,
                error_message="Image file does not exist",
            )
            return

        try:
            reg_image_name = f"{settings.docker_registry.rstrip('/')}/{organization_id}/{container_image_id}"

            with open(image_directory, "rb") as f:
                loaded_image = await docker_client.images.import_image(f)
                loaded_image_name = await get_image_name_or_id(loaded_image)

                if not loaded_image_name:
                    logger.error("Error while pushing to registry: Failed to import docker image to local registry")
                    raise Exception("Failed to import docker image to local registry")

                image = None
                try:
                    image = await docker_client.images.inspect(reg_image_name)
                except aiodocker.DockerError:
                    pass

                if image:
                    logger.error("Error while pushing to registry: Image already exists in docker registry")
                    raise Exception("Image already exists in docker registry")

                image_name, tag = loaded_image_name.split(":")
                await docker_client.images.tag(name=image_name, repo=reg_image_name, tag=tag)

                logger.debug(f"Pushing image to {reg_image_name}")
                auth_config = await get_auth_config()
                await docker_client.images.push(name=reg_image_name, tag=tag, auth=auth_config)

                await clients.app_client.set_container_image_status(
                    container_image_id=container_image_id,
                    status=ContainerImageStatus.PUSH_FINISHED,
                    registry_image_url=f"{reg_image_name}:{tag}",
                )
        except aiodocker.DockerError as e1:
            logger.error(f"Error while pushing to registry: {e1}")
            await clients.app_client.set_container_image_status(
                container_image_id=container_image_id, status=ContainerImageStatus.PUSH_ERROR, error_message=e1
            )
        except Exception as e2:
            logger.error(f"Error while pushing to registry: {e2}")
            await clients.app_client.set_container_image_status(
                container_image_id=container_image_id, status=ContainerImageStatus.PUSH_ERROR, error_message=e2
            )


async def docker_delete(
    pool: asyncpg.Pool, docker_client: aiodocker.Docker, container_image_id: str, registry_image_url: str
):
    if not registry_image_url:
        logger.debug("No registry image URL for container image")
    else:
        logger.debug(f"Delete container image for URL {registry_image_url}")
        try:
            await docker_client.images.delete(name=registry_image_url)
        except aiodocker.DockerError as e:
            logger.error(f"Failed to delete docker image at {registry_image_url}: {e}")
            return

    async with pool.acquire() as conn:
        clients = await db_clients(conn=conn)
        await clients.app_client.delete_container_image(container_image_id=container_image_id)


async def run_async(docker_client: aiodocker.Docker):
    logger.debug("Sleep %d seconds...", settings.daemon_poll_interval)
    await asyncio.sleep(settings.daemon_poll_interval)

    pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )

    while True:
        async with pool.acquire() as conn:
            clients = await db_clients(conn=conn)

            # set status for uploaded container images to push initiated
            uploaded_container_images = await clients.app_client.get_internal_container_images(
                organization_id=None, status=ContainerImageStatus.UPLOAD_FINISHED
            )

            for uci in uploaded_container_images:
                _ = await clients.app_client.set_container_image_status(
                    container_image_id=uci.id, status=ContainerImageStatus.PUSH_INITIATED
                )
                # start background task to push images when upload is finished
                asyncio.run_coroutine_threadsafe(
                    docker_push(
                        pool=pool,
                        docker_client=docker_client,
                        container_image_id=uci.id,
                        organization_id=uci.organization_id,
                        image_directory=uci.temp_directory,
                    ),
                    loop,
                )

            # reset old DB entries
            upload_pending = await clients.app_client.get_container_images(
                organization_id=None, status=ContainerImageStatus.UPLOAD_PENDING
            )
            push_initiated = await clients.app_client.get_container_images(
                organization_id=None, status=ContainerImageStatus.PUSH_INITIATED
            )
            pending_container_images = [*upload_pending, *push_initiated]
            best_before = datetime.now() - timedelta(minutes=1)  # TODO: configure via env
            for pci in pending_container_images:
                # TODO: check best before
                if best_before > pci.updated_at:
                    await clients.app_client.set_container_image_status(
                        container_image_id=pci.id,
                        status=ContainerImageStatus.INITIALIZED,
                        temp_directory="",
                        registry_image_url="",
                        error_message="",
                    )

            # delete images marked for deletion
            to_delete = await clients.app_client.get_container_images(
                organization_id=None, status=ContainerImageStatus.MARKED_FOR_DELETION
            )
            logger.debug(f"Images marked to delete: {to_delete}")
            for dci in to_delete:
                # TODO: do checks if deletion is valid
                asyncio.run_coroutine_threadsafe(
                    docker_delete(
                        pool=pool,
                        docker_client=docker_client,
                        container_image_id=dci.id,
                        registry_image_url=dci.registry_image_url,
                    ),
                    loop,
                )

        logger.debug("Sleep %d seconds...", settings.daemon_poll_interval)
        await asyncio.sleep(settings.daemon_poll_interval)


def run():
    docker_client = aiodocker.Docker()
    try:
        loop.run_until_complete(run_async(docker_client))
    finally:
        loop.run_until_complete(docker_client.close())
        loop.close()
