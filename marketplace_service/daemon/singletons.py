import logging

from ..singletons import settings

logging.basicConfig()
if settings.debug:
    logging.root.setLevel(logging.DEBUG)
else:
    logging.root.setLevel(logging.INFO)
logger = logging.getLogger("MPSD")
