import requests

from ..singletons import settings


def test_read_secret(valid_vault_mutate_client_token):
    # post secret with ROOT token
    secret_id = "SECRET_ID"
    payload = {"data": {"SECRET_KEY": "SECRET DATA"}}
    root_headers = {"X-Vault-Token": settings.vault_root_token, "X-Vault-Request": "true"}
    _r1 = requests.post(
        f"{settings.vault_url}/v1/secret/data/{secret_id}",
        json=payload,
        headers=root_headers,
    )

    # get secret with client token; this token lease should be expired by now
    client_headers = {"X-Vault-Token": valid_vault_mutate_client_token, "X-Vault-Request": "true"}
    response = requests.get(f"{settings.vault_url}/v1/secret/data/{secret_id}", headers=client_headers)
    assert response.raise_for_status
    assert response.status_code == 403

    # login again
    data = {"role_id": "role-id-ci-tests", "secret_id": "secret-id-ci-tests"}
    r = requests.post(f"{settings.vault_url}/v1/auth/approle/login", json=data)
    client_token = r.json()["auth"]["client_token"]

    client_headers = {"X-Vault-Token": client_token, "X-Vault-Request": "true"}
    response = requests.get(f"{settings.vault_url}/v1/secret/data/{secret_id}", headers=client_headers)
    assert response.raise_for_status
    assert response.json()["data"]["data"] == {"SECRET_KEY": "SECRET DATA"}


def test_write_secret_failed(valid_vault_mutate_client_token):
    secret_id = "SECRET_ID2"
    payload = {"data": {"SECRET_KEY": "SECRET DATA"}}

    # write secret with client token must fail
    client_headers = {"X-Vault-Token": valid_vault_mutate_client_token, "X-Vault-Request": "true"}
    response = requests.post(
        f"{settings.vault_url}/v1/secret/data/{secret_id}",
        json=payload,
        headers=client_headers,
    )

    assert response.raise_for_status
    assert "permission denied" in response.json()["errors"][0]
