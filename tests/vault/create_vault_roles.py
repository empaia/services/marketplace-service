import requests

from ..singletons import settings

ENGINE = "secret"
ROOT_HEADERS = {"X-Vault-Token": settings.vault_root_token}


def create_valid_vault_mutate_client():
    policy_path = f"{ENGINE}/data/*"
    policy_capabilities = '{capabilities = ["create", "read", "update", "delete", "list"]}'
    policy_name = f"rw-{ENGINE}"
    policy_data = {"name": policy_name, "policy": f'path "{policy_path}" {policy_capabilities}'}

    role_name = "marketplace-service-rw"
    role_data = {"token_ttl": "5s", "token_max_ttl": "5s", "token_policies": [policy_name]}

    return creat_valut_client_token(policy_data, role_name, role_data)


def creat_valut_client_token(policy_data, role_name, role_data):
    policy_name = policy_data["name"]
    r = requests.put(f"{settings.vault_url}/v1/sys/policies/acl/{policy_name}", headers=ROOT_HEADERS, json=policy_data)

    # enable app role
    type_data = {"type": "approle"}
    r = requests.post(f"{settings.vault_url}/v1/sys/auth/approle", headers=ROOT_HEADERS, json=type_data)

    # create app role
    r = requests.post(f"{settings.vault_url}/v1/auth/approle/role/{role_name}", headers=ROOT_HEADERS, json=role_data)

    # create custom app role_id
    custom_role_id = "role-id-ci-tests"
    role_id_data = {"role_id": custom_role_id}
    r = requests.post(
        f"{settings.vault_url}/v1/auth/approle/role/{role_name}/role-id", headers=ROOT_HEADERS, json=role_id_data
    )

    # create custom secret
    custom_secret_id = "secret-id-ci-tests"
    secret_id_data = {"secret_id": custom_secret_id}
    r = requests.post(
        f"{settings.vault_url}/v1/auth/approle/role/{role_name}/custom-secret-id",
        headers=ROOT_HEADERS,
        json=secret_id_data,
    )

    # login and get client token
    login_data = {"role_id": custom_role_id, "secret_id": custom_secret_id}
    r = requests.post(f"{settings.vault_url}/v1/auth/approle/login", json=login_data)
    client_token = r.json()["auth"]["client_token"]

    return client_token
