from time import sleep

import requests

from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url, settings


def test_get_configuration_renew_client_token(
    default_admin_header, default_customer_header, admin_post_portal_app, valid_configuration
):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    # post vault secret
    post_vault_secret(raw_portal_app["active_app_views"]["v1"]["app"]["ead"]["namespace"], valid_configuration)

    # wait for token renewal
    sleep(6)

    # query app
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]
    r = requests.get(
        f"{mps_url}/v1/customer/apps/{app_id}/config",
        headers=default_customer_header,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app_id"] == app_id
    assert resp["global"] == valid_configuration["content"]
    assert resp["customer"] == {}


def post_vault_secret(secret_id: str, configuration):
    payload = {"data": configuration["content"]}
    headers = {"X-Vault-Token": settings.vault_root_token, "X-Vault-Request": "true"}
    _r1 = requests.post(
        f"{settings.vault_url}/v1/secret/data/{secret_id}/global",
        json=payload,
        headers=headers,
    )
