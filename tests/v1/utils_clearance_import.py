import csv
import datetime
from pathlib import Path

import requests

from marketplace_service.custom_models.v1.clearances import DatePrecision, InfoProvider, InfoSourceType

from ..singletons import mps_url


def read_csv_to_json(filepath: Path):
    data_dict = []
    with open(filepath, encoding="utf-8") as csv_file_handler:
        csv_reader = csv.DictReader(csv_file_handler, delimiter=";")
        for row in csv_reader:
            if row["Company"] == "":
                break
            data_dict.append(row)
    return data_dict


def create_clearance(default_admin_header, post_clearance, post_clearance_item):
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=post_clearance, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item", json=post_clearance_item, headers=default_admin_header
    )
    assert r.status_code == 200
    resp = r.json()
    return clearance_id, resp


def import_clearances_from_csv(default_admin_header, filepath: Path):
    clearance_dict = read_csv_to_json(filepath)

    for clearance_record in clearance_dict:
        clearance = {"organization_id": clearance_record["Company"], "is_hidden": False}

        tags = []
        for tag_group in ["TISSUE", "PROCEDURE", "INDICATION", "ANALYSIS", "STAIN", "CLEARANCE", "IMAGE_TYPE"]:
            for tag_name in str(clearance_record[tag_group]).split(","):
                sanitized_tag = tag_name.strip()
                if sanitized_tag == "":
                    continue
                tags.append({"tag_group": tag_group, "tag_name": tag_name.lstrip().rstrip()})

        desc = clearance_record["Description (optional)"]
        date_precision = DatePrecision(str(clearance_record["product_release_at_precision"]).strip())

        release_date_timestamp = 0
        if date_precision in [DatePrecision.DATE, DatePrecision.QUARTER, DatePrecision.YEAR]:
            release_date_timestamp = int(
                datetime.datetime.strptime(clearance_record["product_release_at"], "%Y-%m-%d").timestamp()
            )

        info_source_type = InfoSourceType(str(clearance_record["Source"]).strip())
        clearance_item = {
            "product_name": clearance_record["Algorithm/Product name"],
            "tags": tags,
            "description": {"EN": desc, "DE": desc},  # DE is set to english description for now!
            "product_release_at_precision": date_precision,
            "product_release_at": release_date_timestamp,
            "info_updated_at": int(datetime.datetime.now().timestamp()),  # TODO: should this be sourced from csv?
            "info_provider": InfoProvider.EMPAIA,
            "info_source_type": info_source_type,
        }

        if info_source_type == InfoSourceType.PUBLIC_WEBSITE:
            references = str(clearance_record["Source content/References"]).split(",")
            source_content = [{"reference_url": ref} for ref in references]
            clearance_item["info_source_content"] = source_content

        _, _ = create_clearance(default_admin_header, clearance, clearance_item)
