import copy
import json
from uuid import uuid4

import requests

from marketplace_service.models.marketplace.app import ListingStatus
from tests.generate_testdata import generate_valid_header_with_organization, generate_valid_v1_post_app
from tests.utils import generate_random_namespace, get_app_with_valid_namespace
from tests.v1.utils import create_monochrome_image

from ..singletons import mps_url


def create_portal_app_with_app_view():
    headers = generate_valid_header_with_organization()
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=headers)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views", headers=headers)
    assert r.status_code == 200
    app_view_id = r.json()["id"]

    return headers, portal_app_id, app_view_id


def create_app_view_with_minimum_metadata_set():
    headers = generate_valid_header_with_organization()
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=headers)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views", headers=headers)
    assert r.status_code == 200
    app_view_id = r.json()["id"]

    json_desc = {
        "name": "PD-L1 Quantifier",
        "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
    }
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        json=json_desc,
        headers=headers,
    )
    assert r.status_code == 200

    purpose = "banner"
    index = 1
    media_metadata = {"caption": {"EN": "test description"}, "alternative_text": {"EN": "alt text"}}
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image = create_monochrome_image(size=(24, 24), color="red", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/jpeg")}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    assert r.status_code == 200

    json_tags = [{"tag_group": "TISSUE", "tag_name": "SKIN"}, {"tag_group": "STAIN", "tag_name": "H_AND_E"}]
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", json=json_tags, headers=headers
    )
    resp = r.json()
    assert r.status_code == 200
    assert "id" in resp

    return headers, portal_app_id, app_view_id


def create_app_with_all_required_data_set():
    headers, portal_app_id, app_view_id = create_app_view_with_minimum_metadata_set()
    post_app_data = get_app_with_valid_namespace(generate_valid_v1_post_app())

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=post_app_data, headers=headers)
    assert r.status_code == 200
    app_id = r.json()["id"]

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/apps/{app_id}",
        headers=headers,
    )
    assert r.status_code == 200

    return headers, portal_app_id, app_view_id, app_id


def create_app_with_approval_request():
    headers, portal_app_id, app_view_id, app_id = create_app_with_all_required_data_set()

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/request-approval",
        headers=headers,
    )
    assert r.status_code == 200

    return headers, portal_app_id, app_view_id, app_id


def post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app, tags=None):
    if tags:
        post_portal_app["tags"] = tags

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    params = {"status": ListingStatus.LISTED, "comment": "Reviewed by admin"}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/status",
        params=params,
        headers=default_admin_header,
    )
    assert r.status_code == 200

    return portal_app_id, r.json()


def post_admin_portal_app_with_fresh_ids_and_namespace(default_admin_header, post_portal_app):
    portal_app_id = str(uuid4())

    post_portal_app_copy = copy.deepcopy(post_portal_app)
    post_portal_app_copy["id"] = portal_app_id

    for version in ["v1", "v2", "v3"]:
        new_namespace = generate_random_namespace(version)
        post_portal_app_copy["active_apps"][version]["ead"]["namespace"] = new_namespace
        post_portal_app_copy["active_apps"][version]["id"] = str(uuid4())

    params = {"existing_portal_app": False, "external_ids": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app_copy, params=params, headers=default_admin_header
    )
    assert r.status_code == 200
    return portal_app_id, r.json()


def post_licensed_portal_app_as_admin(default_admin_header, v1_admin_post_portal_app, organization_id):
    post_portal_app = copy.deepcopy(v1_admin_post_portal_app)

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()

    # grant license for portal app to organization
    data = {"items": [{"organization_id": organization_id, "portal_app_id": resp["id"]}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    return resp["id"], resp


def create_clearance_with_active_item(
    default_admin_header, post_clearance_item, organization_id="test-orga-id", is_hidden=False
):
    clearance = {"organization_id": organization_id, "is_hidden": is_hidden}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item", json=post_clearance_item, headers=default_admin_header
    )
    assert r.status_code == 200
    resp = r.json()
    return clearance_id, resp["active_clearance_item"]
