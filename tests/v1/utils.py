import io
import random
import string
import subprocess
import tempfile
import time
import zipfile
from pathlib import Path

from PIL import Image

from marketplace_service.models.marketplace.app import AppMediaPurpose


def assert_portal_app_responses(resp: dict, resp2: dict):
    assert_json_eq_exclude_path(resp, resp2, "media")
    if "media" in resp and resp["media"] is not None:
        assert resp["media"]["banner"][0]["id"] == resp["media"]["banner"][0]["id"]
        assert resp["media"]["banner"][0]["index"] == resp["media"]["banner"][0]["index"]
        assert resp["media"]["banner"][0]["caption"] == resp["media"]["banner"][0]["caption"]
        assert resp["media"]["banner"][0]["alt_text"] == resp["media"]["banner"][0]["alt_text"]
        assert resp["media"]["banner"][0]["content_type"] == resp["media"]["banner"][0]["content_type"]
        assert resp["media"]["banner"][0]["internal_path"] == resp["media"]["banner"][0]["internal_path"]


def assert_portal_app_view_media(resp: dict, purpose: AppMediaPurpose, index: int, content_type: str):
    assert resp["media"][purpose][index]["content_type"] == content_type
    assert "presigned_media_url" in resp["media"][purpose][index]
    assert "resized_presigned_media_urls" in resp["media"][purpose][index]
    assert resp["media"][purpose][index]["resized_presigned_media_urls"] is not None
    assert resp["media"][purpose][index]["resized_presigned_media_urls"]["w60"] is not None
    assert resp["media"][purpose][index]["resized_presigned_media_urls"]["w400"] is not None
    assert resp["media"][purpose][index]["resized_presigned_media_urls"]["w800"] is not None
    assert resp["media"][purpose][index]["resized_presigned_media_urls"]["w1200"] is not None


def assert_json_eq_exclude_path(json_response_a: dict, json_response_b: dict, exclude_key: str):
    if exclude_key is None:
        assert json_response_a == json_response_b

    if exclude_key not in json_response_a.keys():
        assert json_response_a == json_response_b

    for j_key in json_response_a.keys():
        if exclude_key == j_key:
            assert True  # pass
        else:
            assert json_response_a[j_key] == json_response_b[j_key]


def create_file(filename: str, content_len: int, content: str = None):
    path = Path(filename)
    if content is None:
        content = random_string(content_len)
    path.touch()
    path.write_text(content)
    file_size = -1
    while file_size != path.stat().st_size:
        file_size = path.stat().st_size
        time.sleep(0.1)
    out = cmdline(f'bash -c "md5sum {path}"')
    checksum = out.split(" ")[0]
    return filename.split("/")[-1], path, content_len, content, checksum


def cmdline(command: str) -> str:
    process = subprocess.Popen(args=command, stdout=subprocess.PIPE, shell=True)
    return process.communicate()[0].decode("utf-8")


def random_string(stringLength: int) -> str:
    return "".join(random.choice(string.ascii_uppercase + string.digits) for _ in range(stringLength))


def create_binary_zipped_ui_bundle(compression):
    filename = "index.html"
    html_string = """
        <!DOCTYPE html>
        <html>
            <body>
                <h1>heading</h1>
                <p>test text.</p>
            </body>
        </html>
    """
    html_as_bytes = io.BytesIO(html_string.encode("utf-8"))
    html_file = html_as_bytes.getvalue()
    html_as_bytes.close()
    zipped_file = zip_file(filename, html_file, compression)
    return zipped_file


def zip_file(filename, file, compression=zipfile.ZIP_DEFLATED):
    mem_zip = io.BytesIO()

    with zipfile.ZipFile(mem_zip, mode="w", compression=compression) as zf:
        zf.writestr(filename, file)

    return mem_zip.getvalue()


def create_monochrome_image(size: tuple, color="red", image_format="JPEG"):
    img = Image.new("RGB", size, color=color)
    temp_file = tempfile.NamedTemporaryFile(suffix=f".{image_format.lower()}")
    img.save(temp_file, format=image_format)
    return temp_file
