import copy
from uuid import uuid4

import requests

from marketplace_service.models.marketplace.app import AppConfigurationType
from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_get_valid_configuration_only_global(
    admin_post_portal_app,
    valid_v1_post_app,
    valid_configuration,
    default_admin_header,
    default_customer_header,
    default_vendor_header,
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v1"]

    portal_app_id, _ = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )

    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=default_vendor_header
    )
    resp = r.json()
    assert r.status_code == 200
    app_id = resp["id"]

    r = requests.post(
        f"{mps_url}/v1/vendor/apps/{app_id}/config/{AppConfigurationType.GLOBAL.value}",
        json=valid_configuration,
        headers=default_vendor_header,
    )
    assert r.status_code == 200

    valid_configuration_2 = copy.deepcopy(valid_configuration)
    del valid_configuration_2["content"]["config_int"]
    valid_configuration_2["content"]["new_key"] = "new_value"
    valid_configuration_2["content"]["config_float"] = 9.99

    customer_id = default_customer_header["organization-id"]

    r = requests.post(
        f"{mps_url}/v1/vendor/apps/{app_id}/config/{AppConfigurationType.CUSTOMER.value}/{customer_id}",
        json=valid_configuration_2,
        headers=default_vendor_header,
    )
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/customer/apps/{app_id}/config",
        headers=default_customer_header,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app_id"] == app_id
    assert resp["global"] == valid_configuration["content"]
    assert resp["customer"] == valid_configuration_2["content"]
    for conf in ["global", "customer"]:
        assert "'config_bool': 1" not in str(resp[conf])
        assert "'config_bool': True" in str(resp[conf])


def test_get_invalid_configuration(default_customer_header):
    invalid_app_id = str(uuid4())
    r = requests.get(
        f"{mps_url}/v1/customer/apps/{invalid_app_id}/config",
        headers=default_customer_header,
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid app for ID {invalid_app_id}."


def test_get_empty_config_for_existing_app(
    admin_post_portal_app, valid_v1_post_app, default_admin_header, default_customer_header, default_vendor_header
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v1"]

    portal_app_id, _ = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )

    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=default_vendor_header
    )
    resp = r.json()
    assert r.status_code == 200
    app_id = resp["id"]

    r = requests.get(
        f"{mps_url}/v1/customer/apps/{app_id}/config",
        headers=default_customer_header,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app_id"] == app_id
    assert resp["global"] == {}
    assert resp["customer"] == {}
