import sys
import time
import uuid
from pathlib import Path

import pytest
import requests
from tusclient import client

from marketplace_service.custom_models.v1.resources import UploadType

from ..singletons import mps_url
from . import utils


def test_init_test_directory(init_test_dir):
    assert True


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_simple_upload_invalid_id(default_vendor_header, test_dir):
    try:
        file_name, path, _, _, _ = utils.create_file(f"{test_dir}/test_02_1_{utils.random_string(10)}.tar.gz", 32000)
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=10240,
            metadata={
                "type": UploadType.CONTAINER_IMAGE,
                "id": str(uuid.uuid4()),
                "file_name": file_name,
            },
        )
        uploader.upload()
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        # Container app ID does not exist
        assert str(e) == "Attempt to retrieve create file url with status 404"


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_simple_upload_invalid_type(default_vendor_header, test_dir):
    try:
        file_name, path, _, _, _ = utils.create_file(f"{test_dir}/test_02_1_{utils.random_string(10)}.tar.gz", 32000)
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=10240,
            metadata={
                "type": "INVALID_TYPE",
                "id": str(uuid.uuid4()),
                "file_name": file_name,
            },
        )
        uploader.upload()
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        # Upload type is not valid
        assert str(e) == "Attempt to retrieve create file url with status 400"


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_simple_upload_non_existant_file_path(default_vendor_header):
    file_name = "file.tar.gz"
    path = "does/not/exist/file.tar.gz"
    try:
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=10240,
            metadata={
                "type": UploadType.CONTAINER_IMAGE,
                "id": str(uuid.uuid4()),
                "file_name": file_name,
            },
        )
        uploader.upload()
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        # Upload type is not valid
        assert str(e) == f"invalid file {path}"


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_upload_invalid_file_extension_ci(default_vendor_header, test_dir):
    file_name, path, _, _, _ = utils.create_file(f"{test_dir}/test_02_1_{utils.random_string(10)}.txt", 32000)
    try:
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=10240,
            metadata={
                "type": UploadType.CONTAINER_IMAGE,
                "id": str(uuid.uuid4()),
                "file_name": file_name,
            },
        )
        uploader.upload()
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        # Upload type is not valid
        assert str(e) == "Attempt to retrieve create file url with status 400"


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_upload_invalid_file_extension_ui(default_vendor_header, test_dir):
    file_name, path, _, _, _ = utils.create_file(f"{test_dir}/test_02_1_{utils.random_string(10)}.txt", 32000)
    try:
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=10240,
            metadata={
                "type": UploadType.APP_UI_BUNDLE,
                "id": str(uuid.uuid4()),
                "file_name": file_name,
            },
        )
        uploader.upload()
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        # Upload type is not valid
        assert str(e) == "Attempt to retrieve create file url with status 400"


@pytest.mark.skip("Not working")
def test_start_stop_resume_upload(default_vendor_header):
    post_container_image = {"container_image_name": "Testname"}

    r = requests.post(f"{mps_url}/v1/vendor/container-images", data=post_container_image, headers=default_vendor_header)
    assert r.status_code == 200
    resp = r.json()
    container_image_id = resp["id"]

    try:
        filename = "test.tar"
        resource_file = Path(sys.path[0]).joinpath(f"tests/resources/{filename}")
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=resource_file,
            chunk_size=2000,
            metadata={
                "type": UploadType.CONTAINER_IMAGE,
                "id": container_image_id,
                "file_name": filename,
            },
        )
        uploader.upload(stop_at=16000)
        time.sleep(0.5)
        uploader.upload(stop_at=48000)
        time.sleep(0.5)
        uploader.upload()
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        # Upload type is not valid
        assert str(e) == "Attempt to retrieve create file url with status 400"


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_upload_container_image(default_vendor_header):
    post_container_image = {"container_image_name": "Testname"}

    r = requests.post(f"{mps_url}/v1/vendor/container-images", data=post_container_image, headers=default_vendor_header)
    assert r.status_code == 200
    resp = r.json()
    container_image_id = resp["id"]

    try:
        filename = "test.tar"
        resource_file = Path(sys.path[0]).joinpath(f"tests/resources/{filename}")
        my_client = client.TusClient(url=f"{mps_url}/v1/vendor/files", headers=default_vendor_header)
        uploader = my_client.uploader(
            file_path=resource_file,
            chunk_size=10248,
            metadata={
                "type": UploadType.CONTAINER_IMAGE,
                "id": container_image_id,
                "file_name": filename,
            },
        )
        uploader.upload()

        # daemon tick and push to registry should usually take no longer than 5 secs
        # TODO: add dynamic wait here
        time.sleep(5)

        r = requests.get(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
        assert r.status_code == 200
        result_list = r.json()

        for container_image in result_list:
            if container_image["id"] == container_image_id:
                assert container_image["status"] == "PUSH_FINISHED"
                assert container_image_id in container_image["registry_image_url"]
                assert default_vendor_header["organization-id"] in container_image["registry_image_url"]
    except Exception as e:
        print(f"Exception occured during upload: {e}")
        assert False


def test_teardown_test_dir(teardown_test_dir):
    assert True
