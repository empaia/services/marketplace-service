from uuid import uuid4

import requests

from ..singletons import mps_url


def test_query_portal_apps():
    statistics_id = str(uuid4())
    headers = {"statistics-id": statistics_id}
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={}, headers=headers)
    assert r.status_code == 200

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={}, headers=headers)
    assert r.status_code == 200

    user_id = str(uuid4())
    headers = {"user-id": user_id}
    r = requests.get(f"{mps_url}/v1/moderator/statistics", headers=headers)
    assert r.status_code == 200

    data = r.json()
    assert len(data["items"]) == data["item_count"]

    matched_items = []
    for item in data["items"]:
        if item["statistics_id"] == statistics_id:
            matched_items.append(item)

    assert len(matched_items) == 1
    item = matched_items[0]
    assert item["accessed_at"] > 0
    assert item["page"] == "PUBLIC_MARKETPLACE"


def test_query_market_survey():
    statistics_id = str(uuid4())
    headers = {"statistics-id": statistics_id}
    r = requests.put(f"{mps_url}/v1/public/clearances/query", json={}, headers=headers)
    assert r.status_code == 200

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json={}, headers=headers)
    assert r.status_code == 200

    user_id = str(uuid4())
    headers = {"user-id": user_id}
    r = requests.get(f"{mps_url}/v1/moderator/statistics", headers=headers)
    assert r.status_code == 200

    data = r.json()
    assert len(data["items"]) == data["item_count"]

    matched_items = []
    for item in data["items"]:
        if item["statistics_id"] == statistics_id:
            matched_items.append(item)

    assert len(matched_items) == 1
    item = matched_items[0]
    assert item["accessed_at"] > 0
    assert item["page"] == "PUBLIC_AI_REGISTER"
