import copy
from uuid import uuid4

import requests

from marketplace_service.custom_models.v1.licenses import OrganizationLicenseList
from tests.v1.creation_helper import post_admin_portal_app_with_fresh_ids_and_namespace

from ..singletons import mps_url


def test_post_single_license(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    data = {"items": [{"organization_id": "dummy-orga-id", "portal_app_id": portal_app_id}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    license_list = OrganizationLicenseList.model_validate(resp)
    assert license_list.items[0].organization_id == "dummy-orga-id"
    assert str(license_list.items[0].portal_app_id) == portal_app_id


def test_unique_constraint_violation_same_request(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    data = {
        "items": [
            {"organization_id": "dummy-orga-id", "portal_app_id": portal_app_id},
            {"organization_id": "dummy-orga-id", "portal_app_id": portal_app_id},
        ]
    }
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 400
    resp = r.json()
    assert resp["detail"] == "At least one combination of organiztion_id and portal_app_id is already present in the DB"


def test_query_licenses(default_admin_header, admin_post_portal_app):
    portal_app_id1, _ = post_admin_portal_app_with_fresh_ids_and_namespace(default_admin_header, admin_post_portal_app)
    portal_app_id2, _ = post_admin_portal_app_with_fresh_ids_and_namespace(default_admin_header, admin_post_portal_app)

    data = {
        "items": [
            {"organization_id": "dummy-orga-id1", "portal_app_id": portal_app_id1},
            {"organization_id": "dummy-orga-id1", "portal_app_id": portal_app_id2},
            {"organization_id": "dummy-orga-id2", "portal_app_id": portal_app_id1},
            {"organization_id": "dummy-orga-id2", "portal_app_id": portal_app_id2},
            {"organization_id": "dummy-orga-id3", "portal_app_id": portal_app_id1},
        ]
    }
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    r = requests.put(f"{mps_url}/v1/admin/licenses/query", json={}, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] > 5  # there can be licenses from previous tests

    r = requests.put(f"{mps_url}/v1/admin/licenses/query?skip=0&limit=4", json={}, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] > 4
    assert len(r.json()["items"]) == 4

    r = requests.put(f"{mps_url}/v1/admin/licenses/query?limit=4", json={}, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] > 4
    assert len(r.json()["items"]) == 4

    query = {"organizations": ["dummy-orga-id1", "dummy-orga-id2", "dummy-orga-id3"]}
    r = requests.put(f"{mps_url}/v1/admin/licenses/query?skip=3", json=query, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] == 5
    assert len(r.json()["items"]) == 2

    query = {"organizations": ["dummy-orga-id1"]}
    r = requests.put(f"{mps_url}/v1/admin/licenses/query", json=query, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {"organizations": ["dummy-orga-id1", "dummy-orga-id3"]}
    r = requests.put(f"{mps_url}/v1/admin/licenses/query", json=query, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] == 3

    query = {"organizations": ["dummy-orga-id1", "dummy-orga-id3"], "portal_apps": [portal_app_id1]}
    r = requests.put(f"{mps_url}/v1/admin/licenses/query", json=query, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {"portal_apps": [portal_app_id2]}
    r = requests.put(f"{mps_url}/v1/admin/licenses/query", json=query, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {"organizations": ["dummy-orga-id1"], "portal_apps": [portal_app_id2]}
    r = requests.put(f"{mps_url}/v1/admin/licenses/query", json=query, headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1
    assert len(r.json()["items"]) == 1
    assert r.json()["items"][0]["organization_id"] == "dummy-orga-id1"
    assert r.json()["items"][0]["portal_app_id"] == portal_app_id2


def test_get_license(default_admin_header, admin_post_portal_app):
    portal_app_id, _ = post_admin_portal_app_with_fresh_ids_and_namespace(default_admin_header, admin_post_portal_app)

    data = {
        "items": [
            {"organization_id": "dummy-orga-id1", "portal_app_id": portal_app_id},
        ]
    }
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200
    license_resp = r.json()["items"][0]

    r = requests.get(f"{mps_url}/v1/admin/licenses/{license_resp['id']}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json() == license_resp


def test_get_non_existant_license(default_admin_header):
    license_id = str(uuid4())
    r = requests.get(f"{mps_url}/v1/admin/licenses/{license_id}", headers=default_admin_header)
    assert r.status_code == 404
    assert r.json()["detail"] == f"License with ID {license_id} does not exist"


def test_revoke_and_activate_license(default_admin_header, admin_post_portal_app):
    portal_app_id, _ = post_admin_portal_app_with_fresh_ids_and_namespace(default_admin_header, admin_post_portal_app)

    data = {
        "items": [
            {"organization_id": "dummy-orga-id1", "portal_app_id": portal_app_id},
        ]
    }
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200
    license_resp = r.json()["items"][0]

    r = requests.get(f"{mps_url}/v1/admin/licenses/{license_resp['id']}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active"] is True

    r = requests.put(f"{mps_url}/v1/admin/licenses/{license_resp['id']}/revoke", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active"] is False

    r = requests.get(f"{mps_url}/v1/admin/licenses/{license_resp['id']}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active"] is False

    r = requests.put(f"{mps_url}/v1/admin/licenses/{license_resp['id']}/activate", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active"] is True

    r = requests.get(f"{mps_url}/v1/admin/licenses/{license_resp['id']}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active"] is True
