import uuid
from io import BytesIO

import requests
from PIL import Image

from tests.v1.creation_helper import post_licensed_portal_app_as_admin
from tests.v1.utils import create_monochrome_image

from ..singletons import mps_url


def test_post_organization_logo(default_admin_header, default_customer_header, admin_post_portal_app):
    portal_app_id, _raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )

    for frmt in ["bmp", "png", "jpeg"]:
        tmp_image = create_monochrome_image(size=(24, 24), color="red", image_format=frmt)
        files = {"media_file": ("filename", open(tmp_image.name, "rb"), f"image/{frmt}")}

        # post image as admin
        r = requests.put(
            f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/organization-logo",
            files=files,
            headers=default_admin_header,
        )
        assert r.status_code == 200

        # retrieve image as customer
        r = requests.get(
            f"{mps_url}/v1/customer/portal-apps/{portal_app_id}/organization-logo", headers=default_customer_header
        )
        assert r.status_code == 200
        assert r.content is not None
        assert r.headers["content-type"] == f"image/{frmt}"
        image_stream = BytesIO(r.content)
        image = Image.open(image_stream)
        assert image.size == (24, 24)


def test_post_multiple_organization_logo(default_admin_header, default_customer_header, admin_post_portal_app):
    portal_app_id, _raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )

    for sxy in [128, 512, 1024]:
        tmp_image = create_monochrome_image(size=(sxy, sxy), color="red", image_format="jpeg")
        files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/jpeg")}

        r = requests.put(
            f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/organization-logo",
            files=files,
            headers=default_admin_header,
        )
        assert r.status_code == 200

        r = requests.get(
            f"{mps_url}/v1/customer/portal-apps/{portal_app_id}/organization-logo", headers=default_customer_header
        )
        assert r.status_code == 200
        assert r.content is not None
        assert r.headers["content-type"] == "image/jpeg"
        image_stream = BytesIO(r.content)
        image = Image.open(image_stream)
        assert image.size == (sxy, sxy)


def test_post_organization_logo_invalid(admin_post_portal_app, default_admin_header):
    tmp_image = create_monochrome_image(size=(8, 8), color="red", image_format="jpeg")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/jpeg")}
    portal_app_id = str(uuid.uuid4())
    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/organization-logo", files=files, headers=default_admin_header
    )
    assert r.status_code == 404

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    tmp_image = create_monochrome_image(size=(24, 24), color="red", image_format="gif")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/gif")}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/organization-logo", files=files, headers=default_admin_header
    )
    assert r.status_code == 415
    assert r.json()["detail"] == "Unsupported media type: image/gif"


def test_get_organization_logo_non_existant(default_admin_header, default_customer_header, admin_post_portal_app):
    portal_app_id = str(uuid.uuid4())
    r = requests.get(
        f"{mps_url}/v1/customer/portal-apps/{portal_app_id}/organization-logo", headers=default_customer_header
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid portal app for ID {portal_app_id}."

    portal_app_id, _raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )

    r = requests.get(
        f"{mps_url}/v1/customer/portal-apps/{portal_app_id}/organization-logo", headers=default_customer_header
    )
    assert r.status_code == 404
    assert r.json()["detail"] == "Organization logo does not exist."
