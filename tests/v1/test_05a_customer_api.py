import copy
import uuid

import requests

from marketplace_service.models.marketplace.app import ApiVersion, ListingStatus
from tests.utils import generate_random_namespace
from tests.v1.creation_helper import post_admin_listed_portal_app_with_tags, post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_get_app(default_admin_header, default_customer_header, admin_post_portal_app):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]
    app = raw_portal_app["active_app_views"]["v1"]["app"]

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}", headers=default_customer_header)
    assert r.status_code == 200
    resp = r.json()
    assert app == resp


def test_get_app_view_for_app(default_admin_header, default_customer_header, admin_post_portal_app):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert raw_portal_app["active_app_views"]["v1"]["app"] == resp2[0]["app"]

    active_app_view = raw_portal_app["active_app_views"]["v1"]
    assert active_app_view["portal_app_id"] == resp2[0]["portal_app_id"]
    assert active_app_view["details"] == resp2[0]["details"]
    assert active_app_view["media"] == resp2[0]["media"]
    assert active_app_view["tags"] == resp2[0]["tags"]


def test_query_customer_portal_apps(default_customer_header, default_admin_header):
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200
    resp_public = r.json()

    # grant license for all portal apps to organization
    data = {"items": []}
    for item in resp_public["items"]:
        data["items"].append(
            {"organization_id": default_customer_header["organization-id"], "portal_app_id": item["id"]}
        )
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json={}, headers=default_customer_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["item_count"] == len(resp["items"])

    listed_in_customer_resp = 0
    other_status_in_customer_response = 0
    for item in resp["items"]:
        if item["status"] == ListingStatus.LISTED:
            listed_in_customer_resp += 1
        else:
            other_status_in_customer_response += 1

    assert listed_in_customer_resp == resp_public["item_count"]
    assert other_status_in_customer_response == resp["item_count"] - resp_public["item_count"]

    for portal_app in resp["items"]:
        assert portal_app["status"] in [ListingStatus.LISTED, ListingStatus.DELISTED, ListingStatus.ADMIN_DELISTED]
        assert "active_app_views" in portal_app
        active_app_view = portal_app["active_app_views"]["v1"]
        assert "details" in active_app_view
        assert "media" in active_app_view
        assert "tags" in active_app_view


def test_query_customer_portal_apps_by_app_ids(default_customer_header, default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    portal_app_id, raw_response = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app)
    app_id_v1 = raw_response["active_app_views"]["v1"]["app"]["id"]

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": portal_app_id}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    query = {"apps": [app_id_v1]}
    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=default_customer_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["item_count"] == 1
    assert resp["items"][0]["id"] == portal_app_id
    assert resp["items"][0]["active_app_views"]["v1"]["app"]["id"] == app_id_v1


def test_query_customer_portal_apps_by_tissue_and_stain(
    default_customer_header, default_admin_header, admin_post_portal_app
):
    query = {"tissues": ["SKIN"], "stains": ["ER"]}
    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp_customer1 = r.json()

    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_portal_app["tags"].append({"tag_group": "STAIN", "tag_name": "ER"})
    portal_app_id, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app)

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": portal_app_id}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    query = {"tissues": ["SKIN"], "stains": ["ER"]}
    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp_customer2 = r.json()

    assert resp_customer1["item_count"] == resp_customer2["item_count"] - 1

    query = {"tissues": ["SKIN"], "stains": ["ER"]}
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp_public = r.json()

    assert resp_customer2["item_count"] == 1
    assert resp_customer2["item_count"] == resp_public["item_count"]
    for i in range(resp_customer2["item_count"]):
        assert resp_customer2["items"][i]["id"] == resp_public["items"][i]["id"]


def test_query_customer_portal_apps_non_existant_stain(
    default_customer_header, default_admin_header, admin_post_portal_app
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    portal_app_id, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app)

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": portal_app_id}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    query = {"tissues": ["SKIN"], "stains": ["KI67"]}
    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp_customer = r.json()
    assert resp_customer["item_count"] == 0

    query = {"tissues": ["SKIN"], "stains": ["KI67"]}
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp_public = r.json()

    assert resp_customer["item_count"] == resp_public["item_count"]
    for i in range(resp_customer["item_count"]):
        assert resp_customer["items"][i]["id"] == resp_public["items"][i]["id"]


def test_query_customer_portal_apps_by_app_and_tags(
    default_customer_header, default_admin_header, admin_post_portal_app
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_portal_app["tags"].append({"tag_group": "STAIN", "tag_name": "KI67"})
    portal_app_id, raw_response = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app)
    app_id_v1 = raw_response["active_app_views"]["v1"]["app"]["id"]

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": portal_app_id}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    query = {"apps": [app_id_v1], "tissues": ["SKIN"], "stains": ["KI67"]}
    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp_customer = r.json()
    assert resp_customer["item_count"] == 1

    query = {"apps": [app_id_v1], "stains": ["KI67"]}
    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp_customer = r.json()
    assert resp_customer["item_count"] == 1


def test_query_app_view_for_apps(default_admin_header, admin_post_portal_app, default_customer_header):
    external_id = str(uuid.uuid4())
    post_portal_app = copy.deepcopy(admin_post_portal_app)

    portal_app_id, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    post_portal_app_2 = copy.deepcopy(admin_post_portal_app)
    post_portal_app_2["id"] = portal_app_id
    new_namespace = generate_random_namespace()
    post_portal_app_2["active_apps"]["v1"]["ead"]["namespace"] = new_namespace
    post_portal_app_2["active_apps"]["v1"]["id"] = external_id
    del post_portal_app_2["active_apps"]["v2"]
    del post_portal_app_2["active_apps"]["v3"]

    params = {"existing_portal_app": True, "external_ids": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app_2, params=params, headers=default_admin_header
    )
    resp2 = r.json()
    app_id_2 = resp2["active_app_views"]["v1"]["app"]["id"]

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp3 = r.json()

    query = {"apps": [app_id_2]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp4 = r.json()

    assert resp3[0]["portal_app_id"] == resp4[0]["portal_app_id"]
    assert resp3[0]["app"]["id"] == raw_portal_app["active_app_views"]["v1"]["app"]["id"]
    assert resp4[0]["app"]["id"] == external_id


def test_query_app_view_invalid_list(default_customer_header):
    query = {"apps": str(uuid.uuid4())}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 422


def test_query_app_view_empty_query(default_customer_header):
    query = {}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 422


def test_query_app_view_no_apps_in_query(default_customer_header):
    query = {"apps": []}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 422


def test_query_app_view_app_id_not_a_uuid(default_customer_header):
    query = {"apps": ["some_random_string"]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 422


def test_query_app_view_app_id_valid_uuid(default_customer_header):
    query = {"apps": [str(uuid.uuid4()), str(uuid.uuid4())]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    assert len(r.json()) == 0


def test_query_app_view_app_id_valid_uuid_and_api_version(default_customer_header):
    query = {"apps": [str(uuid.uuid4()), str(uuid.uuid4())], "api_versions": [ApiVersion.V1]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    assert len(r.json()) == 0


def test_post_portal_and_add_app_view_with_same_app(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v3"]
    del post_portal_app["active_apps"]["v2"]

    portal_app_id, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    app_view = {
        "details": {
            "name": "PD-L1 Quantifier",
            "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        },
        "app": {"id": app_id},
    }
    params = {"api_version": "v2", "existing_app": True}
    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views",
        json=app_view,
        params=params,
        headers=default_admin_header,
    )
    assert r.status_code == 200

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp1 = r.json()
    assert len(resp1) == 2
    assert resp1[0]["app"] == resp1[1]["app"]

    query = {"apps": [app_id], "api_versions": [ApiVersion.V1]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert resp1[1] == resp2[0]

    query = {"apps": [app_id], "api_versions": [ApiVersion.V2]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp3 = r.json()
    assert resp1[0] == resp3[0]


def test_get_portal_app_non_existant(default_customer_header):
    portal_app_id = str(uuid.uuid4())
    r = requests.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}", headers=default_customer_header)
    assert r.status_code == 404
    resp_customer = r.json()
    assert resp_customer["detail"] == f"No valid portal app for ID {portal_app_id}."


def test_get_portal_app(default_admin_header, default_customer_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    portal_app_id, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app)

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": portal_app_id}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    r = requests.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}", headers=default_customer_header)
    assert r.status_code == 200
    resp_customer_portal_app = r.json()
    portal_app_details_prefix = "https://portal-nightly.empaia.org/marketplace/app"

    assert "active_app_views" in resp_customer_portal_app
    assert "v1" in resp_customer_portal_app["active_app_views"]
    v1_app_details_url = resp_customer_portal_app["active_app_views"]["v1"]["details"]["marketplace_url"]
    assert v1_app_details_url == f"{portal_app_details_prefix}/{portal_app_id}/v1"

    assert "v2" in resp_customer_portal_app["active_app_views"]
    v2_app_details_url = resp_customer_portal_app["active_app_views"]["v2"]["details"]["marketplace_url"]
    assert v2_app_details_url == f"{portal_app_details_prefix}/{portal_app_id}/v2"

    assert "v3" in resp_customer_portal_app["active_app_views"]
    v3_app_details_url = resp_customer_portal_app["active_app_views"]["v3"]["details"]["marketplace_url"]
    assert v3_app_details_url == f"{portal_app_details_prefix}/{portal_app_id}/v3"

    assert "id" in resp_customer_portal_app
    assert resp_customer_portal_app["id"] == portal_app_id


def test_query_non_licensed_portal_app(default_customer_header, default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    portal_app_id, raw_portal_app = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app)
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    r = requests.put(f"{mps_url}/v1/customer/portal-apps/query", json={}, headers=default_customer_header)
    assert r.status_code == 200
    resp = r.json()

    error_msg = "Organization is not allowed to access portal app data"

    for item in resp["items"]:
        assert item["id"] != portal_app_id

    r = requests.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}", headers=default_customer_header)
    assert r.status_code == 403
    assert r.json()["detail"] == error_msg

    r = requests.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}/active-app", headers=default_customer_header)
    assert r.status_code == 403
    assert r.json()["detail"] == error_msg

    r = requests.get(f"{mps_url}/v1/customer/portal-apps/{portal_app_id}/apps", headers=default_customer_header)
    assert r.status_code == 403
    assert r.json()["detail"] == error_msg

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}", headers=default_customer_header)
    assert r.status_code == 403
    assert r.json()["detail"] == error_msg

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/config", headers=default_customer_header)
    assert r.status_code == 403
    assert r.json()["detail"] == error_msg

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-config", headers=default_customer_header)
    assert r.status_code == 403
    assert r.json()["detail"] == error_msg

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    assert len(r.json()) == 0
