from time import sleep

import pytest
import requests

from marketplace_service.custom_models.v1.resources import ContainerImageStatus

from ..singletons import mps_url


@pytest.mark.skip("Endpoints not yet entirely implemented / tested")
def test_post_container_app_without_metadata(default_vendor_header):
    r = requests.post(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
    assert r.status_code == 422


@pytest.mark.skip("Endpoints not yet entirely implemented / tested")
def test_post_container_app(default_vendor_header):
    data = {"container_image_name": "random name"}
    r = requests.post(f"{mps_url}/v1/vendor/container-images", data=data, headers=default_vendor_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["name"] == data["container_image_name"]
    assert resp["status"] == ContainerImageStatus.INITIALIZED
    assert resp["creator_id"] == default_vendor_header["user-id"]


@pytest.mark.skip("Endpoints not yet entirely implemented / tested")
def test_get_container_apps(default_vendor_header):
    r = requests.get(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
    assert r.status_code == 200
    resp = r.json()

    data = {"container_image_name": "image1"}
    r = requests.post(f"{mps_url}/v1/vendor/container-images", data=data, headers=default_vendor_header)
    assert r.status_code == 200
    container_image_1 = r.json()

    data = {"container_image_name": "image2"}
    r = requests.post(f"{mps_url}/v1/vendor/container-images", data=data, headers=default_vendor_header)
    assert r.status_code == 200
    container_image_2 = r.json()

    r = requests.get(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
    assert r.status_code == 200
    resp2 = r.json()

    assert len(resp) == len(resp2) - 2
    for container_image_before in resp:
        if container_image_1["id"] == container_image_before["id"]:
            assert False
        if container_image_2["id"] == container_image_before["id"]:
            assert False
    for container_image_after in resp2:
        if container_image_2["id"] == container_image_after["id"]:
            assert container_image_2 == container_image_after
        if container_image_2["id"] == container_image_after["id"]:
            assert container_image_2 == container_image_after


@pytest.mark.skip("Endpoints not yet entirely implemented / tested")
def test_delete_container_non_referenced(default_vendor_header):
    data = {"container_image_name": "image1"}
    r = requests.post(f"{mps_url}/v1/vendor/container-images", data=data, headers=default_vendor_header)
    assert r.status_code == 200
    container_image_id = r.json()["id"]

    r = requests.get(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
    assert r.status_code == 200
    resp1 = r.json()

    r = requests.delete(f"{mps_url}/v1/vendor/container-images/{container_image_id}", headers=default_vendor_header)
    assert r.status_code == 200
    assert r.json()["id"] == container_image_id
    assert r.json()["status"] == ContainerImageStatus.MARKED_FOR_DELETION

    r = requests.get(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert len(resp1) == len(resp2)

    # wait daemon tick interval
    sleep(2)

    r = requests.get(f"{mps_url}/v1/vendor/container-images", headers=default_vendor_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert len(resp1) - 1 == len(resp2)
