from uuid import uuid4

import requests

from marketplace_service.models.marketplace.app import AppStatus, ListingStatus

from ..singletons import mps_url


def test_pending_reviews(default_user_header, app_view_approval_requested):
    _, portal_app_id, app_view_id, _ = app_view_approval_requested

    r = requests.get(f"{mps_url}/v1/reviewer/pending-reviews", headers=default_user_header)
    assert r.status_code == 200
    resp = r.json()
    assert len(resp) >= 1
    for pending_review in resp:
        if pending_review["id"] == app_view_id:
            assert pending_review["portal_app_id"] == portal_app_id
        assert pending_review["status"] == AppStatus.PENDING


def test_approve_review_invalid_ids(default_user_header):
    portal_app_id = uuid4()
    app_view_id = uuid4()
    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/approve",
        headers=default_user_header,
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid portal app for ID {portal_app_id}."


def test_reject_review_invalid_ids(default_user_header):
    portal_app_id = uuid4()
    app_view_id = uuid4()
    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/reject", headers=default_user_header
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid portal app for ID {portal_app_id}."


def test_approve_app_view_unapproved_app(default_user_header, app_view_approval_requested):
    _, portal_app_id, app_view_id, _ = app_view_approval_requested

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/approve",
        headers=default_user_header,
    )
    resp = r.json()
    assert r.status_code == 400
    assert resp["detail"] == "App status needs to be 'APPROVED' (Actual: 'PENDING')."


def test_approve_app(default_user_header, app_view_approval_requested):
    _, portal_app_id, _, app_id = app_view_approval_requested

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/apps/{app_id}/approve", headers=default_user_header
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["status"] == AppStatus.APPROVED
    assert resp["id"] == app_id


def test_reject_app(default_user_header, app_view_approval_requested):
    _, portal_app_id, _, app_id = app_view_approval_requested

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/apps/{app_id}/reject", headers=default_user_header
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["status"] == AppStatus.REJECTED
    assert resp["id"] == app_id


def test_approve_app_view(default_user_header, default_vendor_header, app_view_approval_requested):
    _, portal_app_id, app_view_id, app_id = app_view_approval_requested

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/apps/{app_id}/approve", headers=default_user_header
    )
    assert r.status_code == 200

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/approve",
        headers=default_user_header,
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["status"] == AppStatus.APPROVED
    assert resp["id"] == app_view_id

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}", headers=default_vendor_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["active_app_views"]["v1"]["id"] == app_view_id
    assert resp["active_app_views"]["v1"]["app"]["id"] == app_id
    assert resp["status"] == ListingStatus.LISTED


def test_reject_app_view(default_user_header, app_view_approval_requested):
    _, portal_app_id, app_view_id, _ = app_view_approval_requested

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/reject", headers=default_user_header
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["status"] == AppStatus.REJECTED


def test_post_app_view_in_edit_mode(default_vendor_header, default_user_header, app_view_approval_requested):
    _, portal_app_id, app_view_id, app_id = app_view_approval_requested

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=default_vendor_header
    )
    resp = r.json()
    assert r.status_code == 200

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/apps/{app_id}/approve", headers=default_user_header
    )
    assert r.status_code == 200

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/approve",
        headers=default_user_header,
    )

    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views?edit=true&api_version=v1",
        headers=default_vendor_header,
    )
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["id"] != resp["id"]
    assert resp2["details"] == resp["details"]

    for key in resp2["app"].keys():
        if key == "status":
            assert resp2["app"]["status"] == AppStatus.APPROVED
        else:
            assert key in resp2["app"] and key in resp["app"]
            assert resp2["app"][key] == resp["app"][key]

    assert resp2["tags"] == resp["tags"]
    assert resp2["status"] == AppStatus.DRAFT
