import copy
import datetime
import time
import uuid

import requests
from pydantic import HttpUrl

from marketplace_service.custom_models.v1.clearances import (
    ClearanceItem,
    DatePrecision,
    EditorType,
    InfoProvider,
    InfoSourceType,
)

from ..singletons import mps_url


def test_post_invalid_clearance(default_product_provider_header):
    clearance = {}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 422

    clearance = {"is_hidden": None}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 422


def test_post_simple_clearance(default_product_provider_header):
    clearance = {"is_hidden": False}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 200
    resp = r.json()
    assert "id" in resp
    assert resp["is_hidden"] == clearance["is_hidden"]
    assert resp["organization_id"] == default_product_provider_header["organization-id"]
    assert resp["active_clearance_item"] is None
    assert resp["history"] is None
    assert resp["updater_type"] == resp["creator_type"] == EditorType.USER
    assert resp["updater_id"] == resp["creator_id"] == default_product_provider_header["user-id"]
    assert resp["updated_at"] == resp["created_at"]


def test_post_clearance_with_active_item(default_product_provider_header, post_clearance_item_product_provider):
    clearance = {"is_hidden": False, "clearance_item": post_clearance_item_product_provider}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 200
    resp = r.json()
    assert "id" in resp
    assert resp["is_hidden"] == clearance["is_hidden"]
    assert resp["organization_id"] == default_product_provider_header["organization-id"]
    assert resp["active_clearance_item"] is not None
    assert resp["history"] is not None
    assert len(resp["history"]) == 1
    item = ClearanceItem.model_validate(resp["history"][0])
    assert item.product_name == post_clearance_item_product_provider["product_name"]
    expected_ref_url = post_clearance_item_product_provider["info_source_content"][0]["reference_url"]
    assert item.info_source_content[0].reference_url == HttpUrl(expected_ref_url)
    assert len(item.info_source_content[0].label) == len(
        post_clearance_item_product_provider["info_source_content"][0]["label"].keys()
    )
    assert item.info_source_type == post_clearance_item_product_provider["info_source_type"]
    assert item.info_provider == InfoProvider.ORGANIZATION
    assert item.info_updated_at <= int(time.time())
    assert resp["updater_type"] == resp["creator_type"] == EditorType.USER
    assert resp["updater_id"] == resp["creator_id"] == default_product_provider_header["user-id"]
    assert resp["updated_at"] == resp["created_at"]


def test_post_clearance_item_invalid_payload(default_product_provider_header, post_clearance_item_product_provider):
    fake_id = str(uuid.uuid4())
    now = datetime.datetime.now().timestamp()

    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{fake_id}/item", json={}, headers=default_product_provider_header
    )
    assert r.status_code == 422

    temp_clearance_item = {"clearance_id": "invalid"}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{fake_id}/item",
        json=temp_clearance_item,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422

    temp_clearance_item = {
        "product_name": "name",
        "product_release_at": now,
    }
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{fake_id}/item",
        json=temp_clearance_item,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422

    post_clearance_item_product_provider["description"] = None
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{fake_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422

    post_clearance_item_product_provider["description"] = {
        "EN": "Description in english",
        "DE": "Beschreibung auf Deutsch",
    }
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{fake_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"Clearance for ID {fake_id} does not exist in database."


def test_post_clearance_item_valid_payload(default_product_provider_header, post_clearance_item_product_provider):
    clearance = {"is_hidden": False}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    # post another item with different properties
    new_post_clearance_item = copy.deepcopy(post_clearance_item_product_provider)
    new_post_clearance_item["product_name"] = "new product name"
    new_post_clearance_item["description"] = {"EN": "Update desc EN", "DE": "Updated desc DE"}
    new_post_clearance_item["tags"] = [{"tag_group": "SCANNER", "tag_name": "LEICA_APERIO_AT2"}]
    new_post_clearance_item["product_release_at"] = int(datetime.datetime.now().timestamp())
    new_post_clearance_item["product_release_at_precision"] = DatePrecision.QUARTER
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=new_post_clearance_item,
        headers=default_product_provider_header,
    )
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}", headers=default_product_provider_header
    )
    assert r.status_code == 200
    active_clearance_item = r.json()["active_clearance_item"]

    desc_en = [desc["text"] for desc in active_clearance_item["description"] if desc["lang"] == "EN"][0]
    desc_de = [desc["text"] for desc in active_clearance_item["description"] if desc["lang"] == "DE"][0]
    assert desc_en == new_post_clearance_item["description"]["EN"]
    assert desc_de == new_post_clearance_item["description"]["DE"]

    info_source_content = active_clearance_item["info_source_content"]
    label_en = [desc["text"] for desc in info_source_content[0]["label"] if desc["lang"] == "EN"][0]
    label_de = [desc["text"] for desc in info_source_content[0]["label"] if desc["lang"] == "DE"][0]
    assert label_en == new_post_clearance_item["info_source_content"][0]["label"]["EN"]
    assert label_de == new_post_clearance_item["info_source_content"][0]["label"]["DE"]

    assert active_clearance_item["tags"]["scanners"][0]["name"] == new_post_clearance_item["tags"][0]["tag_name"]
    assert active_clearance_item["product_release_at_precision"] == str(
        new_post_clearance_item["product_release_at_precision"].value
    )
    assert active_clearance_item["product_name"] == new_post_clearance_item["product_name"]
    assert active_clearance_item["info_source_type"] == new_post_clearance_item["info_source_type"]


def test_get_clearances(default_product_provider_header, post_clearance_item_product_provider):
    orga_id = str(uuid.uuid4())
    new_product_provider_header = copy.deepcopy(default_product_provider_header)
    new_product_provider_header["organization-id"] = orga_id

    clearance = {"is_hidden": False, "clearance_item": post_clearance_item_product_provider}
    r = requests.post(f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=new_product_provider_header)

    assert r.status_code == 200
    resp = r.json()

    r = requests.get(f"{mps_url}/v1/product-provider/clearances", headers=new_product_provider_header)
    assert r.status_code == 200
    for clearance in r.json():
        if clearance["id"] == resp["id"]:
            assert clearance["active_clearance_item"]["id"] == resp["active_clearance_item"]["id"]

    clearance = {"is_hidden": False, "clearance_item": post_clearance_item_product_provider}
    r = requests.post(f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=new_product_provider_header)
    assert r.status_code == 200
    resp = r.json()

    r = requests.get(f"{mps_url}/v1/product-provider/clearances", headers=new_product_provider_header)
    assert r.status_code == 200
    assert len(r.json()) == 2

    for clearance in r.json():
        assert clearance["organization_id"] == orga_id


def test_clearance_item_constraints(default_product_provider_header, post_clearance_item_product_provider):
    clearance = {"is_hidden": False}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    post_clearance_item_product_provider["info_provider"] = InfoProvider.EMPAIA
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422

    del post_clearance_item_product_provider["info_provider"]
    post_clearance_item_product_provider["info_source_content"] = [
        {"reference_url": "https://goole.de"},
        {"reference_url": "https://bing.com"},
    ]
    post_clearance_item_product_provider["info_source_type"] = InfoSourceType.EMAIL_CONTACT
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert error_msg == "Value error, Info source content not allowed for info source type 'EMAIL_CONTACT' or 'UNKNOWN'"

    del post_clearance_item_product_provider["info_source_content"]
    post_clearance_item_product_provider["info_source_type"] = InfoSourceType.PUBLIC_WEBSITE
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"]
    assert "Value error, Info source content filed must be provided for info source type 'PUBLIC_WEBSITE'" in error_msg


def test_clearance_item_info_source_content(default_product_provider_header, post_clearance_item_product_provider):
    clearance = {"is_hidden": False}
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances", json=clearance, headers=default_product_provider_header
    )
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    post_clearance_item_product_provider["info_source_content"] = [
        {"reference_url": "https://goole.de"},
        {"reference_url": "https://bing.com"},
    ]
    post_clearance_item_product_provider["info_source_type"] = InfoSourceType.UNKNOWN
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert error_msg == "Value error, Info source content not allowed for info source type 'EMAIL_CONTACT' or 'UNKNOWN'"

    post_clearance_item_product_provider["info_source_content"] = None
    post_clearance_item_product_provider["info_source_type"] = InfoSourceType.PUBLIC_WEBSITE
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert error_msg == "Value error, Info source content filed must be provided for info source type 'PUBLIC_WEBSITE'"

    post_clearance_item_product_provider["info_source_content"] = [{"reference_url": "invalid url"}]
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert error_msg == "Input should be a valid URL, relative URL without a base"

    post_clearance_item_product_provider["info_source_content"] = [
        {"reference_url": "invalid http://www.google.de"},
        {"reference_url": "invalid url"},
    ]
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert error_msg == "Input should be a valid URL, relative URL without a base"

    post_clearance_item_product_provider["info_source_content"] = [{"reference_url": "www.google.de"}]
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert error_msg == "Input should be a valid URL, relative URL without a base"

    post_clearance_item_product_provider["info_source_content"] = [
        {"reference_url": "https://www.google.de/some/url/path"}
    ]
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 200

    post_clearance_item_product_provider["info_source_content"] = [
        {
            "reference_url": "https://www.google.de/some/url/path",
            "label": {"DE": "Nur die deutsch Beschreibung des Links"},
        },
        {
            "reference_url": "http://user:pass@example.com:8000/the/path/?query=here#fragment=is;this=bit",
            "label": {"EN": "Eample page", "DE": "Beispielseite"},
        },
    ]
    r = requests.post(
        f"{mps_url}/v1/product-provider/clearances/{clearance_id}/item",
        json=post_clearance_item_product_provider,
        headers=default_product_provider_header,
    )
    assert r.status_code == 200
