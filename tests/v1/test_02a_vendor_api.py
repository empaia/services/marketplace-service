import copy
import json

import requests

from marketplace_service.models.marketplace.app import AppMediaPurpose, AppStatus
from tests.v1.utils import assert_portal_app_responses, create_monochrome_image

from ..singletons import mps_url


def test_post_potal_app_no_organization():
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps")
    assert r.status_code == 422


def test_post_portal_app(default_vendor_header):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=default_vendor_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["active_app_views"]["v1"] is None
    assert resp["status"] == AppStatus.DRAFT
    assert resp["organization_id"] == default_vendor_header["organization-id"]
    assert "created_at" in resp and "updated_at" in resp and "id" in resp


def test_create_portal_app_view(default_vendor_header):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=default_vendor_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views", headers=default_vendor_header)
    assert r.status_code == 200
    resp = r.json()
    assert resp["version"] is None
    assert resp["details"] is None
    assert resp["media"] is None
    assert resp["portal_app_id"] == portal_app_id
    assert resp["review_comment"] is None
    assert resp["reviewer_id"] is None
    assert resp["reviewed_at"] is None
    assert resp["status"] == AppStatus.DRAFT
    app_view_id = resp["id"]

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=default_vendor_header
    )
    resp2 = r.json()
    assert resp == resp2


def test_create_portal_app_view_research_only(default_vendor_header):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=default_vendor_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    params = {"research_only": False}

    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views", params=params, headers=default_vendor_header
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["research_only"] is False


# TODO: add more tests for posting app views in edit mode, for api_version, etc.


def test_add_portal_app_details_wrong_description_format(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_desc = {"name": "PD-L1 Quantifier", "description": "test", "marketplace_url": "http://url.to/store"}
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        json=json_desc,
        headers=headers,
    )
    assert r.status_code == 422
    assert r.json()["detail"][0]["msg"] == "Input should be a valid dictionary"


def test_add_portal_app_details_no_english_description(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_desc = {
        "name": "PD-L1 Quantifier",
        "description": {"FR": "Description en france", "DE": "Beschreibung auf Deutsch"},
    }
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        json=json_desc,
        headers=headers,
    )
    assert r.status_code == 422
    assert r.json()["detail"] == "App description must contain an entry in english (EN)"


def test_add_portal_app_details_correct(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_desc = {
        "name": "PD-L1 Quantifier",
        "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
    }
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        json=json_desc,
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["details"]["name"] == json_desc["name"]
    assert resp["details"]["description"][0]["lang"] == "DE"
    assert resp["details"]["description"][0]["text"] == json_desc["description"]["DE"]
    assert "marketplace_url" in resp["details"]
    api_version = resp["api_version"]
    assert str(resp["details"]["marketplace_url"]).endswith(f"/marketplace/app/{portal_app_id}/{api_version}")
    app_view_id = resp["id"]

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    resp2 = r.json()
    assert resp == resp2


def test_add_portal_app_add_no_media(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    purpose = "banner"
    index = 1
    data = {"description": "test-description"}
    files = {}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    assert r.status_code == 422


def test_add_portal_app_add_invalid_purpose_and_index(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    purpose = "invalid_banner"  # invalid purpose
    index = 1  # valid index
    data = {"caption": "test description", "alternative_text": "alt text"}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files={},
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 422
    assert resp["detail"][0]["loc"] == ["path", "purpose"]

    index = "invalid index"
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/banner/{index}",
        data=data,
        files={},
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 422
    assert resp["detail"][0]["loc"] == ["path", "index"]


def test_add_portal_app_media_invalid_metadata(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    purpose = "banner"
    index = 1
    media_metadata = {"caption": {"DE": "test description"}, "alternative_text": {"DE": "alt text"}}
    data = {"media_metadata": json.dumps(media_metadata)}

    tmp_image = create_monochrome_image(size=(2048, 2048), color="green", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 422
    assert resp["detail"] == "Caption must contain an entry in english (EN)"

    media_metadata = {"caption": {"EN": "test description"}, "alternative_text": {"DE": "alt text"}}
    data = {"media_metadata": json.dumps(media_metadata)}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 422
    assert resp["detail"] == "Alternative text must contain an entry in english (EN)"


def test_add_portal_app_add_valid_png(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    purpose = "banner"
    index = 1
    media_metadata = {"caption": {"EN": "test description"}, "alternative_text": {"EN": "alt text"}}
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image = create_monochrome_image(size=(2048, 2048), color="green", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 200
    assert AppMediaPurpose.BANNER in resp["media"]
    assert len(resp["media"]) == 4
    assert resp["media"][AppMediaPurpose.PEEK] == []
    assert resp["media"][AppMediaPurpose.WORKFLOW] == []
    assert resp["media"][AppMediaPurpose.MANUAL] == []
    assert resp["media"][AppMediaPurpose.BANNER][0]["content_type"] == "image/png"
    assert "presigned_media_url" in resp["media"][AppMediaPurpose.BANNER][0]
    app_view_id = resp["id"]

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    resp2 = r.json()
    assert_portal_app_responses(resp, resp2)


def test_portal_app_post_same_media_purpose_and_index_twice(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    purpose = "peek"
    index = 1
    media_metadata = {"caption": {"EN": "test description"}, "alternative_text": {"EN": "alt text"}}
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image = create_monochrome_image(size=(2048, 2048), color="green", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 200
    assert len(resp["media"]["peek"]) == 1
    media_id = resp["media"]["peek"][0]["id"]
    presigned_media_url = resp["media"]["peek"][0]["presigned_media_url"]

    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 200
    assert len(resp["media"]["peek"]) == 2
    for peek_item in resp["media"]["peek"]:
        if peek_item["id"] == media_id:
            # in raw cases the signature of the presigned url can differ
            delimiter = "X-Amz-Date="
            actual = str(presigned_media_url).split(delimiter)[0]
            expected = str(peek_item["presigned_media_url"]).split(delimiter)[0]
            assert actual == expected
    app_view_id = resp["id"]

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    resp2 = r.json()
    assert_portal_app_responses(resp, resp2)


def test_portal_app_delete_media(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    assert r.status_code == 200
    count_media = len(r.json()["media"]["banner"])

    purpose = "banner"
    index = 1
    media_metadata = {"caption": {"EN": "test description"}, "alternative_text": {"EN": "alt text"}}
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image = create_monochrome_image(size=(2048, 2048), color="green", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 200
    assert len(resp["media"]["banner"]) == count_media + 1

    r = requests.delete(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        headers=headers,
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["media"] is not None
    app_view_id = resp["id"]

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    resp2 = r.json()
    assert_portal_app_responses(resp, resp2)


def test_add_portal_app_add_invalid_tags(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_tags = [
        {"some_tag": "tag_value"},
    ]
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", json=json_tags, headers=headers
    )
    assert r.status_code == 422

    json_tags = [{"tag_group": "TISSUE", "tag_name": "INVALID_TISSUE"}]
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", json=json_tags, headers=headers
    )
    assert r.status_code == 422
    assert r.json()["detail"] == f"Invalid tag name [{json_tags[0]['tag_name']}]"

    json_tags = [{"tag_group": "INVALID_TAG_GROUP", "tag_name": "SKIN"}]
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", json=json_tags, headers=headers
    )
    assert r.status_code == 422
    assert r.json()["detail"] == f"Invalid tag group [{json_tags[0]['tag_group']}]"


def test_add_portal_app_add_valid_tags(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_tags = [
        {"tag_group": "TISSUE", "tag_name": "SKIN"},
        {"tag_group": "STAIN", "tag_name": "H_AND_E"},
        {"tag_group": "CLEARANCE", "tag_name": "CE_IVD"},
    ]
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", json=json_tags, headers=headers
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["tags"] == {
        "analysis": [],
        "indications": [],
        "stains": [
            {"name": "H_AND_E", "tag_translations": [{"lang": "EN", "text": "H&E"}, {"lang": "DE", "text": "H&E"}]}
        ],
        "tissues": [
            {"name": "SKIN", "tag_translations": [{"lang": "EN", "text": "Skin"}, {"lang": "DE", "text": "Haut"}]}
        ],
        "clearances": [
            {"name": "CE_IVD", "tag_translations": [{"lang": "EN", "text": "CE IVD"}, {"lang": "DE", "text": "CE IVD"}]}
        ],
        "procedures": [],
    }
    app_view_id = resp["id"]

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    resp2 = r.json()
    assert_portal_app_responses(resp, resp2)


def test_portal_app_add_technical_app(valid_portal_app_view, valid_v1_post_app):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=headers)
    resp = r.json()
    assert r.status_code == 200
    assert "id" in resp
    app_id = resp["id"]

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/apps/{app_id}",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert "id" in resp
    assert resp["app"]["ead"] == valid_v1_post_app["ead"]
    assert resp["app"]["registry_image_url"] == valid_v1_post_app["registry_image_url"]
    app_view_id = resp["id"]

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}", headers=headers)
    resp2 = r.json()
    assert_portal_app_responses(resp, resp2)


def test_portal_app_add_technical_app_invalid_ead_schema(valid_portal_app_view, valid_v1_post_app):
    headers, portal_app_id, _ = valid_portal_app_view

    post_app = copy.deepcopy(valid_v1_post_app)
    del post_app["ead"]["$schema"]

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=post_app, headers=headers)
    resp = r.json()
    assert r.status_code == 422
    assert resp["detail"] == "Validation of EAD failed: EAD does not define a $schema."


def test_portal_app_add_technical_app_invalid_ead_namespace(valid_portal_app_view, valid_v1_post_app):
    headers, portal_app_id, _ = valid_portal_app_view

    post_app = copy.deepcopy(valid_v1_post_app)
    post_app["ead"]["namespace"] = "Invalid namespace"

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=post_app, headers=headers)
    resp = r.json()
    assert r.status_code == 422
    assert resp["detail"] == "Validation of EAD failed: EAD does not match schema."


def test_remove_portal_app_details(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_desc = {
        "name": "PD-L1 Quantifier",
        "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
    }
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/details",
        json=json_desc,
        headers=headers,
    )
    assert r.status_code == 200

    r = requests.delete(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/details", headers=headers
    )
    resp = r.json()
    assert resp["details"] is None


def test_remove_portal_app_tags(valid_portal_app_view):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    json_tags = [{"tag_group": "TISSUE", "tag_name": "SKIN"}, {"tag_group": "STAIN", "tag_name": "H_AND_E"}]
    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", json=json_tags, headers=headers
    )
    assert r.status_code == 200

    r = requests.delete(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/tags", headers=headers
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["tags"] is None


def test_remove_app_from_app_view(valid_portal_app_view, valid_v1_post_app):
    headers, portal_app_id, app_view_id = valid_portal_app_view

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=headers)
    resp = r.json()
    assert r.status_code == 200
    assert "id" in resp
    app_id = resp["id"]

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/apps/{app_id}",
        headers=headers,
    )
    assert r.status_code == 200

    r = requests.delete(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/apps", headers=headers
    )
    resp = r.json()
    assert r.status_code == 200
    assert resp["app"] is None
