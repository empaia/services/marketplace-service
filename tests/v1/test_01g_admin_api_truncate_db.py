import requests

from ..singletons import mps_url


def test_truncate_v1_db(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    assert r.status_code == 200

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200
    assert r.json()["item_count"] > 0

    r = requests.put(f"{mps_url}/v1/admin/truncate-db", headers=default_admin_header)
    assert r.status_code == 200

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200
    assert r.json()["item_count"] == 0
    assert len(r.json()["items"]) == 0
