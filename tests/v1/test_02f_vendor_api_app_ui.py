import copy
import sys
import zipfile
from io import BytesIO
from pathlib import Path

import pytest
import requests

from marketplace_service.custom_models.v1.resources import AppUIStatus

from ..singletons import mps_url
from .utils import create_binary_zipped_ui_bundle


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_post_app_ui_bundle_without_metadata(default_vendor_header):
    r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", headers=default_vendor_header)
    assert r.status_code == 422


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_post_app_ui_bundle(default_vendor_header):
    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        app_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", app_ui_byte_arr, "application/zip")}
        json = {"app_ui_name": "random name", "app_ui_config": {"csp": None}}
        r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
        resp = r.json()
        assert r.status_code == 200
        assert resp["name"] == json["app_ui_name"]
        assert resp["app_ui_url"] is None
        assert resp["status"] == AppUIStatus.UNZIP_FINISHED
        assert resp["creator_id"] == default_vendor_header["organization-id"]


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_get_app_ui_bundles(default_vendor_header):
    r = requests.get(f"{mps_url}/v1/vendor/app-ui-bundles", headers=default_vendor_header)
    assert r.status_code == 200
    resp = r.json()

    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        app_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", copy.deepcopy(app_ui_byte_arr), "application/zip")}
        data = {"app_ui_name": "NAME_1", "app_ui_config": {"csp": {"default-src": None}}}
        r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=data, headers=default_vendor_header)
        assert r.status_code == 200
        app_ui_1 = r.json()

        files = {"app_ui_file": ("filename", copy.deepcopy(app_ui_byte_arr), "application/zip")}
        data = {"app_ui_name": "NAME_2", "app_ui_config": {"csp": None}}
        r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=data, headers=default_vendor_header)
        assert r.status_code == 200
        app_ui_2 = r.json()

    r = requests.get(f"{mps_url}/v1/vendor/app-ui-bundles", headers=default_vendor_header)
    assert r.status_code == 200
    resp2 = r.json()

    assert len(resp) == len(resp2) - 2
    for app_ui_before in resp:
        if app_ui_1["id"] == app_ui_before["id"]:
            assert False
        if app_ui_2["id"] == app_ui_before["id"]:
            assert False
    for app_ui_after in resp2:
        if app_ui_1["id"] == app_ui_after["id"]:
            assert app_ui_1 == app_ui_after
        if app_ui_2["id"] == app_ui_after["id"]:
            assert app_ui_2 == app_ui_after


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_delete_app_ui_bundles(default_vendor_header):
    # TODO: check deletion of app ui bundle
    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        app_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", app_ui_byte_arr, "application/zip")}
        data = {"app_ui_name": "random name", "app_ui_config": {"csp": None}}
        r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=data, headers=default_vendor_header)
        assert r.status_code == 200
        app_ui_bundle = r.json()["id"]

        r = requests.get(f"{mps_url}/v1/vendor/app-ui-bundles", headers=default_vendor_header)
        assert r.status_code == 200
        resp1 = r.json()

        r = requests.delete(f"{mps_url}/v1/vendor/app-ui-bundles/{app_ui_bundle}", headers=default_vendor_header)
        assert r.status_code == 200
        assert r.json()["id"] == app_ui_bundle

        r = requests.get(f"{mps_url}/v1/vendor/app-ui-bundles", headers=default_vendor_header)
        assert r.status_code == 200
        resp2 = r.json()
        assert len(resp1) - 1 == len(resp2)


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_post_app_ui_bundle_invalid_mime_type(default_vendor_header):
    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        app_ui_byte_arr = BytesIO(fh.read())
        mime_type = "image/png"
        files = {"app_ui_file": ("filename", app_ui_byte_arr, "image/png")}
        json = {"app_ui_name": "random name", "app_ui_config": {"csp": None}}
        r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
        assert r.status_code == 400
        assert r.json()["detail"] == f"Content type {mime_type} not supported."


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_post_app_ui_bundle_invalid_zip_file(default_vendor_header):
    resource_file = Path(sys.path[0]).joinpath("tests/resources/invalid.zip")
    with open(resource_file, "rb") as fh:
        app_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", app_ui_byte_arr, "application/zip")}
        json = {"app_ui_name": "random name", "app_ui_config": {"csp": None}}
        r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
        assert r.status_code == 400
        assert r.json()["detail"] == "Failed to unzip archive. See logs for more details."


@pytest.mark.skip("Enpoints not yet entirely implemented / tested")
def test_post_app_ui_bundle_zip_compression(default_vendor_header):
    binary_resource_file = create_binary_zipped_ui_bundle(zipfile.ZIP_DEFLATED)
    files = {"app_ui_file": ("filename", binary_resource_file, "application/zip")}
    json = {"app_ui_name": "random name", "app_ui_config": {"csp": None}}
    r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
    assert r.status_code == 200

    binary_resource_file = create_binary_zipped_ui_bundle(zipfile.ZIP_STORED)
    files = {"app_ui_file": ("filename", binary_resource_file, "application/zip")}
    r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
    assert r.status_code == 200

    binary_resource_file = create_binary_zipped_ui_bundle(zipfile.ZIP_BZIP2)
    files = {"app_ui_file": ("filename", binary_resource_file, "application/zip")}
    r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
    assert r.status_code == 200

    binary_resource_file = create_binary_zipped_ui_bundle(zipfile.ZIP_LZMA)
    files = {"app_ui_file": ("filename", binary_resource_file, "application/zip")}
    r = requests.post(f"{mps_url}/v1/vendor/app-ui-bundles", files=files, data=json, headers=default_vendor_header)
    assert r.status_code == 200
