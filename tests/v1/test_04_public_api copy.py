import copy

import requests

from tests.utils import generate_random_namespace
from tests.v1.creation_helper import post_admin_listed_portal_app_with_tags

from ..singletons import mps_url


def test_query_all_portal_apps():
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200


def test_query_all_portal_apps_skip_limit():
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query?skip=0&limit=10", json={})
    resp = r.json()
    assert r.status_code == 200
    assert len(resp["items"]) <= 10


def test_get_app_invalid_uuid():
    r = requests.get(
        f"{mps_url}/v1/public/portal-apps/invalid-id",
    )
    resp = r.json()
    assert r.status_code == 422
    assert "Input should be a valid UUID, invalid character" in resp["detail"][0]["msg"]


def test_get_app_from_list():
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    resp = r.json()
    assert r.status_code == 200
    portal_app_id = resp["items"][0]["id"]

    r = requests.get(f"{mps_url}/v1/public/portal-apps/{portal_app_id}", json={})
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2 == resp["items"][0]
    active_app_view = resp2["active_app_views"]["v1"]
    assert "created_at" in active_app_view
    assert "reviewed_at" in active_app_view


def test_query_app_list_by_single_filter(default_admin_header, admin_post_portal_app):
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200
    item_count = r.json()["item_count"]

    tags = [{"tag_group": "TISSUE", "tag_name": "FALLOPIAN_TUBES"}]
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    _, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app, tags)

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == item_count + 1

    tissue_search_word = "FALLOPIAN_TUBES"

    query = {
        "tissues": [
            tissue_search_word,
        ],
    }

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == 1
    assert resp2["items"][0]["active_app_views"]["v1"]["tags"]["tissues"][0]["name"] == tissue_search_word
    assert resp2["items"][0]["active_app_views"]["v2"]["tags"]["tissues"][0]["name"] == tissue_search_word
    assert resp2["items"][0]["active_app_views"]["v3"]["tags"]["tissues"][0]["name"] == tissue_search_word


def test_query_app_list_by_multiple_filter(default_admin_header, admin_post_portal_app):
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200
    item_count = r.json()["item_count"]

    tags_app_1 = [
        {"tag_group": "TISSUE", "tag_name": "THYROID_AND_PARATHYROID"},
        {"tag_group": "STAIN", "tag_name": "CK_AE1_AND_AE3"},
        {"tag_group": "ANALYSIS", "tag_name": "QUANTIFICATION"},
    ]
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    _, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app, tags_app_1)

    tags = [
        {"tag_group": "TISSUE", "tag_name": "THYROID_AND_PARATHYROID"},
        {"tag_group": "STAIN", "tag_name": "H_AND_E"},
        {"tag_group": "INDICATION", "tag_name": "HODGKIN_LYMPHOMA"},
        {"tag_group": "CLEARANCE", "tag_name": "CE_IVD"},
    ]
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_portal_app["active_apps"]["v1"]["ead"]["namespace"] = generate_random_namespace()
    post_portal_app["active_apps"]["v2"]["ead"]["namespace"] = generate_random_namespace()
    post_portal_app["active_apps"]["v3"]["ead"]["namespace"] = generate_random_namespace("v3")
    portal_app_id_2, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app, tags)

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == item_count + 2

    tissue_search_word = "THYROID_AND_PARATHYROID"

    query = {
        "tissues": [
            tissue_search_word,
        ],
    }

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == 2

    stain_search_word = "H_AND_E"

    query = {
        "tissues": [
            tissue_search_word,
        ],
        "stains": [
            stain_search_word,
        ],
    }

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == 1
    assert resp2["items"][0]["id"] == portal_app_id_2

    indication_search_word = "HODGKIN_LYMPHOMA"

    query = {
        "stains": [
            stain_search_word,
        ],
        "indications": [
            indication_search_word,
        ],
    }

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == 1
    assert resp2["items"][0]["id"] == portal_app_id_2

    clearances_search_word = "CE_IVD"

    query = {
        "clearances": [
            clearances_search_word,
        ],
    }

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp2 = r.json()
    assert r.status_code == 200
    assert resp2["item_count"] == 1
    assert resp2["items"][0]["id"] == portal_app_id_2


def test_query_app_list_skip_and_limit():
    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json={})
    assert r.status_code == 200
    item_count = r.json()["item_count"]
    assert item_count == len(r.json()["items"])

    params = {"skip": 1, "limit": 3}

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", params=params, json={})
    assert r.status_code == 200
    resp = r.json()
    assert resp["item_count"] == item_count
    assert len(resp["items"]) != resp["item_count"]
    assert len(resp["items"]) == 2


def test_query_app_list_skip_and_limit_invalid_values():
    params = {"skip": 5, "limit": -1}

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", params=params, json={})
    assert r.status_code == 422
    assert r.json()["detail"] == "Limit may not be negative"

    params = {"skip": -5, "limit": 3}

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", params=params, json={})
    assert r.status_code == 422
    assert r.json()["detail"] == "Skip may not be negative"


def test_query_app_list_skip_and_limit_exceed_size():
    params = {"skip": 0, "limit": 10000}

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", params=params, json={})
    assert r.status_code == 200
    assert r.json()["item_count"] == len(r.json()["items"])


def test_query_app_list_active_app_version():
    query = {
        "active_app_version": "v3",
    }

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", json=query)
    resp = r.json()
    assert r.status_code == 200
    for portal_app in resp["items"]:
        assert "v3" in portal_app["active_app_views"]
        assert portal_app["active_app_views"]["v3"] != []


def test_query_related_portal_apps(default_admin_header, admin_post_portal_app):
    tags_app_1 = [
        {"tag_group": "TISSUE", "tag_name": "SKIN"},
        {"tag_group": "STAIN", "tag_name": "H_AND_E"},
    ]
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    portal_app_id, _ = post_admin_listed_portal_app_with_tags(default_admin_header, post_portal_app, tags_app_1)

    r = requests.put(f"{mps_url}/v1/public/portal-apps/{portal_app_id}/related-portal-apps")
    assert r.status_code == 200
    resp = r.json()
    for portal_app in resp["items"]:
        if portal_app["id"] == portal_app_id:
            assert False


# TODO: Extend Tests!
