import copy
import sys
from io import BytesIO
from pathlib import Path
from uuid import uuid4

import requests

from marketplace_service.custom_models.v1.resources import AppUIStatus
from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_post_portal_with_app_ui_admin_endpoint(default_admin_header, default_customer_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        appp_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", appp_ui_byte_arr, "application/zip")}
        r = requests.put(f"{mps_url}/v1/admin/apps/{app_id}/app-ui-bundles", files=files, headers=default_admin_header)
        assert r.status_code == 200
        assert r.json()["status"] == AppUIStatus.UNZIP_FINISHED

    # stream file from customer api
    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-files/index.html", headers=default_customer_header)
    assert r.status_code == 200
    assert "<h1>heading</h1>" in str(r.content.decode("utf-8"))
    assert "<p>test text.</p>" in str(r.content.decode("utf-8"))


def test_post_app_ui_bundle_invalid_app_id(default_customer_header):
    app_id = str(uuid4())
    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-files/style.css", headers=default_customer_header)
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid app UI bundle for app ID {app_id}"
