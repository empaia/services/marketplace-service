import requests

from marketplace_service.models.marketplace.app import AppStatus
from tests.v1.utils import assert_portal_app_responses

from ..singletons import mps_url


def test_portal_app_revoke_approval_invalid_app_view(app_view_with_input_data_set):
    headers, portal_app_id, app_view_id, _app_id = app_view_with_input_data_set

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/revoke-approval",
        headers=headers,
    )
    assert r.status_code == 400
    resp = r.json()
    assert "Can not change portal app status" in resp["detail"]


def test_portal_app_revoke_approval(app_view_with_input_data_set):
    headers, portal_app_id, app_view_id, _app_id = app_view_with_input_data_set

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/request-approval",
        headers=headers,
    )
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["status"] == AppStatus.PENDING
    assert resp["app"]["status"] == AppStatus.PENDING

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/revoke-approval",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["status"] == AppStatus.DRAFT
    assert resp["app"]["status"] == AppStatus.PENDING


def test_portal_app_request_approval(app_view_with_input_data_set):
    headers, portal_app_id, app_view_id, _app_id = app_view_with_input_data_set

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/request-approval",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}",
        headers=headers,
    )
    assert r.status_code == 200
    resp2 = r.json()
    assert_portal_app_responses(resp, resp2)
