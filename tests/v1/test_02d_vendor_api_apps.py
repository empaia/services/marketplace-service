import copy
import uuid

import requests

from marketplace_service.models.marketplace.app import AppStatus

from ..singletons import mps_url


def test_post_app_for_non_uuid_portal_app_id(default_vendor_header):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/NO_UUID_APP/apps", headers=default_vendor_header)
    assert r.status_code == 422


def test_post_app_for_invalid_portal_app(default_vendor_header, valid_v1_post_app):
    random_uuid = uuid.uuid4()
    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{random_uuid}/apps", json=valid_v1_post_app, headers=default_vendor_header
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid portal app for ID {random_uuid}."


def test_post_app_for_valid_portal_app(default_vendor_header, valid_v1_post_app):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=default_vendor_header)
    portal_app_id = r.json()["id"]

    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=default_vendor_header
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["status"] == AppStatus.DRAFT
    assert resp["ead"] is not None and resp["app_ui_url"] is not None and resp["registry_image_url"] is not None


def test_get_app(default_vendor_header, valid_v1_post_app):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=default_vendor_header)
    portal_app_id = r.json()["id"]

    r = requests.post(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=default_vendor_header
    )
    resp = r.json()
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps/{resp['id']}", headers=default_vendor_header
    )
    resp2 = r.json()
    assert r.status_code == 200
    assert resp == resp2

    r = requests.delete(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps/{resp['id']}", headers=default_vendor_header
    )
    resp2 = r.json()
    assert r.status_code == 200
    assert resp == resp2

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps/{resp['id']}", headers=default_vendor_header
    )
    resp2 = r.json()
    assert r.status_code == 404
    assert resp2["detail"] == f"No valid app for ID {resp['id']}."


def test_get_apps(default_vendor_header, valid_v1_post_app):
    r = requests.post(f"{mps_url}/v1/vendor/portal-apps", headers=default_vendor_header)
    portal_app_id = r.json()["id"]

    versions = []
    for i in range(0, 3):
        post_app = copy.deepcopy(valid_v1_post_app)
        version = f"v3.11{i}"
        post_app["ead"]["namespace"] = f"org.empaia.vendor.app.{version}"
        versions.append(version)
        r = requests.post(
            f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps",
            json=post_app,
            headers=default_vendor_header,
        )
        assert r.status_code == 200

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", headers=default_vendor_header)
    resp = r.json()
    assert r.status_code == 200
    assert len(resp) == 3
    assert resp[0]["ead"]["namespace"] != valid_v1_post_app["ead"]["namespace"]
    assert resp[0]["registry_image_url"] == valid_v1_post_app["registry_image_url"]
    assert resp[0]["app_ui_url"] == valid_v1_post_app["app_ui_url"]
    assert resp[0]["status"] == AppStatus.DRAFT
    assert resp[0]["version"] in versions

    r = requests.delete(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps/{resp[0]['id']}", headers=default_vendor_header
    )
    assert r.status_code == 200

    r = requests.get(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", headers=default_vendor_header)
    resp = r.json()
    assert r.status_code == 200
    assert len(resp) == 2
