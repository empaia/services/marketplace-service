import copy

import requests

from ..singletons import mps_url


def test_post_portal_app_with_active_and_legacy_app(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_app_v2 = copy.deepcopy(admin_post_portal_app["active_apps"]["v1"])
    post_app_v2["ead"]["namespace"] = f"{post_app_v2['ead']['namespace']}999"  # modify new namespace
    post_app_v2["ead"]["name"] = "Legacy App"
    post_app_v2["ead"]["name_short"] = "LA01"
    post_app_v2["registry_image_url"] = "https://registry.gitlab.com/empaia/integration/la01"
    post_app_v2["app_ui_url"] = "http://la01.test.org"
    post_app_v2["app_ui_configuration"] = {}
    post_portal_app["active_apps"]["v2"] = post_app_v2

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    active_app_view = resp["active_app_views"]["v1"]
    assert active_app_view["app"]["ead"] == post_portal_app["active_apps"]["v1"]["ead"]
    assert active_app_view["app"]["registry_image_url"] == post_portal_app["active_apps"]["v1"]["registry_image_url"]
    assert active_app_view["app"]["app_ui_url"] is None
    active_app_view_v2 = resp["active_app_views"]["v2"]
    assert active_app_view_v2["app"]["ead"] == post_portal_app["active_apps"]["v2"]["ead"]
    assert active_app_view_v2["app"]["registry_image_url"] == post_portal_app["active_apps"]["v2"]["registry_image_url"]
    assert active_app_view_v2["app"]["app_ui_url"] == post_portal_app["active_apps"]["v2"]["app_ui_url"]


def test_post_portal_only_valid_v1_app(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v2"]
    del post_portal_app["active_apps"]["v3"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["active_app_views"]["v1"] is not None
    assert resp["active_app_views"]["v2"] is None
    assert resp["active_app_views"]["v3"] is None


def test_post_portal_only_valid_v2_app(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v1"]
    del post_portal_app["active_apps"]["v3"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["active_app_views"]["v1"] is None
    assert resp["active_app_views"]["v2"] is not None
    assert resp["active_app_views"]["v3"] is None


def test_post_portal_only_valid_v3_app(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v1"]
    del post_portal_app["active_apps"]["v2"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    assert resp["active_app_views"]["v1"] is None
    assert resp["active_app_views"]["v2"] is None
    assert resp["active_app_views"]["v3"] is not None


def test_post_portal_app_retrieve_by_customer_api(default_admin_header, default_customer_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    active_app_id_v1 = resp["active_app_views"]["v1"]["app"]["id"]
    active_app_id_v2 = resp["active_app_views"]["v2"]["app"]["id"]
    active_app_id_v3 = resp["active_app_views"]["v3"]["app"]["id"]

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": resp["id"]}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    query = {"apps": [active_app_id_v1]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()

    active_app = resp2[0]["app"]
    assert active_app["ead"] == post_portal_app["active_apps"]["v1"]["ead"]
    assert active_app["registry_image_url"] == post_portal_app["active_apps"]["v1"]["registry_image_url"]
    assert active_app["app_ui_url"] is None

    query = {"apps": [active_app_id_v2]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp3 = r.json()

    active_app = resp3[0]["app"]
    assert active_app["ead"] == post_portal_app["active_apps"]["v2"]["ead"]
    assert active_app["registry_image_url"] == post_portal_app["active_apps"]["v2"]["registry_image_url"]
    assert active_app["app_ui_url"] == post_portal_app["active_apps"]["v2"]["app_ui_url"]

    query = {"apps": [active_app_id_v3]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp4 = r.json()

    active_app = resp4[0]["app"]

    assert active_app["ead"] == post_portal_app["active_apps"]["v3"]["ead"]
    assert active_app["registry_image_url"] == post_portal_app["active_apps"]["v3"]["registry_image_url"]
    assert active_app["app_ui_url"] == post_portal_app["active_apps"]["v3"]["app_ui_url"]
