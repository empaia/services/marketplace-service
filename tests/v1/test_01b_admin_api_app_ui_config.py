import copy
import sys
from io import BytesIO
from pathlib import Path

import requests

from marketplace_service.custom_models.v1.resources import AppUIStatus
from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_post_portal_app_with_app_ui_config(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    assert resp["active_app_views"]["v1"]["app"]["has_frontend"] is False
    assert resp["active_app_views"]["v2"]["app"]["has_frontend"] is True
    assert resp["active_app_views"]["v3"]["app"]["has_frontend"] is True

    resp_ui_config_v1 = resp["active_app_views"]["v1"]["app"]["app_ui_configuration"]
    resp_ui_config_v2 = resp["active_app_views"]["v2"]["app"]["app_ui_configuration"]
    resp_ui_config_v3 = resp["active_app_views"]["v3"]["app"]["app_ui_configuration"]
    assert resp_ui_config_v1 == {"csp": None, "iframe": None, "tested": None}
    assert resp_ui_config_v2 != {"csp": None, "iframe": None, "tested": None}
    assert resp_ui_config_v3 != {"csp": None, "iframe": None, "tested": None}

    expected_ui_config_v2 = admin_post_portal_app["active_apps"]["v2"]["app_ui_configuration"]
    assert "csp" in resp_ui_config_v2
    csp = resp_ui_config_v2["csp"]
    assert "font_src" in csp
    assert "unsafe_inline" in csp["font_src"]
    assert csp["font_src"]["unsafe_inline"] == expected_ui_config_v2["csp"]["font_src"]["unsafe_inline"]
    assert "style_src" in csp
    assert "unsafe_inline" in csp["style_src"]
    assert csp["style_src"]["unsafe_inline"] == expected_ui_config_v2["csp"]["style_src"]["unsafe_inline"]


def test_post_portal_app_with_app_ui_config_invalid(default_admin_header, admin_post_portal_app):
    portal_app_copy = copy.deepcopy(admin_post_portal_app)
    portal_app_copy["active_apps"]["v1"]["app_ui_configuration"] = {"invalid_key": "invalid_value"}

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=portal_app_copy, headers=default_admin_header)
    assert r.status_code == 422

    portal_app_copy["active_apps"]["v1"]["app_ui_configuration"] = {"csp": {"default_src": "unsafe_inline"}}

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=portal_app_copy, headers=default_admin_header)
    assert r.status_code == 422

    portal_app_copy["active_apps"]["v1"]["app_ui_configuration"] = {"csp": {"font_src": "https://google.com"}}

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=portal_app_copy, headers=default_admin_header)
    assert r.status_code == 422


def test_post_portal_app_without_ui(default_admin_header, default_customer_header, admin_post_portal_app):
    copy_post_admin_portal_app = copy.deepcopy(admin_post_portal_app)

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, copy_post_admin_portal_app, default_customer_header["organization-id"]
    )
    assert raw_portal_app["active_app_views"]["v1"]["app"]["has_frontend"] is False
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert resp2[0]["app"]["has_frontend"] is False


def test_post_portal_app_only_with_config(default_admin_header, default_customer_header, admin_post_portal_app):
    copy_post_admin_portal_app = copy.deepcopy(admin_post_portal_app)
    del copy_post_admin_portal_app["active_apps"]["v2"]["app_ui_url"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=copy_post_admin_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    assert resp["active_app_views"]["v2"]["app"]["has_frontend"] is True
    app_id = resp["active_app_views"]["v2"]["app"]["id"]

    # grant license for portal app to organization
    data = {"items": [{"organization_id": default_customer_header["organization-id"], "portal_app_id": resp["id"]}]}
    r = requests.post(f"{mps_url}/v1/admin/licenses", json=data, headers=default_admin_header)
    assert r.status_code == 200

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert resp2[0]["app"]["has_frontend"] is True


def test_post_portal_app_with_valid_active_app_view(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    copy_post_admin_portal_app = copy.deepcopy(admin_post_portal_app)

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, copy_post_admin_portal_app, default_customer_header["organization-id"]
    )
    assert raw_portal_app["active_app_views"]["v1"]["app"]["has_frontend"] is False
    assert raw_portal_app["active_app_views"]["v1"]["app"]["has_frontend"] is False
    assert raw_portal_app["active_app_views"]["v2"]["app"]["has_frontend"] is True
    assert raw_portal_app["active_app_views"]["v3"]["app"]["has_frontend"] is True
    app_id_v1 = raw_portal_app["active_app_views"]["v1"]["app"]["id"]
    app_id_v2 = raw_portal_app["active_app_views"]["v2"]["app"]["id"]
    app_id_v3 = raw_portal_app["active_app_views"]["v3"]["app"]["id"]

    query = {"apps": [app_id_v1]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert resp2[0]["app"]["has_frontend"] is False

    query = {"apps": [app_id_v2]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert resp2[0]["app"]["has_frontend"] is True

    query = {"apps": [app_id_v3]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()
    assert resp2[0]["app"]["has_frontend"] is True


def test_post_portal_app_without_ui_and_add_ui_bundle(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    copy_post_admin_portal_app = copy.deepcopy(admin_post_portal_app)
    del copy_post_admin_portal_app["active_apps"]["v2"]["app_ui_configuration"]
    del copy_post_admin_portal_app["active_apps"]["v2"]["app_ui_url"]

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, copy_post_admin_portal_app, default_customer_header["organization-id"]
    )
    assert raw_portal_app["active_app_views"]["v1"]["app"]["has_frontend"] is False
    assert raw_portal_app["active_app_views"]["v2"]["app"]["has_frontend"] is False
    assert raw_portal_app["active_app_views"]["v3"]["app"]["has_frontend"] is True
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        appp_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", appp_ui_byte_arr, "application/zip")}
        r = requests.put(f"{mps_url}/v1/admin/apps/{app_id}/app-ui-bundles", files=files, headers=default_admin_header)
        assert r.status_code == 200
        assert r.json()["status"] == AppUIStatus.UNZIP_FINISHED

        query = {"apps": [app_id]}
        r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
        assert r.status_code == 200
        resp2 = r.json()
        assert resp2[0]["app"]["has_frontend"] is True
