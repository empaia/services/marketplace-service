import copy
import json
import uuid

import requests

from marketplace_service.models.marketplace.app import ApiVersion, AppMediaPurpose, AppStatus, ListingStatus
from tests.utils import generate_random_namespace
from tests.v1.utils import assert_portal_app_view_media, create_monochrome_image

from ..singletons import mps_url


def test_post_portal_app(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    assert "id" in resp
    assert "status" in resp
    assert resp["status"] == ListingStatus.DRAFT
    active_app_views = resp["active_app_views"]["v1"]
    assert "portal_app_id" in active_app_views
    assert "research_only" in active_app_views
    assert active_app_views["research_only"] is False
    assert active_app_views["status"] == AppStatus.APPROVED
    assert active_app_views["details"]["name"] == admin_post_portal_app["details"]["name"]
    actual_details = active_app_views["details"]["description"][0]["text"]
    expected_details = admin_post_portal_app["details"]["description"]["DE"]
    assert actual_details == expected_details
    assert active_app_views["app"]["ead"] == admin_post_portal_app["active_apps"]["v1"]["ead"]


def test_post_portal_app_with_orga_name_and_app_docu(default_admin_header, admin_post_portal_app):
    post_portal_app = admin_post_portal_app.copy()
    post_portal_app["organization_name"] = "Nice AI company"
    post_portal_app["active_apps"]["v1"]["app_documentation_url"] = "http://ai-vendor.org/fancy-app/documentation-v1"
    post_portal_app["active_apps"]["v3"]["app_documentation_url"] = "http://ai-vendor.org/fancy-app/documentation-v3"
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    assert resp["organization_name"] == post_portal_app["organization_name"]
    app_v1_docu = resp["active_app_views"]["v1"]["app"]["app_documentation_url"]
    assert app_v1_docu == post_portal_app["active_apps"]["v1"]["app_documentation_url"]
    app_v3_docu = resp["active_app_views"]["v3"]["app"]["app_documentation_url"]
    assert app_v3_docu == post_portal_app["active_apps"]["v3"]["app_documentation_url"]
    assert resp["active_app_views"]["v2"]["app"]["app_documentation_url"] is None


def test_post_portal_app_non_research_only(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_portal_app["research_only"] = False
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    active_app_views = resp["active_app_views"]["v1"]
    assert "research_only" in active_app_views
    assert active_app_views["research_only"] is False


def test_post_portal_app_same_namespace_twice(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    assert r.status_code == 200

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    namespace = admin_post_portal_app["active_apps"]["v1"]["ead"]["namespace"]
    assert r.status_code == 400
    assert r.json()["detail"] == f"EAD with namespace '{namespace}' already exists."


def test_post_portal_app_malformed_ead_namespace(default_admin_header, admin_post_portal_app):
    portal_app_copy = copy.deepcopy(admin_post_portal_app)
    portal_app_copy["active_apps"]["v1"]["ead"]["namespace"] = "org.invalid.vendor.app.v0"

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=portal_app_copy, headers=default_admin_header)
    error_msg = "Validation of EAD failed: EAD does not match schema."
    assert r.status_code == 422
    assert r.json()["detail"] == error_msg


def test_post_portal_app_malformed_ead(default_admin_header, admin_post_portal_app):
    portal_app_copy = copy.deepcopy(admin_post_portal_app)
    del portal_app_copy["active_apps"]["v1"]["ead"]["$schema"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=portal_app_copy, headers=default_admin_header)
    assert r.status_code == 422
    assert r.json()["detail"] == "Validation of EAD failed: EAD does not define a $schema."


def test_post_to_existing_portal_app_new_version(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    post_portal_app_2 = copy.deepcopy(admin_post_portal_app)
    post_portal_app_2["id"] = portal_app_id
    new_namespace_v1 = generate_random_namespace()
    new_namespace_v2 = generate_random_namespace()
    new_namespace_v3 = generate_random_namespace("v3")
    post_portal_app_2["active_apps"]["v1"]["ead"]["namespace"] = new_namespace_v1
    post_portal_app_2["active_apps"]["v2"]["ead"]["namespace"] = new_namespace_v2
    post_portal_app_2["active_apps"]["v3"]["ead"]["namespace"] = new_namespace_v3

    params = {"existing_portal_app": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app_2, params=params, headers=default_admin_header
    )
    resp = r.json()
    assert r.status_code == 200
    assert "id" in resp
    assert "status" in resp
    assert resp["status"] == ListingStatus.DRAFT
    active_app_views = resp["active_app_views"]

    for version in ["v1", "v2", "v3"]:
        assert "portal_app_id" in active_app_views[version]
        assert active_app_views[version]["portal_app_id"] == portal_app_id
        assert active_app_views[version]["status"] == AppStatus.APPROVED
        assert active_app_views[version]["details"]["name"] == post_portal_app["details"]["name"]
        res_desc = active_app_views[version]["details"]["description"][0]["text"]
        assert res_desc == post_portal_app["details"]["description"]["DE"]
    assert active_app_views["v1"]["app"]["ead"]["namespace"] == new_namespace_v1
    assert active_app_views["v2"]["app"]["ead"]["namespace"] == new_namespace_v2
    assert active_app_views["v3"]["app"]["ead"]["namespace"] == new_namespace_v3


def test_post_to_existing_portal_app_invalid(default_admin_header, admin_post_portal_app):
    external_portal_app_id = str(uuid.uuid4())
    post_portal_app_2 = copy.deepcopy(admin_post_portal_app)
    post_portal_app_2["id"] = external_portal_app_id

    params = {"existing_portal_app": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app_2, params=params, headers=default_admin_header
    )

    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid portal app for ID {external_portal_app_id}."


def test_post_to_existing_portal_app_invalid_uuid(default_admin_header, admin_post_portal_app):
    external_id = "not-an-uuid"

    portal_app_ext_ids = copy.deepcopy(admin_post_portal_app)
    portal_app_ext_ids["active_apps"]["v1"]["id"] = external_id

    params = {"existing_portal_app": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=portal_app_ext_ids, params=params, headers=default_admin_header
    )
    resp = r.json()
    assert r.status_code == 422
    assert "Input should be a valid UUID, invalid character" in resp["detail"][0]["msg"]


def test_post_external_id_for_portal_app(default_admin_header, admin_post_portal_app):
    external_portal_app_id = str(uuid.uuid4())
    external_app_id_v1 = str(uuid.uuid4())
    external_app_id_v2 = str(uuid.uuid4())
    external_app_id_v3 = str(uuid.uuid4())

    post_portal_app = copy.deepcopy(admin_post_portal_app)

    post_portal_app["id"] = external_portal_app_id
    post_portal_app["active_apps"]["v1"]["id"] = external_app_id_v1
    post_portal_app["active_apps"]["v2"]["id"] = external_app_id_v2
    post_portal_app["active_apps"]["v3"]["id"] = external_app_id_v3

    new_namespace_v1 = generate_random_namespace()
    new_namespace_v2 = generate_random_namespace()
    new_namespace_v3 = generate_random_namespace("v3")
    post_portal_app["active_apps"]["v1"]["ead"]["namespace"] = new_namespace_v1
    post_portal_app["active_apps"]["v2"]["ead"]["namespace"] = new_namespace_v2
    post_portal_app["active_apps"]["v3"]["ead"]["namespace"] = new_namespace_v3

    params = {"external_ids": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, params=params, headers=default_admin_header
    )
    assert r.status_code == 200
    resp = r.json()
    assert r.status_code == 200
    assert "id" in resp
    active_app_views = resp["active_app_views"]
    for version in ["v1", "v2", "v3"]:
        assert "portal_app_id" in active_app_views[version]
        assert active_app_views[version]["portal_app_id"] == external_portal_app_id
        assert active_app_views[version]["status"] == AppStatus.APPROVED
        assert active_app_views[version]["details"]["name"] == post_portal_app["details"]["name"]
        res_desc = active_app_views[version]["details"]["description"][0]["text"]
        assert res_desc == post_portal_app["details"]["description"]["DE"]

    assert active_app_views["v1"]["app"]["ead"]["namespace"] == new_namespace_v1
    assert active_app_views["v2"]["app"]["ead"]["namespace"] == new_namespace_v2
    assert active_app_views["v3"]["app"]["ead"]["namespace"] == new_namespace_v3

    assert active_app_views["v1"]["app"]["id"] == external_app_id_v1
    assert active_app_views["v2"]["app"]["id"] == external_app_id_v2
    assert active_app_views["v3"]["app"]["id"] == external_app_id_v3


def test_post_portal_app_external_ids(default_admin_header, admin_post_portal_app):
    external_app_id_v1 = str(uuid.uuid4())
    external_app_id_v2 = str(uuid.uuid4())
    external_app_id_v3 = str(uuid.uuid4())

    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_portal_app["active_apps"]["v1"]["id"] = external_app_id_v1
    post_portal_app["active_apps"]["v2"]["id"] = external_app_id_v2
    post_portal_app["active_apps"]["v3"]["id"] = external_app_id_v3

    params = {"external_ids": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, params=params, headers=default_admin_header
    )
    resp = r.json()
    assert r.status_code == 200
    assert "id" in resp
    active_app_views = resp["active_app_views"]
    assert active_app_views["v1"]["status"] == AppStatus.APPROVED
    assert active_app_views["v1"]["details"]["name"] == post_portal_app["details"]["name"]
    assert (
        active_app_views["v1"]["details"]["description"][0]["text"] == post_portal_app["details"]["description"]["DE"]
    )
    assert active_app_views["v1"]["app"]["ead"] == post_portal_app["active_apps"]["v1"]["ead"]

    assert active_app_views["v1"]["app"]["id"] == external_app_id_v1
    assert active_app_views["v2"]["app"]["id"] == external_app_id_v2
    assert active_app_views["v3"]["app"]["id"] == external_app_id_v3


def test_post_portal_app_external_ids_missing(default_admin_header, admin_post_portal_app):
    params = {"external_ids": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, params=params, headers=default_admin_header
    )
    resp = r.json()
    assert r.status_code == 404
    assert resp["detail"] == "Missing external ID for app."


def test_post_to_existing_portal_app_with_external_app_id(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    post_portal_app2 = copy.deepcopy(admin_post_portal_app)
    post_portal_app2["id"] = portal_app_id

    external_app_id_v1 = str(uuid.uuid4())
    external_app_id_v2 = str(uuid.uuid4())
    external_app_id_v3 = str(uuid.uuid4())
    post_portal_app2["active_apps"]["v1"]["id"] = external_app_id_v1
    post_portal_app2["active_apps"]["v2"]["id"] = external_app_id_v2
    post_portal_app2["active_apps"]["v3"]["id"] = external_app_id_v3

    new_namespace_v1 = generate_random_namespace()
    new_namespace_v2 = generate_random_namespace()
    new_namespace_v3 = generate_random_namespace("v3")

    post_portal_app2["active_apps"]["v1"]["ead"]["namespace"] = new_namespace_v1
    post_portal_app2["active_apps"]["v2"]["ead"]["namespace"] = new_namespace_v2
    post_portal_app2["active_apps"]["v3"]["ead"]["namespace"] = new_namespace_v3

    params = {"existing_portal_app": True, "external_ids": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app2, params=params, headers=default_admin_header
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["id"] == portal_app_id
    assert resp["active_app_views"]["v1"]["app"]["ead"]["namespace"] == new_namespace_v1
    assert resp["active_app_views"]["v2"]["app"]["ead"]["namespace"] == new_namespace_v2
    assert resp["active_app_views"]["v3"]["app"]["ead"]["namespace"] == new_namespace_v3

    assert resp["active_app_views"]["v1"]["app"]["id"] == external_app_id_v1
    assert resp["active_app_views"]["v2"]["app"]["id"] == external_app_id_v2
    assert resp["active_app_views"]["v3"]["app"]["id"] == external_app_id_v3

    assert resp["active_app_views"]["v1"]["api_version"] == ApiVersion.V1
    assert resp["active_app_views"]["v2"]["api_version"] == ApiVersion.V2
    assert resp["active_app_views"]["v3"]["api_version"] == ApiVersion.V3


def test_post_to_existing_portal_app_without_id(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    params = {"existing_portal_app": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, params=params, headers=default_admin_header
    )
    assert r.status_code == 404
    resp = r.json()
    assert resp["detail"] == "Valid portal app ID is required, when 'existing_portal_app' is True."


def test_post_portal_app_add_media_invalid(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    portal_app_id = resp["id"]
    app_view_id = resp["active_app_views"]["v1"]["id"]

    purpose = "banner"
    index = 1
    data = {"description": "test-description"}
    files = {}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=default_admin_header,
    )
    assert r.status_code == 422


def test_post_portal_app_add_media_valid(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    portal_app_id = resp["id"]
    app_view_id = resp["active_app_views"]["v1"]["id"]

    media_metadata = {
        "caption": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        "alternative_text": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
    }
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image1 = create_monochrome_image(size=(24, 24), color="red", image_format="jpeg")
    files1 = {"media_file": ("filename", open(tmp_image1.name, "rb"), "image/jpeg")}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/peek/1",
        data=data,
        files=files1,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    tmp_image2 = create_monochrome_image(size=(24, 24), color="red", image_format="jpeg")
    files2 = {"media_file": ("filename", open(tmp_image2.name, "rb"), "image/jpeg")}
    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/workflow/1",
        data=data,
        files=files2,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    tmp_image3 = create_monochrome_image(size=(24, 24), color="red", image_format="jpeg")
    files3 = {"media_file": ("filename", open(tmp_image3.name, "rb"), "image/jpeg")}
    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/banner/1",
        data=data,
        files=files3,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    resp = r.json()
    assert len(resp["media"]) == 4
    assert AppMediaPurpose.BANNER in resp["media"]
    assert AppMediaPurpose.WORKFLOW in resp["media"]
    assert AppMediaPurpose.BANNER in resp["media"]
    assert AppMediaPurpose.MANUAL in resp["media"]
    assert resp["media"][AppMediaPurpose.MANUAL] == []
    assert_portal_app_view_media(resp, AppMediaPurpose.PEEK, 0, "image/jpeg")
    assert_portal_app_view_media(resp, AppMediaPurpose.WORKFLOW, 0, "image/jpeg")
    assert_portal_app_view_media(resp, AppMediaPurpose.BANNER, 0, "image/jpeg")


def test_post_portal_app_add_media_resized(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    portal_app_id = resp["id"]
    app_view_id = resp["active_app_views"]["v1"]["id"]

    purpose = "banner"
    index = 0
    media_metadata = {
        "caption": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        "alternative_text": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
    }
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image = create_monochrome_image(size=(2048, 2048), color="green", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=default_admin_header,
    )
    resp = r.json()
    assert r.status_code == 200
    assert AppMediaPurpose.BANNER in resp["media"]
    assert len(resp["media"]) == 4
    assert_portal_app_view_media(resp, AppMediaPurpose.BANNER, 0, "image/png")


def test_delete_portal_app(default_admin_header, admin_post_portal_app):
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=admin_post_portal_app, headers=default_admin_header)
    resp = r.json()
    assert r.status_code == 200
    portal_app_id = resp["id"]
    app_view_id = resp["active_app_views"]["v1"]["id"]

    purpose = "banner"
    index = 1
    media_metadata = {
        "caption": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        "alternative_text": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
    }
    data = {"media_metadata": json.dumps(media_metadata)}
    tmp_image = create_monochrome_image(size=(24, 24), color="red", image_format="png")
    files = {"media_file": ("filename", open(tmp_image.name, "rb"), "image/png")}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views/{app_view_id}/media/{purpose}/{index}",
        data=data,
        files=files,
        headers=default_admin_header,
    )
    resp = r.json()
    assert r.status_code == 200

    params = {"status": ListingStatus.LISTED, "comment": "Reviewed by admin"}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/status",
        params=params,
        headers=default_admin_header,
    )
    resp = r.json()
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/public/portal-apps/{portal_app_id}",
        headers=default_admin_header,
    )
    assert r.status_code == 200
    assert r.json()["id"] == portal_app_id

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", headers=default_admin_header, json={})
    assert r.status_code == 200
    item_count = r.json()["item_count"]

    r = requests.delete(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}",
        headers=default_admin_header,
    )
    assert r.status_code == 200
    assert bool(r.content) is True

    r = requests.get(
        f"{mps_url}/v1/public/portal-apps/{portal_app_id}",
        headers=default_admin_header,
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid portal app for ID {portal_app_id}."

    r = requests.put(f"{mps_url}/v1/public/portal-apps/query", headers=default_admin_header, json={})
    assert r.status_code == 200
    assert r.json()["item_count"] == item_count - 1
