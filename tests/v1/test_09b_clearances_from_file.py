import os

import requests

from tests.v1.utils_clearance_import import import_clearances_from_csv

from ..singletons import mps_url


def test_write_csv_file_to_db(default_admin_header, truncate_db_clearances):
    csv_file_path = os.path.join(os.path.split(os.path.dirname(__file__))[0], "resources", "clearances_normalized.csv")

    import_clearances_from_csv(default_admin_header, csv_file_path)

    query = {}

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 73

    query = {
        "organizations": ["Roche"],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 6

    for c_item in r.json()["items"]:
        assert c_item["organization_id"] == "Roche"

    query = {
        "stains": ["H_AND_E"],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 28

    query = {"tissues": ["BREAST"], "stains": ["ER"], "clearances": ["CE_IVD"], "image_types": ["CAMERA"]}

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1

    query = {}

    r = requests.put(f"{mps_url}/v1/public/clearances/query?skip=10&limit=40", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 73
    assert len(r.json()["items"]) == 40

    query = {}

    r = requests.put(f"{mps_url}/v1/public/clearances/query?skip=70&limit=10", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 73
    assert len(r.json()["items"]) == 3
