import copy

import requests

from marketplace_service.models.marketplace.app import ApiVersion
from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_post_portal_and_add_new_app_view(default_admin_header, admin_post_portal_app, admin_post_app_view):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v2"]
    del post_portal_app["active_apps"]["v3"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    portal_app_id = r.json()["id"]

    r = requests.get(
        f"{mps_url}/v1/public/portal-apps/{portal_app_id}",
        headers=default_admin_header,
    )
    assert r.status_code == 200
    assert r.json()["active_app_views"]["v1"] is not None
    assert r.json()["active_app_views"]["v2"] is None
    assert r.json()["active_app_views"]["v3"] is None

    params = {"api_version": "v2"}
    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views",
        json=admin_post_app_view,
        params=params,
        headers=default_admin_header,
    )
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/public/portal-apps/{portal_app_id}",
        headers=default_admin_header,
    )
    assert r.status_code == 200
    assert r.json()["active_app_views"]["v1"] is not None
    assert r.json()["active_app_views"]["v2"] is not None
    assert r.json()["active_app_views"]["v3"] is None


def test_post_portal_and_add_app_view_with_same_app(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v3"]
    del post_portal_app["active_apps"]["v2"]

    portal_app_id, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    app_view = {
        "details": {
            "name": "PD-L1 Quantifier",
            "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        },
        "app": {"id": app_id},
    }
    params = {"api_version": "v2", "existing_app": True}
    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views",
        json=app_view,
        params=params,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    active_app = r.json()["app"]
    assert active_app["id"] == app_id
    assert active_app["registry_image_url"] == post_portal_app["active_apps"]["v1"]["registry_image_url"]

    query = {"apps": [app_id]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp = r.json()
    assert len(resp) == 2


def test_post_portal_and_overwrite_active_app_view(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v3"]
    del post_portal_app["active_apps"]["v2"]

    portal_app_id, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]

    app_view = {
        "details": {
            "name": "Name",
            "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        },
        "app": {
            "ead": {
                "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
                "name": "New App 01",
                "name_short": "NA01",
                "namespace": "org.empaia.vendor_name.new_app_01.v1",
                "description": "Human readable description",
                "inputs": {
                    "my_wsi": {"type": "wsi"},
                    "my_rectangle": {"type": "rectangle", "reference": "inputs.my_wsi"},
                },
                "outputs": {"tumor_cell_count": {"type": "integer"}},
            },
            "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_new",
            "app_ui_url": "http://some-app-ui.org",
            "app_ui_configuration": {},
        },
    }
    params = {"api_version": "v1"}
    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/app-views",
        json=app_view,
        params=params,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    active_app = r.json()["app"]
    assert active_app["id"] != app_id
    app_id_2 = active_app["id"]
    assert active_app["registry_image_url"] == app_view["app"]["registry_image_url"]

    query = {"apps": [app_id, app_id_2]}
    r = requests.put(f"{mps_url}/v1/customer/app-views/query", json=query, headers=default_customer_header)
    assert r.status_code == 200
    resp = r.json()
    assert len(resp) == 2
    assert resp[0]["api_version"] == ApiVersion.V1
    assert resp[1]["api_version"] == ApiVersion.V1
    assert resp[0]["app"]["id"] == app_id_2
    assert resp[1]["app"]["id"] == app_id
    assert resp[0]["app"]["portal_app_id"] == resp[1]["app"]["portal_app_id"]
    assert resp[0]["app"]["portal_app_id"] == resp[1]["app"]["portal_app_id"]
    assert resp[0]["app"]["ead"] == app_view["app"]["ead"]
    assert resp[0]["app"]["registry_image_url"] == app_view["app"]["registry_image_url"]
    assert resp[0]["app"]["app_ui_url"] == app_view["app"]["app_ui_url"]
