import datetime
import uuid

import requests

from marketplace_service.custom_models.v1.clearances import (
    Clearance,
    ClearanceItem,
    EditorType,
    InfoProvider,
    InfoSourceType,
)
from tests.v1.creation_helper import create_clearance_with_active_item

from ..singletons import mps_url


def test_post_invalid_clearance(default_admin_header):
    clearance = {"organization_id": "test-orga-id"}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 422

    clearance = {"organization_id": None}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 422

    clearance = {"is_hidden": False}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 422

    clearance = {}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 422


def test_post_simple_clearance(default_admin_header):
    clearance = {"organization_id": "test-orga-id", "is_hidden": False}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    assert "id" in resp
    assert resp["is_hidden"] == clearance["is_hidden"]
    assert resp["organization_id"] == clearance["organization_id"]
    assert resp["active_clearance_item"] is None
    assert resp["history"] is None
    assert resp["updater_type"] == resp["creator_type"] == EditorType.ADMIN
    assert resp["updater_id"] == resp["creator_id"] == "admin"
    assert resp["updated_at"] == resp["created_at"]


def test_post_clearance_with_active_item(default_admin_header, post_clearance_item_admin):
    orga_id = "test-orga-id"
    clearance = {"is_hidden": False, "organization_id": orga_id, "clearance_item": post_clearance_item_admin}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    assert "id" in resp
    assert resp["is_hidden"] == clearance["is_hidden"]
    assert resp["organization_id"] == orga_id
    assert resp["active_clearance_item"] is not None
    assert resp["history"] is not None
    assert len(resp["history"]) == 1
    item = ClearanceItem.model_validate(resp["history"][0])
    assert item.product_name == post_clearance_item_admin["product_name"]
    expected_ref_url = post_clearance_item_admin["info_source_content"][0]["reference_url"]
    assert str(expected_ref_url) in str(item.info_source_content[0].reference_url)
    assert len(item.info_source_content[0].label) == len(
        post_clearance_item_admin["info_source_content"][0]["label"].keys()
    )
    assert item.info_source_type == post_clearance_item_admin["info_source_type"]
    assert item.info_provider == InfoProvider.EMPAIA
    assert item.info_updated_at == int(post_clearance_item_admin["info_updated_at"])
    assert resp["updater_type"] == resp["creator_type"] == EditorType.ADMIN
    assert resp["updater_id"] == resp["creator_id"] == default_admin_header["user-id"]
    assert resp["updated_at"] == resp["created_at"]


def test_post_clearance_item_invalid_payload(default_admin_header, post_clearance_item_admin):
    fake_id = str(uuid.uuid4())
    now = datetime.datetime.now().timestamp()

    r = requests.post(f"{mps_url}/v1/admin/clearances/{fake_id}/item", json={}, headers=default_admin_header)
    assert r.status_code == 422

    temp_clearance_item = {"clearance_id": "invalid"}
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{fake_id}/item", json=temp_clearance_item, headers=default_admin_header
    )
    assert r.status_code == 422

    temp_clearance_item = {
        "product_name": "name",
        "product_release_at": now,
    }
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{fake_id}/item", json=temp_clearance_item, headers=default_admin_header
    )
    assert r.status_code == 422

    post_clearance_item_admin["description"] = None
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{fake_id}/item", json=post_clearance_item_admin, headers=default_admin_header
    )
    assert r.status_code == 422

    post_clearance_item_admin["description"] = {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"}
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{fake_id}/item", json=post_clearance_item_admin, headers=default_admin_header
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"Clearance for ID {fake_id} does not exist in database."


def test_post_clearance_item_valid_payload(default_admin_header, post_clearance_item_admin):
    clearance = {"organization_id": "test-orga-id", "is_hidden": False}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    assert Clearance.model_validate(r.json())
    posted_response = r.json()

    r = requests.get(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item/{posted_response['active_clearance_item']['id']}",
        headers=default_admin_header,
    )
    assert r.status_code == 200
    retrieved_response = r.json()
    # description dict order can vary
    assert retrieved_response["description"][0] in posted_response["active_clearance_item"]["description"]
    assert retrieved_response["description"][1] in posted_response["active_clearance_item"]["description"]
    del retrieved_response["description"]
    del posted_response["active_clearance_item"]["description"]
    assert retrieved_response == posted_response["active_clearance_item"]


def test_post_clearance_item_with_all_tags(default_admin_header, post_clearance_item_admin):
    clearance = {"organization_id": "test-orga-id", "is_hidden": False}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_admin_header)
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    stain = {"tag_group": "STAIN", "tag_name": "E_CADHERIN"}
    post_clearance_item_admin["tags"].append(stain)
    indication = {"tag_group": "INDICATION", "tag_name": "COLON_ADENOCARCINOMA"}
    post_clearance_item_admin["tags"].append(indication)
    analysis = {"tag_group": "ANALYSIS", "tag_name": "PROGNOSTIC_PREDICTION"}
    post_clearance_item_admin["tags"].append(analysis)
    clearances = {"tag_group": "CLEARANCE", "tag_name": "CE_IVD"}
    post_clearance_item_admin["tags"].append(clearances)
    image_type = {"tag_group": "IMAGE_TYPE", "tag_name": "WSI"}
    post_clearance_item_admin["tags"].append(image_type)
    procedure = {"tag_group": "PROCEDURE", "tag_name": "BIOPSY"}
    post_clearance_item_admin["tags"].append(procedure)
    scanner = {"tag_group": "SCANNER", "tag_name": "LEICA_APERIO_AT2"}
    post_clearance_item_admin["tags"].append(scanner)
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    assert Clearance.model_validate(r.json())
    posted_response = r.json()

    for tag_group in ["tissues", "stains", "indications", "analysis", "clearances", "image_types", "procedures"]:
        assert tag_group in posted_response["active_clearance_item"]["tags"]
        assert len(posted_response["active_clearance_item"]["tags"][tag_group]) == 1


def test_get_clearance_item_simple(default_admin_header, post_clearance_item_admin):
    clearance_id, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    clearance = Clearance.model_validate(r.json())
    assert clearance.id == clearance.history[0].clearance_id
    assert clearance.active_clearance_item == clearance.history[0]

    # post a new active item
    post_clearance_item_admin["product_name"] = "New product name 2"
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    clearance_with_new_active_item = r.json()

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    clearance = r.json()
    # description entries might differ in order
    del clearance["active_clearance_item"]["description"]
    del clearance_with_new_active_item["active_clearance_item"]["description"]
    assert clearance["active_clearance_item"] == clearance_with_new_active_item["active_clearance_item"]
    assert len(clearance["history"]) == 2


def test_get_all_clearances(default_admin_header, post_clearance_item_admin):
    r = requests.get(f"{mps_url}/v1/admin/clearances", headers=default_admin_header)
    assert r.status_code == 200
    clearances_before_insert = r.json()

    # create new clearance
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    r = requests.get(f"{mps_url}/v1/admin/clearances", headers=default_admin_header)
    assert r.status_code == 200
    clearances_after_insert = r.json()
    assert len(clearances_before_insert) + 1 == len(clearances_after_insert)


def test_hide_clearance(default_admin_header, post_clearance_item_admin):
    clearance_id, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    r = requests.put(f"{mps_url}/v1/admin/clearances/{clearance_id}/hide", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["is_hidden"] is True

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["is_hidden"] is True


def test_unhide_clearance(default_admin_header, post_clearance_item_admin):
    clearance_id, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    r = requests.put(f"{mps_url}/v1/admin/clearances/{clearance_id}/hide", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["is_hidden"] is True

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["is_hidden"] is True

    r = requests.put(f"{mps_url}/v1/admin/clearances/{clearance_id}/unhide", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["is_hidden"] is False

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["is_hidden"] is False


def test_activate_invalid_clearance_item(default_admin_header, post_clearance_item_admin):
    clearance_id, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    # post another new clearance item
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_admin_header,
    )
    assert r.status_code == 200

    # post invalid item activation item
    post_item_activation = {}
    r = requests.put(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/activate",
        json=post_item_activation,
        headers=default_admin_header,
    )
    assert r.status_code == 422

    # post non existant clearance item id
    fake_id = str(uuid.uuid4())
    post_item_activation = {"active_item_id": fake_id}
    r = requests.put(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/activate",
        json=post_item_activation,
        headers=default_admin_header,
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"No valid clearance item for ID {fake_id}."


def test_activate_old_clearance_item(default_admin_header, post_clearance_item_admin):
    clearance_id, old_clearance_item = create_clearance_with_active_item(
        default_admin_header, post_clearance_item_admin
    )

    # post another new clearance item
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_admin_header,
    )
    assert r.status_code == 200
    clearance_with_new_active_item = r.json()

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active_clearance_item"]["id"] == clearance_with_new_active_item["active_clearance_item"]["id"]

    # set the active clearance item to the old item
    post_item_activation = {"active_item_id": old_clearance_item["id"]}
    r = requests.put(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/activate",
        json=post_item_activation,
        headers=default_admin_header,
    )
    assert r.status_code == 200

    r = requests.get(f"{mps_url}/v1/admin/clearances/{clearance_id}", headers=default_admin_header)
    assert r.status_code == 200
    assert r.json()["active_clearance_item"]["id"] == old_clearance_item["id"]


def test_clearance_item_constraints(default_product_provider_header, post_clearance_item_admin):
    clearance = {"organization_id": "test-orga", "is_hidden": False}
    r = requests.post(f"{mps_url}/v1/admin/clearances", json=clearance, headers=default_product_provider_header)
    assert r.status_code == 200
    resp = r.json()
    clearance_id = resp["id"]

    post_clearance_item_admin["info_provider"] = InfoProvider.ORGANIZATION
    post_clearance_item_admin["info_source_type"] = InfoSourceType.UNKNOWN
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert "Info source content not allowed for info source type 'EMAIL_CONTACT' or 'UNKNOWN'" in error_msg

    post_clearance_item_admin["info_provider"] = InfoProvider.EMPAIA
    post_clearance_item_admin["info_source_type"] = InfoSourceType.UNKNOWN
    del post_clearance_item_admin["info_source_content"]
    r = requests.post(
        f"{mps_url}/v1/admin/clearances/{clearance_id}/item",
        json=post_clearance_item_admin,
        headers=default_product_provider_header,
    )
    assert r.status_code == 422
    error_msg = r.json()["detail"][0]["msg"]
    assert "Info source type for provider 'EMPAIA' must be either 'PUBLIC_WEBSITE' or 'EMAIL_CONTACT'" in error_msg
