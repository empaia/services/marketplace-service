import requests

from marketplace_service.models.marketplace.app import AppStatus, ListingStatus

from ..singletons import mps_url


def test_app_view_set_non_functional(app_view_with_minimum_data_set):
    headers, portal_app_id, app_view_id = app_view_with_minimum_data_set

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/non-functional",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app"] is None
    assert resp["non_functional"] is True

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/functional",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app"] is None
    assert resp["non_functional"] is False


def test_app_view_request_approval_functional_wo_app(app_view_with_minimum_data_set):
    headers, portal_app_id, app_view_id = app_view_with_minimum_data_set

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/request-approval",
        headers=headers,
    )
    assert r.status_code == 400
    resp = r.json()
    assert resp["detail"] == "Incomplete portal app data (missing app data)."


def test_app_view_request_approval_non_functional(app_view_with_minimum_data_set):
    headers, portal_app_id, app_view_id = app_view_with_minimum_data_set

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/non-functional",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app"] is None
    assert resp["non_functional"] is True

    r = requests.put(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}/request-approval",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app"] is None
    assert resp["non_functional"] is True
    assert resp["status"] == AppStatus.PENDING


def test_app_view_approve_non_functional(app_view_with_minimum_data_set):
    headers, portal_app_id, app_view_id = app_view_with_minimum_data_set

    r = requests.get(
        f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/app-views/{app_view_id}",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["status"] == AppStatus.PENDING

    r = requests.get(
        f"{mps_url}/v1/reviewer/pending-reviews",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert len(resp) > 0
    exists = False
    for review in resp:
        assert review["status"] == AppStatus.PENDING
        if review["id"] == app_view_id:
            assert review["portal_app_id"] == portal_app_id
            assert review["status"] == AppStatus.PENDING
            exists = True
    assert exists

    r = requests.put(
        f"{mps_url}/v1/reviewer/portal-apps/{portal_app_id}/app-views/{app_view_id}/approve",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["status"] == AppStatus.APPROVED

    r = requests.put(
        f"{mps_url}/v1/public/portal-apps/query",
        json={},
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["item_count"] > 0
    assert resp["item_count"] == len(resp["items"])

    exists = False
    for portal_app in resp["items"]:
        if portal_app["id"] == portal_app_id:
            assert portal_app["status"] == ListingStatus.LISTED
            assert portal_app["active_app_views"]["v1"]["non_functional"] is True
            exists = True
    assert exists
