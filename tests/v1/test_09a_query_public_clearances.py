import requests

from tests.v1.creation_helper import create_clearance_with_active_item

from ..singletons import mps_url


def test_query_clearance_no_filter_simple(default_admin_header, post_clearance_item_admin):
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json={})
    assert r.status_code == 200
    assert r.json()["item_count"] == len(r.json()["items"])
    assert r.json()["item_count"] > 0


def test_query_clearance_all_filter_simple(default_admin_header, post_clearance_item_admin):
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    query = {
        "organizations": ["b10648a7-340d-43fc-a2d9-4d91cc86f33f"],
        "tissues": ["BREAST"],
        "stains": ["H_AND_E", "PR"],
        "indications": ["MELANOMA"],
        "analysis": ["DETECTION"],
        "image_types": ["WSI"],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == len(r.json()["items"]) == 0


def test_query_clearance_filter_by_orga(default_admin_header, post_clearance_item_admin):
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    organization_id = "orga123"
    _, _ = create_clearance_with_active_item(
        default_admin_header, post_clearance_item_admin, organization_id=organization_id
    )

    query = {
        "organizations": [organization_id],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    resp = r.json()
    assert resp["item_count"] == len(resp["items"])
    assert resp["item_count"] > 0
    for item in resp["items"]:
        assert item["organization_id"] == "orga123"


def test_query_clearance_filter_by_individual_tags_simple(default_admin_header, post_clearance_item_admin):
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    organization_id = "orga-with-alt-tags"

    post_clearance_item_admin["tags"] = [
        {"tag_group": "TISSUE", "tag_name": "ESOPHAGUS"},
        {"tag_group": "STAIN", "tag_name": "HER2"},
        {"tag_group": "INDICATION", "tag_name": "SMALL_CELL_LUNG_CARCINOMA"},
        {"tag_group": "ANALYSIS", "tag_name": "CLASSIFICATION"},
        {"tag_group": "CLEARANCE", "tag_name": "CE_IVD"},
        {"tag_group": "IMAGE_TYPE", "tag_name": "WSI"},
        {"tag_group": "PROCEDURE", "tag_name": "SMEAR"},
        {"tag_group": "SCANNER", "tag_name": "3DHISTECH_PANNORAMIC_480_DX"},
    ]
    _, _ = create_clearance_with_active_item(
        default_admin_header, post_clearance_item_admin, organization_id=organization_id
    )

    for tag_tuple in [
        ("tissues", ["ESOPHAGUS"]),
        ("stains", ["HER2"]),
        ("indications", ["SMALL_CELL_LUNG_CARCINOMA"]),
        ("analysis", ["CLASSIFICATION"]),
        ("clearances", ["CE_IVD"]),
        ("image_types", ["WSI"]),
        ("procedures", ["SMEAR"]),
        ("scanners", ["3DHISTECH_PANNORAMIC_480_DX"]),
    ]:
        query = {tag_tuple[0]: tag_tuple[1]}

        r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
        assert r.status_code == 200
        resp = r.json()
        assert resp["item_count"] == len(resp["items"])
        assert resp["items"][0]["organization_id"] == organization_id


def test_query_clearance_skip_limit_simple(default_admin_header, post_clearance_item_admin):
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)
    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin)

    r = requests.put(f"{mps_url}/v1/public/clearances/query?skip=0&limit=1", json={})
    assert r.status_code == 200
    assert len(r.json()["items"]) == 1
    assert r.json()["item_count"] > 1


def test_query_clearance_filter_by_individual_tags(
    default_admin_header, post_clearance_item_admin, truncate_db_clearances
):
    organization_id_1 = "orga-1"

    post_clearance_item_admin["tags"] = [
        {"tag_group": "TISSUE", "tag_name": "SOFT_TISSUE"},
        {"tag_group": "TISSUE", "tag_name": "BREAST"},
        {"tag_group": "STAIN", "tag_name": "KAPPA"},
        {"tag_group": "INDICATION", "tag_name": "COLON_ADENOCARCINOMA"},
        {"tag_group": "ANALYSIS", "tag_name": "SEGMENTATION"},
        {"tag_group": "CLEARANCE", "tag_name": "CE_IVD"},
        {"tag_group": "IMAGE_TYPE", "tag_name": "ROI"},
    ]

    _, _ = create_clearance_with_active_item(
        default_admin_header, post_clearance_item_admin, organization_id=organization_id_1
    )

    organization_id_2 = "orga-2"

    post_clearance_item_admin["tags"] = [
        {"tag_group": "TISSUE", "tag_name": "GALLBLADDER"},
        {"tag_group": "TISSUE", "tag_name": "SOFT_TISSUE"},
        {"tag_group": "TISSUE", "tag_name": "BONE_MARROW"},
        {"tag_group": "STAIN", "tag_name": "ROS1"},
        {"tag_group": "STAIN", "tag_name": "KAPPA"},
        {"tag_group": "INDICATION", "tag_name": "SMALL_CELL_LUNG_CARCINOMA"},
        {"tag_group": "ANALYSIS", "tag_name": "DETECTION"},
        {"tag_group": "ANALYSIS", "tag_name": "GRADING"},
        {"tag_group": "CLEARANCE", "tag_name": "FDA_IVD"},
        {"tag_group": "CLEARANCE", "tag_name": "CE_IVD"},
        {"tag_group": "IMAGE_TYPE", "tag_name": "CAMERA"},
    ]
    _, _ = create_clearance_with_active_item(
        default_admin_header, post_clearance_item_admin, organization_id=organization_id_2
    )

    query = {
        "organizations": [organization_id_1, organization_id_2],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {
        "tissues": ["SOFT_TISSUE"],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {
        "tissues": ["BREAST", "GALLBLADDER"],
    }

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {"tissues": ["SOFT_TISSUE"], "stains": ["KAPPA"]}

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {"tissues": ["BONE_MARROW"], "stains": ["KAPPA"]}

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1

    query = {"image_types": ["CAMERA", "ROI"]}

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 2

    query = {"organizations": [organization_id_2], "tissues": ["SOFT_TISSUE"]}

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json=query)
    assert r.status_code == 200
    assert r.json()["item_count"] == 1
    assert r.json()["items"][0]["organization_id"] == organization_id_2


def test_query_clearance_not_hidden(default_admin_header, post_clearance_item_admin):
    r = requests.put(f"{mps_url}/v1/public/clearances/query", json={})
    assert r.status_code == 200
    item_count = r.json()["item_count"]

    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin, is_hidden=True)

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json={})
    assert r.status_code == 200
    resp = r.json()
    assert resp["item_count"] == item_count

    _, _ = create_clearance_with_active_item(default_admin_header, post_clearance_item_admin, is_hidden=False)

    r = requests.put(f"{mps_url}/v1/public/clearances/query", json={})
    assert r.status_code == 200
    resp = r.json()
    assert resp["item_count"] == item_count + 1
