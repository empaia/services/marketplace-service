import copy
import sys
from io import BytesIO
from pathlib import Path

import requests

from marketplace_service.custom_models.v1.resources import AppUIStatus
from marketplace_service.models.marketplace.app import OS, Browser
from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_get_app_ui_config(default_admin_header, default_customer_header, admin_post_portal_app):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-config", headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()

    expected_ui_config = admin_post_portal_app["active_apps"]["v2"]["app_ui_configuration"]
    assert raw_portal_app["active_app_views"]["v2"]["app"]["app_ui_configuration"] == resp2
    assert "csp" in resp2
    assert "font_src" in resp2["csp"]
    assert "unsafe_inline" in resp2["csp"]["font_src"]
    assert resp2["csp"]["font_src"]["unsafe_inline"] == expected_ui_config["csp"]["font_src"]["unsafe_inline"]
    assert resp2["csp"]["font_src"]["unsafe_eval"] is None
    assert "style_src" in resp2["csp"]
    assert "unsafe_inline" in resp2["csp"]["style_src"]
    assert resp2["csp"]["style_src"]["unsafe_inline"] == expected_ui_config["csp"]["style_src"]["unsafe_inline"]
    assert resp2["csp"]["style_src"]["unsafe_eval"] is None


def test_get_app_ui_config_with_iframe_section(default_admin_header, default_customer_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    post_portal_app["active_apps"]["v2"]["app_ui_configuration"].update({"iframe": {"allow_popups": True}})
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-config", headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()

    assert raw_portal_app["active_app_views"]["v2"]["app"]["app_ui_configuration"] == resp2
    assert "iframe" in resp2
    assert "allow_popups" in resp2["iframe"]
    assert resp2["iframe"]["allow_popups"] is True


def test_get_app_ui_config_with_tested_section_valid(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    tested_config = {"tested": [{"browser": Browser.CHROME, "version": "100.23.0", "os": OS.Win10}]}
    post_portal_app["active_apps"]["v2"]["app_ui_configuration"].update(tested_config)

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-config", headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()

    assert raw_portal_app["active_app_views"]["v2"]["app"]["app_ui_configuration"] == resp2
    assert "tested" in resp2
    assert resp2["tested"] and tested_config["tested"]


def test_get_app_ui_config_with_tested_section_invalid(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    tested_config = {"tested": [{"version": "100.23.0", "os": OS.Win10}]}
    post_portal_app["active_apps"]["v2"]["app_ui_configuration"].update(tested_config)
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 422
    assert r.json()["detail"][0]["msg"] == "Field required"

    tested_config = {"tested": [{"browser": "NO", "version": "100.23.0", "os": OS.Win10}]}
    post_portal_app["active_apps"]["v2"]["app_ui_configuration"].update(tested_config)
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 422
    assert "Input should be 'firefox', 'chrome', 'edge' or 'safari'" in r.json()["detail"][0]["msg"]

    tested_config = {"tested": [{"browser": Browser.EDGE, "version": "100.23.0b", "os": OS.Win10}]}
    post_portal_app["active_apps"]["v2"]["app_ui_configuration"].update(tested_config)
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 422
    assert "String should match pattern" in r.json()["detail"][0]["msg"]

    tested_config = {"tested": [{"browser": Browser.EDGE, "version": "1", "os": OS.Win10}]}
    post_portal_app["active_apps"]["v2"]["app_ui_configuration"].update(tested_config)
    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 422
    assert "String should match pattern" in r.json()["detail"][0]["msg"]


def test_get_app_ui_config_only_style_src(default_admin_header, default_customer_header, admin_post_portal_app):
    portal_app_copy = copy.deepcopy(admin_post_portal_app)
    ui_config = {"csp": {"style_src": {"unsafe_inline": False}}}
    portal_app_copy["active_apps"]["v2"]["app_ui_configuration"] = ui_config

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, portal_app_copy, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-config", headers=default_customer_header)
    assert r.status_code == 200
    resp2 = r.json()

    assert raw_portal_app["active_app_views"]["v2"]["app"]["app_ui_configuration"] == resp2
    assert "csp" in resp2
    assert "style_src" in resp2["csp"]
    assert "unsafe_inline" in resp2["csp"]["style_src"]
    assert resp2["csp"]["style_src"]["unsafe_inline"] == ui_config["csp"]["style_src"]["unsafe_inline"]
    assert resp2["csp"]["style_src"]["unsafe_eval"] is None


def test_get_app_ui_config_not_exists(default_admin_header, default_customer_header, admin_post_portal_app):
    portal_app_copy = copy.deepcopy(admin_post_portal_app)
    del portal_app_copy["active_apps"]["v2"]["app_ui_configuration"]

    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, portal_app_copy, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-config", headers=default_customer_header)
    assert r.status_code == 200
    assert r.json() == {"csp": None, "iframe": None, "tested": None}


def test_customer_get_ui_bundle_without_filename_param(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        appp_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", appp_ui_byte_arr, "application/zip")}
        r = requests.put(f"{mps_url}/v1/admin/apps/{app_id}/app-ui-bundles", files=files, headers=default_admin_header)
        assert r.status_code == 200
        assert r.json()["status"] == AppUIStatus.UNZIP_FINISHED

    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-files/", headers=default_customer_header)
    assert r.status_code == 200
    assert "<h1>heading</h1>" in str(r.content.decode("utf-8"))
    assert "<p>test text.</p>" in str(r.content.decode("utf-8"))


def test_customer_get_ui_bundle_access_forbidden(default_admin_header, default_customer_header, admin_post_portal_app):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        appp_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", appp_ui_byte_arr, "application/zip")}
        r = requests.put(f"{mps_url}/v1/admin/apps/{app_id}/app-ui-bundles", files=files, headers=default_admin_header)
        assert r.status_code == 200
        assert r.json()["status"] == AppUIStatus.UNZIP_FINISHED

    resource = "..%2F..%2Findex.html"
    r = requests.get(f"{mps_url}/v1/customer/apps/{app_id}/app-ui-files/{resource}", headers=default_customer_header)
    assert r.status_code == 404
    assert r.json()["detail"] == "Access to requested resource path forbidden"


def test_customer_get_ui_bundle_invalid_resource_file(
    default_admin_header, default_customer_header, admin_post_portal_app
):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v2"]["app"]["id"]

    resource_file = Path(sys.path[0]).joinpath("tests/resources/index.zip")
    with open(resource_file, "rb") as fh:
        appp_ui_byte_arr = BytesIO(fh.read())
        files = {"app_ui_file": ("filename", appp_ui_byte_arr, "application/zip")}
        r = requests.put(f"{mps_url}/v1/admin/apps/{app_id}/app-ui-bundles", files=files, headers=default_admin_header)
        assert r.status_code == 200
        assert r.json()["status"] == AppUIStatus.UNZIP_FINISHED

    invalid_resource_file = "not-exist.html"
    r = requests.get(
        f"{mps_url}/v1/customer/apps/{app_id}/app-ui-files/{invalid_resource_file}", headers=default_customer_header
    )
    assert r.status_code == 404
    assert r.json()["detail"] == f"Resource {invalid_resource_file} does not exist for app ID {app_id}."
