import requests

from ..singletons import mps_url


def test_clear_db(truncate_db, truncate_db_clearances):
    assert True


def test_get_tags():
    r = requests.get(f"{mps_url}/v1/public/tags")
    resp = r.json()
    assert r.status_code == 200
    assert "tissues" in resp
    assert len(resp["tissues"]) == 44
    assert "stains" in resp
    assert len(resp["stains"]) == 197
    assert "analysis" in resp
    assert len(resp["analysis"]) == 8
    assert "indications" in resp
    assert len(resp["indications"]) == 65
    assert "clearances" in resp
    assert len(resp["clearances"]) == 9
    assert "image_types" in resp
    assert len(resp["image_types"]) == 3
    assert "procedures" in resp
    assert len(resp["procedures"]) == 105
    assert "scanners" in resp
    assert len(resp["scanners"]) == 56
