from uuid import uuid4

import requests

from marketplace_service.models.marketplace.app import AppConfigurationType

from ..singletons import mps_url, settings


def test_portal_app_post_configuration(valid_portal_app_view, valid_v1_post_app, valid_configuration):
    headers, portal_app_id, _ = valid_portal_app_view

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=headers)
    resp = r.json()
    assert r.status_code == 200
    app_id = resp["id"]

    r = requests.post(
        f"{mps_url}/v1/vendor/apps/{app_id}/config/global",
        json=valid_configuration,
        headers=headers,
    )
    assert r.status_code == 200

    r = requests.get(
        f"{mps_url}/v1/vendor/apps/{app_id}/config",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app_id"] == app_id
    assert resp["global"] == valid_configuration["content"]
    assert resp["customer"] == {}


def test_portal_app_get_configuration(valid_portal_app_view, valid_v1_post_app, valid_configuration):
    headers, portal_app_id, _ = valid_portal_app_view

    r = requests.post(f"{mps_url}/v1/vendor/portal-apps/{portal_app_id}/apps", json=valid_v1_post_app, headers=headers)
    resp = r.json()
    assert r.status_code == 200
    app_id = resp["id"]
    ead = resp["ead"]

    customer_dict = {
        str(uuid4()): {"content": {"test_key_cust_1": "test_value1"}},
        str(uuid4()): {"content": {"test_key_cust_2": "test_value2"}},
        str(uuid4()): {"content": {"test_key_cust_3": "test_value3"}},
    }

    post_vault_secret(ead["namespace"], AppConfigurationType.GLOBAL, valid_configuration)
    for key in customer_dict:
        post_vault_secret(ead["namespace"], AppConfigurationType.CUSTOMER, customer_dict[key], customer_id=key)

    r = requests.get(
        f"{mps_url}/v1/vendor/apps/{app_id}/config",
        headers=headers,
    )
    assert r.status_code == 200
    resp = r.json()
    assert resp["app_id"] == app_id
    assert resp["global"] == valid_configuration["content"]
    assert resp["customer"] == {}

    for key in customer_dict:
        r = requests.get(
            f"{mps_url}/v1/vendor/apps/{app_id}/config/{key}",
            headers=headers,
        )
        assert r.status_code == 200
        resp = r.json()
        assert resp["app_id"] == app_id
        assert resp["global"] == valid_configuration["content"]
        assert resp["customer"] == customer_dict[key]["content"]


def post_vault_secret(namespace: str, config_type: AppConfigurationType, configuration, customer_id: str = None):
    payload = {"data": configuration["content"]}
    headers = {"X-Vault-Token": settings.vault_root_token, "X-Vault-Request": "true"}
    url = f"{settings.vault_url}/v1/secret/data/{namespace}/{config_type.value}"
    if config_type == AppConfigurationType.CUSTOMER:
        url = f"{settings.vault_url}/v1/secret/data/{namespace}/{config_type.value}/{customer_id}"

    _r1 = requests.post(url, json=payload, headers=headers)
