import requests

from tests.v1.creation_helper import post_licensed_portal_app_as_admin

from ..singletons import mps_url


def test_get_app(default_admin_header, default_customer_header, admin_post_portal_app):
    _, raw_portal_app = post_licensed_portal_app_as_admin(
        default_admin_header, admin_post_portal_app, default_customer_header["organization-id"]
    )
    app_id = raw_portal_app["active_app_views"]["v1"]["app"]["id"]
    app = raw_portal_app["active_app_views"]["v1"]["app"]

    organization_id = default_customer_header["organization-id"]
    r = requests.get(f"{mps_url}/v1/compute/organizations/{organization_id}/apps/{app_id}")
    assert r.status_code == 200
    resp = r.json()
    assert app == resp
