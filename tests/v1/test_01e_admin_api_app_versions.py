import copy

import requests

from marketplace_service.models.marketplace.app import ListingStatus
from tests.utils import generate_random_namespace

from ..singletons import mps_url


def test_post_portal_updated_app_for_existing_portal_app(default_admin_header, admin_post_portal_app):
    post_portal_app = copy.deepcopy(admin_post_portal_app)
    del post_portal_app["active_apps"]["v1"]
    del post_portal_app["active_apps"]["v2"]

    r = requests.post(f"{mps_url}/v1/admin/portal-apps", json=post_portal_app, headers=default_admin_header)
    assert r.status_code == 200
    resp1 = r.json()
    portal_app_id = resp1["id"]

    post_portal_app2 = copy.deepcopy(admin_post_portal_app)
    del post_portal_app2["active_apps"]["v2"]
    post_portal_app2["id"] = portal_app_id

    new_namespace_v3 = generate_random_namespace("v3")
    post_portal_app2["active_apps"]["v3"]["ead"]["namespace"] = new_namespace_v3

    params = {"existing_portal_app": True}

    r = requests.post(
        f"{mps_url}/v1/admin/portal-apps", json=post_portal_app2, params=params, headers=default_admin_header
    )
    assert r.status_code == 200
    resp2 = r.json()
    assert resp1["id"] == resp2["id"]
    assert resp1["active_app_views"]["v1"] is None
    assert resp2["active_app_views"]["v1"] is not None
    assert resp1["active_app_views"]["v3"]["id"] != resp2["active_app_views"]["v3"]["id"]
    assert resp1["active_app_views"]["v3"]["version"] != resp2["active_app_views"]["v3"]["version"]
    assert resp2["active_app_views"]["v3"]["app"]["ead"]["namespace"] == new_namespace_v3
    assert resp1["active_app_views"]["v3"]["app"]["id"] != resp2["active_app_views"]["v3"]["app"]["id"]

    params = {"status": ListingStatus.LISTED, "comment": "Reviewed by admin"}

    r = requests.put(
        f"{mps_url}/v1/admin/portal-apps/{portal_app_id}/status",
        params=params,
        headers=default_admin_header,
    )
    assert r.status_code == 200

    r = requests.get(f"{mps_url}/v1/public/portal-apps/{portal_app_id}")
    assert r.status_code == 200
    resp3 = r.json()
    assert resp3["active_app_views"]["v1"] is not None
    assert resp3["active_app_views"]["v3"]["version"] == resp2["active_app_views"]["v3"]["version"]
