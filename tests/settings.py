from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    mps_url: str
    vault_url: str
    vault_root_token: str
    mps_db_host: str
    mps_db_port: str
    mps_db_username: str
    mps_db_password: str
    mps_db: str
    test_dir: str

    model_config = SettingsConfigDict(env_file=".env", env_prefix="PYTEST_", extra="ignore")
