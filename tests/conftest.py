from pathlib import Path

import pytest
import requests

from tests.generate_testdata import (
    generate_app_configuartion,
    generate_app_ead_draft3_json,
    generate_nested_app_configuartion,
    generate_organization_json,
    generate_valid_admin_post_portal_app,
    generate_valid_header_with_organization,
    generate_valid_post_clearance_item,
    generate_valid_v1_app_view,
    generate_valid_v1_post_app,
)
from tests.utils import (
    clear_directory,
    get_admin_app_view_with_valid_namespace,
    get_admin_portal_app_with_valid_namespace,
    get_app_with_valid_namespace,
)
from tests.v1.creation_helper import (
    create_app_view_with_minimum_metadata_set,
    create_app_with_all_required_data_set,
    create_app_with_approval_request,
    create_portal_app_with_app_view,
)
from tests.vault.create_vault_roles import create_valid_vault_mutate_client

from .singletons import mps_url

pytest.namespace_version = 0

test_data_directory = Path("./tests/tmp_data/")


@pytest.fixture(scope="module", autouse=False)
def truncate_db():
    _ = requests.put(
        f"{mps_url}/v1/admin/truncate-db",
        headers={"user-id": "admin"},
    )


@pytest.fixture(scope="module", autouse=False)
def truncate_db_clearances():
    _ = requests.put(
        f"{mps_url}/v1/admin/truncate-db-clearances",
        headers={"user-id": "admin"},
    )


@pytest.fixture(scope="module", autouse=False)
def init_test_dir(request):
    test_data_directory.mkdir(parents=True, exist_ok=True)
    clear_directory(test_data_directory.resolve())
    return test_data_directory.resolve()


@pytest.fixture(scope="module", autouse=False)
def teardown_test_dir(request):
    clear_directory(test_data_directory.resolve())
    test_data_directory.rmdir()


@pytest.fixture()
def test_dir(request):
    return test_data_directory.resolve()


@pytest.fixture(scope="session", autouse=True)
def default_vendor_header(request):
    header = generate_valid_header_with_organization(user_id="vendor-user", organization_id="vendor-orga")
    return header


@pytest.fixture(scope="session", autouse=True)
def default_customer_header(request):
    header = generate_valid_header_with_organization(user_id="customer-user", organization_id="customer-orga")
    return header


@pytest.fixture(scope="session", autouse=True)
def default_product_provider_header(request):
    header = generate_valid_header_with_organization(
        user_id="product-provider-user", organization_id="product-provider-orga"
    )
    return header


@pytest.fixture(scope="session", autouse=True)
def default_user_header(request):
    return {"user-id": "testuser"}


@pytest.fixture(scope="session", autouse=True)
def default_admin_header(request):
    return {"user-id": "admin"}


@pytest.fixture(scope="session", autouse=True)
def valid_organization(request):
    org = generate_organization_json("name", 0)
    return org


@pytest.fixture(scope="session", autouse=True)
def valid_ead(request):
    ead = generate_app_ead_draft3_json()
    return ead


@pytest.fixture(scope="session", autouse=True)
def valid_configuration(request):
    config = generate_app_configuartion()
    return config


@pytest.fixture(scope="session", autouse=True)
def valid_nested_configuration(request):
    config = generate_nested_app_configuartion()
    return config


@pytest.fixture(scope="session", autouse=False)
def valid_portal_app_view(request):
    return create_portal_app_with_app_view()


@pytest.fixture(autouse=True)
def valid_v1_post_app(request):
    v1_post_app = generate_valid_v1_post_app()
    post_app = get_app_with_valid_namespace(v1_post_app)
    return post_app


@pytest.fixture(scope="session", autouse=False)
def app_view_with_minimum_data_set(request):
    app_view_minimum_data = create_app_view_with_minimum_metadata_set()
    return app_view_minimum_data


@pytest.fixture(scope="session", autouse=False)
def app_view_with_input_data_set(request):
    app_view_input_data = create_app_with_all_required_data_set()
    return app_view_input_data


@pytest.fixture(scope="function", autouse=False)
def app_view_approval_requested(request):
    app_view_approval = create_app_with_approval_request()
    return app_view_approval


@pytest.fixture(autouse=True)
def admin_post_portal_app(request):
    admin_post_app = generate_valid_admin_post_portal_app()
    post_portal_app = get_admin_portal_app_with_valid_namespace(portal_app=admin_post_app)
    return post_portal_app


@pytest.fixture(autouse=True)
def admin_post_app_view(request):
    admin_app_view = generate_valid_v1_app_view()
    post_app_view = get_admin_app_view_with_valid_namespace(app_view=admin_app_view)
    return post_app_view


@pytest.fixture(autouse=True)
def post_clearance_item_admin(request):
    clearance_item = generate_valid_post_clearance_item(as_admin=True)
    return clearance_item


@pytest.fixture(autouse=True)
def post_clearance_item_product_provider(request):
    clearance_item = generate_valid_post_clearance_item(as_admin=False)
    clearance_item_copy = clearance_item.copy()
    del clearance_item_copy["info_updated_at"]
    return clearance_item_copy


@pytest.fixture(scope="session", autouse=True)
def valid_vault_mutate_client_token(request):
    client_token = create_valid_vault_mutate_client()
    return client_token
