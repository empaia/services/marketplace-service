import copy
import random
import string
from pathlib import Path


def get_app_with_valid_namespace(app: dict) -> dict:
    app_copy = copy.deepcopy(app)
    if app_copy["ead"]["$schema"].endswith("ead-schema.v3.json"):
        valid_namespace = generate_random_namespace(version="v3")
    else:
        valid_namespace = generate_random_namespace()
    app_copy["ead"]["namespace"] = valid_namespace
    return app_copy


def get_admin_app_view_with_valid_namespace(app_view: dict) -> dict:
    app_view_copy = copy.deepcopy(app_view)
    valid_namespace = generate_random_namespace()
    app_view_copy["app"]["ead"]["namespace"] = valid_namespace
    return app_view_copy


def get_admin_portal_app_with_valid_namespace(portal_app: dict) -> dict:
    portal_app_copy = copy.deepcopy(portal_app)
    portal_app_copy["active_apps"]["v1"]["ead"]["namespace"] = generate_random_namespace()
    portal_app_copy["active_apps"]["v2"]["ead"]["namespace"] = generate_random_namespace()
    portal_app_copy["active_apps"]["v3"]["ead"]["namespace"] = generate_random_namespace(version="v3")
    return portal_app_copy


def clear_directory(directory: str):
    path = Path(directory)
    for child in path.iterdir():
        if child.is_file():
            child.unlink()
        else:
            clear_directory(child)
            child.rmdir()


def generate_random_namespace(version: str = None, size: int = 6):
    vendor = "".join(random.choice(string.ascii_lowercase) for _ in range(size))
    app = "".join(random.choice(string.ascii_lowercase) for _ in range(size))
    version_string = f"{int(random.random() * 100)}"
    if version and version == "v3":
        version_string = f"3.{version_string}"
    return f"org.empaia.{vendor}.{app}.v{version_string}"
