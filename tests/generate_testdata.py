import datetime
from uuid import uuid4

from marketplace_service.custom_models.v1.clearances import DatePrecision, InfoProvider, InfoSourceType


def generate_app_ead_draft3_json() -> dict:
    ead = {
        "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
        "name": "Tutorial App 01",
        "name_short": "TA01",
        "namespace": "org.empaia.vendor_name.tutorial_app_01.#",
        "description": "Human readable description",
        "inputs": {"my_wsi": {"type": "wsi"}, "my_rectangle": {"type": "rectangle", "reference": "inputs.my_wsi"}},
        "outputs": {"tumor_cell_count": {"type": "integer"}},
    }
    return ead


def generate_app_ead_v3_json() -> dict:
    ead = {
        "$schema": "https://gitlab.com/empaia/integration/definitions/-/raw/main/ead/ead-schema.v3.json",
        "name": "Tutorial App 01 v3",
        "name_short": "TA01v3",
        "namespace": "org.empaia.vendor_name.tutorial_app_01.#",
        "description": "Human readable description",
        "io": {
            "my_wsi": {"type": "wsi"},
            "my_rectangle": {"type": "rectangle", "reference": "io.my_wsi"},
            "tumor_cell_count": {"type": "integer"},
        },
        "modes": {"standalone": {"inputs": ["my_wsi", "my_rectangle"], "outputs": ["tumor_cell_count"]}},
    }
    return ead


def generate_organization_json(name: str, organization_id: int):
    data = {
        "organization_id": organization_id,
        "keycloak_id": str(uuid4()),
        "name": name,
        "normalized_name": name,
        "street_name": "string",
        "street_number": "string",
        "zip_code": "string",
        "place_name": "string",
        "country_code": "string",
        "department": "string",
        "email": "string",
        "phone_number": "string",
        "fax_number": "string",
        "website": "string",
        "picture": "",
        "organization_role": "HOSPITAL",
        "account_state": "ACTIVE",
        "date_created": "2021-06-17T16:01:59.808Z",
        "date_last_change": "2021-06-17T16:01:59.808Z",
        "contact_person_user_id": 0,
        "clientGroups": [
            {
                "client_group_id": 0,
                "group_organization_id": 0,
                "group_type": "AAA_SERVICE",
                "group_namespace": "string",
                "group_authorization_from": [
                    {"client_group_authorization_id": 0, "authorization_from": 0, "authorization_for": 0}
                ],
                "group_authorization_for": [
                    {"client_group_authorization_id": 0, "authorization_from": 0, "authorization_for": 0}
                ],
                "clients": [
                    {
                        "client_id": "string",
                        "name": "string",
                        "url": "string",
                        "group_id": 0,
                        "keycloak_id": "string",
                        "description": "string",
                        "token_lifetime_in_seconds": 0,
                        "redirect_uris": ["string"],
                    }
                ],
            }
        ],
        "solutions": [{"organization_id": 0, "solution_id": 0}],
        "user_count": 0,
    }
    return data


def generate_app_configuartion():
    config = {
        "content": {
            "config_str": "some str",
            "config_int": 100,
            "config_float": 7.8910,
            "config_bool": True,
        }
    }
    return config


def generate_nested_app_configuartion():
    config = {
        "content": {
            "config_str": "some str",
            "config_int": 5,
            "config_float": 7.8901,
            "config_bool": True,
            "config_none": None,
            "config_dict": {
                "my_wsi": {"type": "wsi"},
                "my_rectangles": {"type": "rectangle", "reference": "inputs.my_wsi"},
            },
            "config_list": ["a", "b", "c"],
        }
    }
    return config


def generate_valid_app_ui_config():
    app_ui_config = {"csp": {"style_src": {"unsafe_inline": True}, "font_src": {"unsafe_inline": True}}}
    return app_ui_config


def generate_valid_header_with_organization(user_id: str = "testuser", organization_id: str = "testorga"):
    header = {"user-id": user_id, "organization-id": organization_id}
    return header


def generate_valid_v1_post_app():
    valid_ead = generate_app_ead_v3_json()
    valid_app = {
        "ead": valid_ead,
        "registry_image_url": "https://path.to.some.url/",
        "app_ui_url": "https://path.to.app.ui.url/",
    }
    return valid_app


def generate_valid_admin_post_portal_app():
    valid_admin_portal_app = {
        "organization_id": "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
        "status": "LISTED",
        "details": {
            "name": "PD-L1 Quantifier",
            "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        },
        "active_apps": {
            "v1": {
                "ead": generate_app_ead_draft3_json(),
                "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
            },
            "v2": {
                "ead": generate_app_ead_draft3_json(),
                "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
                "app_ui_url": "http://app1.emapaia.org",
                "app_ui_configuration": generate_valid_app_ui_config(),
            },
            "v3": {
                "ead": generate_app_ead_v3_json(),
                "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
                "app_ui_url": "http://app1.emapaia.org",
                "app_ui_configuration": generate_valid_app_ui_config(),
            },
        },
        "tags": [{"tag_group": "TISSUE", "tag_name": "SKIN"}],
    }
    return valid_admin_portal_app


def generate_valid_v1_app_view():
    valid_admin_app_view = {
        "details": {
            "name": "PD-L1 Quantifier",
            "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        },
        "app": {
            "ead": generate_app_ead_draft3_json(),
            "registry_image_url": "https://registry.gitlab.com/empaia/integration/ap_xyz",
            "app_ui_url": "http://app1.emapaia.org",
            "app_ui_configuration": generate_valid_app_ui_config(),
        },
    }
    return valid_admin_app_view


def generate_valid_post_clearance_item(as_admin: bool = False):
    now = int(datetime.datetime.now().timestamp())
    valid_post_clearance_item = {
        "product_name": "Product name",
        "description": {"EN": "Description in english", "DE": "Beschreibung auf Deutsch"},
        "tags": [
            {"tag_group": "TISSUE", "tag_name": "SKIN"},
        ],
        "product_release_at": now,
        "product_release_at_precision": DatePrecision.YEAR,
        "info_updated_at": now,
        "info_source_type": InfoSourceType.PUBLIC_WEBSITE,
        "info_source_content": [
            {
                "reference_url": "https://a.valid.website.org",
                "label": {"EN": "Label in english", "DE": "Label auf Deutsch"},
            },
        ],
    }
    if as_admin:
        valid_post_clearance_item["info_provider"] = InfoProvider.EMPAIA
    return valid_post_clearance_item
