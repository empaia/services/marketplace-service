import json

import pytest
import requests

from .settings import Settings

settings = Settings()
mps_url = settings.mps_url.strip("/")


EXCLUDED_PATHS = []


def test_route_v1_public():
    r = requests.get(f"{mps_url}/v1/public/openapi.json")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        if path in EXCLUDED_PATHS:
            continue

        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{mps_url}/v1/public{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url)
            assert r.status_code != 403


@pytest.mark.parametrize(
    "url_segment", ["customer", "compute", "product-provider", "vendor", "reviewer", "admin", "moderator"]
)
def test_routes_v1(url_segment):
    r = requests.get(f"{mps_url}/v1/{url_segment}/openapi.json")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        if path in EXCLUDED_PATHS:
            continue

        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{mps_url}/v1/{url_segment}{path_no_vars}"
        for op in ops.keys():
            r = requests.request(op, url)
            assert r.status_code == 403
