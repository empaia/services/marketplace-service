# Marketplace Service

This repos uses submodules - clone with `git clone --recurse-submodules`

## Routes v1

The second iteration implements all routes for the new marketplace (portal-v2). It is partitioned in several sub-api's taylored to the function range of the API consumers.
The marketplace-service is basically responsible for the following tasks:

* provide app information for EMPAIA app portal (e.g., metadata, images, tags, api version compliance)
* provide technical information for apps (e.g., container image urls, app uis, configurations)
* create and manage new portal/technical apps (e.g., populate metadata, media, container images, app uis and configurations)
* review portal apps (approve or reject new portal apps)
* admin functionality (e.g., create new apps, truncate db, etc.)

## General Auth Concept

* All, except `public`routes require a `user-id` header
* All organization related routes (`customer`, `vendor`, `compute`) require an `organization_id` header

### Roles

* Organization role `APP_CUSTOMER` can access /customer routes
* Organization role `APP_MAINTAINER` can access /vendor routes
* Organization role `COMPUTE_PROVIDER` can access /compute routes
* Organization role `CLEARANCE_MAINTAINER` can access /product-provider routes
* Global role `MODERATOR` can access /reviewer routes
* Global role `ADMIN` can access /admin routes

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone marketplace-service

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd marketplace-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Or start `marketplace_service` with uvicorn and only a development database / vault / minio with docker-compose.

```bash
docker-compose up -d marketplace-service-db vault minio-service-init
mpsctl migrate-db
uvicorn --host=0.0.0.0 --port=8000 --reload marketplace_service.app:app
```

Access the interactive OpenAPI specification in a browser:

* http://localhost:8000/docs

* http://localhost:8000/v1/public/docs
* http://localhost:8000/v1/customer/docs
* http://localhost:8000/v1/product-provider/docs
* http://localhost:8000/v1/compute/docs
* http://localhost:8000/v1/vendor/docs
* http://localhost:8000/v1/reviewer/docs
* http://localhost:8000/v1/admin/docs

### Stop and Remove

```bash
docker-compose down
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle marketplace_service tests tests_auth
pylint marketplace_service tests tests_auth
pytest tests --maxfail=1  # requires service running
```
