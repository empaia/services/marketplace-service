# Changelog

## 0.4.40 - 44

* renovate

## 0.4.38 & 39

* new optional attributes `organization_name` and `app_documentation_url` in app models
* new routes PUT and GET `oranizations/organization_id/logo` (admin and customer)

## 0.4.30 - 37

* renovate

## 0.4.29

* change order of statistics items to descending

## 0.4.28

* added statistics for public marketplace and ai register

## - 0.4.27

* renovate

## 0.4.22 & 0.4.23

* renovate

## 0.4.21

* renovate

## 0.4.20

* updated submodules

## 0.4.19

* renovate

## 0.4.18

* renovate

## 0.4.17

* renovate

## 0.4.16

* renovate

## 0.4.15

* renovate

## 0.4.14

* initial public commit